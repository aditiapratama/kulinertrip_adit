<?php defined('BASEPATH') || exit('No direct script access allowed');

    /*
    	----------------------------------------
    	Author By Sonny
        lastt.404@gmail.com
    	sonnyply.28@gmail.com
    	mbahju.com
    	----------------------------------------
    */

	$lang['msgError']    = array('Failed','Failed to send request');
	$lang['msgWarning']    = array('Warning','Request can\'t be processed');
	$lang['msgDelete']    = array('Delete' , 'Data has been deleted');
	$lang['msgSave']    = array('Saved', 'New data has been saved');
	$lang['msgEdit']    = array('Edited', 'Data has been dited');
