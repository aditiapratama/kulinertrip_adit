<?php defined('BASEPATH') OR exit('No direct script access allowed');

class con_hasilfilter extends MY_Core
{
    function __construct(){
        parent::__construct();
        $this->urllanding = curl_url() . "Ctr_landing_page_kuliner/getLandingKulinerFilter";
        $this->urliklankanan = curl_url() . "Ctr_iklan/getIklanLandingKuliner";
        $this->urllistbawah = curl_url() . "Ctr_data_filter/getJsonListBawah";
        $this->urllokasi = curl_url() . "Ctr_data_filter/getDataLatLong";
        
        $this->idhari = day_php(date("l"));
    }
    

    //function index($pilihan="",$itempilihan=""){
    function index($idtab=""){
        
        if(empty($idtab))
            $idtab="dekat.html";
         
       if(!empty($_POST)) 
        $this->session->set_userdata('filtered_ss',$_POST);
         
        
        $xsessionfilter =$this->session->userdata('filtered_ss');
        
        
        //print_r($xsessionfilter);
        
        $datafilter = $xsessionfilter['json'];
        
        
        $provinsi = $this->session->userdata('propinsipilih');               
        $kabupaten = $this->session->userdata('kabupatenpilih'); 
        $latitude = $this->session->userdata('latitude');               
        $longitude = $this->session->userdata('longitude'); 
        
        $json_param_post = array(
                'latitude' => $latitude,
                'longitude' => $longitude,
                'kabupaten' => $kabupaten,
                'propinsi' => $provinsi,
                'idhari' => "1",
                'jam' => "17:38",
                'language' => "ID",
                'jenispilihan' => "kategori",
                'filterdata' => $datafilter,
                'iduser' => "1",
                'idtab' => $idtab
            );
            //print_r($json_param_post);
        $jsondata = curl_post_data(
            $this->urllanding,
            array('jsonParamAwal' => json_encode($json_param_post))
        );
        //echo $jsondata;

          
//        $provinsi = $this->session->userdata('provinsi');               
//        $kabupaten = $this->session->userdata('kabupaten'); 
////        print_r($kabupaten);
////        echo $kabupaten."===>".$provinsi."==>";
//        $xparamkab = array(
//            'kabupaten' => $kabupaten,
//            'propinsi' => $provinsi            
//            );
//        
//        $jsondatakab = curl_post_data(
//            $this->urllokasi,
//            array('jsonparamlokasi' => json_encode($xparamkab))
//        );
//        
//       // echo $jsondatakab;
//        $jsonDataKabArr = json_decode($jsondatakab, TRUE);
//        $this->session->set_userdata('latitude',$jsonDataKabArr[0]['latitude']);
//        $this->session->set_userdata('longitude',$jsonDataKabArr[0]['longitude']);
//        $this->session->set_userdata('kabupatenpilih',$jsonDataKabArr[0]['kabupaten']);
//        $this->session->set_userdata('propinsipilih',$jsonDataKabArr[0]['propinsi']);
        
/*        
       $xlat = $this->session->userdata('latitude');
       //echo "latitude--->".$xlat;
        if(empty($xlat)){
            $json_param_post = array(
                'latitude' => "-7.74766584",
                'longitude' => "110.42292105",
                'kabupaten' => "Kabupaten Sleman",
                'propinsi' => "Daerah Istimewa Yogyakarta",
                'idhari' => "1",
                'jam' => "17:38",
                'language' => "ID",
                'jenispilihan' => "kategori",
                'pilihan' => "restoran",
                'iduser' => "1"
            );
        } else {
            $json_param_post = array(
                'latitude' => $this->session->userdata('latitude'),
                'longitude' => $this->session->userdata('longitude'),
                'kabupaten' => $this->session->userdata('kabupatenpilih'),
                'propinsi' => $this->session->userdata('propinsipilih'),
                'idhari' => "1",
                'jam' => "17:38",
                'language' => "ID",
                'jenispilihan' => $pilihan,
                'pilihan' => $itempilihan,
                'iduser' => "1"
            );
        }
  */
        
        
        $jsondata = curl_post_data(
            $this->urllanding,
            array('jsonParamAwal' => json_encode($json_param_post))
        );
        
      
        $jsonDataIklanKanan = curl_post_data(
            $this->urliklankanan,
            array('jsonParamAwal' => json_encode($json_param_post))
        );
        
     //   echo $jsondata;

        $jsonDataListBawah = curl_post_data(
            $this->urllistbawah,
            array('jsonParamListBawah' => json_encode(
                array(
                    'language' => "ID"
                )
            ))
        );

        $data = json_decode($jsondata, TRUE);
        $data = $data['0'];

        $isdiluarDaerahCakupan = $data['isdiluarDaerahCakupan'];
        $jsonDataPrimer = $data['jsonDataPrimer'];
        $jsonDataPrimerArr = json_decode($jsonDataPrimer, TRUE);
//        $jsonLandingKuliner = $jsonDataPrimerArr[0]['jsonLandingKuliner'];
//
//        $jsonLandingKulinerArr =  json_decode($jsonLandingKuliner, TRUE);

//        $jsonDataKuliner = $jsonLandingKulinerArr[0]['jsonDataKuliner'];
//        $jsonDataKulinerArr = json_decode($jsonDataKuliner, TRUE);
        //print_r($jsonDataPrimerArr);
        $judul = $jsonDataPrimerArr[0]['judul'];
        $idtab = $jsonDataPrimerArr[0]['idtab'];
        //$judul2 = $jsonDataKulinerArr[0]['judul1'];
        $datakuliner = $jsonDataPrimerArr[0]['data'];
        $datakulinerArr = json_decode($datakuliner, TRUE);

        // Data Iklan Landing Kuliner
        $jsonDataIklanKananArr = json_decode($jsonDataIklanKanan,TRUE);
        $IklanKanan = $jsonDataIklanKananArr[0]['IklanKanan'];
        $IklanKananArr = json_decode($IklanKanan, TRUE);

        $JsonIklanBanner = $IklanKananArr[0]['jsonIklanBar'];
        $jsonIklan1 = $IklanKananArr[0]['jsonIklanData1'];
        $jsonIklanPropinsi = $IklanKananArr[0]['jsonIklanPropinsi'];
        $jsonIklan2 = $IklanKananArr[0]['jsonIklanData2'];
        $jsonIklanKabupaten = $IklanKananArr[0]['jsonIklanKabupaten'];
        $jsonTopRank = $IklanKananArr[0]['jsonTopRank'];
        $jsonWisataPopuler = $IklanKananArr[0]['jsonWisataPopuler'];

        $JsonIklanBanner = json_decode($JsonIklanBanner, TRUE);;
        $jsonIklan1 = $IklanKananArr[0]['jsonIklanData1'];
        $jsonIklanPropinsi = $IklanKananArr[0]['jsonIklanPropinsi'];
        $jsonIklan2 = $IklanKananArr[0]['jsonIklanData2'];
        $jsonIklanKabupaten = $IklanKananArr[0]['jsonIklanKabupaten'];
        $jsonTopRank = $IklanKananArr[0]['jsonTopRank'];
        $jsonWisataPopuler = $IklanKananArr[0]['jsonWisataPopuler'];

        $jsonIklan1Arr = json_decode($jsonIklan1,TRUE);
        $jsonIklan2Arr = json_decode($jsonIklan2,TRUE);
        $jsonWisataPopulerArr = json_decode($jsonWisataPopuler,TRUE);

        $dataparam = array(
            'iklan' => array(
                'iklanpropinsi' => array(
                    'judul' => $jsonIklan1Arr[0]['JudulGroup'],
                    
                    'data' => $jsonIklan1Arr[0]['data'],
                ),

                'iklankabupaten' => array(
                    'judul' => $jsonIklan2Arr[0]['JudulGroup'],
                    'data' => $jsonIklan2Arr[0]['data'],
                ),

                'toprange' => array(
                    'judul' => 'Top Rank',
                    'data' => json_encode(
                        array('data' => array(
                            array(
                                'nama' => 'Susana Ambarwati',
                                'ulasan' => 1123,
                                'following' => 981,
                                'linkthumbnail' => 'assets/icon/avatar1.png',
                                'icon' => 'iconikuti.svg'
                            ),
                            array(
                                'nama' => 'Riana Indah Kurnia',
                                'ulasan' => 928,
                                'following' => 820,
                                'linkthumbnail' => 'assets/icon/avatar2.png',
                                'icon' => 'iconmengikuti.svg'
                            ),
                            array(
                                'nama' => 'Susana Ambarwati',
                                'ulasan' => 928,
                                'following' => 820,
                                'linkthumbnail' => 'assets/icon/avatar1.png',
                                'icon' => 'icondiikuti.svg'

                            ),
                            array(
                                'nama' => 'Susana Ambarwati',
                                'ulasan' => 928,
                                'following' => 820,
                                'linkthumbnail' => 'assets/icon/avatar1.png',
                                'icon' => 'iconsalingmengikuti.svg'

                            ),
                            array(
                                'nama' => 'Riana Indah Kurnia',
                                'ulasan' => 928,
                                'following' => 820,
                                'linkthumbnail' => 'assets/icon/avatar2.png',
                                'icon' => 'iconikuti.svg'

                            ),
                            array(
                                'nama' => 'Susana Ambarwati',
                                'ulasan' => 928,
                                'following' => 820,
                                'linkthumbnail' => 'assets/icon/avatar1.png',
                                'icon' => 'iconikuti.svg'

                            ),
                        ))
                    )
                ),

                'wisatapopuler' => array(
                    'judul' => $jsonWisataPopulerArr[0]['judul'],
                    'data' => $jsonWisataPopulerArr[0]['data'],
                )
            ),
            'footkategori' => $jsonDataListBawah,
            'title' => $judul,
            'title' => $judul,
            'judul' => $judul,
            'idtab' => $idtab,           
            //'judulatas' => $judul2,
            'call_page' => 'hasilfilter/view',
            'datakuliner' => $datakulinerArr,
        );
      
        
        $this->parser->parse(MASTER_PAGE, $dataparam);
    }
    
}