<?php defined('BASEPATH') OR exit('No direct script access allowed');

class con_komentar extends MY_Core
{
    function __construct(){
        parent::__construct();
        $this->setkomentar = curl_url() . "Ctr_ulasan/setkomentar";
        $this->getkomentar = curl_url() . "Ctr_ulasan/getkomentar";
//        $this->urliklankanan = curl_url() . "Ctr_iklan/getIklanLandingKuliner";
//        $this->urllistbawah = curl_url() . "Ctr_data_filter/getJsonListBawah";
//        $this->urllokasi = curl_url() . "Ctr_data_filter/getDataLatLong";
//        
//        $this->idhari = day_php(date("l"));
    }
    

    function index($itemkawasan=""){
        
//        $provinsi = $this->session->userdata('provinsi');               
//        $kabupaten = $this->session->userdata('kabupaten'); 
////        print_r($kabupaten);
////        echo $kabupaten."===>".$provinsi."==>";
//        $xparamkab = array(
//            'kabupaten' => $kabupaten,
//            'propinsi' => $provinsi            
//            );
//        
//        $jsondatakab = curl_post_data(
//            $this->urllokasi,
//            array('jsonparamlokasi' => json_encode($xparamkab))
//        );
//        
//       // echo $jsondatakab;
//        $jsonDataKabArr = json_decode($jsondatakab, TRUE);
//        $this->session->set_userdata('latitude',$jsonDataKabArr[0]['latitude']);
//        $this->session->set_userdata('longitude',$jsonDataKabArr[0]['longitude']);
//        $this->session->set_userdata('kabupatenpilih',$jsonDataKabArr[0]['kabupaten']);
//        $this->session->set_userdata('propinsipilih',$jsonDataKabArr[0]['propinsi']);
/*        
       $xlat = $this->session->userdata('latitude');
       //echo "latitude--->".$xlat;
        if(empty($xlat)){
            $json_param_post = array(
                'latitude' => "-7.74766584",
                'longitude' => "110.42292105",
                'kabupaten' => "Kabupaten Sleman",
                'propinsi' => "Daerah Istimewa Yogyakarta",
                'idhari' => "1",
                'jam' => "17:38",
                'language' => "ID",
                'jenispilihan' => "kategori",
                'pilihan' => "restoran",
                "start"=>"0",
                "limit"=>"12",
                'kawasanArea' =>$itemkawasan,
                'iduser' => "1"
            );
        } else {
            $json_param_post = array(
                'latitude' => $this->session->userdata('latitude'),
                'longitude' => $this->session->userdata('longitude'),
                'kabupaten' => $this->session->userdata('kabupatenpilih'),
                'propinsi' => $this->session->userdata('propinsipilih'),
                'idhari' => "1",
                'session' => "1",
                'jam' => "17:38",
                'language' => "ID",                
                'kawasanArea' =>$itemkawasan,
                "start"=>"0",
                "limit"=>"12",
                'iduser' => "1"
            );
        }
        
        $jsondata = curl_post_data(
            $this->urllanding,
            array('jsonParamAwal' => json_encode($json_param_post))
        );
        
        //echo $jsondata;
        
        $jsonDataIklanKanan = curl_post_data(
            $this->urliklankanan,
            array('jsonParamAwal' => json_encode($json_param_post))
        );
        
        //echo $jsonDataIklanKanan;

        $jsonDataListBawah = curl_post_data(
            $this->urllistbawah,
            array('jsonParamListBawah' => json_encode(
                array(
                    'language' => "ID"
                )
            ))
        );

        $data = json_decode($jsondata, TRUE);
        $data = $data['0'];

        $judul = $data['judul'];
        $subjudul = $data['subjudul'];
        $iskawasan = $data['iskawasan'];
        $diskripsi = $data['diskripsi'];
        
        $jsonDataPrimer = $data['dataKawasanArea'];
        $jsonDataPrimerArr = json_decode($jsonDataPrimer, TRUE);
        //$jsonDataKuliner = $jsonDataPrimerArr[0]['dataKawasanArea'];
        //print_r($jsonDataPrimerArr);

//        $jsonLandingKulinerArr =  json_decode($jsonLandingKuliner, TRUE);
//        $jsonDataKuliner = $jsonLandingKulinerArr[0]['jsonDataKuliner'];
        
        //$jsonDataKulinerArr = json_decode($jsonDataKuliner, TRUE);
        
        
        $datakulinerArr = $jsonDataPrimerArr;

        // Data Iklan Landing Kuliner
        $jsonDataIklanKananArr = json_decode($jsonDataIklanKanan,TRUE);
        $IklanKanan = $jsonDataIklanKananArr[0]['IklanKanan'];
        $IklanKananArr = json_decode($IklanKanan, TRUE);
        
        

        $JsonIklanBanner = $IklanKananArr[0]['jsonIklanBar'];
        $jsonIklan1 = $IklanKananArr[0]['jsonIklanData1'];
        $jsonIklanPropinsi = $IklanKananArr[0]['jsonIklanPropinsi'];
        $jsonIklan2 = $IklanKananArr[0]['jsonIklanData2'];
        $jsonIklanKabupaten = $IklanKananArr[0]['jsonIklanKabupaten'];
        $jsonTopRank = $IklanKananArr[0]['jsonTopRank'];
        $jsonWisataPopuler = $IklanKananArr[0]['jsonWisataPopuler'];

        $JsonIklanBanner = json_decode($JsonIklanBanner, TRUE);;
        $jsonIklan1 = $IklanKananArr[0]['jsonIklanData1'];
        $jsonIklanPropinsi = $IklanKananArr[0]['jsonIklanPropinsi'];
        $jsonIklan2 = $IklanKananArr[0]['jsonIklanData2'];
        $jsonIklanKabupaten = $IklanKananArr[0]['jsonIklanKabupaten'];
        $jsonTopRank = $IklanKananArr[0]['jsonTopRank'];
        $jsonWisataPopuler = $IklanKananArr[0]['jsonWisataPopuler'];

        $jsonIklan1Arr = json_decode($jsonIklan1,TRUE);
        $jsonIklan2Arr = json_decode($jsonIklan2,TRUE);
        $jsonWisataPopulerArr = json_decode($jsonWisataPopuler,TRUE);

        $dataparam = array(
            'iklan' => array(
                'iklanpropinsi' => array(
                    'judul' => $jsonIklan1Arr[0]['JudulGroup'],
                    
                    'data' => $jsonIklan1Arr[0]['data'],
                ),

                'iklankabupaten' => array(
                    'judul' => $jsonIklan2Arr[0]['JudulGroup'],
                    'data' => $jsonIklan2Arr[0]['data'],
                ),

                'toprange' => array(
                    'judul' => 'Top Rank',
                    'data' => json_encode(
                        array('data' => array(
                            array(
                                'nama' => 'Susana Ambarwati',
                                'ulasan' => 1123,
                                'following' => 981,
                                'linkthumbnail' => 'assets/icon/avatar1.png',
                                'icon' => 'iconikuti.svg'
                            ),
                            array(
                                'nama' => 'Riana Indah Kurnia',
                                'ulasan' => 928,
                                'following' => 820,
                                'linkthumbnail' => 'assets/icon/avatar2.png',
                                'icon' => 'iconmengikuti.svg'
                            ),
                            array(
                                'nama' => 'Susana Ambarwati',
                                'ulasan' => 928,
                                'following' => 820,
                                'linkthumbnail' => 'assets/icon/avatar1.png',
                                'icon' => 'icondiikuti.svg'

                            ),
                            array(
                                'nama' => 'Susana Ambarwati',
                                'ulasan' => 928,
                                'following' => 820,
                                'linkthumbnail' => 'assets/icon/avatar1.png',
                                'icon' => 'iconsalingmengikuti.svg'

                            ),
                            array(
                                'nama' => 'Riana Indah Kurnia',
                                'ulasan' => 928,
                                'following' => 820,
                                'linkthumbnail' => 'assets/icon/avatar2.png',
                                'icon' => 'iconikuti.svg'

                            ),
                            array(
                                'nama' => 'Susana Ambarwati',
                                'ulasan' => 928,
                                'following' => 820,
                                'linkthumbnail' => 'assets/icon/avatar1.png',
                                'icon' => 'iconikuti.svg'

                            ),
                        ))
                    )
                ),

                'wisatapopuler' => array(
                    'judul' => $jsonWisataPopulerArr[0]['judul'],
                    'data' => $jsonWisataPopulerArr[0]['data'],
                )
            ),
            'footkategori' => $jsonDataListBawah,
            'title' => $judul,
            'judul' => $judul,
            'subjudul' => $subjudul,
            'diskripsi' => $diskripsi,
            'iskawasan' => $iskawasan,
            'call_page' => 'kawasan/view',
            'datakuliner' => $datakulinerArr,
        );

        $this->parser->parse(MASTER_PAGE, $dataparam);
 * 
 */
    }
    
function getlistkomentar(){
   $json_param_set = array(
            'idjeniskomentar' =>"2", // 1 : Foto, 2 :ulasan
            'idaktifitas' =>"ULS201804130000001",// ulasan, foto
            'iduser' => "UR201805020000001",
            'startlimit' => "0",
            'endlimit' => "15", ///--->DESC
        ); 
   
   
   
   
   $jsonstatus = curl_post_data(
            $this->getkomentar,
            array('paramlistkomentar' => json_encode($json_param_set))
        );
   echo $jsonstatus;
}

function setKomentar(){
    $json_param_set = array(
            'idjeniskomentar' =>"2", // 1 : Foto, 2 :Ulasan
            'idaktifitas' =>"",//Id nya  ulasan, id nya foto
            'iduser' => "",            
            'komentar' => "", //isi komentar
            'idkomentarsebelumnya' => "", //idparent
            'iduserkomenentarparent' => "" //Id user yang memberi konetar di parent
        );
    
    $jsonstatus = curl_post_data(
            $this->setkomentar,
            array('jsonParamAwal' => json_encode($json_param_set))
        );
}


}