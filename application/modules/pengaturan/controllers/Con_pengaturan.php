<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Con_pengaturan extends MY_Core
{
    function __construct(){
        parent::__construct();
        $this->getkoleksidefult = curl_url() . "Ctr_user_koleksi/getKoleksiDefault";
        //$this->urliklankanan = curl_url() . "Ctr_iklan/getJsonIklanDetailKuliner";
    }

    function index(){
       
    }
    
    function setpengaturan(){
     
       $xarrayuser  = array(
            "namauser" =>   (!empty($userName))?$userName:"Nama User", 
            "foto" =>   (!empty($photo))?$photo:base_url('assets/images/profile1.jpg'), 
            "urlbackground" => (!empty($urlbackground))?$urlbackground:base_url('assets/images/food2.jpg'), 
            "kotaasal" => (!empty($kotaasal))?$kotaasal:"Belum Diisi Kota Asal", 
            "grade" => (!empty($idgrade))?$idgrade:"Tripper", 
            "level" => (!empty($idlevel))?$idlevel:"Level 1"          
         ) ; 
       $xarraymenukiri = "";
       $dataparam = array(
                'databanner' => $this->load->view('__banner_profile', $xarrayuser,TRUE), 
                'menukiri' => $this->load->view('__menu_sosmed_left', $xarraymenukiri,TRUE),
                'kontent'    =>$this->load->view('content','',TRUE),      
                'call_page' => 'pengaturan/view',
            );
  
     $this->parser->parse(MASTER_PAGE, $dataparam);  
    
  }
  /*
    function setpernahkesini(){
      $iduser  = $this->session->userdata('iduser');
      $userName = $this->session->userdata('userName');
      $photo  = $this->session->userdata('user_url_foto');
      $urlbackground = $this->session->userdata('urlbackground');
      $kotaasal = $this->session->userdata('kotaasal');
      $idgrade = $this->session->userdata('idgrade');
      $idlevel = $this->session->userdata('idlevel');
      $kabupaten = $this->session->userdata('kabupatenpilih');
      
     
     $xarrayuser  = array(
            "namauser" =>   (!empty($userName))?$userName:"Nama User", 
            "foto" =>   (!empty($photo))?$photo:base_url('assets/images/profile1.jpg'), 
            "urlbackground" => (!empty($urlbackground))?$urlbackground:base_url('assets/images/food2.jpg'), 
            "kotaasal" => (!empty($kotaasal))?$kotaasal:"Belum Diisi Kota Asal", 
            "grade" => (!empty($idgrade))?$idgrade:"Tripper", 
            "level" => (!empty($idlevel))?$idlevel:"Level 1"          
         ) ; 
     
     
       $json_param_postKuliner = $this->setparampost(0,15,"1");
        $jsondatakuliner = curl_post_data(
            $this->listcheckin,
            array('param' => json_encode($json_param_postKuliner))
        );
        //echo $jsondata;
        
        $jsondataArrKuliner = json_decode($jsondatakuliner, true);
      //$this->session->unset_userdata('lastseen');  
      //  $databuf = $jsondataArrKuliner[0];
      
      $json_param_postWisata = $this->setparampost(0,15,"2");
        $jsondatawisata = curl_post_data(
            $this->listcheckin,
            array('param' => json_encode($json_param_postWisata))
        );
        //echo $jsondata;
        
        $jsondataArrwisata = json_decode($jsondatawisata, true);
      
     $xarraymenukiri  = ""; 
     $xarraymenukanan  = ""; 
     $xarraycontent  = array(
         'issetpin' => 'N',
         'issetcontrol' => 'N',
         'kabupaten' => 'Semua Area',
         'datakuliner' =>$jsondataArrKuliner,
         'datawisata' =>$jsondataArrwisata,
     ); 
     
      $dataparam = array(
                'databanner' => $this->load->view(MASTER_TEMA.'/sosmed/__banner_profile', $xarrayuser,TRUE),
                'menukiri' => $this->load->view(MASTER_TEMA.'/sosmed/__menu_sosmed_left', $xarraymenukiri,TRUE),
                'menukanan' => $this->load->view(MASTER_TEMA.'/sosmed/__menu_sosmed_right', $xarraymenukanan,TRUE),
                'kontent' => $this->load->view('content', $xarraycontent,TRUE),                
                'call_page' => 'aktifitas/view',
            );
  
      $this->parser->parse(MASTER_PAGE, $dataparam);  
    }
    
    function setparampost($xlimitstart,$xlimitend,$idsession){
        $xlat = $this->session->userdata('latitude');
        if(empty($xlat)){
            $json_param_post = array(
                'latitude' => "-7.74766584",
                'longitude' => "110.42292105",
                'kabupaten' => "Kabupaten Sleman", 
                'propinsi' => "Daerah Istimewa Yogyakarta",
                'idhari' => "1",
                'jam' => "17:38",
                'language' => "ID",
                'iduser' => $this->session->userdata('iduser'),
                'idsession'=>$idsession,
                'idakses' =>"3",
                'withprop' => "",
                'withkab' => "",
                'limitstart'=>$xlimitstart,
                'limitend'=>$xlimitend
            );
        } else {
            $json_param_post = array(
                'latitude' => $this->session->userdata('latitude'),
                'longitude' => $this->session->userdata('longitude'),
                'kabupaten' => $this->session->userdata('kabupatenpilih'),
                'propinsi' => $this->session->userdata('propinsipilih'),
                'idhari' => "1",
                'jam' => "17:38",
                'language' => "ID",
                'iduser' => $this->session->userdata('iduser'),
                'idsession'=>$idsession,
                'limitstart'=>$xlimitstart,
                'limitend'=>$xlimitend,
                'withkab' => "",
                'withprop' => "",
                'idakses' =>"3"
            );
        }
       return $json_param_post; 
    }
   * 
   */
}