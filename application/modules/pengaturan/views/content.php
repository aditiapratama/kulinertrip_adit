<div class="content-row">
	<div class="content-col">

		<div class="setting-profile-page">
			<div class="setting-profile-title">
				<div class="sp-left">
					<h3>Biodata Diri</h3>
					<h4>- Akun Belum Terverifikasi</h4>
				</div>
				<div class="sp-right">
					<a href="" ><span class="ic-sm-edit-blue"></span>Edit</a>
				</div>
			</div>
			<div class="row setting-profile-content">
				<div class="col-md-4">
					<div class="setting-profile-photo">
						<img src="images/profile2.jpg" />
						<div class="button-upload-photo">
							<button class="btn btn-primary btn-simple-grey"><span class="ic-photo"></span>Ubah foto profil</button>
							<input type="file" name="upload">
						</div>
					</div>

					<div style="text-align:center;margin: 40px auto 0;display:table;">
						<button type="button" class="btn btn-primary btn-simple-grey" data-toggle="modal" data-target="#pilihphotoku">Pilih Photo Profil</button>
						<p>Dummy untuk Pilih Photo</p>
					</div>

				</div>
				<div class="col-md-8">
					<form class="setting-profile-form">
						
						<div class="form-group form-inline">
							<label class="control-label">Nama Lengkap <span class="required">*</span></label>
							<div class="input-group">
								<input class="form-control" placeholder="" type="text">
								<span class="input-group-addon ic-sm-locked"></span>
							</div>
							<div class="modern-input-desc">digunakan sebagai inputan</div>
						</div>
						<div class="form-group form-inline">
							<label class="control-label">Nama Akun <span class="required">*</span></label>
							<div class="input-group">
								<input class="form-control" placeholder="" type="text">
								<span class="input-group-addon ic-sm-earth"></span>
							</div>
							<div class="modern-input-desc">digunakan sebagai inputan</div>
						</div>
						<div class="form-group form-inline">
							<label class="control-label">Tanggal Lahir</label>
							<div class="input-group">
								<input class="form-control" placeholder="" type="date">
								<span class="input-group-addon ic-sm-locked"></span>
							</div>
						</div>
						<div class="form-group form-inline">
							<label class="control-label">Jenis Kelamin</label>
							<div class="radio">
								<label class="mr-20">
									<input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
									Laki-laki
								</label>
								<label>
									<input type="radio" name="optionsRadios" id="optionsRadios1" value="option1">
									Perempuan
								</label>
								<div class="ml-30 iconbox"><span class="ic-sm-locked"></span></div>
							</div>
						</div>
						<div class="form-group">
							<label>Alamat Surat</label>
							<div class="input-group">
								<textarea class="form-control" rows="3"></textarea>
								<span class="input-group-addon ic-sm-locked"></span>
							</div>
							<div class="modern-input-desc">digunakan sebagai inputan</div>
						</div>
						<div class="form-group">
							<label>Deskripsi Diri</label>
							<div class="input-group">
								<textarea class="form-control" rows="3"></textarea>
								<span class="input-group-addon ic-sm-earth"></span>
							</div>
						</div>

						<div class="setting-profile-form-title">Link Sosial Media</div>

						<div class="form-group form-inline">
							<label class="control-label">Instagram</label>
							<div class="input-group">
								<input class="form-control" placeholder="" type="text">
								<span class="input-group-addon ic-sm-earth"></span>
							</div>
							<div class="modern-input-desc">digunakan sebagai inputan</div>
						</div>
						<div class="form-group form-inline">
							<label class="control-label">Twitter</label>
							<div class="input-group">
								<input class="form-control" placeholder="" type="text">
								<span class="input-group-addon ic-sm-earth"></span>
							</div>
							<div class="modern-input-desc">digunakan sebagai inputan</div>
						</div>
						<div class="form-group form-inline">
							<label class="control-label">Facebook</label>
							<div class="input-group">
								<input class="form-control" placeholder="" type="text">
								<span class="input-group-addon ic-sm-earth"></span>
							</div>
							<div class="modern-input-desc">digunakan sebagai inputan</div>
						</div>
						<div class="form-group form-inline">
							<label class="control-label">Web</label>
							<div class="input-group">
								<input class="form-control" placeholder="" type="text">
								<span class="input-group-addon ic-sm-earth"></span>
							</div>
							<div class="modern-input-desc">digunakan sebagai inputan</div>
						</div>
						<div class="form-group form-inline">
							<label class="control-label">Email Publish</label>
							<div class="input-group">
								<input class="form-control" placeholder="" type="text">
								<span class="input-group-addon ic-sm-earth"></span>
							</div>
							<div class="modern-input-desc">digunakan sebagai inputan</div>
						</div>
						<div class="form-group">
							<label>Email Akun</label>
							<div class="input-group">
								<input class="form-control" placeholder="" type="text">
								<span class="input-group-addon ic-sm-locked"></span>
							</div>
						</div>
						<div class="form-group">
							<label>Nomor Handphone</label>
							<div class="input-group">
								<input class="form-control" placeholder="" type="text">
								<span class="input-group-addon ic-sm-locked"></span>
							</div>
						</div>
						

					</form>

					<div class="setting-profile-form-title">Wakili Kota Anda</div>
					<div class="setting-profile-info">
						<div class="divbox-table-cell">
							<p>Jadilah Triper Guide untuk mewakili tempat tinggal anda saat ini. Saat ini anda sebagai Tripper Guide di Kota Yogyakarta</p>
						</div>
						<div class="divbox-table-cell">
							<img src="<?php echo base_url() ?>/assets/images/TripperGuideillustration.png" alt="">
						</div>
					</div>
					
					<div class="input-group sp-btn-gantikota">
						<input class="form-control" placeholder="Ganti Kota Saya" type="text">
						<button class="btn btn-primary btn-simple-grey" type="button">Ganti Kota</button>
					</div>

				</div>
			</div>
		</div>

	</div>
</div>


<!-- Modal -->
<div class="modal fade" id="pilihphotoku" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Ubah Foto Profil</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-5">
						<div class="setting-profile-photo-option">
							<img class="sp-photo-option-square-big" src="images/profile2.jpg" />
						</div>
						<div class="button-upload-photo">
							<button class="btn btn-primary btn-simple-grey"><span class="ic-photo"></span>Ubah Foto profil</button>
							<input type="file" name="upload">
						</div>
					</div>
					<div class="col-md-7">
						<div class="sp-photo-option-title">Foto Profil</div>
						<ul class="sp-photo-option-list">
							<li><img class="sp-photo-option-square" src="images/profile2.jpg" /></li>
							<li><img class="sp-photo-option-square" src="images/profile2.jpg" /></li>
							<li><img class="sp-photo-option-square" src="images/profile2.jpg" /></li>
							<li><img class="sp-photo-option-square" src="images/profile2.jpg" /></li>
							<li><img class="sp-photo-option-square" src="images/profile2.jpg" /></li>
						</ul>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-md-6">
						<div class="setting-profile-photo-option-rectangle">
							<img class="sp-photo-option-rectangle-big" src="images/food3.jpg" />
						</div>
						<div class="button-upload-photo">
							<button class="btn btn-primary btn-simple-grey"><span class="ic-photo"></span>Ubah Background Profil</button>
							<input type="file" name="upload">
						</div>
					</div>
					<div class="col-md-6">
						<div class="sp-photo-option-title">Foto Background</div>
						<ul class="sp-photo-option-list">
							<li><img class="sp-photo-option-rectangle" src="images/food3.jpg" /></li>
							<li><img class="sp-photo-option-rectangle" src="images/food3.jpg" /></li>
							<li><img class="sp-photo-option-rectangle" src="images/food3.jpg" /></li>
							<li><img class="sp-photo-option-rectangle" src="images/food3.jpg" /></li>
						</ul>
					</div>
				</div>
				<hr>
			</div>
			<div class="modal-footer modal-footer-simple">
				<button type="button" class="btn btn-primary btn-fullw">Simpan Perubahan</button>
			</div>
		</div>
	</div>
</div>