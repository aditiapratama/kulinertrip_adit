<html lang="en">

<!-- begin.head -->
<?php //include'register-head.php'; ?>
<!-- end.head -->
<link rel="stylesheet" href="<?php echo base_url('assets/css/layouts.css') ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/css/typography.css') ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/css/iconcss.css') ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min') ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome.min') ?>">
    
<body>
	<!-- begin.header -->
	<div class="container-layout">
		<section class="">
			
                    <?php echo  $databanner; ?>
		</section>
		<section class="content-body">
			<div class="layout-row">
				<div class="layout-col sidebar-left">
					<?php echo  $menukiri; ?>
				</div>
				<div class="layout-col">
					 <?php echo  $kontent; ?>
				</div>
				
			</div>
		</section>
	</div>
	<!-- begin.footer -->
	
	<!-- end.footer -->



	<!-- begin.js -->
	
	<!-- end.js -->
</body>
</html>
