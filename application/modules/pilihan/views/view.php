
<div class="container">
    <?php //echo '<pre>'.print_r($datakuliner, true).'</pre>'; ?>
    <div class="row">
        <div class="style-content">
            <div class="col-sm-9">
                <div class="row">
                    <h2 class="content-title"><?php echo $judulatas?></h2>
                    <h4 class="content-title"><?php echo $title?></h4>

                    <?php $this->load->view('__itemkuliner',$datakuliner); ?>
                    <div class="clearfix"></div>
                </div>

                

                <div class="content-kategori">
                    <?php get_footer_kategori((!empty($footkategori) ? $footkategori : '')) ?>
                    <div class="clearfix"></div>
                </div>
                
            
                <?php $datahastag=""; $this->load->view('__hastag',$datahastag); ?>
            

            </div>

            <div class="col-sm-3">
                <?php get_iklankanan(!empty($iklan) ? $iklan : false) ?>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>

<div class="view-modal">
    <div class="view-frame">
        <div class="frame-kiri">
            <div class="panel-up"></div>
            <div class="panel-down"></div>
        </div>
        <div class="frame-kanan"></div>
    </div>
</div>