<?php if (!empty($data['data'])) { ?>

    <div class="" id="kanan-toprange">
        <div class="side-kanan bg-white">
            <h4 class="kanan-title"><?php echo $data['judul'] ?> </h4>
            <?php $dataArrBuf = json_decode($data['data'],true);
            if(!empty($dataArrBuf['data'])){
                $dt = $dataArrBuf['data'];
                for ($i = 0; $i < count($dt); $i++) { ?>

            <div class="col-md-12 padding-5 col-list">
                <div class="thumbnail-kanan toprange">
                    <div class="range-image">
                        <img src="<?php echo base_url($dt[$i]['linkthumbnail']); ?>" alt="">
                    </div>
                    <div class="range-desc">
                        <div> <p class="range-title"><?php echo $dt[$i]['nama'] ?></p> </div>
                        <p><?php echo $dt[$i]['ulasan'] ?> Ulasan</p>
                        <p><?php echo $dt[$i]['following'] ?> Follower</p>
                        <span class="range-add" data-toggle="tooltip" title="Ikuti" data-placement="top">
                        <object width="30px" type="image/svg+xml" data="<?php echo base_url('assets/icon/sosmed/'.$dt[$i]['icon']) ?>"></object>    
<!--                            <img data-u="image" src="<?php //echo base_url('assets/icon/ikutiicon.png') ?>" width="30px" />-->
                        </span>
                    </div>
                </div>
            </div>    <?php }} ?>

            <div class="clearfix"></div>
        </div>
    </div>
<?php } ?>

