<?php
if($out = get_data_kanan($data['data'])){
foreach ( $out as $out__) { $key = $out__['data']; ?>

            <div class="col-sm-6 padding-5 col-set-2">
                <div class="kanan-frame">
                    <button class="btn btn-xs" data-toggle="tooltip" title="Simpan ke Koleksi" data-placement="top"><i class="fa fa-fw fa-bookmark"></i></button>
                    <a class="link" href="<?php echo base_url('kuliner/detail/'. $key['UrlPage'] .'.html') ?>" target="_blank">
                        <div class="thumbnail-kanan">
                            <div class="image">
                                <img src="<?php echo $key['linkthumbnail'] ?>" alt="">
                                <span class="label label-rating"><?php echo $key['rating'] ?></span>
                            </div>
                            <div class="description">
                                <p class="desc-title"><?php echo $key['JudulProduk'] ?></p>
                                <p class="desc-area" class="kategori"><?php echo $key['kategoritext']."-". $key['kawasanArea'] .", ".$key['kabupaten'] ?></p>
                                <span class="desc-over">
                                    <div class="report">
                                        <span data-toggle="tooltip" title="Tersimpan" data-placement="top" class="text-iklan"><img data-u="image" src="<?php echo base_url('assets/icon/iconsimpan.png') ?>" width="14px"/> 0 </span>
                                        <span data-toggle="tooltip" title="Ulasan" data-placement="top" class="text-iklan"><img data-u="image" src="<?php echo base_url('assets/icon/iconulasan.png') ?>" width="14px"/> 0</span>
                                        <span data-toggle="tooltip" title="Foto" data-placement="top" class="text-iklan"><img data-u="image" src="<?php echo base_url('assets/icon/iconfoto.png') ?>" width="14px"/> 0</span>
                                    </div>
                                    <p class="buka">     
                                        <?php if(trim($key['KeteranganBuka'])==='Buka'){ ?>

                                        <img data-u="image" src="<?php echo base_url('assets/icon/iconopen.png') ?>" width="10px" style="padding-left: 3px;" data-toggle="tooltip" title="Tersimpan" data-placement="top"/>
                                            <span class="text-buka"> <?php echo $key['KeteranganBuka']; ?></span>
                                            <span class="text-keteranganbuka"></span>

                                        <?php } else { ?>

                                        <img data-u="image" src="<?php echo base_url('assets/icon/iconclose.png') ?>" width="10px" style="padding-left: 3px;" data-toggle="tooltip" title="Tersimpan" data-placement="top"/>&nbsp;&nbsp;<?php echo $key['KeteranganBuka']; ?>

                                        <?php } ?>
                                    </p>
                                    <p>
                                        <span class="lokasi" data-toggle="tooltip" title="<?php echo (!empty($data['tootipjarak'])) ?  $data['tootipjarak']:  'Berdasarkan Titik Pusat Keramaian Kota'; ?>" data-placement="bottom">
                                            <i class="fa fa-fw fa-location-arrow"> </i> <?php echo $key['jarak'] ?> km
                                        </span>
                                    </p>
                                </span>
                            </div>
                        </div>
                    </a>
                </div>
            </div>      <?php } ?>

<?php } ?>

