<?php if (!empty($data['data'])) { ?>

    <div class="" id="kanan-provinsi">
        <div class="side-kanan bg-white">
            <h4 class="kanan-title"><?php echo $data['judul'] ?> </h4>

            <?php $dataArrBuf = json_decode($data['data'],true);
            //echo '<pre>'.print_r($dataArrBuf, true).'</pre>';  
            if(!empty($dataArrBuf)){
            foreach ( $dataArrBuf as $xdatakey) {
            $key = $xdatakey['data']; ?>

            <div class="col-sm-6 padding-5 col-set-2">
                <div class="kanan-frame">
                    <button class="btn btn-xs" data-toggle="tooltip" title="Simpan ke Koleksi" data-placement="top"><i class="fa fa-fw fa-bookmark"></i></button>
                    <a class="link" href="<?php echo base_url('kuliner/detail/'. $key['UrlPage'] .'.html') ?>" target="_blank">
                        <div class="thumbnail-kanan">
                            <div class="image">
                                <img src="<?php echo $key['linkthumbnail'] ?>" alt="">
                                <span class="label label-rating"><?php echo $key['rating'] ?></span>
                            </div>
                            <div class="description">
                                <p class="desc-title"><?php echo (!empty($key['judul']) ? $key['judul'] : (!empty($key['JudulProduk']) ? $key['JudulProduk'] : '')) ?></p>
                                <p class="desc-area" class="kategori"><?php echo  str_replace("Wisata", "", $key['jeniswisata'])."-". $key['kawasanArea'] .", ".$key['kabupaten'] ?></p>
                                <span class="desc-over location">
                                    <div class="report">
<!--                                                <i class="fa fa-fw fa-bookmark" data-toggle="tooltip" title="Disimpan" data-placement="top"></i>  -->
                                        <?php //echo '<pre>'.print_r($key, true).'</pre>'; ?>        
                                                <img data-u="image" src="<?php echo base_url('assets/icon/iconsimpan.png') ?>" width="14px" data-toggle="tooltip" title="Tersimpan" data-placement="top"/>    <span class="text-iklan">0 </span>
                                                <img data-u="image" src="<?php echo base_url('assets/icon/iconulasan.png') ?>" width="14px" data-toggle="tooltip" title="Ulasan" data-placement="top"/>    <span class="text-iklan">0</span>
                                                <img data-u="image" src="<?php echo base_url('assets/icon/iconfoto.png') ?>" width="14px" data-toggle="tooltip" title="Foto" data-placement="top"/>    <span class="text-iklan">0</span>
                                           </div>
                                    <p class="buka">     
                                        <?php 
                                                if(trim($key['KeteranganBuka'])==='Buka'){                                                    
                                                ?>
                                        
                                                   <img data-u="image" src="<?php echo base_url('assets/icon/iconopen.png') ?>" width="10px" style="padding-left: 3px;" data-toggle="tooltip" title="Tersimpan" data-placement="top"/>  
                                                   <span class="text-buka">  <?php echo $key['KeteranganBuka']; ?></span> <span class="text-keteranganbuka">  <?php //echo $key['KeteranganJam']; ?></span>
                                                <?php }else
                                                {?>
                                                  <img data-u="image" src="<?php echo base_url('assets/icon/iconclose.png') ?>" width="10px" style="padding-left: 3px;" data-toggle="tooltip" title="Tersimpan" data-placement="top"/>&nbsp;&nbsp;<?php echo $key['KeteranganBuka']; ?>
                                                <?php }?>
                                    </p>
                                    <p data-toggle="tooltip" title="<?php echo (!empty($data['tootipjarak'])) ?  $data['tootipjarak']:  'Berdasarkan Titik Pusat Keramaian Kota'; ?>" data-placement="bottom">
                                        <i class="fa fa-fw fa-location-arrow"></i> <?php echo $key['jarak'] ?>km
                                    </p>
                                    
                                </span>
                            </div>
                        </div>
                    </a>
                </div>
            </div>      <?php }} ?>

            <a class="more" href="#">Lihat Selanjutnya</a>
            <div class="clearfix"></div>
        </div>
    </div>
<?php } ?>

