<?php if (!empty($data['data'])) { ?>

    <div class="" id="kanan-kategori">
        <div class="side-kanan bg-white">
            <h4 class="title"><?php echo $data['judul'] ?> </h4>
            <?php for ($icoun = 0; $icoun < count($data['data']); $icoun++) { $jsonKulinerKategori = $data['data'][$icoun];  ?>

                <div class="col-sm-6 padding-5 col-set-2">
                    <div class="kanan-frame">
                        <button class="btn btn-xs" data-toggle="tooltip" title="Simpan ke Koleksi" data-placement="top"><i class="fa fa-fw fa-bookmark"></i></button>
                        <a class="link" href="<?php echo base_url('kuliner/' . $jsonKulinerKategori['idx'] . '.html') ?>" target="_blank">
                            <div class="thumbnail-kanan">
                                <div class="image">
                                    <img src="<?php echo $jsonKulinerKategori['linkthumbnail'] ?>" alt="">
                                    <span class="label label-rating"><?php echo $jsonKulinerKategori['rating'] ?></span>
                                </div>
                                <div class="description">
                                    <p class="desc-title"><?php echo (!empty($jsonKulinerKategori['judul']) ? $jsonKulinerKategori['judul'] : (!empty($jsonKulinerKategori['JudulProduk']) ? $jsonKulinerKategori['JudulProduk'] : '')) ?></p>
                                    <p class="desc-area" class="kategori"><?php echo $key['kategoritext']."-". $key['kawasanArea'] .", ".$key['kabupaten'] ?></p>
                                    <span class="desc-over">
                                        <div class="report">
                                            <i class="fa fa-fw fa-bookmark" data-toggle="tooltip" title="Disimpan" data-placement="top"></i>  0
                                            <i class="fa fa-fw fa-clipboard" data-toggle="tooltip" title="Ulasan" data-placement="top"></i>  0
                                            <i class="fa fa-fw fa-image" data-toggle="tooltip" title="Foto" data-placement="top"></i>  0
                                        </div>
                                        <p class="buka"><i class="fa fa-fw fa-clock-o text-green"></i><?php echo $key['jadwalBukaSekarang'] ?></p>
                                        <p data-toggle="tooltip" title="Berdasarkan Titik Pusat Keramaian Kota" data-placement="bottom">
                                            <i class="fa fa-fw fa-location-arrow"></i> <?php echo $key['jarak'] ?>
                                        </p>
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>    <?php } ?>

            <a class="more" href="#">Lihat Selanjutnya</a>
            <div class="clearfix"></div>
        </div>
    </div>
<?php } ?>

