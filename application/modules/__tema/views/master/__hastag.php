<div class="row">
    <div class="col-sm-12">
        <div id="hashtag" class="detail-kuliner-box">
            <form class="form-inline">
                <label>#</label>
                <input type="text" class="form-control" placeholder="Cari berdasarkan hashtag">
                <button type="submit" class="btn btn-default"><img class="img-item-wisata" src="<?php echo base_url(); ?>assets/img/wisata/search-icon.svg" alt="cari"></button>
            </form>
            <label class="hashtag-title">Popular Hashtag di Sleman, D.I Yogyakarta</label>
            <div>
                <?php for($i=0;$i<6;$i++) { ?>

                <a class="tag" href="">sateenak</a>
                <a class="tag" href="">baksohits</a>
                <a class="tag" href="">jajanseru</a>
                <a class="tag" href="">semplakenak</a>
                <a class="tag" href="">vegetarian</a>
                <a class="tag" href="">bestspageti</a>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

