<?php for($i = 0; $i < count($datakuliner); $i++){
                        $buka = json_decode($datakuliner[$i]['jsonJadwalBuka']);
                        ?>

                        <div class="col-sm-4 col-set-2">
                            <div class="col-set-view">
                                <a target="_blank" href="<?php echo base_url('kuliner/detail/'. $datakuliner[$i]['UrlPage'] .'.html') ?>">
                                    <div class="thumbnail-custom">
                                        <div class="image">
                                            <img src="<?php echo $datakuliner[$i]['linkthumbnail']; ?>" alt="">
                                            <span class="label label-default label-rating"><?php echo $datakuliner[$i]['rating']; ?> </span>
                                            <?php 
                                                                    $pagehalal = "";    
                                                                    switch ($datakuliner[$i]['IsNonHalal']) {
                                                                        case 1:
                                                                            $pagehalal = "Mengandung Babi";    


                                                                            break;
                                                                        case 2:
                                                                            $pagehalal = "Megandung Alkohol";    


                                                                            break;
                                                                        case 3:
                                                                            $pagehalal = " <span> Mengandung Babi </span> <br/>";    
                                                                            $pagehalal .= " <span> Megandung Alkohol </span>";    


                                                                            break;

                                                                        default:
                                                                            break;
                                                                    } 
                                               echo (!empty($pagehalal) ? '<p><span>'. $pagehalal .'<span> <span class="pull-right">Non Halal</span></p>' : ''); 
                                            ?>
                                        </div>
                                        <div class="description">
                                            <h4 class="des-title">
                                                <?php echo ucwords(strtolower($datakuliner[$i]['JudulProduk'])); ?>
                                                <span class="pull-right location" data-toggle="tooltip" title="Berdasarkan Titik Pusat Keramaian Kota" data-placement="bottom">
                                            <i class="fa fa-fw fa-location-arrow"></i><?php echo $datakuliner[$i]['jarak']; ?>Km
                                        </span>
                                            </h4>
                                            
                                            <p class="area"><?php echo $datakuliner[$i]['kategoritext']." - ". $datakuliner[$i]['jenisMasakan']; ?></p>
                                            <p class="area"><?php echo $datakuliner[$i]['kawasanArea']; ?></p>
                                            
                                            <div class="report">
<!--                                                <i class="fa fa-fw fa-bookmark" data-toggle="tooltip" title="Disimpan" data-placement="top"></i>  -->
                                                <img data-u="image" src="<?php echo base_url('assets/icon/iconsimpan.png') ?>" width="14px" data-toggle="tooltip" title="Tersimpan" data-placement="top"/>    <span class="text-keteranganbuka"><?php echo $datakuliner[$i]['jmlFavorit']; ?> </span>
                                                <img data-u="image" src="<?php echo base_url('assets/icon/iconulasan.png') ?>" width="14px" data-toggle="tooltip" title="Ulasan" data-placement="top"/>    <span class="text-keteranganbuka"><?php echo $datakuliner[$i]['jmlUlasan']; ?></span>
                                                <img data-u="image" src="<?php echo base_url('assets/icon/iconfoto.png') ?>" width="14px" data-toggle="tooltip" title="Foto" data-placement="top"/>    <span class="text-keteranganbuka"><?php echo $datakuliner[$i]['jmlFoto']; ?></span>
                                           </div>
                                            
                                            <p class="buka">
                                                <?php 
                                                if(trim($datakuliner[$i]['KeteranganBuka'])==='Buka'){                                                    
                                                ?>  
                                                   <img data-u="image" style="float:left" src="<?php echo base_url('assets/icon/iconbuka.png') ?>" width="14px" data-toggle="tooltip" title="Tersimpan" data-placement="top"/> 
                                                <div style="top:3px; left:10px;"> 
                                                     <span class="text-buka">  <?php echo $datakuliner[$i]['KeteranganBuka']; ?></span> <span class="text-keteranganbuka">  <?php echo $datakuliner[$i]['KeteranganJam']; ?></span>
                                                   </div>  
                                                <?php }else
                                                {?>
                                              
                                                  <img data-u="image" style="float:left" src="<?php echo base_url('assets/icon/icontutup.png') ?>" width="14px" data-toggle="tooltip" title="Tersimpan" data-placement="top"/>    
                                                <div style="top:3px; left:10px;">   
                                                    &nbsp;&nbsp;<?php echo $datakuliner[$i]['KeteranganBuka']; ?>
                                                 </div>       
                                                <?php }?>
<!--                                                <i class="fa fa-fw fa-clock-o <?php //echo ((!empty($buka[0]->isbuka)) ? (($buka[0]->isbuka == 'Y') ? 'text-green' : 'text-grey') : 'text-grey') ?> " onclick="produk"></i> <span ><?php //echo $datakuliner[$i]['jadwalBukaSekarang']; ?></span>-->
                                                  
                                            </p>
                                        </div>
                                    </div>
                                </a>
                                <button class="btn btn-xs btn-bookmark" data-toggle="tooltip" title="Simpan ke Koleksi" data-placement="top">
<!--                                    <i class="fa fa-fw fa-bookmark"></i>-->
                                 <img data-u="image" src="<?php echo base_url('assets/icon/simpanico.png') ?>" width="14px" data-toggle="tooltip" title="Ulasan" data-placement="top"/>
                                </button>
                            </div>
                        </div>  <?php } ?>
