<header>
    <div class="header-top">
        <div class="container section-navbar">
            <div class="col-sm-2 section-navbar-box topleftmenu">
                <div class="row">
                    <div class="my-top-box">
                        <h2 class="title">
                            <a style ="color:white" href="<?php echo base_url(); ?>">Tanpa Judul </a>
                        </h2>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 section-navbar-box topcentermenu">
                <div class="row">
                    <div class="my-top-box">
                        <div class="navbar-button-menu">
                            <a class="" id="showmenu">
                                <i class="fa fa-fw fa-bars"></i>
                            </a>
                        </div>
                        <div class="navbar-search">
                            <form action="search" method="GET">
                                <input type="search" placeholder="Apa yang anda cari ?" autocomplete="off" id="search" name="search">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 section-navbar-box toprightmenu">
                <div class="row">
                    <div class="my-top-box">
                        <div class="navbar-topicon-search">
                            <span class="my-icon">
                                <img src="<?php echo base_url('assets/icon/search.png'); ?>">
                            </span>
                        </div>
                        <div class="navbar-topicon-start" id="topicon-start">
                            <span class="my-icon show_filter">
                                <img src="<?php echo base_url('assets/icon/filter.png'); ?>">
                                <span class="badge badges-filter">0</span>
                            </span>
                            <span class="my-icon show_filter">
                                <img src="<?php echo base_url('assets/icon/map_white.png'); ?>">                                
                            </span>
                            <div class="account user pull-right">
                                <div class="user-foto">
                                    <img src="<?php echo base_url('assets/icon/avatar.png'); ?>" alt="">
                                </div>

                                <div class="user-hover">
                                    <ul>
                                        <li>                        
                                            <div class="user-info">
                                                <h4>Mulya Hari Vano </h4>
                                                <p class="">
                                                    <span>Emas-Tripper Guide-Level 5 </span>
                                                    <span><i class="fa fa-fw fa-gbp"></i> Emas</span>
                                                    <span><i class="fa fa-fw fa-euro"></i> Level 5</span>
                                                </p>
                                            </div>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-fw fa-bell text-blue"></i>
                                                Notifikasi
                                                <span class="label label-warning notif">2</span>
                                            </a>
                                        </li>
                                        <li><a href="#"><i class="fa fa-fw fa-user text-purple"></i> Akun</a></li>
                                        <li><a href="#"><i class="fa fa-fw fa-history text-red"></i> Histori</a></li>
                                        <li class="driver"></li>
                                        <li><a href="#"><i class="fa fa-fw fa-lock text-orange"></i> Log Out</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="navbar-topicon-twoand" id="topicon-twoand">
                            <div class="my-top-box">
                                <span class="my-icon pull-right show_filter">
                                    <img src="<?php echo base_url('assets/icon/filter.png'); ?>">
                                </span>
                                <span class="my-icon pull-right">
                                    <img src="<?php echo base_url('assets/icon/map_white.png'); ?>">
                                </span>

                                <span class="my-icon pull-right change-wilayah show_daerah">
                                    <img src="<?php echo base_url('assets/icon/change_area_white.png'); ?>">

                                    <div class="top-wilayahhover">
                                        <div class="top-wilayahhover-content">
                                            <a>
                                                <div class="">Kabupaten Sleman</div>
                                                <div class="">D.I Yogyakarta</div>
                                            </a>

                                            <img src="<?php echo base_url('assets/img/test.jpg') ?>" alt="">
                                            <a>Ganti Area</a>
                                        </div>
                                    </div>
                                </span>

                                <span class="my-icon pull-right">
                                    <img src="<?php echo base_url('assets/icon/wisata_white.png'); ?>">
                                </span>
                            </div>
                        </div>

                        <div class="my-clear"></div>
                    </div>
                </div>
            </div>

            <div class="dropdown_kat" id="show_menu" style="display: none;">
                <div class="container menuhover-frame">
                    <div class="menuhover-left">
                        <div class="menuhover-user">
                            <div class="menuhover-userfoto">
                                <img src="<?php echo base_url('assets/icon/avatar.png'); ?>" alt="">
                            </div>
                            <div class="menuhover-userinfo">
                                <h4>Mulya Hari Vano</h4>
                                <p class="">
                                    <span><i class="fa fa-fw fa-gbp"></i> Emas</span>
                                    <span><i class="fa fa-fw fa-euro"></i> Tripper Guide-Level 5</span>
                                </p>
                            </div>
                        </div>
                        <ul class="menuhover-height">
                            <li><a data-target="menu_a">Kategori</a></li>
                            <li><a data-target="menu_b">Masakan</a></li>
                            <li><a data-target="menu_c">Tujuan</a></li>
                            <li><a data-target="menu_d">Waktu</a></li>
                            <li><a data-target="menu_e">Halal / Non Halal</a></li>
                            <li class="menuhover-driver"></li>
                        </ul>
                    </div>
                    <div class="menuhover-right">
                        <div class="menuhover-right-frame">
                            <div class="menuhover-content">
                                <div class="menuhover-refresh" style="display: none;">
                                    <i class="fa fa-fw fa-refresh fa-2x fa-spin"></i>
                                </div>

                                <div class="menuhover-contentframe">
                                    <div class="menuhover-contentright">
                                        <div class="menuhover-contentdata">
                                            <div class="menuhover-contentdesc">
                                                Bak Pia & Pia Jogja
                                            </div>
                                            <img src="<?php echo base_url('assets/img/test.jpg') ?>" alt="">
                                        </div>
                                        <div class="menuhover-contentdata">
                                            <div class="menuhover-contentdesc">
                                                Bak Pia & Pia Jogja
                                            </div>
                                            <img src="<?php echo base_url('assets/img/test.jpg') ?>" alt="">
                                        </div>
                                        <div class="menuhover-contentdata">
                                            <div class="menuhover-contentdesc">
                                                Bak Pia & Pia Jogja
                                            </div>
                                            <img src="<?php echo base_url('assets/img/test.jpg') ?>" alt="">
                                        </div>
                                        <div class="menuhover-contentdata">
                                            <div class="menuhover-contentdesc">
                                                Bak Pia & Pia Jogja
                                            </div>
                                            <img src="<?php echo base_url('assets/img/test.jpg') ?>" alt="">
                                        </div>

                                        <div class="my-clear"></div>
                                    </div>

                                    <div class="menuhover-contentleft">
                                        <div class="menuhover-contenttab" id="menu_a" style="display: none;">
                                            <div class="menuhover-contenttitle text-center">Jelajahi Cafe,restoran dan tempat lainnya berdasarkan Kategori </div>
                                            <?php foreach ( $kategori as $xdatakey) { ?>
                                            <div class="col-md-4"><a href="<?php echo base_url('pilihan/kategori/'. str_replace(" ", "-", $xdatakey['Kategori'])).'.html';  ?>" target="_blank"><?php echo $xdatakey['Kategori'] ?></a></div>
                                            <?php }?>

                                            <div class="my-clear"></div>
                                        </div>
                                        <div class="menuhover-contenttab" id="menu_b" style="display: none;">
                                            <div class="menuhover-contenttitle text-center">Jelajahi cafe, restoran dan tempat lainnya berdasarkan Jenis Masakan </div>
                                            <?php foreach ( $masakan as $masakankey) {  ?>

                                            <div class="col-sm-12">
                                                <h5 class="text-red"><?php echo $masakankey['jenismakanan']; ?></h5>
                                            </div>

                                            <?php
                                                $jsonhild0  =  $masakankey['child'];
                                                $jsonhildarr0 = json_decode($jsonhild0,true);
                                                if(!empty($jsonhildarr0))
                                                    foreach ( $jsonhildarr0 as $child0key) {  
                                                    $jenismasakan = str_replace(" ", "-", $child0key['jenismakanan']);
                                                     $jenismasakan = str_replace("/", "_", $jenismasakan); 
                                                    ?>
                                                    <div class="col-md-4"><a href="<?php echo base_url('pilihan/masakan/'. $jenismasakan).'.html';  ?>" target="_blank"><?php echo $child0key['jenismakanan'] ?></a></div>
                                                    <?php
                                                        $child0key1  =  $child0key['child'];
                                                        $child1key1arr1 = json_decode($child0key1,true);
                                                        if(!empty($child1key1arr1))
                                                            foreach ( $child1key1arr1 as $child1key) { 
                                                                 $jenismasakan2 = str_replace(" ", "-", $child1key['jenismakanan']);
                                                                 $jenismasakan2 = str_replace("/", "_", $jenismasakan2); 
                                                                    ?>
                                                                  
                                                                <div class="col-md-4"><a href="<?php echo base_url('pilihan/masakan/'.$jenismasakan2 ).'.html';  ?>" target="_blank"><?php echo $child1key['jenismakanan'] ?></a></div>
                                                            <?php }?>
                                                        <?php }?>
                                            <?php }?>

                                            <div class="my-clear"></div>
                                        </div>
                                        <div class="menuhover-contenttab" id="menu_c" style="display: none;">
                                           <div class="menuhover-contenttitle text-center">Jelajahi cafe, restoran dan tempat lainnya berdasarkan Jenis Tujuan </div>
                                            <?php foreach ( $tujuan as $xdatakey) { 
                                                $jenistujuan = str_replace(" ", "-", $xdatakey['jenistujuanresto']);
                                                $jenistujuan = str_replace("/", "_", $jenistujuan); 
                                                ?>
                                                <div class="col-md-4"><a href="<?php echo base_url('pilihan/tujuan/'. $jenistujuan).'.html';  ?>" target="_blank"><?php echo $xdatakey['jenistujuanresto'] ?></a></div>
                                            <?php }?>

                                            <div class="my-clear"></div>
                                        </div>
                                        <div class="menuhover-contenttab" id="menu_d" style="display: none;">
                                            <div class="menuhover-contenttitle text-center">Jelajahi cafe, restoran dan tempat lainnya berdasarkan Jenis Waktu </div>
                                            <?php foreach ( $waktu as $xdatakey) { 
                                                $jeniswaktu = str_replace(" ", "-", $xdatakey['jeniswaktu']);
                                                $jeniswaktu = str_replace("/", "_", $jeniswaktu); 
                                                ?>
                                            <div class="col-md-4"><a href="<?php echo base_url('pilihan/waktu/'. $jeniswaktu).'.html';  ?>" target="_blank"><?php echo $xdatakey['jeniswaktu'] ?></a></div>
                                            <?php }?>

                                            <div class="my-clear"></div>
                                        </div>
                                        <div class="menuhover-contenttab" id="menu_e" style="display: none;">
                                            <div class="menuhover-contenttitle text-center">Jelajahi cafe, restoran dan tempat lainnya berdasarkan Halal dan Non Halal </div>

                                            <div class="col-sm-12">
                                                <h5 class="text-red">Halal</h5>
                                            </div>
                                            <div class="col-md-4"><a href="<?php echo base_url('pilihan/halal/halal').'.html';  ?>" target="_blank">Halal</a></div>

                                            <div class="col-sm-12">
                                                <h5 class="text-red">Non Halal</h5>
                                            </div>

                                            <?php foreach ( $halal[1]['child'] as $xdatakey) { ?>
                                                  <div class="col-md-4"><a href="<?php echo base_url('pilihan/halal/'.str_replace(" ", "-",$xdatakey['StatusHalal'])).'.html';  ?>" target="_blank"><?php echo $xdatakey['StatusHalal']; ?></a></div>
                                            <?php } ?>

                                            <div class="my-clear"></div>
                                        </div>

                                        <div class="my-clear"></div>
                                    </div>

                                    <div class="my-clear"></div>
                                </div>

                                <div class="my-clear"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="search-suggestion-box" class="col-md-7 col-md-offset-2">
            <div id="search-suggestion" class="input-keyword">
            </div>
            <div id="search-suggestion" class="default">
                <ul class="list-unstyled top-helper">
                    <li><i class="fa fa-map-marker"></i> Makanan Khas Sini</li>
                    <li><i class="fa fa-search"></i> Terakhir Kali Dilihat</li>
                </ul>
                <header class="header-helper clearfix">
                    Hasil Pencarian Anda <span>: 80 tempat ditemukan di <a href="" class="blue-link">Sleman, D.I Yogyakarta</a></span>
                    <a class="pull-right red-link" href="">Lihat selengkapnya</a>
                </header>
                <ul class="media-list content-suggestion">
                    <li class="media">
                        <a href="" target="_blank">
                        <div class="media-left">
                            <img class="media-object" src="http://dummyimage.com/64x36/4d494d/686a82.gif&text=placeholder+image">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Sushi Tei</h4>
                            Jl. Gejayan No. 9, Catur Tunggal, Caturtunggal, Sleman, D.I Yogyakarta
                        </div>
                        <ul class="media-footer">
                            <li><i class="fa fa-location-arrow"></i> Buka</li>
                            <li><i class="fa fa-location-arrow"></i> 2 km</li>
                        </ul>
                        </a>
                    </li>
                    <li class="media">
                        <a href="" target="_blank">
                        <div class="media-left">
                            <img class="media-object" src="http://dummyimage.com/64x36/4d494d/686a82.gif&text=placeholder+image">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Sushi Tei</h4>
                            Jl. Gejayan No. 9, Catur Tunggal, Caturtunggal, Sleman, D.I Yogyakarta
                        </div>
                        <ul class="media-footer">
                            <li><i class="fa fa-location-arrow"></i> Buka</li>
                            <li><i class="fa fa-location-arrow"></i> 2 km</li>
                        </ul>
                        </a>
                    </li>
                 
                </ul>
                <footer class="footer-helper">
                    Masukkan kata kunci baru pencarian anda
                </footer>
            </div>
        </div>
    </div>


    <div class="header-mobile" style="display: none;">
        <div class="container">
            <div class="row">
                <div class="mobile-top">
                    <div class="top-1st">
                        <div class="mobile-button-menu">
                            <a href="#menu" id="resmenu_open">
                                <i class="fa fa-fw fa-bars"></i>
                            </a>
                        </div>
                        <a href="#"><img src="<?php echo base_url('assets/icon/wisata_white.png'); ?>"></a>
                        <h2>Kuliner Trip</h2>
                    </div>
                    <div class="top-2nd">
                        <div class="mobile-button-menu">&nbsp;</div>
                        <a id="search_open" href="#search"><img src="<?php echo base_url('assets/icon/search.png'); ?>"></a>
                        <h2 class="kab">Kabupaten Sleman</h2>
                        <h2 class="prov">D.I Yogyakarta</h2>
                    </div>
                </div>
                <div class="mbl-search" id="show_search" style="display: none;">
                    <div class="mbl-heightsearch">
                        <div class="mbl-search-form">
                            <a class="search-close" id="search_close">
                                <i class="fa fa-fw fa-times"></i>
                            </a>
                            <h2><i class="fa fa-fw fa-map-marker"></i> Kuliner Trip</h2>
                            <input type="search" placeholder="Apa yang anda cari ?" autocomplete="off" id="mbl-search" name="search" class="my-input">
                            <button><i class="fa fa-fw fa-search"></i></button>
                        </div>

                        <div class="mbl-search-content">
                            <a class="" href="<?php echo base_url('kulinerkhas'); ?>">
                                <i class="fa fa-fw fa-map-marker"></i>
                                <span>Makanan Khas Sini</span>
                            </a>
                            <a class="">
                                <i class="fa fa-fw fa-history"></i>
                                <span>Terakhir Di Lihat</span>
                            </a>
                        </div>
                    </div>
                    <div class="mbl-rell">
                        <div class="mbl-out">
                            <h5 class="mbl-heighth5 text-center">Hasil Pencarian Anda</h5>
                            <div class="mbl-result" id="mbl-result"></div>
                        </div>
                    </div>
                </div>
                <div class="mbl-dropdown" id="show_menu_resp" style="display: none;">
                    <div class="menuhover-frame">
                        <div class="menuhover-user">
                            <a class="res_close" id="resmnu_close">
                                <i class="fa fa-fw fa-times"></i>
                            </a>
                            <div class="menuhover-userfoto">
                                <img src="<?php echo base_url('assets/icon/avatar.png'); ?>" alt="">
                            </div>
                            <div class="menuhover-userinfo">
                                <h4>Mulya Hari Vano</h4>
                                <span>
                                    <a href="#">Sign In</a>
                                </span>
                                <span>
                                    <a href="#">Sign Up</a>
                                </span>
                            </div>
                        </div>
                        <ul>
                            <li><a id="showsecond" data-target="menu_1">Test AAA <i class="fa fa-fw fa-angle-right"></i></a></li>
                            <li><a id="" data-target="menu_2">Test BBB <i class="fa fa-fw fa-angle-right"></i></a></li>
                            <li><a id="" data-target="menu_3">Test CCC <i class="fa fa-fw fa-angle-right"></i></a></li>
                            <li><a id="" data-target="menu_4">Test DDD <i class="fa fa-fw fa-angle-right"></i></a></li>
                        </ul>
                    </div>

                    <div class="dropdown-2nd" id="show_menu_resp_2nd" style="display: none;">
                        <div class="dropdown-back"><a><i class="fa fa-fw fa-angle-left"></i> Back</a></div>
                        <ul id="menu_1">
                            <li><a href="#">2and Menu AAA <i class="fa fa-fw fa-angle-right"></i></a></li>
                            <li><a href="#">2and Menu AAA <i class="fa fa-fw fa-angle-right"></i></a></li>
                            <li><a href="#">2and Menu AAA <i class="fa fa-fw fa-angle-right"></i></a></li>
                            <li><a href="#">2and Menu AAA <i class="fa fa-fw fa-angle-right"></i></a></li>
                        </ul>
                        <ul id="menu_2">
                            <li><a href="#">2and Menu BBB <i class="fa fa-fw fa-angle-right"></i></a></li>
                            <li><a href="#">2and Menu BBB <i class="fa fa-fw fa-angle-right"></i></a></li>
                            <li><a href="#">2and Menu BBB <i class="fa fa-fw fa-angle-right"></i></a></li>
                            <li><a href="#">2and Menu BBB <i class="fa fa-fw fa-angle-right"></i></a></li>
                        </ul>
                        <ul id="menu_3">
                            <li><a href="#">2and Menu CCC <i class="fa fa-fw fa-angle-right"></i></a></li>
                            <li><a href="#">2and Menu CCC <i class="fa fa-fw fa-angle-right"></i></a></li>
                            <li><a href="#">2and Menu CCC <i class="fa fa-fw fa-angle-right"></i></a></li>
                            <li><a href="#">2and Menu CCC <i class="fa fa-fw fa-angle-right"></i></a></li>
                        </ul>
                        <ul id="menu_4">
                            <li><a href="#">2and Menu DDD <i class="fa fa-fw fa-angle-right"></i></a></li>
                            <li><a href="#">2and Menu DDD <i class="fa fa-fw fa-angle-right"></i></a></li>
                            <li><a href="#">2and Menu DDD <i class="fa fa-fw fa-angle-right"></i></a></li>
                            <li><a href="#">2and Menu DDD <i class="fa fa-fw fa-angle-right"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="header-2nd">
        <div class="container section-menu">
            <div class="col-sm-4">
                <div class="my-top-box text-center">
                    <a class="text-red" href="<?php echo base_url(); ?>">
                        <img src="<?php echo base_url('assets/icon/kuliner_on.png'); ?>"><br />
                        Kuliner
                    </a>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="my-top-box text-center">
                    <a class="text-grey"  href="<?php echo base_url('wisata'); ?>">
                        <img src="<?php echo base_url('assets/icon/wisata_off.png'); ?>"><br />
                        Wisata
                    </a>
                </div>
            </div>
            <div class="col-sm-4 pull-right">
                <div class="my-top-box text-center my-top-wilayah show_daerah">
                    <p>Kota Saat Ini / Ubah Kota</p>
                    <span class="provpilihan"></span><span class="kabpilihan"></span>
                </div>
            </div>
            
        </div>
    </div>

</header>
<?php $this->load->view(MASTER_TEMA . '/form/pilih_daerah'); ?>
<?php $this->load->view(MASTER_TEMA . '/form/modal_filter1'); ?>
<?php $this->load->view(MASTER_TEMA . '/form/modal_filter2'); ?>
<?php $this->load->view(MASTER_TEMA . '/form/modal_filter_masakan'); ?>
