<footer>
            <div class="container">
                <div class="style-footer">
                    <div class="row">
                        <div class="media">
                            <div class="col-md-2"><div class="logo"><img src="<?php echo base_url('assets/icon/logo.png') ?>"></div></div>
                            <div class="col-md-10">
                                <div class="social">
                                    <a><i class="fa fa-fw fa-facebook"></i></a>
                                    <a><i class="fa fa-fw fa-twitter"></i></a>
                                    <a><i class="fa fa-fw fa-instagram"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12"><hr></div>

                        <div class="content">
                            <div class="col-md-4" >
                                <h2 class="title">About US</h2>
                                <p>Home </p>
                                <p>About Us</p>
                                <p>FAQ </p>
                            </div>
                            <div class="col-md-4">
                                <h2 class="title">Aplication </h2>
                                <p>Android </p>
                                <p>IOS </p>
                            </div>
                            <div class="col-md-4">
                                <h2 class="title">Join </h2>
                                <p>Advertiser </p>
                                <p>Tenant </p>
                                <p>Team Work</p>
                                <p>Tripper </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>