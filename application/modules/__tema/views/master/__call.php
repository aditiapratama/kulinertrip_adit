<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="utf-8" />
    <title><?php echo (!empty($title) ? $title : 'Hallo') ?></title>
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script>var domain = "<?php echo base_url();?>";</script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">

    <!--<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />-->

    <link rel="stylesheet" href="<?php echo base_url('assets/js/jquery-ui/jquery-ui.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap-submenu.min.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/fonts/awesome/4.7.0/css/font-awesome.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/fonts/material-icons.css');?>">

    <link rel="stylesheet" href="<?php echo base_url('assets/css/kuliner.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/kuliner.media.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/lg.css') ?>">

    <link rel="stylesheet" href="<?php echo base_url('assets/css/salim/slider.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/salim/general.css') ?>">

    <link rel="stylesheet" href="<?php echo base_url('assets/css/menu-wisata.css') ?>">

    <link rel="stylesheet" href="<?php echo base_url('assets/css/imam/module-2.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/imam/module-2-responsive.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/imam/ulasan.css') ?>">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/js/lg/dist/css/lightgallery.min.css')?>" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/js/lg/dist/css/lg-fb-comment-box.min.css')?>" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/slick/slick.css')?>" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/slick/slick-theme.css')?>" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tag-editor/1.0.20/jquery.tag-editor.min.css" />

    <!--[if !IE]> --><script src="<?php echo base_url('assets/js/jquery-2.1.4.min.js'); ?>"></script><!-- <![endif]-->
    <!--[if IE]><script src="<?php echo base_url('assets/js/jquery-1.11.3.min.js'); ?>"></script><![endif]-->
    <!--[if lt IE 9]>
    <!--<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>-->
    <!--<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>-->
    <!--[endif]-->
    <script src="<?php echo base_url('assets/js/jquery-ui/jquery-ui.js'); ?>"></script>
    <!--<script src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>
    <script src="<?php echo base_url('assets/js/jssor.slider-27.1.0.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/js/lg/dist/js/lightgallery.min.js') ?>" type="text/javascript"></script>
    
    <script src="<?php echo base_url('assets/js/lg/modules/lg-thumbnail.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/js/lg/modules/lg-fullscreen.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/js/lg/modules/lg-zoom.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/js/lg/modules/hash.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/slick/slick.js') ?>" type="text/javascript"></script>
    
    <!--LigthGallerry -->
    
    
</head>

<body>

<div class="kuliner-modal"></div>
<?php $this->load->view('__header', get_header_data()) ?>
<?php $this->load->view('__content') ?>
<?php $this->load->view('__footer') ?>

<script src="<?php echo base_url('assets/js/jquery.redirect.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/KulinerTrip.SDK.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.matchHeight.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/menu-wisata.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/wisata.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/wisata_landing.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/kuliner.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/salim/price.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/salim/bootstrap-slider.min.js') ?>" type="text/javascript"></script>
<!--<script src="--><?php //echo base_url('assets/js/salim/general.js') ?><!--" type="text/javascript"></script>-->
<script src="<?php echo base_url('assets/js/salim/filter.js') ?>" type="text/javascript"></script>
</body>
</html>
