<?php
    if(!empty($call_page)){
        $_dir = explode('/', $call_page);
        $count = count($_dir);

        switch ($count) {
            case 5 : {
                if (is_file(APPPATH . 'modules/' . $_dir[0] . '/views/' . $_dir[1] . '/' . $_dir[2]  . '/' . $_dir[3]  . '/' . $_dir[4] . EXT)){
                    return $this->load->view($call_page);
                }
            }
            case 4 : {
                if (is_file(APPPATH . 'modules/' . $_dir[0] . '/views/' . $_dir[1] . '/' . $_dir[2]  . '/' . $_dir[3] . EXT)){
                    return $this->load->view($call_page);
                }
            }
            case 3 : {
                if (is_file(APPPATH . 'modules/' . $_dir[0] . '/views/' . $_dir[1] . '/' . $_dir[2] . EXT)){
                    return $this->load->view($call_page);
                }
            }
            case 2 : {
                if (is_file(APPPATH . 'modules/' . $_dir[0] . '/views/' . $_dir[1] . EXT)){
                    return $this->load->view($call_page);
                }
            }
            case 1 || 0 : {
                if (is_file(APPPATH . 'modules/' . $_dir[0] . '/views/' . $_dir[1] . EXT)){
                    return $this->load->view($call_page);
                }
            }
            default : {
                return $this->load->view('errors/html/error_empty');
            }
        }
    } else {
        $this->load->view('errors/html/error_empty');
    }
