<div class="content-foot">
    <div class="row">
        <div class="col-sm-12">
            <h4>Tema </h4>
            <h5 class="des">Jelajahi Restourant, Cafe dan tempat makan lainnya berdasarkan tema</h5>
        </div>
        <div class="row">
            <div class="tema-frame">
                <div class="col-sm-6 col-set-2">
                    <div class="tema">
                        <div class="image">
                            <img src="<?php echo base_url('assets/img/6.JPG') ?>" alt="">
                        </div>
                        <div class="deskripsion">
                            <h6 class="title">Popular restaurant minggu ini</h6>
                            <p class="des-tema-bawahjudul">D.I Yogyakarta</p>
                            <p class="des-tema">Temukan tempat popular minggu ini yang menyuguhkan pengalaman tak terlupakan</p>
                            <div class="report">
                                <img data-u="image" src="<?php echo base_url('assets/icon/iconsimpan.png') ?>" width="14px" data-toggle="tooltip" title="Tersimpan" data-placement="top"/>    <span class="text-ketreport">5</span>
                                <img data-u="image" src="<?php echo base_url('assets/icon/iconsuka.png') ?>" width="14px" data-toggle="tooltip" title="Suka" data-placement="top"/>    <span class="text-ketreport">6</span>
                                <img data-u="image" src="<?php echo base_url('assets/icon/iconkomentar.png') ?>" width="14px" data-toggle="tooltip" title="Komentar" data-placement="top"/>    <span class="text-ketreport">7</span>
                            </div>
                           <p class="des-tema-tempat">30 Tempat</p>
                        </div>
                        <div class="my-clear"></div>
                    </div>
                </div>
                <div class="col-sm-6 col-set-2">
                    <div class="tema">
                        <div class="image">
                            <img src="<?php echo base_url('assets/img/6.JPG') ?>" alt="">
                        </div>
                        <div class="deskripsion">
                            <h6 class="title">Popular restaurant minggu ini</h6>
                            <p class="des-tema-bawahjudul">D.I Yogyakarta</p>
                            <p class="des-tema">Temukan tempat popular minggu ini yang menyuguhkan pengalaman tak terlupakan</p>
                            <div class="report">
                                <img data-u="image" src="<?php echo base_url('assets/icon/iconsimpan.png') ?>" width="14px" data-toggle="tooltip" title="Tersimpan" data-placement="top"/>    <span class="text-ketreport">5</span>
                                <img data-u="image" src="<?php echo base_url('assets/icon/iconsuka.png') ?>" width="14px" data-toggle="tooltip" title="Suka" data-placement="top"/>    <span class="text-ketreport">6</span>
                                <img data-u="image" src="<?php echo base_url('assets/icon/iconkomentar.png') ?>" width="14px" data-toggle="tooltip" title="Komentar" data-placement="top"/>    <span class="text-ketreport">7</span>
                            </div>
                           <p class="des-tema-tempat">30 Tempat</p>
                        </div>
                        <div class="my-clear"></div>
                    </div>
                </div>
                <div class="col-sm-6 col-set-2">
                    <div class="tema">
                        <div class="image">
                            <img src="<?php echo base_url('assets/img/6.JPG') ?>" alt="">
                        </div>
                        <div class="deskripsion">
                            <h6 class="title">Popular restaurant minggu ini</h6>
                            <p class="des-tema-bawahjudul">D.I Yogyakarta</p>
                            <p class="des-tema">Temukan tempat popular minggu ini yang menyuguhkan pengalaman tak terlupakan</p>
                            <div class="report">
                                <img data-u="image" src="<?php echo base_url('assets/icon/iconsimpan.png') ?>" width="14px" data-toggle="tooltip" title="Tersimpan" data-placement="top"/>    <span class="text-ketreport">5</span>
                                <img data-u="image" src="<?php echo base_url('assets/icon/iconsuka.png') ?>" width="14px" data-toggle="tooltip" title="Suka" data-placement="top"/>    <span class="text-ketreport">6</span>
                                <img data-u="image" src="<?php echo base_url('assets/icon/iconkomentar.png') ?>" width="14px" data-toggle="tooltip" title="Komentar" data-placement="top"/>    <span class="text-ketreport">7</span>
                            </div>
                           <p class="des-tema-tempat">30 Tempat</p>
                        </div>
                        <div class="my-clear"></div>
                    </div>
                </div>

                <div class="col-sm-6 col-set-2">
                    <div class="tema">
                        <div class="image">
                            <img src="<?php echo base_url('assets/img/6.JPG') ?>" alt="">
                        </div>
                        <div class="deskripsion">
                            <h6 class="title">Popular restaurant minggu ini</h6>
                            <p class="des-tema-bawahjudul">D.I Yogyakarta</p>
                            <p class="des-tema">Temukan tempat popular minggu ini yang menyuguhkan pengalaman tak terlupakan</p>
                            <div class="report">
                                <img data-u="image" src="<?php echo base_url('assets/icon/iconsimpan.png') ?>" width="14px" data-toggle="tooltip" title="Tersimpan" data-placement="top"/>    <span class="text-ketreport">5</span>
                                <img data-u="image" src="<?php echo base_url('assets/icon/iconsuka.png') ?>" width="14px" data-toggle="tooltip" title="Suka" data-placement="top"/>    <span class="text-ketreport">6</span>
                                <img data-u="image" src="<?php echo base_url('assets/icon/iconkomentar.png') ?>" width="14px" data-toggle="tooltip" title="Komentar" data-placement="top"/>    <span class="text-ketreport">7</span>
                            </div>
                            <p class="des-tema-tempat">30 Tempat</p>
                        </div>
                        <div class="my-clear"></div>
                    </div>
                </div>
                <div class="col-sm-6 col-set-2">
                    <div class="tema">
                        <div class="image">
                            <img src="<?php echo base_url('assets/img/6.JPG') ?>" alt="">
                        </div>
                        <div class="deskripsion">
                            <h6 class="title">Popular restaurant minggu ini</h6>
                            <p class="des-tema-bawahjudul">D.I Yogyakarta</p>
                            <p class="des-tema">Temukan tempat popular minggu ini yang menyuguhkan pengalaman tak terlupakan</p>
                            <div class="report">
                                <img data-u="image" src="<?php echo base_url('assets/icon/iconsimpan.png') ?>" width="14px" data-toggle="tooltip" title="Tersimpan" data-placement="top"/>    <span class="text-ketreport">5</span>
                                <img data-u="image" src="<?php echo base_url('assets/icon/iconsuka.png') ?>" width="14px" data-toggle="tooltip" title="Suka" data-placement="top"/>    <span class="text-ketreport">6</span>
                                <img data-u="image" src="<?php echo base_url('assets/icon/iconkomentar.png') ?>" width="14px" data-toggle="tooltip" title="Komentar" data-placement="top"/>    <span class="text-ketreport">7</span>
                            </div>
                           <p class="des-tema-tempat">30 Tempat</p>
                        </div>
                        <div class="my-clear"></div>
                    </div>
                </div>
                <div class="col-sm-6 col-set-2">
                    <div class="tema">
                        <div class="image">
                            <img src="<?php echo base_url('assets/img/6.JPG') ?>" alt="">
                        </div>
                        <div class="deskripsion">
                            <h6 class="title">Popular restaurant minggu ini</h6>
                            <p class="des-tema-bawahjudul">D.I Yogyakarta</p>
                            <p class="des-tema">Temukan tempat popular minggu ini yang menyuguhkan pengalaman tak terlupakan</p>
                            <div class="report">
                                <img data-u="image" src="<?php echo base_url('assets/icon/iconsimpan.png') ?>" width="14px" data-toggle="tooltip" title="Tersimpan" data-placement="top"/>    <span class="text-ketreport">5</span>
                                <img data-u="image" src="<?php echo base_url('assets/icon/iconsuka.png') ?>" width="14px" data-toggle="tooltip" title="Suka" data-placement="top"/>    <span class="text-ketreport">6</span>
                                <img data-u="image" src="<?php echo base_url('assets/icon/iconkomentar.png') ?>" width="14px" data-toggle="tooltip" title="Komentar" data-placement="top"/>    <span class="text-ketreport">7</span>
                            </div>
                            <p class="des-tema-tempat">30 Tempat</p>
                        </div>
                        <div class="my-clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>