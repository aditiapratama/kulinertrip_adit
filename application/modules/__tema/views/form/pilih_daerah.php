<div class="modal right fade" id="modal-daerah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">

				<div class="modal-header main-header">
					<!-- <div class="modal-close" target="modal-daerah" style="cursor:pointer;"><span aria-hidden="true">&times;</span></div> -->
					<div class="m-header">
						<div class="logo_modal_daerah" style="width:50px;">
							<img class="img-responsive" style="vertical-align: middle;" width="10px" src="<?php echo base_url('assets/img/filter Iconk.png');?>">
						</div>
						<div class="logotext" style="top: -15px;position: absolute;">
							<h2>Select </h2>
						</div>

					</div>
					<div class="boxdaerah">
						<div style="margin: 0 auto;">
							<div class="btn-group input-provinsi hide" style="">
								<input id="searchprovinsi" readonly type="search" class="form-control">
								<span id="provinsiclear" class="glyphicon glyphicon-remove-circle"></span>
							</div>
							<div class="btn-group input-kabupaten hide" style="">
								<input id="searchkabupaten" readonly type="search" class="form-control">
								<span id="kabupatenclear" class="glyphicon glyphicon-remove-circle"></span>
							</div>
						</div>
					</div>
					<div class="modal-close" target="modal-daerah" style="cursor:pointer;position:absolute;right: 20px;position: absolute;"><span aria-hidden="true">&times;</span></div>
				</div>

				<div class="modal-body">
					<div>
						<h4>Provinsi</h4>
						<div id="t">
							<div id="main-provinsi" style="text-align:center;width:100%;">
							</div>
						</div>
					</div>

					<div style="margin:10px;position:relative;">
						<hr class="" style="width:100%">
					</div>
					<div class="fullwidth">
						<h4>Kabupaten</h4>
						<div id="main-kabupaten" style="margin: 0 auto;">
						</div>
					</div>
					<div class="fullwidth" style="margin:10px;">
						<hr class="" style="width:100% !important;">
					</div>
					<div style="height:30px;text-align:center;" valign="middle">

					</div>
					<div class="fullwidth" style="margin:10px;">
						<hr class="" style="width:100% !important;">
					</div>



				</div>
				<div class="modal-footer" style="text-align:center;background-color:#efefef;">
                                    <span style="height:30px;" valign="middle">Atur filter pencarian anda, atau anda dapat mengaturnya nanti</span>
						<div style="width:30px;display:inline-block;position:absolute;vertical-align:middle;"><img class="img-responsive" style="vertical-align: middle;" width="10px" src="<?php echo base_url('assets/img/filter Iconk.png');?>"></div>
                                                <br/>
	        <button class="btn btn-success terapkan_daerah">Terapkan</button>
	      </div>

			</div>
		</div> <!-- modal-dialog -->
	</div> <!-- modal -->
