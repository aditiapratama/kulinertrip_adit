<div class="modal right fade" id="modal_filter1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">

				<div class="modal-header main-header">
					<div class="modal-close closem" target="modal_filter1" style="cursor:pointer;"><span aria-hidden="true">&times;</span></div>
					<div style="display:inline-block;">
						<div class="container logotext" style="position:relative;left:0px;">
							<h2>Filter</h2>
						</div>
						<div class="" style="padding-left:12px;padding-right:12px;">
							<div id="filter_checked"></div><div class="dellall hide"><a href="javascript:void(0)" class="hapusselected1">Hapus semua</a></div>
						</div>
					</div>

				</div>

				<div class="modal-body">
					<div class="container filter">
						<div class="col-md-3">
							<h3>Kategori</h3>
							<div id="kategori">

							</div>
						</div>
						<div class="col-md-3">
							<h3>Masakan</h3>
							<div id="masakan">

							</div>
							<!-- <div id="masakan_selanjutnya" style="width:100%;"><a href="javascript:void(0)" class="show_masakan filter-a-next">selanjutnya <i class="material-icons carret_next">arrow_drop_down</i></a></div> -->

						</div>
						<div class="col-md-3">
							<h3>Tujuan</h3>
							<div id="tujuan">
							</div>
						</div>
						<div class="col-md-3">
							<h3>Waktu</h3>
							<div id="waktu">
							</div>
						</div>
					</div>
					<div style="text-align:center;" class="filter">
						<hr style="border-top:3px solid #e5e5e5;">
						<h4>Filter Lanjutan</h4>
					</div>
					<div class="container filter">
						<div class="col-md-3">
							<h3>Fasilitas</h3>
							<div id="fasilitas">
							</div>
						</div>
						<div class="col-md-3">
							<h3>Cita Rasa</h3>
							<div id="citarasa">
							</div>
						</div>
						<div class="col-md-3">
							<h3>Cara Masak</h3>
							<div id="caramasak">
							</div>
						</div>
						<div class="col-md-3">
							<h3>Harga</h3>
							<div id="harga" class="col-md-12" style="padding:0px;">
								<div class="col-sm-12" style="padding:0px;"><input id="ex12c" type="text" style="padding:0px;"/></div>
								<div class="" style="padding-top:30px;">
								<div class="col-sm-4 hg-from"  style="padding:0px;">
									<div class="form-group">
									  <input id="harga_start" type="text" class="form-control from price hide" name="from" value="0">
										<span class="price harga_start hrg hrga"></span>
									</div>
								</div>
								<div class="col-sm-2" style="padding:0px;text-align:center;padding:5px;">
									<span>TO</span>
								</div>
								<div class="col-sm-4 hg-from" style="padding:0px;">
									<div class="form-group">
									  <input type="text" id="harga_end" class="form-control to price hide" name="to" value="300000">
										<span class="price harga_end hrg hrga"></span>
									</div>
								</div>
								</div>
							</div>
							<h3>Halal / Non Halal</h3>
							<div id="halal">
							</div>
						</div>
					</div>
					<div class="alertmasakan3 hide" style="">
						<div class="">
							<i class="material-icons carret_next">report_problem</i>
						</div><div class="" style="padding-left:30px;">
							Batas maksimum 3 pilihan
						</div>
					</div>
				</div>
				<div class="modal-footer" style="text-align:center;background-color:#efefef;">
	        <button class="btn btn-success terapkan_filter">Terapkan</button>
	      </div>

			</div>
		</div> <!-- modal-dialog -->
	</div> <!-- modal -->
