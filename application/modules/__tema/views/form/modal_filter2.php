<div class="modal right fade" id="modal_filter2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-back"></div>
		<div class="modal-dialog" role="document">
			<div class="modal-content container" style="padding-left:0px;padding-right:0px;">
				<!--  -->
				<div class="modal-header main-header">

						<div style="text-align:;display:;position:relative;">
							<div class="logotext">
								<h2 class="kategori2 non-fasilitas2 non-tujuan2 non-citarasa2 non-caramasak2 hide">Kategori</h2>
								<h2 class="fasilitas2 non-kategori2 non-tujuan2 non-citarasa2 non-caramasak2 hide">Fasilitas</h2>
								<h2 class="tujuan2 non-kategori2 non-fasilitas2 non-citarasa2 non-caramasak2 hide">Tujuan</h2>
								<h2 class="citarasa2 non-kategori2 non-fasilitas2 non-tujuan2 non-caramasak2 hide">Cita rasa</h2>
								<h2 class="caramasak2 non-kategori2 non-fasilitas2 non-tujuan2 non-citarasa2 hide">Cara masak</h2>
							</div>

						</div>
						<div class="modal-close" target="modal_filter2" style="cursor:pointer;position:absolute;right: 20px;top: 35px;"><span aria-hidden="true">&times;</span></div>
						<div>
							<div id="selected22" style="position:relative;display:inline-block;">

							</div>
							<div class="dellall-kategori"><a href="javascript:void(0)" class="dellall-kategori kategoridell non-fasilitasdell non-tujuandell non-citarasadell non-caramasakdell hide">Hapus semua</a></div>
							<div class="dellall_tujuan"><a href="javascript:void(0)" class="dellall_tujuan tujuandell non-kategoridell non-fasilitasdell non-citarasadell non-caramasakdell hide">Hapus semua</a></div>
							<div class="dellall-fasilitas"><a href="javascript:void(0)" class="dellall-fasilitas fasilitasdell non-kategoridell non-tujuandell non-citarasadell non-caramasakdell hide">Hapus semua</a></div>
							<div class="dellall-citarasa"><a href="javascript:void(0)" class="dellall-citarasa citarasadell non-kategoridell non-fasilitasdell non-tujuandell non-caramasakdell hide">Hapus semua</a></div>
							<div class="dellall-caramasak"><a href="javascript:void(0)" class="dellall-caramasak caramasakdell non-kategoridell non-fasilitasdell non-tujuandell non-citarasadell hide">Hapus semua</a></div>
						</div>
				</div>
				<div class="modal-body">
							<div id="kategori2" class=" filter kategori2 non-fasilitas2 non-tujuan2 non-citarasa2 non-caramasak2 hide p12">
								<div class="col-md-12">
									<div class="wrap">
									  <div class="mat-div">
									    <label for="first-name" class="mat-label"></label>
									    <input type="text" class="mat-input search-kategori" placeholder="Ketik untuk menyaring" id="first-name">
											<i class="fa fa-fw fa-search fa-2x" style="position:absolute;top: 37px;"></i>
											<i class="material-icons del-filter pointer" target="kategori" style="position:absolute;top: 42px;right:0px;">clear</i>
									  </div>
									</div>
								</div>
								<div id="kategori2-konten">
								</div>

							</div>
							<div id="tujuan2" class="filter tujuan2 non-kategori2 non-fasilitas2 non-citarasa2 non-caramasak2 hide p12">
								<div class="col-md-12">
									<div class="wrap">
									  <div class="mat-div">
									    <label for="first-name" class="mat-label"></label>
									    <input type="text" class="mat-input search-tujuan" id="first-name">
											<i class="fa fa-fw fa-search fa-2x" style="position:absolute;top: 37px;"></i>
											<i class="material-icons del-filter pointer" target="tujuan" style="position:absolute;top: 42px;right:0px;">clear</i>
									  </div>
									</div>
								</div>
								<div id="tujuan2-konten">
								</div>
							</div>

							<div id="fasilitas2" class=" filter fasilitas2 non-kategori2 non-tujuan2 non-citarasa2 non-caramasak2 hide p12">
								<div class="col-md-12">
									<div class="wrap">
									  <div class="mat-div">
									    <label for="first-name" class="mat-label"></label>
									    <input type="text" class="mat-input search-fasilitas" id="first-name">
											<i class="fa fa-fw fa-search fa-2x" style="position:absolute;top: 37px;"></i>
											<i class="material-icons del-filter pointer" target="fasilitas" style="position:absolute;top: 42px;right:0px;">clear</i>
									  </div>
									</div>
								</div>
								<div id="fasilitas2-konten">
								</div>
							</div>
							<div id="citarasa2" class="filter citarasa2 non-kategori2 non-fasilitas2 non-tujuan2 non-caramasak2 hide p12">
								<div class="col-md-12">
									<div class="wrap">
									  <div class="mat-div">
									    <label for="first-name" class="mat-label"></label>
									    <input type="text" class="mat-input search-citarasa" id="first-name">
											<i class="fa fa-fw fa-search fa-2x" style="position:absolute;top: 37px;"></i>
											<i class="material-icons del-filter pointer" target="citarasa" style="position:absolute;top: 42px;right:0px;">clear</i>
									  </div>
									</div>
								</div>
								<div id="citarasa2-konten">
								</div>
							</div>
							<div id="caramasak2" class="filter caramasak2 non-kategori2 non-fasilitas2 non-tujuan2 non-citarasa2 hide p12">
								<div class="col-md-12">
									<div class="wrap">
									  <div class="mat-div">
									    <label for="first-name" class="mat-label"></label>
									    <input type="text" class="mat-input search-caramasak" id="first-name">
											<i class="fa fa-fw fa-search fa-2x" style="position:absolute;top: 37px;"></i>
											<i class="material-icons del-filter pointer" target="caramasak" style="position:absolute;top: 42px;right:0px;">clear</i>
									  </div>
									</div>
								</div>
								<div id="caramasak2-konten">
								</div>
							</div>
							<div class="alertmasakan3 hide" style="">
								<div class="">
									<i class="material-icons carret_next">report_problem</i>
								</div><div class="" style="padding-left:30px;">
									Batas maksimum 3 pilihan
								</div>
							</div>

				</div>
				<div class="modal-footer" style="text-align:center;background-color:#efefef;">
	        <button class="btn terapkan-kategori2 btn-success kategori2 non-fasilitas2 non-tujuan2 non-citarasa2 non-caramasak2 hide">Terapkan</button>
					<button class="btn terapkan-tujuan2 btn-success tujuan2 non-kategori2 non-fasilitas2 non-citarasa2 non-caramasak2 hide">Terapkan</button>
					<button class="btn terapkan-fasilitas2 btn-success fasilitas2 non-kategori2 non-tujuan2 non-citarasa2 non-caramasak2 hide">Terapkan</button>
					<button class="btn terapkan-citarasa2 btn-success citarasa2 non-kategori2 non-fasilitas2 non-tujuan2 non-caramasak2 hide">Terapkan</button>
					<button class="btn terapkan-caramasak2 btn-success caramasak2 non-kategori2 non-fasilitas2 non-tujuan2 non-citarasa2 hide">Terapkan</button>
	      </div>

			</div>
		</div> <!-- modal-dialog -->
	</div> <!-- modal -->
