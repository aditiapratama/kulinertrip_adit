<div class="modal right fade" id="modal_filter_masakan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-back"></div>
		<div class="modal-dialog" role="document">
			<div class="modal-content container" style="padding-left:0px;padding-right:0px;">

				<div class="modal-header main-header">
					<div class="modal-close" target="modal_filter_masakan" style="cursor:pointer;top: 30px;right: 30px;position: absolute;"><span aria-hidden="true">&times;</span></div>
						<div class="logotext" style="">
							<h2>Masakan</h2>
						</div>
						<div>
							<div id="pilihanmasakan2" style="position:relative;"></div>
							<div class="dellall-masakan"><a href="javascript:void(0)" class="dellall-masakan hide">Hapus semua</a></div>
						</div>
				</div>

				<div class="modal-body">
							<div id="masakan2" class="masakan2 filter">
								<div id="masakan2-konten" class="p12" style="padding-top:20px;margin-top:20px;">
                  <br><br>

								</div>
							</div>
							<div class="alertmasakan3 hide" style="">
								<div class="">
									<i class="material-icons carret_next">report_problem</i>
								</div><div class="" style="padding-left:30px;">
									Batas maksimum 3 pilihan
								</div>
							</div>
				</div>
				<div class="modal-footer" style="text-align:center;background-color:#efefef;">
	        <button class="btn btn-success terapkan-masakan">Terapkan</button>
	      </div>

			</div>
		</div> <!-- modal-dialog -->
	</div> <!-- modal -->
