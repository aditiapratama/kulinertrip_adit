<div class="bg-white kategori" id="kategori">
    <div class="row">
        <h5 class="punya">Di Indonesia Kami Mempunyai :</h5>

        <?php
            $footkategoriArr = json_decode($footkategori,true);
            $fasilitasx = $footkategoriArr[0]['jsonFasilitasTenant'];
            $fasilitas = json_decode($fasilitasx,true);
            if(!empty($fasilitas)){ ?>

            <div class="col-sm-3">
                <h4 class="title">Fasilitas </h4>
                <ul>
                    <?php
                        $i = 1;
                        foreach ($fasilitas as $key) {
                            if($i < 7){ ?>

                    <li>
                        <a target="_blank" href="<?php echo base_url() . "Ctrfilter/filterbykategori/fasilitas/" . $key['idx'] ?>">
                            <?php echo $key['jenisfasilitas'] ?>&nbsp;&nbsp;(<?php echo $key['jumlah'] ?>)
                        </a>
                    </li>

                    <?php } else { ?>

                    <li id="list-fasilitas" style="display: none">
                        <a target="_blank" href="<?php echo base_url() . "Ctrfilter/filterbykategori/fasilitas/" . $key['idx'] ?>">
                            <?php echo $key['jenisfasilitas'] ?>&nbsp;&nbsp;(<?php echo $key['jumlah'] ?>)
                        </a>
                    </li>

                    <?php } $i++; } ?>

                </ul>
                <a class="more" id="landing-kat" data-d="fasilitas">Lihat Selengkapnya</a>
            </div>      <?php } ?>

            <?php
            $jsonKategoriTenant = $footkategoriArr[0]['jsonKategoriTenant'];
            $kategori = json_decode($jsonKategoriTenant,true);
            if(!empty($kategori)){ ?>

            <div class="col-sm-3">
                <h4 class="title">Kategori </h4>
                <ul>
                    <?php $i = 1; foreach ($kategori as $key) { if($i < 7){ ?>

                    <li>
                        <a target="_blank" href="<?php echo base_url() . "Ctrfilter/filterbykategori/kategori/" . $key['idx'] ?>">
                            <?php echo $key['Kategori'] ?>&nbsp;&nbsp;(<?php echo $key['jumlah'] ?>)
                        </a>
                    </li>

                    <?php } else { ?>

                    <li id="list-kategori" style="display: none">
                        <a target="_blank" href="<?php echo base_url() . "Ctrfilter/filterbykategori/kategori/" . $key['idx'] ?>">
                            <?php echo $key['Kategori'] ?>&nbsp;&nbsp;(<?php echo $key['jumlah'] ?>)
                        </a>
                    </li>

                    <?php } $i++;} ?>

                </ul>
                <a class="more" id="landing-kat" data-d="kategori">Lihat Selengkapnya</a>
            </div>      <?php } ?>


            <?php
            $jsonJenisMasakan = $footkategoriArr[0]['jsonJenisMasakan'];
            $masakan = json_decode($jsonJenisMasakan,true);
            if(!empty($masakan)){ ?>

            <div class="col-sm-3">
                <h4 class="title">Masakan </h4>
                <ul>
                    <?php $i = 1; foreach ($masakan as $key) { if($i < 7){ ?>

                    <li>
                        <a target="_blank" href="<?php echo base_url() . "Ctrfilter/filterbykategori/masakan/" . $key['idx'] ?>">
                            <?php echo $key['jenismakanan'] ?>&nbsp;&nbsp;(<?php echo $key['jumlah'] ?>)
                        </a>
                    </li>

                    <?php } else { ?>

                    <li id="list-masakan" style="display: none">
                        <a target="_blank" href="<?php echo base_url() . "Ctrfilter/filterbykategori/masakan/" . $key['idx'] ?>">
                            <?php echo $key['jenismakanan'] ?>&nbsp;&nbsp;(<?php echo $key['jumlah'] ?>)
                        </a>
                    </li>

                    <?php } $i++;} ?>

                </ul>
                <a class="more" id="landing-kat" data-d="masakan">Lihat Selengkapnya</a>
            </div>      <?php } ?>


            <?php
            $jsonJenisTujuan = $footkategoriArr[0]['jsonJenisTujuan'];
            $tujuan = json_decode($jsonJenisTujuan,true);
            if(!empty($tujuan)){ ?>

            <div class="col-sm-3">
            <h4 class="title">Tujuan </h4>
                <ul><?php $i = 1; foreach ($tujuan as $key) { if($i < 7){ ?>

                    <li>
                        <a target="_blank" href="<?php echo base_url() . "Ctrfilter/filterbykategori/tujuan/" . $key['idx'] ?>">
                            <?php echo $key['jenistujuanresto'] ?>&nbsp;&nbsp;(<?php echo $key['jumlah'] ?>)
                        </a>
                    </li>

                    <?php } else { ?>

                        <li id="list-tujuan" style="display: none">
                        <a target="_blank" href="<?php echo base_url() . "Ctrfilter/filterbykategori/tujuan/" . $key['idx'] ?>">
                            <?php echo $key['jenistujuanresto'] ?>&nbsp;&nbsp;(<?php echo $key['jumlah'] ?>)
                        </a>
                    </li>

                    <?php } $i++;} ?>

                </ul>
                <a class="more" id="landing-kat" data-d="tujuan">Lihat Selengkapnya</a>
            </div>
        </div>      <?php } ?>

</div>