<?php defined('BASEPATH') OR exit('No direct script access allowed');

class con_kategori_wisata extends MY_Core
{
    function __construct(){
        parent::__construct();
    }

    function index(){
    	$jsonKategoriWisata = '[{"jsonJenisWisataProvKab":"[{\"idx\":\"1\",\"jeniswisata\":\"Pegunungan\",\"icon\":\"http:\\\/\\\/localhost\\\/media.kulinertrip.com\\\/resource\\\/imgKulinerTrip\\\/kuliner_default.jpg\",\"jumlah\":\"1\"},{\"idx\":\"2\",\"jeniswisata\":\"Pantai\",\"icon\":\"http:\\\/\\\/localhost\\\/media.kulinertrip.com\\\/resource\\\/imgKulinerTrip\\\/kuliner_default.jpg\",\"jumlah\":\"1\"},{\"idx\":\"3\",\"jeniswisata\":\"Wahana\",\"icon\":\"http:\\\/\\\/localhost\\\/media.kulinertrip.com\\\/resource\\\/imgKulinerTrip\\\/kuliner_default.jpg\",\"jumlah\":\"4\"},{\"idx\":\"4\",\"jeniswisata\":\"Mall\",\"icon\":\"http:\\\/\\\/localhost\\\/media.kulinertrip.com\\\/resource\\\/imgKulinerTrip\\\/kuliner_default.jpg\",\"jumlah\":\"3\"},{\"idx\":\"5\",\"jeniswisata\":\"Wisata Air (Sungai; Air Terjun)\",\"icon\":\"http:\\\/\\\/localhost\\\/media.kulinertrip.com\\\/resource\\\/imgKulinerTrip\\\/kuliner_default.jpg\",\"jumlah\":\"4\"},{\"idx\":\"6\",\"jeniswisata\":\"Wisata Sejarah\",\"icon\":\"http:\\\/\\\/localhost\\\/media.kulinertrip.com\\\/resource\\\/imgKulinerTrip\\\/kuliner_default.jpg\",\"jumlah\":\"8\"},{\"idx\":\"7\",\"jeniswisata\":\"Wisata Edukasi\",\"icon\":\"http:\\\/\\\/localhost\\\/media.kulinertrip.com\\\/resource\\\/imgKulinerTrip\\\/kuliner_default.jpg\",\"jumlah\":\"14\"},{\"idx\":\"10\",\"jeniswisata\":\"Wisata Rohani\",\"icon\":\"http:\\\/\\\/localhost\\\/media.kulinertrip.com\\\/resource\\\/imgKulinerTrip\\\/kuliner_default.jpg\",\"jumlah\":\"16\"},{\"idx\":\"11\",\"jeniswisata\":\"Wisata Taman\",\"icon\":\"http:\\\/\\\/localhost\\\/media.kulinertrip.com\\\/resource\\\/imgKulinerTrip\\\/kuliner_default.jpg\",\"jumlah\":\"17\"},{\"idx\":\"12\",\"jeniswisata\":\"Wisata Buah\",\"icon\":\"http:\\\/\\\/localhost\\\/media.kulinertrip.com\\\/resource\\\/imgKulinerTrip\\\/kuliner_default.jpg\",\"jumlah\":\"16\"}]"}]';

    	$data['kategoriWisata'] = json_decode($jsonKategoriWisata);

        $page = $this->load->view('__menu', $data, true);
        echo $page;
    }

}