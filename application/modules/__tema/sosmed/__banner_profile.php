<div class="banner-box">
	<img class="banner-img" src="<?php echo $urlbackground; ?>" alt="banner" />

	<div class="profile-main-box">
		<div class="pm-top">
			<div class="pm-top-left">
				<div class="pm-photo">
					<img src="<?php echo $foto ?>" alt="profile" />
				</div>
				<a class="btn btn-primary" href=""><span class="ic-follow"></span> ikuti</a>
			</div>
			<div class="pm-top-right">
				<h3><?php echo $namauser; ?></h3>
				<h4><?php echo $kotaasal; ?></h4>
				<h5>Find your favorite and capture it !</h5>
				<ul>
					<li class="color-trophy-1"><span class="ic-trophy-1"></span><p><?php echo $grade; ?></p></li>
					<li class="color-trophy-local"><span class="ic-trophy-local"></span><p><?php echo $level; ?></p></li>
				</ul>
			</div>
		</div>
		<div class="pm-bottom">
			<ul>
				<li><a href="" class="ic-instagram"></a></li>
				<li><a href="" class="ic-twitter-red"></a></li>
				<li><a href="" class="ic-facebook-red"></a></li>
				<li><a href="" class="ic-mail"></a></li>
			</ul>
		</div>
	</div>
</div>

<div class="nav-banner">
	<ul class="banner-status-top">
		<li><span class="ic-follow"></span>Diikuti (999)</li>
		<li><span class="ic-following"></span>Mengikuti (999)</li>
		<li><span class="ic-md-review"></span>Ulasan (999)</li>
		<li><span class="ic-md-photo"></span>Foto (999)</li>
		<li><span class="ic-md-collection"></span>Koleksi (999)</li>
	</ul>
</div>

<div class="nav-banner">
	<ul class="banner-status-bottom">
		<li>
			<img src="<?php echo base_url('assets/images/Top10KoleksiTerbaik.png') ?>" alt="Top10KoleksiTerbaik" />
		</li>
		<li>
			<img src="<?php echo base_url('assets/images/Top10PengirimFoto terbaik.png') ?>" alt="Top10PengirimFoto" />
		</li>
		<li>
			<img src="<?php echo base_url('assets/images/Top10PengulasTerbaik.png') ?>" alt="Top10PengulasTerbaik" />
		</li>
		<li>
			<img src="<?php echo base_url('assets/images/top10TripperGuide.png') ?>" alt="top10TripperGuide" />
		</li>
	</ul>
</div>