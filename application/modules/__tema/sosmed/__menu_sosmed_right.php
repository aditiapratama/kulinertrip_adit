<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<div class="content-sidebar-list">
	<title>Rekomendasi Teman</title>
	<ul>

		<li>
			<div class="user-item-box">
				<div class="user-item-photo">
					<img src="images/profile2.jpg" alt="">
				</div>
				<div class="user-item-desc">
					<div class="user-item-desc-name">Sari Wulandari</div>
					<div class="user-item-desc-info">
						<ul>
							<li>999 Ulasan</li>
							<li>999 Follower</li>
						</ul>					
					</div>
				</div>
				<div class="user-item-action">
					<a href="" class="ic-follow" title=""></a>
				</div>
			</div>
		</li>

		<li>
			<div class="user-item-box">
				<div class="user-item-photo">
					<img src="images/profile2.jpg" alt="">
				</div>
				<div class="user-item-desc">
					<div class="user-item-desc-name">Sari Wulandari</div>
					<div class="user-item-desc-info">
						<ul>
							<li>999 Ulasan</li>
							<li>999 Follower</li>
						</ul>					
					</div>
				</div>
				<div class="user-item-action">
					<a href="" class="ic-friend" title=""></a>
				</div>
			</div>
		</li>

		<li>
			<div class="user-item-box">
				<div class="user-item-photo">
					<img src="images/profile2.jpg" alt="">
				</div>
				<div class="user-item-desc">
					<div class="user-item-desc-name">Sari Wulandari</div>
					<div class="user-item-desc-info">
						<ul>
							<li>999 Ulasan</li>
							<li>999 Follower</li>
						</ul>
					</div>
				</div>
				<div class="user-item-action">
					<a href="" class="ic-followers" title=""></a>
				</div>
			</div>
		</li>

		<li>
			<div class="user-item-box">
				<div class="user-item-photo">
					<img src="images/profile2.jpg" alt="">
				</div>
				<div class="user-item-desc">
					<div class="user-item-desc-name">Sari Wulandari</div>
					<div class="user-item-desc-info">
						<ul>
							<li>999 Ulasan</li>
							<li>999 Follower</li>
						</ul>
					</div>
				</div>
				<div class="user-item-action">
					<a href="" class="ic-following" title=""></a>
				</div>
			</div>
		</li>
	</ul>
</div>