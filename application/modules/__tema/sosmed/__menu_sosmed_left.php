<ul class="nav-sidebar">
	<title>Aktifitas</title>
        
        <li class="active">
            <a href="<?php echo base_url('p_checkin')?>" title="">Pernah Kesini
			<span class="badge-menu-kiri pull-right">100</span>
		</a>
	</li>
        
	<li>
		<a href="profile.php" title="">Profile
			<span class="badge-menu-kiri pull-right ">100</span>
		</a>
	</li>
	<li>
		<a href="kuliner.php" title="">Kuliner
			<span class="badge-menu-kiri pull-right">999</span>
		</a>
	</li>
	<li>
		<a href="wisata.php" title="">Wisata
			<span class="badge-menu-kiri pull-right">999</span>
		</a>
	</li>
	<li>
		<a href="maps.php" title="">Maps
			<span class="badge-menu-kiri pull-right">999</span>
		</a>
	</li>
	<li>
		<a href="collection.php" title="">Koleksi Tempat
			<span class="badge-menu-kiri pull-right">999</span>
		</a>
	</li>
	<li>
		<a href="friend-1.php" title="">Pertemanan 1
			<span class="badge-menu-kiri pull-right">999</span>
		</a>
	</li>
	<li>
		<a href="friend-2.php" title="">Pertemanan 2
			<span class="badge-menu-kiri pull-right">999</span>
		</a>
	</li>
</ul>

<ul class="nav-sidebar">
	<title>Koleksi</title>
	<li>
		<a href="tripper-guide-3.php" title="">Koleksi Tripper Guide umum
			<span class="badge-menu-kiri pull-right">999</span>
		</a>
	</li>
	<li>
		<a href="tripper-guide-1.php" title="">Koleksi Tripper Guide 1
			<span class="badge-menu-kiri pull-right">999</span>
		</a>
	</li>
	<li>
		<a href="tripper-guide-2.php" title="">Koleksi Tripper Guide 2
			<span class="badge-menu-kiri pull-right">999</span>
		</a>
	</li>
	<li>
		<a href="tema.php" title="">Koleksi Tema
			<span class="badge-menu-kiri pull-right">999</span>
		</a>
	</li>
</ul>

<ul class="nav-sidebar">
	<title>Pencapaian</title>
	<li>
		<a href="dialog.php" title="">dialog
			<span class="badge-menu-kiri pull-right">5</span>
		</a>
	</li>
	<li>
		<a href="profile.php" title="">lainnya
			<span class="badge-menu-kiri pull-right">999</span>
		</a>
	</li>
</ul>