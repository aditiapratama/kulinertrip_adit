<div class="item-vertical">
    <div class="itemsub-box">
        <div class="itemsub-photo">
            <div class="panel-navtop">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <img class="profile-photo-small" src="<?php echo base_url('assets/images/profile3.jpg') ?>" alt="photo" />
                    </div>
                    <div class="col-lg-6 col-md-6 text-right">
                        <a href="" data-toggle="tooltip" title="simpan ke koleksi" class="ic-pinned-circle"></a>
                    </div>
                </div>
            </div>
            <img src="<?php echo base_url('assets/images/food1.jpg') ?>" alt="Foto" />
            
        </div>
    </div>
    <div class="itemsub-desc-vertical">
        <div class="itemsub-desc">
            <h4>Wisata Air Terjun</h4>
            <h5>Oleh : <a href="">Monica Sanjaya</a></h5>
            <p><span class="ic-distance"></span>Makasar</p>
            <p><span class="ic-distance"></span>10 Tempat</p>
            <p><span class="ic-distance"></span>2Km</p>
        </div>
        <div class="panel-bottom">
            <div class="col-md-7">
                <p><span class="ic-open-toko"></span>20 Kali dilihat</p>
            </div>
            <div class="col-md-5 text-right">
                <div class="iconbox"><span class="ic-saved"></span>9</div>
                <div class="iconbox"><span class="ic-review"></span>9</div>
                <div class="iconbox"><span class="ic-photo"></span>9</div>
            </div>
        </div>
    </div>
</div>