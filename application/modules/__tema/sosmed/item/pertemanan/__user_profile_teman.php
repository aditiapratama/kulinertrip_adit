<div class="user-item-box">
    <div class="user-item-photo">
            <img src="<?php echo $data['foto'] ?>" alt="">
    </div>
    <div class="user-item-desc">
        <div class="user-item-desc-name"><?php echo $data['namauser'] ?></div>
        <div class="user-item-desc-info">
            <ul>
                <li><div class="ic-review"></div><?php echo $data['jumlah_ulasan'] ?></li>
                <li><div class="ic-photo"></div><?php echo $data['jumlah_foto'] ?></li>
                <li><div class="ic-sm-friend-2"></div><?php echo $data['jumlah_pengikut'] ?></li>
            </ul>					
        </div>
    </div>
    <div class="user-item-action">
        <a href="" class="<?php echo $data['class_icon'] ?>" title=""></a>
    </div>
</div>