<div class="item-assessment">
    <ul>
        <li>
            <div class="item-assessment-total">7.5</div>
        </li>
        <li>
            <div class="item-assessment-item">
                <h3>10</h3>
                <div class=""></div>
                <h5>Rasa Makanan</h5>
            </div>	
        </li>
        <li>
            <div class="item-assessment-item">
                <h3>1.0</h3>
                <div class=""></div>
                <h5>Rasa Makanan</h5>
            </div>	
        </li>
        <li>
            <div class="item-assessment-item">
                <h3>7.7</h3>
                <div class=""></div>
                <h5>Rasa Makanan</h5>
            </div>	
        </li>
        <li>
            <div class="item-assessment-item">
                <h3>6</h3>
                <div class=""></div>
                <h5>Rasa Makanan</h5>
            </div>	
        </li>
    </ul>
</div>
<hr>
<div class="item-article">
    <div class="item-article-top">
        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        <p>
            <a href="" class="hastag">#makankeluarga</a>
            <a href="" class="hastag">#makanbersama</a>
        </p>
        <p></p>
        <sub>Ulasan ini dan segala isinya merupakan pendapat pribadi dari pengulas dan tidak terkait dengan kulinerTrip</sub>
    </div>
    <div class="item-article-gallery">
        <ul>
            <li><img src="<?php echo base_url('assets/images/food3.jpg') ?>" alt="" /></li>
            <li><img src="<?php echo base_url('assets/images/food2.jpg') ?>" alt="" /></li>
            <li><img src="<?php echo base_url('assets/images/food1.jpg') ?>" alt="" /></li>
            <li><img src="<?php echo base_url('assets/images/food3.jpg') ?>" alt="" /></li>
        </ul>
        <p>- Dengan <a href="" title="">Agung Wijaya, Khairani</a></p>
        <div class="panel-bottom">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <a href="" class="ic-like">999</a>
                    <a href="" class="ic-comment">999</a>
                    <a href="" class="ic-share"></a>
                    <a href="" class="ic-report">999</a>
                </div>
                <div class="col-lg-6 col-md-6 text-right">
                    <a href="">500 Komentar</a>
                </div>
            </div>
        </div>

    </div>
</div>