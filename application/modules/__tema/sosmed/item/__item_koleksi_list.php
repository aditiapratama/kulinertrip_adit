<div class="item-horizontal">
    <div class="itemsub-box">
        <div class="itemsub-photo">
            <div class="panel-navtop">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <a href="" data-toggle="tooltip" title="simpan ke koleksi" class="ic-pinned-circle"></a>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="rate-value-box">9.5</div>
                    </div>
                </div>
            </div>
            <img src="<?php echo base_url('assets/images/food1.jpg') ?>" alt="Foto" />
        </div>
        <div class="itemsub-desc">
            <div class="panel-navtop">
                <a href="" data-toggle="tooltip" title="Hooray!" class="iconbox"><div class="ic-pinned-circle"></div></a>
                <a href="" data-toggle="tooltip" title="Hooray!" class="iconbox"><div class="ic-delete-circle"></div></a>
                <a href="" data-toggle="tooltip" title="Hooray!" class="iconbox"><div class="ic-locked-circle"></div></a>
            </div>
            <h4>Restoran Terpopuler Di Sleman</h4>
            <h5><div class="iconbox"><div class="ic-sm-place"></div>D.I.Yogyakarta</div></h5>
            <h5><div class="iconbox"><div class="ic-"></div>Terakhir diperbaharui</div></h5>
            <div class="panel-bottom">
                <div class="divbox-table-cell">
                    <div class="iconbox"><div class="ic-distance"></div>10 Tempat</div>
                </div>
                <div class="divbox-table-cell text-right">
                    <div class="iconbox"><div class="ic-saved"></div>999</div>
                    <div class="iconbox"><div class="ic-review"></div>999</div>
                    <div class="iconbox"><div class="ic-photo"></div>999</div>
                </div>
            </div>
        </div>
    </div>
</div>