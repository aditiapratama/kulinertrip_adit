<div class="item-horizontal">
    <div class="itemsub-box">
        <div class="itemsub-photo">
            <div class="panel-navtop">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <?php if($issetpin == 'Y'){ ?>
                        <a href="" data-toggle="tooltip" title="simpan ke koleksi" class="ic-pinned-circle"></a>
                       <?php }?>  
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="rate-value-box"><?php echo $data['rating'] ?></div>
                    </div>
                </div>
            </div>
            <img src="<?php echo $data['thumbnail'] ?>" alt="Foto" />
            
        </div>
        <div class="itemsub-desc">
            <?php if($issetcontrol=='Y'){ ?>
            <div class="panel-navtop">
                <a href="" data-toggle="tooltip" title="Pin It" class="iconbox"><span class="ic-pinned-circle"></span></a>
                <a href="" data-toggle="tooltip" title="Delete" class="iconbox"><span class="ic-delete-circle"></span></a>
                <a href="" data-toggle="tooltip" title="Lock" class="iconbox"><span class="ic-locked-circle"></span></a>
            </div>
            <?php }?> 
            <h4><?php echo $data['judul_produk'] ?></h4>
            <h5><?php echo $data['jenis_wisata'] ?></h5>
            <h5><?php echo $data['kabupaten'] ?> - <?php echo $data['provinsi'] ?></h5>
            <?php if($data['buka_status']=='Buka'){?>
               <p><span class="ic-open-toko"></span> <b>Buka</b> <?php echo $data['jam_buka'] ?></p>
            <?php } else { ?>
               <p><span class="ic-close-toko"></span> <b>Tutup</b> </p>
            <?php }?>  
            <p><span class="ic-distance"></span><?php echo $data['jarak'] ?>Km</p>
        </div>
    </div>
    <div class="panel-bottom">
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <P><?php echo $data['tgl_checkin']; ?></P>
                <div class="iconbox">Via <?php echo $data['gadget'] ?> <span class="<?php echo $data['iconclass'] ?>"></span></div>
                <p><?php echo $data['jumlah_dilihat']?> dilihat</p>
            </div>
            <div class="col-lg-6 col-md-6 text-right">
                <div class="iconbox"><span class="ic-saved"></span>Tersimpan <?php echo $data['jumlah_favorit'] ?></div>
                <div class="iconbox"><span class="ic-review"></span>Ulasan <?php echo $data['jumlah_ulasan'] ?></div>
                <div class="iconbox"><span class="ic-photo"></span>Foto <?php echo $data['jumlah_foto'] ?></div>
            </div>
        </div>
    </div>
</div>