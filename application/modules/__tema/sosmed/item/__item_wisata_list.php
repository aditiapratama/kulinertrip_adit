<div class="item-horizontal">
    <div class="itemsub-box">
        <div class="itemsub-photo">
            <div class="panel-navtop">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <?php if($issetpin == 'Y'){ ?>
                            <a href="" data-toggle="tooltip" title="simpan ke koleksi" class="ic-pinned-circle"></a>
                        <?php }?> 
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="rate-value-box">9.5</div>
                    </div>
                </div>
            </div>
            <img src="<?php echo $data['thumbnail'] ?>" alt="Foto" />
        </div>
        <div class="itemsub-desc">
            <?php if($issetcontrol=='Y'){ ?>
            <div class="panel-navtop">
                <a href="" data-toggle="tooltip" title="Hooray!" class="iconbox"><div class="ic-pinned-circle"></div></a>
                <a href="" data-toggle="tooltip" title="Hooray!" class="iconbox"><div class="ic-delete-circle"></div></a>
                <a href="" data-toggle="tooltip" title="Hooray!" class="iconbox"><div class="ic-locked-circle"></div></a>
            </div>
           <?php }?>  
`            <h4><?php echo $data['judul_produk'] ?></h4>
             <h5><?php echo $data['jenis_wisata'] ?></h5>
            <h5><?php echo $data['kabupaten'] ?> - <?php echo $data['provinsi'] ?></h5>
            <?php if($data['buka_status']=='Buka'){?>
               <p><span class="ic-open-toko"></span> <b>Buka</b> <?php echo $data['jam_buka'] ?></p>
            <?php } else { ?>
               <p><span class="ic-close-toko"></span> <b>Tutup</b> </p>
            <?php }?>  
            <p><span class="ic-distance"></span><?php echo $data['jarak'] ?>Km</p>
            <div class="panel-bottom">
                <div class="divbox-table-cell">
                    <div class="iconbox"><div class="ic-distance"></div>2km</div>
                </div>
                <div class="divbox-table-cell text-right">
                    <div class="iconbox"><div class="ic-saved"></div><?php echo $data['jumlah_favorit'] ?></div>
                    <div class="iconbox"><div class="ic-review"></div><?php echo $data['jumlah_ulasan'] ?></div>
                    <div class="iconbox"><div class="ic-photo"></div><?php echo $data['jumlah_foto'] ?></div>
                </div>
            </div>

        </div>

    </div>
    <div class="panel-bottom">
        <article>
           <?php echo $data['blog_in'] ?> 
        </article>
        <div class="divbox-table text-right">
            <a href="">Lihat Selanjutnya</a>
        </div>

    </div>
</div>