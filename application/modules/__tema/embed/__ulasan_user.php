<div id="add-kuliner-review" class="detail-kuliner-box clearfix">
          <div class="col-md-2">
            <img class="img-review-profile" src="http://via.placeholder.com/100x100">
          </div>
          <div class="col-md-10 col-sm-10 col-xs-12">
            <!-- section-rating -->
            <div class="section-rating">
              <h4 class="title">Rating</h4>
              <span id="subtitle"> <?php echo $data['JudulProduk'].' - '.$data['area']; ?>, menanti review anda untuk meningkatkan layanannya </span>
              <div id="score" class="sum">
                <div class="sum-score">
                  <span class="text-score Roboto-300" data-total-rating="rating-tulis-ulasan">0</span>
                </div>
                <div class="score text-center" id="score-rasa">
                  <span class="rating-value rating-item-10--color Roboto" data-toggle-score="tulis-ulasan-rasa" style="color: rgb(232, 15, 10);"> 0 </span>
                  <div class="rating-box" data-rating-group="rating-tulis-ulasan" data-score-name="tulis-ulasan-rasa" data-hoverable="true" data-score="0">
                    <span class="rating-item rating-item-1" value="1"></span>
                    <span class="rating-item rating-item-2" value="2"></span>
                    <span class="rating-item rating-item-3" value="3"></span>
                    <span class="rating-item rating-item-4" value="4"></span>
                    <span class="rating-item rating-item-5" value="5"></span>
                    <span class="rating-item rating-item-6" value="6"></span>
                    <span class="rating-item rating-item-7" value="7"></span>
                    <span class="rating-item rating-item-8" value="8"></span>
                    <span class="rating-item rating-item-9" value="9"></span>
                    <span class="rating-item rating-item-10" value="10"></span>
                  </div>
                  <hr>
                  <span>Rasa Makanan</span>
                </div>
                <div class="score text-center">
                  <span class="rating-value rating-item-10--color Roboto" data-toggle-score="tulis-ulasan-lokasi" style="color: rgb(232, 15, 10);">0</span>
                  <div class="rating-box" data-rating-group="rating-tulis-ulasan" data-score-name="tulis-ulasan-lokasi" data-hoverable="true" data-score="0">
                    <span class="rating-item rating-item-1" value="1"></span>
                    <span class="rating-item rating-item-2" value="2"></span>
                    <span class="rating-item rating-item-3" value="3"></span>
                    <span class="rating-item rating-item-4" value="4"></span>
                    <span class="rating-item rating-item-5" value="5"></span>
                    <span class="rating-item rating-item-6" value="6"></span>
                    <span class="rating-item rating-item-7" value="7"></span>
                    <span class="rating-item rating-item-8" value="8"></span>
                    <span class="rating-item rating-item-9" value="9"></span>
                    <span class="rating-item rating-item-10" value="10"></span>
                  </div>
                  <hr>
                  <span>Susana</span>
                </div>
                <div class="score">
                  <span class="rating-value rating-item-4--color Roboto" data-toggle-score="tulis-ulasan-kebersihan" style="color: rgb(232, 15, 10);">0</span>
                  <div class="rating-box" data-rating-group="rating-tulis-ulasan" data-score-name="tulis-ulasan-kebersihan" data-hoverable="true" data-score="0">
                    <span class="rating-item rating-item-1" value="1"></span>
                    <span class="rating-item rating-item-2" value="2"></span>
                    <span class="rating-item rating-item-3" value="3"></span>
                    <span class="rating-item rating-item-4" value="4"></span>
                    <span class="rating-item rating-item-5" value="5"></span>
                    <span class="rating-item rating-item-6" value="6"></span>
                    <span class="rating-item rating-item-7" value="7"></span>
                    <span class="rating-item rating-item-8" value="8"></span>
                    <span class="rating-item rating-item-9" value="9"></span>
                    <span class="rating-item rating-item-10" value="10"></span>
                  </div>
                  <hr>
                  <span>Kebersihan</span>
                </div>
                <div class="score">
                  <span class="rating-value rating-item-10--color Roboto" data-toggle-score="tulis-ulasan-pelayanan" style="color: rgb(232, 15, 10);">0</span>
                  <div class="rating-box" data-rating-group="rating-tulis-ulasan" data-score-name="tulis-ulasan-pelayanan" data-hoverable="true" data-score="0">
                    <span class="rating-item rating-item-1" value="1"></span>
                    <span class="rating-item rating-item-2" value="2"></span>
                    <span class="rating-item rating-item-3" value="3"></span>
                    <span class="rating-item rating-item-4" value="4"></span>
                    <span class="rating-item rating-item-5" value="5"></span>
                    <span class="rating-item rating-item-6" value="6"></span>
                    <span class="rating-item rating-item-7" value="7"></span>
                    <span class="rating-item rating-item-8" value="8"></span>
                    <span class="rating-item rating-item-9" value="9"></span>
                    <span class="rating-item rating-item-10" value="10"></span>
                  </div>
                  <hr><span>Pelayanan</span>
                </div>
              </div>
            </div>
            <!-- end section-rating -->
            <!-- section-form-ulasan -->
            <div id="section-form-ulasan--form-basic">
              <div class="form-group">
                <label class="subtitle">Subject</label>
                <input name="" class="form-control input-round" placeholder="Tujuan Kunjungan" type="text" required>
              </div>
              <div class="form-group input-isi-ulasan">
                <label class="subtitle">Ulasan</label>
                <textarea class="form-control input-square" rows="3" data-min-word="30" placeholder="Bagikan dan ceritakan pengalaman anda saat mengunjungi tempat ini" required></textarea>
                <span class="text-count"></span>
              </div>
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <input name="" class="form-control input-round" placeholder="@Tag Teman" type="text">
                    <label>Ajak teman anda untuk berbagi</label>
                  </div>
                </div>
                <div class="col-md-3 col-md-offset-5 clearfix">
                  <div class="form-group">
                    <label class="btn btn-default input-round btn-upload pull-right" for="inputImageUlasan" title="Upload image file">
                      <input class="sr-only inputImage" id="inputImageUlasan" name="file" accept="image/*" type="file" multiple=""  data-source="ulasan-image">
                      <img class="img-item-wisata" style="width: 24px;" src="<?php echo base_url('assets/img/wisata/data-foto-icon.svg'); ?>"> Tambahkan Foto
                    </label>
                    <label>Berbagi dengan Kami</label>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="col-xs-1">#</label>
                    <div class="col-xs-11">
                      <input name="tag_friend" class="tag-friend form-control input-round" placeholder="@Tag Teman" type="text">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- end section-form-ulasan -->
            <!-- section-form-ulasan informasi-tambahan -->
            <div id="section-form-ulasan--informasi-tambahan">
              <h5>Informasi Tambahan</h5>
              <div class="informasi-tambahan-box">
                <p> Berikan informasi tambahan untuk melengkapi ulasan anda ( Dapatkan poin tambahan ) </p>
                <div class="informasi-tambahan--item"> 
                  <span>Berapa orang? </span>
                  <span> <input class="form-control input-sm input-round required-number" name="" value="1" type="text" style="width: 45px; text-align: center;"> </span> <span class="point"> + 0.5 Poin </span>  
                </div>
                <div class="informasi-tambahan--item" style="flex-direction: row; flex-wrap: wrap;"> 
                  <h5>Menu yang dipesan</h5>
                  
                </div>
                <div class="informasi-tambahan--item input-menu-order"> 
                    <span> <input class="form-control input-sm input-round" name="" value="" type="text"> </span><span class="label-name">Harga/Porsi (Rp) </span>
                    <span> <input class="form-control input-sm input-round price" name="" value="0" type="text" style="width: 130px;"> </span> <span class="point"> + 1 Poin </span><br>
                    Cita rasa : 
                    <div class="rating">
                        <input type="radio" id="star5" name="rating" value="5" /><label class = "full" for="star5" title="Lezat"></label>
                        <input type="radio" id="star4" name="rating" value="4" /><label class = "full" for="star4" title="Enak"></label>
                        <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Lumayan"></label>
                        <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Biasa saja"></label>
                        <input type="radio" id="star1" name="rating" value="1"  checked/><label class = "full" for="star1" title="Mengecewakan"></label>
                    </div>
                    <span class="label-name">Mengecewakan </span> 
                    <span class="point"> + 1 Poin </span> <a href="#" class="delete-item">Delete</a>
                </div>
                <a href="#" class="btn-add-recommend btn-menu-order">
                    Tambah daftar menu lain yang anda pesan
                </a>
              </div>
              <div class="informasi-tambahan-box">
                <div class="informasi-tambahan--item"> 
                  Total biaya yang dikeluarkan dalam kunjungan <span class="label-name">Total (Rp)</span> 
                  <span> <input class="form-control input-sm input-round price" name="" value="0" style="width: 100px;" type="text"> </span> 
                  <span class="point"> + 0.5 Poin </span>
                </div>
                <div class="informasi-tambahan--item"> 
                  <span style="flex: 0 1 100%;"> Menu yang direkomendasi </span>
                </div>
                <div class="input-menu-recommend informasi-tambahan--item"> 
                  <span class=""> <input class="form-control input-sm input-round" name="" value="" type="text"> </span>
                  <span class="point"> + 1 Poin </span> <a href="#" class="delete-item">Delete</a>
                </div>
                <a href="#" id="btn-recommend-menu" class="btn-add-recommend btn-menu-recommend">
                  Tambah kolom list menu yang direkomendasikan 
                </a>
              </div>
              <div class="informasi-tambahan-box">
                <div class="informasi-tambahan--item Roboto"> 
                  Apakah anda akan mengunjungi kembali tempat ini dilain waktu
                </div>
                <div class="informasi-tambahan--item"> 
                  <ul class="list-inline">
                    <li class="radio">
                      <label>
                        <input name="ulasan_form_ingin_berkunjung_lagi" checked="" type="radio"> pasti
                      </label>
                    </li>
                    <li class="radio">
                      <label>
                        <input name="ulasan_form_ingin_berkunjung_lagi" checked="" type="radio"> Mungkin
                      </label>
                    </li>
                    <li class="radio">
                      <label>
                        <input name="ulasan_form_ingin_berkunjung_lagi" checked="" type="radio"> Tidak lagi
                      </label>
                    </li>
                    <li>
                      <span class="point"> + 0.5 Poin </span> 
                    </li>
                  </ul>
                </div> 
              </div>
            </div>
            <!-- end section-form-ulasan informasi-tambahan -->
            <!-- section-view-image -->
            <div id="preview-image-ulasan" class="detail-kuliner-box clearfix">
            </div>
            <p>- Dengan <a href="#">Agung Wijaya, Khairani</a></p>
            <div id="aksi-kirim-ulasan" class="clearfix">
                <button type="submit" class="btn btn-default btn-sm btn-action pull-right" disabled="">kirim</button> <button type="submit" class="btn btn-danger btn-sm btn-action pull-right">Simpan Draft</button> 
            </div>
          </div>
        </div>