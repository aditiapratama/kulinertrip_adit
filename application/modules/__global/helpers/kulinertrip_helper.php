<?php defined('BASEPATH') OR exit('No direct script access allowed');

if( ! function_exists('indonesia_word') ) {
    function indonesia_word($text = ''){
        $a = strtolower($text);
        return ucwords($a);
    }
}

if( ! function_exists('hide_text_landing') ) {
    function hide_text_landing($text = '', $lenght = 40){
        if (strlen($text) > $lenght) {
            return substr($text, 0, $lenght) . '....';
        }
        return $text;
    }
}

if( ! function_exists('blog_hide') ) {
    function blog_hide($blog = ''){
        return strlen($blog);
    }
}

if( ! function_exists('ctr_replace') ) {
    function ctr_replace($text = '') {
        $replace = array('/', '.', ',', '[', ']', '(', ')', '_', '=', '+', ';', ':', '>', '<', '?', '!', '#', '%', '$', '^', '*');
        $hasil = str_replace($replace, "", $text);
        $hasil1 = str_replace(" ", "-", $hasil);

        return strtolower($hasil1);
    }
}

if( ! function_exists('strip_replace') ) {
    function strip_replace($text = ''){
        return str_replace("-", " ", $text);
    }
}

if( ! function_exists('rupiah') ) {
    function rupiah($x = ''){
        return "Rp " . number_format($x, 2, ',', '.');
    }
}

if( ! function_exists('day_php') ) {
    function day_php($d = '') {
        switch ($d) {
            case 'Sunday':
                $idhari = 1;
                return $idhari;
                break;
            case 'Monday':
                $idhari = 2;
                return $idhari;
                break;
            case 'Tuesday':
                $idhari = 3;
                return $idhari;
                break;
            case 'Wednesday':
                $idhari = 4;
                return $idhari;
                break;
            case 'Thursday':
                $idhari = 5;
                return $idhari;
                break;
            case 'Friday':
                $idhari = 6;
                return $idhari;
                break;
            case 'Saturday':
                $idhari = 7;
                return $idhari;
                break;
            default:
                return true;
                break;
        }
    }
}

if( ! function_exists('jam_format') ) {
    function jam_format($j = '') {
        if (strlen($j) > 5) {
            return substr($j, 0, -3);
        } else {
            return $j;
        }
    }
}

if( ! function_exists('city_replace') ) {
    function city_replace($text = '') {
        $replace = array('daerah istimewa', 'kota', 'kabupaten', 'provinsi', 'desa', 'dusun');
        $d = strtolower(trim($text));
        $c = str_replace($replace, "", $d);
        $e = str_replace(" ", "-", trim($c));
        return trim($e);
    }
}

if( ! function_exists('city_replace') ) {
    function curl_data($url = '', $param = '') {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "jsonParamDetail=" . $param);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }
}

if( ! function_exists('image_url') ) {
    function image_url(){
        $pageURL = 'http';
        if (@$_SERVER["HTTPS"] == "on") {
            $pageURL .= "s";
        }
        $pageURL .= "://";
        if ($_SERVER["SERVER_PORT"] != "80") {
            $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . "/media.kulinertrip.com/";
        } else {
            $pageURL .= $_SERVER["SERVER_NAME"] . "/media.kulinertrip.com/";
        }
        return '//172.104.166.160/media.kulinertrip.com/';
    }
}

/* ================================================================================================================== */
if( ! function_exists('get_header_data') ) {
    function get_header_data() {
        
        $SN =& get_instance();
        $SN->urllistfilter = curl_url() . "Ctr_data_filter/getJsonFilterPilihDataContent";        
        $xlat = $SN->session->userdata('latitude');
       //echo "latitude--->".$xlat;
        if(empty($xlat)){
            $json_param_post = array(
                'latitude' => "-7.74766584",
                'longitude' => "110.42292105",
                'kabupaten' => "Kabupaten Sleman",
                'propinsi' => "Daerah Istimewa Yogyakarta",
                'idhari' => "1",
                'jam' => "17:38",
                'language' => "ID",                
                'iduser' => "1"
            );
        } else {
            $json_param_post = array(
                'latitude' => $SN->session->userdata('latitude'),
                'longitude' => $SN->session->userdata('longitude'),
                'kabupaten' => $SN->session->userdata('kabupatenpilih'),
                'propinsi' => $SN->session->userdata('propinsipilih'),
                'idhari' => "1",
                'jam' => "17:38",
                'language' => "ID",         
                'iduser' => "1"
            );
        }
        

        if($jsondatafilter = curl_post_data(
            $SN->urllistfilter,
            array('jsonParamFilter' => json_encode($json_param_post))
        )){
            //echo $jsondatafilter;
            $jsondatafilterarr = json_decode($jsondatafilter, TRUE);;
            $jsonKategoriTenant = $jsondatafilterarr[0]['jsonKategoriTenantProvKab'];
            $jsonJenisMasakan= $jsondatafilterarr[0]['jsonJenisMasakanProvKabParentChild'];
            $jsonJenisTujuan= $jsondatafilterarr[0]['jsonJenisTujuanProvKab'];
            $jsonJenisWaktu= $jsondatafilterarr[0]['jsonJenisWaktuProvKab'];
            $jsonListJenisHalal= $jsondatafilterarr[0]['jsonListJenisHalal'];


            $jsonKategoriTenantArr = json_decode($jsonKategoriTenant, TRUE);
            $jsonJenisMasakanArr = json_decode($jsonJenisMasakan, TRUE);
            $jsonJenisTujuanArr = json_decode($jsonJenisTujuan, TRUE);
            $jsonJenisWaktuArr = json_decode($jsonJenisWaktu, TRUE);
            $jsonListJenisHalalArr = json_decode($jsonListJenisHalal, TRUE);

            $dataparam = array(
                'kategori'=> $jsonKategoriTenantArr,
                'masakan'=>  $jsonJenisMasakanArr,
                'tujuan'=> $jsonJenisTujuanArr,
                'waktu'=> $jsonJenisWaktuArr,
                'halal'=> $jsonListJenisHalalArr,
            );

            return $dataparam;
        } else {
            return false;
        }
    }
}

/* ================================================================================================================== */
if( ! function_exists('isJson') ) {
    function isJson($string = '') {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}

if( ! function_exists('ellipsis') ) {
    function ellipsis($text = '', $length = 30) {
        if (strlen($text) > $length) {
            return substr($text, 0, $length) . '...';
        }
        return $text;
    }
}

if( ! function_exists('truncate_human_language') ) {
    function truncate_human_language($text = '', $explode = ',', $exclude_word = '1234567890') {
        $wordlen = explode($explode, $text);
        if (count($wordlen) <= 1) {
            return $wordlen[0];
        }

        return $wordlen[0] . " dan lainnya";
    }
}
