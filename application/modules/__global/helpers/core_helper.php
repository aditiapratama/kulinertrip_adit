<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if( ! function_exists('url_detail') ) {
    function url_detail(){
        $url = "http://172.104.166.160/kulinertripbo/index.php/ctrlandingpage/getLandingKuliner";
        //$url = "http://localhost/kulinertripbo/index.php/ctrlandingpage/getLandingKuliner";
        return $url;
    }
}

if( ! function_exists('curl_url') ) {
    function curl_url(){
        return "http://172.104.166.160/kulinertripbo/index.php/";
        //return "http://localhost/kulinertripbo/index.php/";
    }
}

if( ! function_exists('curl_post_data') ) {
    function curl_post_data($url, $params){
        if (is_array($params)) $params = http_build_query($params, NULL, '&');

        try {
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $data = curl_exec($ch);
            if(!$data) {
                return curl_error($ch);
            }

            curl_close($ch);
            return $data;
        } catch (Exception $e) {
            //echo 'Message: ' . $e->getMessage();
            return false;
        }
    }
}

if( ! function_exists('json_encode') ) {
    if ( ! class_exists('Services_JSON')) require_once(APPPATH.'helpers/JSON.php');

    $json = new Services_JSON();
    function json_encode($data = null){
        if($data == null) return false;
        return $this->json->encode($data);
    }
}

if( ! function_exists('json_decode') ) {
    if ( ! class_exists('Services_JSON')) require_once(APPPATH.'helpers/JSON.php');

    $json = new Services_JSON();
    function json_decode($data = null){
        if($data == null) return false;
        return $this->json->decode($data);
    }
}



