<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if( ! function_exists('get_footer_kategori') ){
    function get_footer_kategori($data = ''){
        if(!empty($data)){
            $SN =& get_instance();
            return  $SN->load->view(MASTER_TEMA . '/__content_footer_kategori', $data);
        }

        return false;
    }
}

if( ! function_exists('get_footer_tema') ){
    function get_footer_tema($param = ''){
        if($param){
            $SN =& get_instance();
            return  $SN->load->view(MASTER_TEMA . '/__content_footer_tema');
        }

        return false;
    }
}

if( ! function_exists('get_footer_temawisata') ){
    function get_footer_temawisata($param = ''){
        if($param){
            $SN =& get_instance();
            return  $SN->load->view(MASTER_TEMA . '/__content_footer_temawisata');
        }

        return false;
    }
}