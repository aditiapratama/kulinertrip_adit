<?php defined('BASEPATH') OR exit('No direct script access allowed');

if( ! function_exists('get_data_kanan') ) {
    function get_data_kanan($param = ''){
        $SN =& get_instance();

        if(!empty($param)){
            $data = json_decode($param, true);
            if(!empty($data)){
                return $data;
            } else {
                return false;
            }
        } else {
            return false;
        }

        return false;
    }
}