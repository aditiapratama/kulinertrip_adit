<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if( ! function_exists('get_item_kuliner') ){
    function get_item_kuliner($data = ''){
        if(!empty($data)){
            $SN =& get_instance();
            return  $SN->load->view(MASTER_PAGE . '/itemkuliner', $data);
        }

        return false;
    }
}
