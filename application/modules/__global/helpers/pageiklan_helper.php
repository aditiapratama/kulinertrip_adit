<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if( ! function_exists('get_iklankanan') ){
    function get_iklankanan($param = array()){
        //print_r($param);
        if($param){
            $SN =& get_instance();
            foreach ($param as $key => $value){
                if (is_file(APPPATH . 'modules/' . MASTER_TEMA . '/views/iklankanan/__kanan_' . $key . EXT)){
                    $SN->load->view(MASTER_TEMA . '/iklankanan/__kanan_' . $key, array('data' => $value));
                }
            }
        }

        return false;
    }
}