<?php defined('BASEPATH') OR exit('No direct script access allowed');

class con_global extends MY_Core
{
    function __construct()
    {
        parent::__construct();
        $this->urllokasi = curl_url() . "Ctr_data_filter/getDataLatLong";
    }

    function nourl()
    {
        echo '404';
    }

    function curlgetdaerah()
    {
        $url = curl_url() . "ctrlandingpage/getJsonFilter";
        return $url;
    }

    function set_daerah()
    {
        $xPropinsi = $_POST['provinsi']['nama'];
        $kabupaten = $_POST['kabupaten']['nama'];


        $this->session->set_userdata('provinsi', $xPropinsi);
        $this->session->set_userdata('kabupaten', $kabupaten);
        $xparamkab = array(
            'kabupaten' => $this->session->userdata('kabupaten'),
            'propinsi' => $this->session->userdata('provinsi')
        );

        $jsondatakab = curl_post_data(
            $this->urllokasi,
            array('jsonparamlokasi' => json_encode($xparamkab))
        );

        $jsonDataKabArr = json_decode($jsondatakab, TRUE);

        $this->session->set_userdata('latitude', $jsonDataKabArr[0]['latitude']);
        $this->session->set_userdata('longitude', $jsonDataKabArr[0]['longitude']);
        $this->session->set_userdata('kabupatenpilih', $jsonDataKabArr[0]['kabupaten']);
        $this->session->set_userdata('propinsipilih', $jsonDataKabArr[0]['propinsi']);
    }

    function json_filter()
    {
        // $url = curl_url() . "ctrlandingpage/getJsonFilterAll";
        // $lat = $this->session->userdata('lat');
        // $lon = $this->session->userdata('long');
        // $xkabupaten = $this->session->namakabupatenpilih;
        // $xpropinsi = $this->session->namapropinsipilih;
        //
        // $data = curl_post_data($url,
        //     array('jsonParamFilter' => json_encode(
        //         array(
        //             'latitude' => $lat,
        //             'longitude' => $lon,
        //             'language' => 'ID',
        //             'kabupaten' => $xkabupaten,
        //             'propinsi' => $xpropinsi,
        //             'jenisfilter' => 'provinsikuliner',
        //         ))
        //     )
        // );

        $data = file_get_contents(base_url('assets/dataJSON/json-Filter.txt'));
        echo $data;
    }

    function get_daerah()
    {
        $url = curl_url() . "ctrlandingpage/getJsonFilter";
        $lat = $this->session->userdata('lat');
        $lon = $this->session->userdata('long');
        $xkabupaten = $this->session->namakabupatenpilih;
        $xpropinsi = $this->session->namapropinsipilih;

        $data = curl_post_data($url,
            array('jsonParamFilter' => json_encode(
                array(
                    'latitude' => $lat,
                    'longitude' => $lon,
                    'language' => 'ID',
                    'kabupaten' => $xkabupaten,
                    'propinsi' => $xpropinsi,
                    'jenisfilter' => 'provinsikuliner',
                ))
            )
        );

        echo $data;
    }

    function read_json_daerah()
    {
        $str = file_get_contents(base_url('assets/dataJSON/JSonDaerah02Maret2018.txt'));
        echo $str;

    }

    function terapkan_filter()
    {
        $this->session->set_userdata('filtered_ss', $_POST);
        echo json_encode($this->session->userdata('filtered_ss'));
    }

    function jumlah_filter()
    {
        echo 0;
        // echo json_encode($_POST);
    }

    function get_selected()
    {
        return $this->session->userdata('filtered_ss');
    }

    function get_pilihdaerah()
    {
        $xarrpropoinsi = $_POST['filterpropinsi'];
        $xkodeprop = "";
        $xkodekabupaten = "";
        $xnamapropinsi = "";
        $xnamakabupaten = "";
        if (!empty($xarrpropoinsi[0])) {
            $xarrprop = explode("_", $xarrpropoinsi[0]);
            $xkodeprop = $xarrprop[1];
            $rowprop = $this->modelindex->getDetailpropinsi($xkodeprop);
            $xnamapropinsi = $rowprop->namapropoinsi;
            $rowmap = $this->modelindex->getDetailKodeDaerahByProp($xnamapropinsi);
            $xarrlatlong = explode(",", $rowmap->Latlong);
            $lat = $xarrlatlong[0];
            $long = $xarrlatlong[1];
        }

        if (!empty($xarrpropoinsi[1])) {
            $xarrkabupaten = explode("_", $xarrpropoinsi[1]);
            $xkodekabupaten = $xarrkabupaten[1];
            $rowkab = $this->modelindex->getDetailkabupaten($xkodekabupaten);
            $rowmap = $this->modelindex->getDetailKodeDaerahByProp($xnamapropinsi);
            $xarrlatlong = explode(",", $rowmap->Latlong);

            $lat = $xarrlatlong[0];
            $long = $xarrlatlong[1];
            $xnamakabupaten = $rowkab->namakabupaten;
        } else {
            $xnamakabupaten = "";
        }

        $this->session->set_userdata('namapropinsipilih', $xnamapropinsi);
        $this->session->set_userdata('namakabupatenpilih', $xnamakabupaten);
        $this->session->set_userdata('isfrompilihdaerah', TRUE);
    }
}
