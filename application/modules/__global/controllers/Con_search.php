<?php defined('BASEPATH') OR exit('No direct script access allowed');

class con_search extends MY_Core
{
    function __construct(){
        parent::__construct();
        $this->urlseacrh = curl_url() . "Ctr_landing_page_kuliner/getJsonHasilSearch";
    }

    function index(){
        $a = $this->input->post('s');
        $session = $this->input->post('session');
        $link = "kuliner/";

        $propinsi = $this->session->userdata('propinsipilih');               
        $kabupaten = $this->session->userdata('kabupatenpilih'); 
        $latitude = $this->session->userdata('latitude');               
        $longitude = $this->session->userdata('longitude'); 

        if (is_null($kabupaten) && is_null($propinsi)) {
            $kabupaten = "Sleman";
            $propinsi = "Yogyakarta";
        }
        if (is_null($a)) {

        } else {
            $param = array(
                'propinsi' => $propinsi,
                'kabupaten' => $kabupaten,
                'kawasanArea' => "",
                'language' => "ID",
                'latitude' => $latitude,
                'longitude' => $longitude,
                'session' => $session,
                'start' => 0,
                'limit' => 6,
                'keyword' => $a
            );

            //$url = curl_url() . "ctrlandingpage/getJsonHasilSearch";
            $xparam = json_encode($param);
            
            $jsondata = curl_post_data($this->urlseacrh, array('jsonParamSearch' => $xparam));

            $data = $this->getdata(($jsondata));

            //  var_dump($data);//
            $grouping = array();
            foreach ($data as $item) {
                    if (!array_key_exists($item['kategoritext'], $grouping)) {
                        $grouping[$item['kategoritext']] = array();
                    }
                        array_push($grouping[$item['kategoritext']], $item);
            }

             //var_dump($grouping);

            if (empty($data) || $a === "") {

            } else {
                echo '
                <ul class="list-unstyled top-helper">
                    <li><i class="fa fa-map-marker"></i> Makanan Khas Sini</li>
                    <li><i class="fa fa-search"></i> Terakhir Kali Dilihat</li>
                </ul>
                <header class="header-helper clearfix">
                    Hasil Pencarian Anda <span>: '.count($data).' tempat ditemukan di <a href="" class="blue-link">Sleman, D.I Yogyakarta</a></span>
                    <a class="pull-right red-link" href="">Lihat selengkapnya</a>
                </header>
                <div class="container-content">
                <ul class="media-list content-suggestion">';
                foreach ($grouping as $kategori => $item) {
                    $total_item = count($item);
                    if($total_item > 5) $total_item = 5;

                    for($i=0; $i< $total_item; $i++)
                    {
                        if($i == ($total_item - 1)){
                            echo ($total_item > 3 )? '<li class="media clearfix last-category-item item-3 show-more" data-category="'.$kategori.'" data-total-item='.$total_item.'>' : '<li class="media clearfix last-category-item item-'.$total_item.'" data-category="'.$kategori.'">';
                        }else{
                            echo ($i>1)? '<li class="media pin hidden">' : '<li class="media">';
                        }
                        $xBuffjadwal = "";        
                                
                                       if(trim($item[$i]['KeteranganBuka'])==='Buka'){                                                                                                  
                                           $xBuffjadwal = '<img data-u="image" src="'.base_url('assets/icon/iconopen.png').'" width="10px" style="padding-left: 3px;" data-toggle="tooltip" title="Tersimpan" data-placement="top"/>  
                                                      <span class="text-buka"> '. $item[$i]['KeteranganBuka'].'</span> <span class="text-keteranganbuka"> '. $item[$i]['KeteranganJam'].'</span>';
                                                }else
                                                {
                                               $xBuffjadwal ='<img data-u="image" src="'.base_url('assets/icon/iconclose.png') .'" width="10px" style="padding-left: 3px;" data-toggle="tooltip" title="Tersimpan" data-placement="top"/>&nbsp;&nbsp;'.$item[$i]['KeteranganBuka'];
                                                }
                        echo '<a href="'.base_url("kuliner/detail/". $item[$i]['UrlPage'] .".html").'" target="_blank" style="z-index:200;">
                        <div class="media-left">
                            <img class="media-object" src="'.$item[$i]['linkthumbnail'].'">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">'.$item[$i]['JudulProduk'].'</h4>
                            '.$item[$i]['AlamatSurat'].', '.$item[$i]['kabupaten'].'
                        </div>
                        <ul class="media-footer">
                            <li> '.$xBuffjadwal.'</li>
                            <li><i class="fa fa-location-arrow"></i> '.$item[$i]['jarak'].' km</li>
                        </ul>
                        </a>';
                        if($i == ($total_item - 1) && $total_item > 3){
                            echo '<a href="#" id="saya" class="button-more blue-link"><i class="fa fa-angle-double-down" aria-hidden="true"></i></a>';
                        }
                        echo '</li>';

                    }
                }
                 
                echo '
                </ul>
                </div>
                <footer class="footer-helper">
                    Masukkan kata kunci baru pencarian anda
                </footer>';
                foreach ($data as $key => $value) {

                    // echo '<a href="' . base_url($link) . $value['idx'] . '">
                    //     <div class="search-box-list">
                    //         <div class="search-box-foto"><img src="'. $value['linkthumbnail'] .'" alt=""><div class="clearfix"></div></div>
                    //         <div class="search-box-content" >
                    //             <h4>' . $value['JudulProduk'] . '</h4>
                    //             <div class="content">
                    //                 <div>
                    //                     <img src="' . image_url() . 'aseets/icon/Lokasi.svg">
                    //                     ' . hide_text_landing($value['AlamatSurat']) . '
                    //                 </div>
                    //                 <div>
                    //                     <img src="' . image_url() . 'aseets/icon/Buka.svg">
                    //                     ' . $value['jadwalBukaSekarang'] . '
                    //                 </div>
                    //             </div>
                    //             <div class="my-clear"></div>
                    //         </div>
                    //     </div>
                    // </a>';
                }
            }
        }
    }

    function getdata($param) {
        $d = date("l");
        $day = day_php($d);

//        $url = curl_url() . "ctrlandingpage/getJsonHasilSearch";
//        $xparam = json_encode($param);
//        $jsondata = curl_post_data($url, $xparam);
//        print_r($xparam);
//        exit();

        $data = json_decode($param, 1);

        $xdata = json_decode($data[0]['dataSearch'], 1);
        if (empty($xdata)) {
            return $xdata;
        }

        foreach ($xdata as $key => $value) {
            $jadwal = json_decode($value['data']['jsonJadwalBuka'], 1);
            foreach ($jadwal as $key => $xjadwal) {
                if ($xjadwal['idhari'] == $day) {
                    $datajadwal = array('idx' => $xjadwal['idx'],
                        'NamaHari' => $xjadwal['NamaHari'],
                        'jambuka' => $xjadwal['jambuka'],
                        'jamtutup' => $xjadwal['jamtutup'],
                        'isbuka' => $xjadwal['isbuka'],
                        'is24jam' => $xjadwal['is24jam']
                    );
                }
            }
            $datakeluar[] = array('JudulProduk' => $value['data']['JudulProduk'],
                'area' => $value['data']['area'],
                'AlamatSurat' => $value['data']['AlamatSurat'],
                'UrlPage' => $value['data']['UrlPage'],
                'kabupaten' => $value['data']['kabupaten'],
                'kategoritext' => $value['data']['kategoritext'],
                'KeteranganBuka'=> $value['data']['KeteranganBuka'],
                'KeteranganJam'=> $value['data']['KeteranganJam'],
                'jarak' => $value['data']['jarak'],
                'rating' => $value['data']['rating'],
                'jmlFoto' => $value['data']['jmlFoto'],
                'jmlFavorit' => $value['data']['jmlFavorit'],
                'jmlUlasan' => $value['data']['jmlUlasan'],
                'linkthumbnail' => $value['data']['linkthumbnail'],
                'idx' => $value['data']['idx'],
                'jadwal' => $datajadwal,
                'jmlData' => $data[0]['jmlData'],
                'ketHasilSearch' => $data[0]['ketHasilSearch'],
                'jadwalBukaSekarang' => $value['data']['jadwalBukaSekarang']
            );
        }
        return $datakeluar;
    }
}
