<div class="content-row">
	<div class="content-col">

		<div class="friendpage-banner">
			<div class="friendpage-banner-photo">
				<img src="<?php echo base_url('assets/images/search-friend.png') ?>" alt="">
                                
                                
			</div>
			<div class="friendpage-banner-desc">
				<h4>Undang Teman Anda</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</p>
                                <a href="<?php echo base_url('friendshipcall'); ?>" style="text-decoration: none;"><button type="button" class="btn btn-success btn-simple">Undang Teman Anda</button></a>
			</div>
		</div>

		<div class="friendpage-listgroup-box">
			<div class="friendpage-listgroup-panel">
                            
				<label>Pengikut (<?php echo count($pengikut) ?>)</label>
                                <?php if(count($pengikut)<=0){ ?>
				<p>Anda Belum Diikuti oleh Siapapun</p>
                                <?php }?>
			</div>
			<div class="friendpage-listgroup-list">
				<ul>
                                    <?php 
                                    
                                    if(count($pengikut)>0){ 
                                      for ($i=0;$i< count($pengikut);$i++) { 
                                       $xdata = array(
                                        'data' =>  $pengikut[$i]
                                       );
                                       
                                       ?>
					<li>
                                        <?php $this->load->view(MASTER_TEMA.'/sosmed/item/pertemanan/__user_profile_teman', $xdata) ?>   
                                        </li>    
                                      <?php                                    
                                    }
                                    
                                      }?>    				
				</ul>

				<div class="actionbox-button">
					<button type="button" class="btn btn-default btn-normal">Lihat Selanjutnya (+20)</button>
				</div>

			</div>
		</div>

		<div class="friendpage-listgroup-box">
			<div class="friendpage-listgroup-panel">
				<label>Mengikuti (<?php echo count($mengikuti) ?>)</label>
                                <?php if(count($mengikuti)<=0){ ?>
				   <p>Anda Belum Diikuti oleh Siapapun</p>
                                <?php }?>
			</div>
			<div class="friendpage-listgroup-list">
                          <ul>  
                            <?php 
                                    
                                    if(count($mengikuti)>0){ 
                                      for ($i=0;$i< count($mengikuti);$i++) { 
                                       $xdata = array(
                                        'data' =>  $mengikuti[$i]
                                       );
                                       
                                       ?>
					<li>
                                        <?php $this->load->view(MASTER_TEMA.'/sosmed/item/pertemanan/__user_profile_teman', $xdata) ?>   
                                        </li>    
                                      <?php                                    
                                    }
                                    
                                      }?>    				
                                        
                          </ul>
			</div>

			<div class="actionbox-button">
				<button type="button" class="btn btn-default btn-normal">Lihat Selanjutnya (+20)</button>
			</div>
		</div>

	</div>
</div>