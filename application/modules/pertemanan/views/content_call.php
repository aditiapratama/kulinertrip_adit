<div class="content-row">
	<div class="content-col">

		<div class="friendpage-banner">
			<div class="friendpage-banner-photo">
				
                                <img src="<?php echo base_url('assets/images/search-friend.png') ?>" alt="">
			</div>
			<div class="friendpage-banner-desc">
				<h4>Undang Teman Anda & berbahagialah bersama mereka</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</p>
			</div>
		</div>

		<div id="SocialTabOnTopTab" class="nav-tabs" role="tablist">
			<ul>
				<li class="active" role="presentation">
					<a id="kulinertrip-tab" class="active" href="#kulinertrip" role="tab" data-toggle="tab" aria-controls="kulinertrip" aria-expanded="true" title="">KulinerTrip</a>
				</li>
				<li role="presentation">
					<a id="facebook-tab" href="#facebook" role="tab" data-toggle="tab" aria-controls="facebook" aria-expanded="false" title="">Facebook</a>
				</li>
				<li role="presentation">
					<a id="googleplus-tab" href="#googleplus" role="tab" data-toggle="tab" aria-controls="googleplus" aria-expanded="false" title="">Google+</a>
				</li>
			</ul>
		</div>

		<div class="friendpage-listgroup-box">

			<div id="SocialTabOnTop" class="tab-content">
				<div id="kulinertrip" class="tab-pane fade active in" role="tabpanel" aria-labelledby="kulinertrip-tab">
					<div class="friendpage-listgroup-label">
						<label>Temukan Teman di KulinerTrip</label>
						<div class="input-group">
							<input type="text" class="form-control" placeholder="cari berdasarkan nama, email atau akun KulinerTrip" aria-describedby="basic-addon1">
							<span class="input-group-addon" id="basic-addon1">x</span>
						</div>
						<p><b class="red">50</b> orang ditemukan</p>
						<p>Rekomendasi teman untuk diikuti</p>
					</div>

				</div>
				<div id="facebook" class="tab-pane fade in" role="tabpanel" aria-labelledby="facebook-tab">
					<div class="friendpage-listgroup-label">
						<label>Temukan Teman di facebook</label>
					</div>
					<div class="actionbox-button">
						<a class="" href="">facebook</a>
						<p>Hubungkan akun facebook anda dengan kulinerTrip untuk melanjutkan</p>
					</div>

				</div>
				<div id="googleplus" class="tab-pane fade in" role="tabpanel" aria-labelledby="googleplus-tab">
					<div id="SocialTabOnBottomTab" class="nav-tabs" role="tablist">
						<ul>
							<li class="active" role="presentation">
								<a id="sudahgabung-tab" class="active" href="#sudahgabung" role="tab" data-toggle="tab" aria-controls="sudahgabung" aria-expanded="true" title="">Sudah Bergabung di kulinerTrip (999)</a>
							</li>
							<li role="presentation">
								<a id="belumgabung-tab" href="#belumgabung" role="tab" data-toggle="tab" aria-controls="belumgabung" aria-expanded="false" title="">Belum Bergabung di kulinerTrip (999)</a>
							</li>
						</ul>
					</div>
					<div id="SocialTabOnBottom" class="tab-content">

						<div id="sudahgabung" class="tab-pane fade active in" role="tabpanel" aria-labelledby="sudahgabung-tab">
							<div class="divbox-table friendpage-listgroup-label">
								<div class="divbox-table-cell-left">
									<p><b class="red">50</b> orang ditemukan</p>
								</div>
								<div class="divbox-table-cell-right">
									<button type="button" class="btn btn-default">ikuti semua</button>
								</div>
							</div>

							<div class="friendpage-listgroup-list">
								<ul>
									<li>
										<div class="user-item-box">
											<div class="user-item-photo">
												<img src="<?php echo base_url('assets/images/profile2.jpg');?>" alt="">
											</div>
											<div class="user-item-desc">
												<div class="user-item-desc-name">Sari Wulandari</div>
												<div class="user-item-desc-info">
													<ul>
														<li>999 Ulasan</li>
														<li>999 Follower</li>
													</ul>					
												</div>
											</div>
											<div class="user-item-action">
												<a href="" class="ic-follow" title=""></a>
											</div>
										</div>
									</li>
								</ul>
								<div class="actionbox-button">
									<button type="button" class="btn btn-default btn-normal">Lihat Selanjutnya (+20)</button>
								</div>
							</div>

						</div>
						<div id="belumgabung" class="tab-pane fade in" role="tabpanel" aria-labelledby="belumgabung-tab">
							<div class="divbox-table friendpage-listgroup-label">
								<div class="divbox-table-cell-left">
									<p>Undang Teman anda untuk bergabung di KulinerTrip</p>
								</div>
								<div class="divbox-table-cell-right">
									
								</div>
							</div>

							<div class="friendpage-listgroup-list">
								<ul>
									<li>
										<div class="user-item-box">
											<div class="user-item-photo">
												<img src="<?php echo base_url('assets/images/profile2.jpg')?>" alt="">
											</div>
											<div class="user-item-desc">
												<div class="user-item-desc-name">Sari Wulandari</div>
											</div>
											<div class="user-item-action">
												<button type="button" class="btn btn-invite" title="">Undang</button>
											</div>
										</div>
									</li>
								</ul>
								<div class="actionbox-button">
									<button type="button" class="btn btn-default btn-normal">Lihat Selanjutnya (+20)</button>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>

		</div>

	</div>
</div>