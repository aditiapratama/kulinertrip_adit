<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class con_pertemanan extends MY_Core {

    function __construct() {
        parent::__construct();
        $this->listmengikuti = curl_url() . "Ctr_data_sosmed/getmengikuti";
        $this->listdiikuti = curl_url() . "Ctr_data_sosmed/diikuti";
        $this->postmegikuti = curl_url() . "Ctr_data_sosmed/postmengikuti";
        $this->searctagteman = curl_url() . "Ctr_data_user_api/getUserData";
        //$this->urliklankanan = curl_url() . "Ctr_iklan/getJsonIklanDetailKuliner";
    }

    function index() {
        
    }

    function setpertemanan() {
        //$iduser  = $this->session->userdata('iduser');
        $iduser = "UR201805020000001";

        $json_param_mengikuti = array(
            "iduser" => $iduser,
            "limitstart" => '0',
            "limitend" => '20',
                );

        $responsemengikuti = curl_post_data(
                $this->listmengikuti, array('param' => json_encode($json_param_mengikuti))
        );
        //echo $responsemengikuti;

        $responsemengikutiArr = json_decode($responsemengikuti, true);

        $json_param_diikuti = array(
            "iduser" => $iduser,
            "limitstart" => '0',
            "limitend" => '20',
                );

        $responsediikuti = curl_post_data(
                $this->listdiikuti, array('param' => json_encode($json_param_diikuti))
        );
        // echo $responsediikuti;

        $responsediikutiArr = json_decode($responsediikuti, true);





        $xarraymenukiri = "";

        $xarraycontent = array(
            'pengikut' => $responsediikutiArr,
            'mengikuti' => $responsemengikutiArr,
        );


        $dataparam = array(
            'databanner' => $this->load->view(MASTER_TEMA . '/sosmed/__banner_profile', $this->getbannner(), TRUE),
            'menukiri' => $this->load->view(MASTER_TEMA . '/sosmed/__menu_sosmed_left', $xarraymenukiri, TRUE),
            'kontent' => $this->load->view('content', $xarraycontent, TRUE),
            'title' => "",
            'call_page' => 'pertemanan/view',
        );

        $this->parser->parse(MASTER_PAGE, $dataparam);
    }

    

    function setcallpertemanan() {
        //$iduser  = $this->session->userdata('iduser');
        $iduser = "UR201805020000001";
        $xarraymenukiri = "";
        $xarraycontent = "";

        $dataparam = array(
            'databanner' => $this->load->view(MASTER_TEMA . '/sosmed/__banner_profile', $this->getbannner(), TRUE),
            'menukiri' => $this->load->view(MASTER_TEMA . '/sosmed/__menu_sosmed_left', $xarraymenukiri, TRUE),
            'kontent' => $this->load->view('content_call', $xarraycontent, TRUE),
            'title' => "Pertemanan Nama User",
            'call_page' => 'pertemanan/view',
        );

        $this->parser->parse(MASTER_PAGE, $dataparam);
    }

    function getbannner() {
        $userName = $this->session->userdata('userName');
        $photo = $this->session->userdata('user_url_foto');
        $urlbackground = $this->session->userdata('urlbackground');
        $kotaasal = $this->session->userdata('kotaasal');
        $idgrade = $this->session->userdata('idgrade');
        $idlevel = $this->session->userdata('idlevel');
        $kabupaten = $this->session->userdata('kabupatenpilih');


        $xarrayuser = array(
            "namauser" => (!empty($userName)) ? $userName : "Nama User",
            "foto" => (!empty($photo)) ? $photo : base_url('assets/images/profile1.jpg'),
            "urlbackground" => (!empty($urlbackground)) ? $urlbackground : base_url('assets/images/food2.jpg'),
            "kotaasal" => (!empty($kotaasal)) ? $kotaasal : "Belum Diisi Kota Asal",
            "grade" => (!empty($idgrade)) ? $idgrade : "Tripper",
            "level" => (!empty($idlevel)) ? $idlevel : "Level 1"
                );
        return $xarrayuser;
    }
    
// Call With Ajaxj
    function searchtagteman() {
        $tagteman = $_POST['datatag'];
        //print_r($_POST);
        $jsonuser = curl_post_data(
                $this->searctagteman, array('param' => $tagteman)
        );
        echo $jsonuser;
    }

    
    // Insert Mengikti
    function postmengikuti() {
        $xiduser = "UR201805020000001";
        $xiduserdikuti = $_POST['iduserdikuti'];
        $json_param_post = array(
            'iduser' => $xiduser,
            'iduserdiikuti' => $xiduserdikuti
        );

        $response = curl_post_data(
                $this->postmegikuti, array('param' => json_encode($json_param_post))
        );
    }
}
