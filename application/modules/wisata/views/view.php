<div class="row" id="content">         
    <?php //echo '<pre>'.print_r($datawisata, true).'</pre>'; ?>
            <div class="col-md-12">
                <div class="container">
                  <div class="col-md-12">
                  <figure id="landmark-wisata">
                      
                      <div class="image-block">
                        <img src="<?php echo base_url(); ?>assets/img/landmarkwisatajogja.jpg" class="img-responsive">
                      </div>
                      <div class="title-block">
                        <label><span>D.I Yogyakarta <?php //echo $this->session->userdata('propinsipilih'); ?></span></label>
                      </div>
                    </figure>
                  </div>
                </div>
                <div class="container" >
               <div class="style-content">     
                  <div class="col-md-9">
                    Foto land mark dan data 
                    <?php //echo '<pre>'.print_r($datawisata, true).'</pre>'; ?>
                    <?php foreach ($datawisata as $wisata) { ?>

                      <a class="box-item-wisata" href="<?php echo base_url('wisata/detail/'. $wisata['UrlPage'] .'.html') ?>" target="_blank" >
                        <figure class="thumb-item-wisata col-sm-3 col-md-4">
                          <img src="<?php echo $wisata['linkthumbnail'];?>" class="img-responsive">
                          <div href="" class="tooltips simpan-item-icon"><img src="<?php echo base_url(); ?>assets/img/wisata/data-simpan-icon.svg"><span>Simpan ke Koleksi</span></div>
                        </figure>
                        <div class="col-sm-3 col-md-8">
                            <div class="container-judul-item">
                              <h3 class="judul-item-wisata"><?php echo $wisata['JudulProduk'];?> <br><small><?php echo $wisata['jeniswisata']." - ".$wisata['kawasanArea'];?></small></h3>
                              <label class="atribut-item-wisata">
                                  <span class="pull-right location" data-toggle="tooltip" title="Berdasarkan Titik Pusat Keramaian Kota" data-placement="bottom">
                                  <img class="img-item-wisata" src="<?php echo base_url(); ?>assets/img/wisata/jarak-icon.svg">
                                  
                                  <?php echo $wisata['jarak'].' km';?>
                                  </span>    
                              </label>
                            </div>
                           <!--  <h3 class="judul-item-wisata pull-left"><?php echo $wisata['JudulProduk'];?> <br><small>Wisata Sejarah-Gondomana, Kota Yogyakarta</small></h3>
                            <label class="pull-right atribut-item-wisata"><img class="img-item-wisata" src="<?php echo base_url(); ?>assets/img/wisata/jarak-icon.svg"><?php echo $wisata['jarak'].'km';?></label>
                            <div class="clearfix"></div> -->
                            
                            <ul class="list-unstyled atribut-item-wisata pull-left"> 
                              <li><img class="img-item-wisata" src="<?php echo base_url(); ?>assets/img/wisata/buka-on-icon.svg"></i> <?php echo $wisata['jadwalBukaSekarang'];?></li>
                              
                            </ul>
                            <ul class="list-unstyled list-inline text-right atribut-item-wisata">
                              <li><img class="img-item-wisata" src="<?php echo base_url('assets/icon/iconsimpan.png') ?>" data-toggle="tooltip" title="Tersimpan" data-placement="top">6</li>
                              <li><img class="img-item-wisata" src="<?php echo base_url(); ?>assets/img/wisata/data-ulasan-icon.svg"data-toggle="tooltip" title="Ulasan" data-placement="top">7</li>
                              <li><img class="img-item-wisata" src="<?php echo base_url(); ?>assets/img/wisata/data-foto-icon.svg" data-toggle="tooltip" title="Foto" data-placement="top"> 8</li>
                            </ul>
                            <div class="clearfix"></div>
                            
                          
                          <div class="deskripsi-item-wisata text-justify">
                              Monumen Jogja Kembali | Alampriangan.com - Monumen Jogja Kembali atau yang akrab di sebut "monjali" oleh warga setempat merupakan sebuah bangunan bersejarah yang di miliki oleh bangsa indonesia dan terletak di provinsi Daerah Istimewa Yogyakarta. 

                          </div>
                          <span class="pull-right text-danger">Lihat Selanjutnya</span>
                        </div>
                        <div class="clearfix"></div>
                      </a> 

                      <?php } ?> 

                      <div class="paging">
                        <ul class="list-unstyled list-inline">
                          <li><a href="">1</a></li>
                          <li><a href="">2</a></li>
                          <li><a href="">3</a></li>
                          <li><a id="btn-wisata" href="">Next</a></li>
                          <li><a href="">...</a></li>
                          <li><a href="">15</a></li>
                        </ul>
                      </div>
             <div class="">
                    <?php get_footer_temawisata((!empty($iklan) ? $iklan : false), (!empty($iklan_param) ? $iklan_param : false)) ?>
                    <div class="clearfix"></div>
                </div>
                    
                      <?php $datahastag="";
                            $this->load->view('__hastag',$datahastag); ?>    

                    </div>
               </div>    
                    <div class="col-sm-3">
                <?php get_iklankanan(!empty($iklan) ? $iklan : false) ?>

                <div class="clearfix"></div>
            </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

