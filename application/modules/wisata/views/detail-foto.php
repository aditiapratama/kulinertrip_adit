        <?php //echo '<pre>'.print_r($data, true).'</pre>'; ?>
        <?php 
        $images_detail  = json_decode($data['jsonImageDetail']);
        $data_detail    = json_decode($images_detail[0]->data);
        ?>

        <div class="container is-mobile">
          <div class="col-md-12 col-xs-12">
            <div id="header-detail-kuliner">
              <div class="col-sm-5 col-md-5">
                <div id="header-detail-kuliner-mobile" class="visible-xs-block">
                  <ul class="pull-left breadcrumb-location">
                    <li><a href="<?php echo base_url('kawasan/' . str_replace(" ", "-", $data['kawasanArea'])) . '.html'; ?>" target="_blank"><?php echo $data['state']; ?></a></li>
                    <li><a href="<?php echo base_url('kawasan/' . str_replace(" ", "-", $data['kawasanArea'])) . '.html'; ?>" target="_blank"><?php echo $data['kabupaten']; ?></a></li>
                    <li><a href="<?php echo base_url('kawasan/' . str_replace(" ", "-", $data['kawasanArea'])) . '.html'; ?>" target="_blank"><?php echo $data['kawasanArea']; ?></a></li>
                  </ul>
                  <div class="clearfix">
                      <h3 class="judul-item-wisata pull-left"><?php echo $data['JudulProduk'].'-'.$data['kawasanArea']; ?><br><small><?php echo $data['jeniswisata']; ?></small></h3>
                  </div>
                </div>
                <figure class="gambar-profil-kuliner">
                  <img src="<?php echo $data['linkimage']; ?>" class="img-responsive">
                  <label class="atribut-image-kuliner top"><?php echo $data_detail[0]->kategorifoto ?></label>
                  <ul class="atribut-image-kuliner bottom list-inline">
                    <li><i class="fa fa-thumbs-o-up"></i><?php echo $data['jmlLikeFotoProfil']; ?></li>
                    <li><i class="fa fa-comment-o"></i><?php echo $data['jmlKomentarFotoProfil']; ?></li>
                    <li><i class="fa fa-share-alt"></i></li>
                  </ul>
                </figure>
                <div class="image-thumbs hidden-xs">
                  <?php $i=0; while ($i < count($data_detail) && $i< 5) {
                    if($i == 4 ){
                        
                      echo '<div class="image-thumb" data-full-image="'.$data_detail[$i]->linkimage.'"  data-title="'.$data_detail[$i]->kategorifoto.'">
                      <img src="'.$data_detail[$i]->linkthumbnail.'" class="img-responsive">';
                      if(count($data_detail)>5){
                      echo '<label><a href="'.site_url().'kuliner/foto/'.$data['UrlPage'].'.html">+'.(count($data_detail)-5).' lagi</a></label>';
                      }
                      echo '</div>';
                    }else{
                      echo '<div class="image-thumb" data-full-image="'.$data_detail[$i]->linkimage.'" data-title="'.$data_detail[$i]->kategorifoto.'">
                      <img src="'.$data_detail[$i]->linkthumbnail.'" class="img-responsive">
                      </div>';
                    }
                    $i++;
                  }
                  ?>
                </div>
              </div>
              <div class="col-sm-7 col-md-7">
                <ul class="pull-left breadcrumb-location">
                    <li><a href="<?php echo base_url('kawasan/' . str_replace(" ", "-", $data['kawasanArea'])) . '.html'; ?>" target="_blank"><?php echo $data['state']; ?></a></li>
                    <li><a href="<?php echo base_url('kawasan/' . str_replace(" ", "-", $data['kawasanArea'])) . '.html'; ?>" target="_blank"><?php echo $data['kabupaten']; ?></a></li>
                    <li><a href="<?php echo base_url('kawasan/' . str_replace(" ", "-", $data['kawasanArea'])) . '.html'; ?>" target="_blank"><?php echo $data['kawasanArea']; ?></a></li>
                  </ul>
                <ul class="pull-right list-inline">
                    <li><button class="btn btn-primary btn-xs active-button"><img class="img-item-wisata" style="width: 14px;" src="<?php echo base_url('assets/icon/iconsimpanputih.svg'); ?>">&nbsp;Simpan</button></li>
                </ul>
                <div class="clearfix"></div>
                <h3 class="judul-item-wisata hidden-xs"><?php echo $data['JudulProduk'].'-'.$data['area']; ?> <br><small><?php echo $data['jeniswisata']; ?></small></h3>

                <ul class="list-unstyled atribut-item-wisata">
                  <li class="alamat-kuliner"><img class="img-item-wisata" src="<?php echo base_url('assets/icon/iconalamat.png'); ?>"> <span class="kekanandetailatas"><?php echo $data['AlamatSurat'] . ', ' . $data['kabupaten'] . ' ' . $data['state']; ?></span></li>
                  <li class="status-jadwal-kuliner">
                  
                      
                      
                                                <?php 
                                                if(trim($data['KeteranganBuka'])==='Buka'){                                                    
                                                ?>  
                                                   <img class="img-item-wisata" src="<?php echo base_url('assets/icon/iconbuka.png');?>"> <span class="kekanandetailatas"> 
                                                   <span class="kekanandetailatasbuka">  <?php echo $data['KeteranganBuka']; ?></span>  <?php echo $data['KeteranganJam']; ?>
                                                <?php }else
                                                {?>
                                                   <img data-u="image" src="<?php echo base_url('assets/icon/icontutup.png') ?>" width="14px" data-toggle="tooltip" title="Tersimpan" data-placement="top"/> <span class="kekanandetailatas"> <?php echo $data['KeteranganBuka']; ?> </span>
                                                <?php }?>
<!--                                                <i class="fa fa-fw fa-clock-o <?php //echo ((!empty($buka[0]->isbuka)) ? (($buka[0]->isbuka == 'Y') ? 'text-green' : 'text-grey') : 'text-grey') ?> " onclick="produk"></i> <span ><?php //echo $datakuliner[$i]['jadwalBukaSekarang']; ?></span>-->
                                                  
                                           
                      </span> <span class="tombol-jadwal-kuliner visible-xs-inline kekanandetailatas">Lihat jadwal</span>                      
                    <div class="jadwal-kuliner">
                      <?php
                      $jadwal_buka = json_decode($data['jsonJadwalBuka']);
                      foreach ($jadwal_buka as $jadwal) {
                        echo "<div><span>".$jadwal->NamaHari."</span><span>".$jadwal->jambuka."-".$jadwal->jamtutup." WIB</span>";
                        if(!empty($jadwal->keterangan)) "<span>".$jadwal->keterangan."</span>";
                        echo "</div>";
                      }
                      ?>

                    </li>
                  <li><span class="location" data-toggle="tooltip" title="Berdasarkan Titik Pusat Keramaian Kota" data-placement="bottom">
                                    <img class="img-item-wisata" src="<?php echo base_url(); ?>assets/img/wisata/jarak-icon.svg"/>
                                     <span class="kekanandetailatas">   <?php echo $data['jarak'];?>km </span> 
                                    </span></li>
                  <li>
                    <img class="img-item-wisata" src="<?php echo base_url(); ?>assets/img/wisata/jarak-icon.svg"> Map
                  </li>
                  <li class="clearfix map">
                    <div class="col-md-8 google-maps">
                      <a href="http://maps.google.com/maps?q=<?php echo $data['mapaddress']; ?>&z=10" target="_blank"><img src="https://maps.googleapis.com/maps/api/staticmap?center=<?php echo $data['mapaddress']; ?>&zoom=13&size=400x110&maptype=roadmap&markers=anchor:32,10%7Cicon:icon:<?php echo base_url()."assets/icon/iconmarker.svg"?>color:red%7Clabel:S%7C<?php echo $data['mapaddress']; ?>&key=AIzaSyAU41R8r4EPPCefTKIEoDV6aPWVShRjyNQ&format=png" class="img-responsive"></a>
                    </div>
                    <div class="col-md-3 google-streets">
                      <div>
                        <h6>Google Street View</h6>
                        <a href="http://maps.google.com/maps?q=&layer=c&cbll=<?php echo $data['mapaddress']; ?>" target="_blank">
                    <img src="https://maps.googleapis.com/maps/api/streetview?size=400x180&location=<?php echo $data['mapaddress']; ?>&heading=151.78&key=AIzaSyAU41R8r4EPPCefTKIEoDV6aPWVShRjyNQ&pitch=-0.76" class="img-responsive">
                  </a>
                      </div>
                      <a href="" class="text-center text-muted">
                        <i class="fa fa-exclamation-triangle fa-2x" aria-hidden="true"></i><span>Perbaharui Informasi</span>
                      </a>
                    </div>
                </li>
                </ul>
                      <ul class="list-unstyled list-inline text-right atribut-item-wisata">
                        <li><img class="img-item-wisata2" src="<?php echo base_url(); ?>assets/img/wisata/data-simpan-icon.svg"><?php echo 'Simpan '.$data['jmlFavorit']; ?></li>
                                    <li><img class="img-item-wisata2" src="<?php echo base_url(); ?>assets/img/wisata/data-ulasan-icon.svg"><?php echo 'Ulasan '.$data['jmlUlasan']; ?></li>
                                    <li><img class="img-item-wisata2" src="<?php echo base_url(); ?>assets/img/wisata/data-foto-icon.svg">&nbsp;<?php echo 'Foto '.$data['jmlFoto']; ?></li>
                      </ul>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <?php 
                    $galeri_foto    = json_decode($data['jsonGaleriFoto']);
                    $data_galeri    = json_decode($galeri_foto[0]->data);
                  ?>
                  <div class="row">
                    <div class="col-md-9">

                      <div id="detail-kuliner-informasi"  class="tab-page">
                        <ul class="tabs-kuliner clearfix tabs-flying">
              <a href="<?php echo site_url().'wisata/blog/'.$data['UrlPage'].'.html';?>" class="tab-item">Blog</a>
              <a target="_blank" href="<?php echo site_url().'wisata/foto/'.$data['UrlPage'].'.html';?>" class="tab-item active">Foto</a>
              <a target="_blank" href="<?php echo site_url().'wisata/ulasan/'.$data['UrlPage'].'.html';?>" class="tab-item">Ulasan</a>
            </ul>

            <ul class="tabs-kuliner no-flying clearfix">
              <a href="<?php echo site_url().'wisata/blog/'.$data['UrlPage'].'.html';?>" class="tab-item">Blog</a>
              <a target="_blank" href="<?php echo site_url().'wisata/foto/'.$data['UrlPage'].'.html';?>" class="tab-item active">Foto</a>
              <a target="_blank" href="<?php echo site_url().'wisata/ulasan/'.$data['UrlPage'].'.html';?>" class="tab-item">Ulasan</a>
            </ul>
                      </div>

                      <div id="foto-detail-kuliner">
                        <div class="pull-right upload-gambar-kuliner">
                          <?php if($galeri_foto[0]->jmlGaleriFoto != 0){ ?>
                            Berbagi dengan kami <button class="btn btn-danger"><img class="img-item-wisata" src="<?php echo base_url(); ?>assets/img/wisata/data-foto-icon.svg"> Tambah Foto</button>
                          <?php }else{ ?>
                          <div class="text-center upload-gambar-kuliner">
                            Jadilah pengunggah foto yang pertama <button class="btn btn-danger"><img class="img-item-wisata" src="<?php echo base_url(); ?>assets/img/wisata/data-foto-icon.svg"> Tambah Foto</button>
                          </div>
                          <?php }?>                          
                        </div>
                        <div class="clearfix"></div>
                      </div>
                      
                      <div id="detail-foto-360" class="detail-kuliner-box is-mobile">
                    <h3 class="detail-title">Foto 360&deg;</h3>


                    <a class="col-md-3 foto-360">
                        <img src="<?php echo base_url('assets/icon/foto360/foto360.png'); ?>" class="img-responsive">
                    </a>

                    <a class="col-md-3 foto-360">
                        <img src="<?php echo base_url('assets/icon/foto360/foto360a.png'); ?>" class="img-responsive">
                    </a>
                    <a class="col-md-3 foto-360">
                        <img src="<?php echo base_url('assets/icon/foto360/foto360b.png'); ?>" class="img-responsive">
                    </a>
                    <a class="col-md-3 foto-360">
                        <img src="<?php echo base_url('assets/icon/foto360/foto360c.png'); ?>" class="img-responsive">
                    </a>
                    
                    <a class="col-md-3 foto-360">
                        <img src="<?php echo base_url('assets/icon/foto360/foto360.png'); ?>" class="img-responsive">
                    </a>

                    <a class="col-md-3 foto-360">
                        <img src="<?php echo base_url('assets/icon/foto360/foto360a.png'); ?>" class="img-responsive">
                    </a>
                    <a class="col-md-3 foto-360">
                        <img src="<?php echo base_url('assets/icon/foto360/foto360b.png'); ?>" class="img-responsive">
                    </a>
                    <a class="col-md-3 foto-360">
                        <img src="<?php echo base_url('assets/icon/foto360/foto360c.png'); ?>" class="img-responsive">
                    </a>
                    
                    <div class="clearfix"></div>
                </div>

                      
                      <div id="foto-detail-kuliner" class="detail-kuliner-box is-mobile">
                        <h3 class="detail-title">Foto</h3>
                        <?php 
                        $galeri_show = (count($data_galeri) > 20)? 20 : count($data_galeri);
                        $sisa = count($data_galeri) - 20;
                        for($i=0; $i<$galeri_show; $i++) { ?>
                        <a class="col-md-3 col-xs-6 foto-360">
                          <img src="<?php echo $data_galeri[$i]->linkimage; ?>" class="img-responsive">
                        </a>
                        <?php } ?>
                        <div class="clearfix"></div>
                        <?php if($sisa > 0 ){ 
                          echo "<a href='' class='next-image'>Lihat Selanjutnya (+".$sisa.")</a>"; 
                        } ?>
                      </div>
                        <?php $datahastag="";
               $this->load->view('__hastag',$datahastag); ?>    



                    </div>
                    <div class="col-md-3 style-content">
           <?php get_iklankanan(!empty($iklan) ? $iklan : false) ?>
          <div class="clearfix"></div>
                  </div>
            

      </div>
    </div>
    
                </div>
              </div>

            </div>
            <div class="clearfix"></div>

            <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>
            <script type="text/javascript" src="<?php echo base_url().'assets/js/imam/module-2.js'; ?>"></script>