<?php
//echo '<pre>'.print_r($data, true).'</pre>'; 
$images_detail = json_decode($data['jsonImageDetail']);
$data_detail = json_decode($images_detail[0]->data);
?>


<div class="gallery-info-box">
  <div class="slide-caption-wrap">
<!--    // Photo captions will be populated here-->

   <div class="frame-kanan">
            <div class="infouser">
                <div class="infouser-foto">
                    <img src="<?php echo base_url('assets/icon/avatar.png') ?>" alt="">
                </div>
                <div class="infouser-text">
                    <div class="infouser-nama">
                        Mulya Hari Vano
                    </div>
                    <div class="infouser-panel">
                        <i class="fa fa-fw fa-thumbs-up"></i> 0 &nbsp;
                        <i class="fa fa-fw fa-thumbs-down"></i> 0   &nbsp;
                        <i class="fa fa-fw fa-bookmark"></i> 0  &nbsp;
                        <i class="fa fa-fw fa-share"></i> 0 &nbsp;
                    </div>
                </div>
            </div>
        </div>

  </div>
<!--    <div class="comment"> Ini komen Ini komen Ini komen Ini komen Ini komen Ini komen </div>-->
<!--  // include advert

  // include comments-->

</div>




<script type="text/javascript">
    function showgallery(i){
                         
                         $(document).ready(function() {
                           var $lg = $(this);
                           //alert($commentBox.html());
                          var jsonData = <?php echo $data['jsonImageGallery'] ?>;
                         $lg.lightGallery({
                             appendSubHtmlTo: '.lg-item',
                            download:false, 
                            dynamic: true,
                            index: parseInt(i),
                            html:true,
                            mode: 'lg-fade',
                            addClass: 'fb-comments',
                            dynamicEl:jsonData
                            });  
                            
                         

// Perform any action just before opening the gallery
$lg.on('onAfterOpen.lg',function(event){
    // Hide the original comments
    $('.lg-sub-html').hide();
    // Insert sidebar into the gallery
    $('.gallery-info-box').clone().appendTo('.lg').show();
});

// Perform any action after the slide has been loaded
$lg.on('onAfterAppendSubHtml.lg',function(event){
    var lgSubContent = $('.lg-sub-html').html();
    // Populate the sidebar with the captions
    $('.lg .slide-caption-wrap').html(lgSubContent);
});

// Perform any action just after closing the gallery
$lg.on('onCloseAfter.lg',function(event){
    // Remove the gallery sidebar
    $('.lg .gallery-info-box').remove();
});
    
                         });
                        }
                        
    $(document).on('onCloseAfter.lg', function(event) {
        $(document).data('lightGallery').destroy(true);
    });   
   
    
//		$(document).ready(function() {
////			$("#lightgallery").lightGallery({
////                                download:false,
////                                zoom:true,
////                            });
//                            
////                         $("#detaildaftarmenu").lightGallery({
////                                download:false,
////                                zoom:true,
////                            });   
//
//
//                        function showgallery(i){
//                         alert("test"+i);   
//                        }
//                         var jsonData = <?php //echo $data['jsonImageGallery'] ?>;
//                         $(this).lightGallery({
//                            download:false, 
//                            dynamic: true,
//                            index: 2,
//                            html:true,
//                            dynamicEl:jsonData
//                            });
////                        alert(<?php //echo $data['jsonImageGallery'] ?>);
//                        
//		});
                
	</script>
        
<div class="container is-mobile">            
    <div class="col-md-12 col-xs-12">
        <div id="header-detail-kuliner">
            <div class="col-sm-5 col-md-5">
                <div id="header-detail-kuliner-mobile" class="visible-xs-block">
                    <ul class="pull-left breadcrumb-location">
                        <li><?php echo $data['state']; ?></li>
                        <li><?php echo $data['kabupaten']; ?></li>
                        <li><?php echo $data['city']; ?></li>
                    </ul>
                    <div class="clearfix">
                        <h3 class="judul-item-wisata pull-left"><?php echo $data['JudulProduk'] . '-' . $data['kawasanArea']; ?><br><small><?php echo $data['jeniswisata']; ?></small></h3>
                    </div>
                </div>
                
                <figure id="figur1" class="gambar-profil-kuliner" style="display: none;">
                    <img src="<?php echo $data['linkimage']; ?>" class="img-responsive">
                    <label class="atribut-image-kuliner top"><?php echo $data_detail[0]->kategorifoto ?></label>
                    <ul class="atribut-image-kuliner bottom list-inline">
                        <li><i class="fa fa-thumbs-o-up"></i><?php echo $data['jmlLikeFotoProfil']; ?></li>
                        <li><i class="fa fa-comment-o"></i><?php echo $data['jmlKomentarFotoProfil']; ?></li>
                        <li><i class="fa fa-share-alt"></i></li>
                    </ul>
                </figure>
                
                <figure id="figur2" class="gambar-profil-kuliner2">
                    <img src="<?php echo $data['linkimage']; ?>" class="img-responsive">
                    <label class="atribut-image-kuliner top"><?php echo $data['JudulProduk'] ?></label>
                    <ul class="atribut-image-kuliner bottom list-inline">
                        <li><i class="fa fa-thumbs-o-up"></i><?php echo $data['jmlLikeFotoProfil']; ?></li>
                        <li><i class="fa fa-comment-o"></i><?php echo $data['jmlKomentarFotoProfil']; ?></li>
                        <li><i class="fa fa-share-alt"></i></li>
                    </ul>
                </figure>
                
                
                <div class="image-thumbs hidden-xs">
                    <?php
                    $i = 0;
                    
                    while ($i < count($data_detail) && $i < 5) {
                        if ($i == 4) {

                            echo '<div class="image-thumb" data-full-image="' . $data_detail[$i]->linkimage . '"  data-title="' . $data_detail[$i]->kategorifoto . '" onclick="showgallery('.$i.');">
                      <img src="' . $data_detail[$i]->linkthumbnail . '" class="img-responsive">';
                            if (count($data_detail) > 5) {
                                echo '<label><a href="' . site_url() . 'kuliner/foto/' . $data['UrlPage'] . '.html">+' . (count($data_detail) - 5) . ' lagi</a></label>';
                            }
                            echo '</div>';
                        } else {
                            echo '<div class="image-thumb" data-full-image="' . $data_detail[$i]->linkimage . '" data-title="' . $data_detail[$i]->kategorifoto . '" onclick="showgallery('.$i.')">
                      <img src="' . $data_detail[$i]->linkthumbnail . '" class="img-responsive">
                      </div>';
                        }
                        $i++;
                    }
                    ?>
                </div>
            </div>
            <div class="col-sm-7 col-md-7">
                <ul class="pull-left breadcrumb-location hidden-xs">
                    <li><a href="<?php echo base_url('kawasan/' . str_replace(" ", "-", $data['kawasanArea'])) . '.html'; ?>" target="_blank"><?php echo $data['state']; ?></a></li>
                    <li><a href="<?php echo base_url('kawasan/' . str_replace(" ", "-", $data['kawasanArea'])) . '.html'; ?>" target="_blank"><?php echo $data['kabupaten']; ?></a></li>
                    <li><a href="<?php echo base_url('kawasan/' . str_replace(" ", "-", $data['kawasanArea'])) . '.html'; ?>" target="_blank"><?php echo $data['kawasanArea']; ?></a></li>
                </ul>
                <ul class="pull-right list-inline">
                    <li><button class="btn btn-primary btn-xs active-button"><img class="img-item-wisata" style="width: 14px;" src="<?php echo base_url('assets/icon/iconsimpanputih.svg'); ?>">&nbsp;Simpan</button></li>
                </ul>
                <div class="clearfix"></div>
                <h3 class="judul-item-wisata hidden-xs"><?php echo $data['JudulProduk'] . '-' . $data['area']; ?> <br><small><?php echo $data['jeniswisata']; ?></small></h3>

                <ul class="list-unstyled atribut-item-wisata">
                    <li class="alamat-kuliner"><img class="img-item-wisata" src="<?php echo base_url('assets/icon/iconalamat.png'); ?>"> <span class="kekanandetailatas"><?php echo $data['AlamatSurat'] . ', ' . $data['kabupaten'] . ' ' . $data['state']; ?></span></li>
                    <li class="status-jadwal-kuliner">
                        
                      
                      
                                                <?php 
                                                if(trim($data['KeteranganBuka'])==='Buka'){                                                    
                                                ?>  
                                                   <img class="img-item-wisata" src="<?php echo base_url('assets/icon/iconbuka.png');?>"> <span class="kekanandetailatas"> 
                                                   <span class="kekanandetailatasbuka">  <?php echo $data['KeteranganBuka']; ?></span>  <?php echo $data['KeteranganJam']; ?>
                                                <?php }else
                                                {?>
                                                   <img data-u="image" src="<?php echo base_url('assets/icon/icontutup.png') ?>" width="14px" data-toggle="tooltip" title="Tersimpan" data-placement="top"/> <span class="kekanandetailatas"> <?php echo $data['KeteranganBuka']; ?> </span>
                                                <?php }?>
<!--                                                <i class="fa fa-fw fa-clock-o <?php //echo ((!empty($buka[0]->isbuka)) ? (($buka[0]->isbuka == 'Y') ? 'text-green' : 'text-grey') : 'text-grey') ?> " onclick="produk"></i> <span ><?php //echo $datakuliner[$i]['jadwalBukaSekarang']; ?></span>-->
                                                  
                                           
                      </span> <span class="tombol-jadwal-kuliner visible-xs-inline kekanandetailatas">Lihat jadwal</span>                      
                    <div class="jadwal-kuliner">
                      <?php
                      $jadwal_buka = json_decode($data['jsonJadwalBuka']);
                      foreach ($jadwal_buka as $jadwal) {
                        echo "<div><span>".$jadwal->NamaHari."</span><span>".$jadwal->jambuka."-".$jadwal->jamtutup." WIB</span>";
                        if(!empty($jadwal->keterangan)) "<span>".$jadwal->keterangan."</span>";
                        echo "</div>";
                      }
                      ?>
                                </li>
                                <li>
                                    <span class="location" data-toggle="tooltip" title="Berdasarkan Titik Pusat Keramaian Kota" data-placement="bottom">
                                    <img class="img-item-wisata" src="<?php echo base_url(); ?>assets/img/wisata/jarak-icon.svg"/>
                                     <span class="kekanandetailatas">   <?php echo $data['jarak'];?>km </span> 
                                    </span>
                                </li>
                                <li>
                                    <img class="img-item-wisata" src="<?php echo base_url(); ?>assets/img/wisata/jarak-icon.svg"> Map
                                </li>
                                <li class="clearfix map">
                                    <div class="col-md-8 google-maps">
                                        <a href="http://maps.google.com/maps?q=<?php echo $data['mapaddress']; ?>&z=10" target="_blank"><img src="https://maps.googleapis.com/maps/api/staticmap?center=<?php echo $data['mapaddress']; ?>&zoom=13&size=400x110&maptype=roadmap&markers=anchor:32,10%7Cicon:icon:<?php echo base_url() . "assets/icon/iconmarker.svg" ?>color:red%7Clabel:S%7C<?php echo $data['mapaddress']; ?>&key=AIzaSyAU41R8r4EPPCefTKIEoDV6aPWVShRjyNQ&format=png" class="img-responsive"></a>
                                    </div>
                                    <div class="col-md-3 google-streets">
                                        <div>
                                            <h6>Google Street View</h6>
                                            <a href="http://maps.google.com/maps?q=&layer=c&cbll=<?php echo $data['mapaddress']; ?>" target="_blank">
                                                <img src="https://maps.googleapis.com/maps/api/streetview?size=400x180&location=<?php echo $data['mapaddress']; ?>&heading=151.78&key=AIzaSyAU41R8r4EPPCefTKIEoDV6aPWVShRjyNQ&pitch=-0.76" class="img-responsive">
                                            </a>
                                        </div>
                                        <a href="" class="text-center text-muted">
                                            <i class="fa fa-exclamation-triangle fa-2x" aria-hidden="true"></i><span>Perbaharui Informasi</span>
                                        </a>
                                    </div>
                                </li>
                                </ul>
                                <ul class="list-unstyled list-inline text-right atribut-item-wisata">
                                    <li><img class="img-item-wisata2" src="<?php echo base_url(); ?>assets/img/wisata/data-simpan-icon.svg"><?php echo 'Simpan '.$data['jmlFavorit']; ?></li>
                                    <li><img class="img-item-wisata2" src="<?php echo base_url(); ?>assets/img/wisata/data-ulasan-icon.svg"><?php echo 'Ulasan '.$data['jmlUlasan']; ?></li>
                                    <li><img class="img-item-wisata2" src="<?php echo base_url(); ?>assets/img/wisata/data-foto-icon.svg">&nbsp;<?php echo 'Foto '.$data['jmlFoto']; ?></li>
                                </ul>
                        </div>
                        <div class="clearfix"></div>
                        </div>

                        <div class="row">
                            <div class="col-md-9">
                                <div id="detail-daftar-menu" class="detail-kuliner-box hidden-xs" style="padding-left: 20px;padding-right:0px;">
                                    <h3 class="detail-title">Share Ke Teman</h3>
                                    <div class="col-md-7ths " style="float:left; margin-right: 5px; "><img style ="height: 31px; width:122.25px" src="<?php echo base_url(); ?>assets/icon/share/facebook.png" class="img-responsive"></div>
                                    <div class="col-md-7ths " style="float:left;margin-right: 5px;"><img style ="height: 31px; width:122.25px" src="<?php echo base_url(); ?>assets/icon/share/line.png" class="img-responsive"></div>
                                    <div class="col-md-7ths " style="float:left;margin-right: 5px;"><img style ="height: 31px; width:122.25px" src="<?php echo base_url(); ?>assets/icon/share/twitter.png" class="img-responsive"></div>
                                    <div class="col-md-7ths " style="float:left;margin-right: 5px;"><img style ="height: 31px; width:122.25px" src="<?php echo base_url(); ?>assets/icon/share/whatsapp.png" class="img-responsive"></div>
                                    <div class="col-md-7ths " style="float:left;margin-right: 5px;"><img style ="height: 31px; width:122.25px" src="<?php echo base_url(); ?>assets/icon/share/email.png" class="img-responsive"></div>
                                    <div class="col-md-7ths " style="float:left;margin-right: 5px;"><img style ="height: 31px; width:122.25px" src="<?php echo base_url(); ?>assets/icon/share/kulinertrip.png" class="img-responsive"></div>
                                    <div class="col-md-7ths " style="float:left;margin-right: 5px;"><img style ="height: 31px; width:122.25px" src="<?php echo base_url(); ?>assets/icon/share/share.png" class="img-responsive"></div>                          
                                    <div class="clearfix"></div>
                                </div> 


                                
                                <!-- #2:Foto 360 yang akan ditampilkan jika mobile -->
                                <div id="detail-foto-360" class="detail-kuliner-box hidden-xs">
            <h3 class="detail-title">Foto 360&deg;</h3>

            <?php //for($i=0; $i<3; $i++) { ?>
              <a class="col-md-3 foto-360">
                <img src="<?php echo base_url('assets/icon/foto360/foto360.png');?>" class="img-responsive">
              </a>
            <?php //} ?>
            <a class="col-md-3 foto-360">
                <img src="<?php echo base_url('assets/icon/foto360/foto360a.png');?>" class="img-responsive">
              </a>
            <a class="col-md-3 foto-360">
                <img src="<?php echo base_url('assets/icon/foto360/foto360b.png');?>" class="img-responsive">
              </a>
            
            <div class="col-md-3 foto-360 banyak-lagi">
              <img src="<?php echo base_url('assets/icon/foto360/foto360c.png');?>" class="img-responsive">
              <label><?php echo '<a href="'.site_url().'kuliner/foto/'.$data['UrlPage'].'.html">+30 lagi</a>'; ?></label>
            </div>
            <div class="clearfix"></div>
          </div>
                                <!-- End #2 -->

                                <div id="more-detail-info">ingin tahu lebih lanjut hal-hal yang menarik dari Taman Sari <a href="#detail-foto-360">klik disini</a>
                                </div>

                                <?php
                                $fasilitas = json_decode($data['jsonFasilitas']);
                                $item_fasilitas = json_decode($fasilitas[0]->fasilitas);
                                ?>
                                <div id="fasilitas-detail-kuliner" class="detail-kuliner-box">
                                    Lihat Fasilitas 
                                    <ul class="list-inline">
                                        <?php $jumlah_fasilitas = ($fasilitas[0]->jmlFasilitas <= 6) ? $fasilitas[0]->jmlFasilitas : 6;
                                        for ($i = 0; $i < $jumlah_fasilitas; $i++) {
                                            ?>
                                            <li><img class="img-responsive" src="<?php echo $item_fasilitas[$i]->imgfasilitas; ?>"></li>
<?php } ?>
                                    </ul>
                                    <a href="#" class="visible-xs-block">Lihat lebih</a>
                                    <div class="lebih-banyak">
                                        <h4>Fasilitas</h4>
                                        <ul>
<?php
foreach ($item_fasilitas as $item) {
    echo "<li><img class='img-responsive' src='" . $item->imgfasilitas . "'><span>" . $item->jenisfasilitas . "</span></li>";
}
?>
                                        </ul>
                                    </div>
                                </div>
                                <div id="detail-kuliner-informasi">
                                    <ul class="tabs-kuliner clearfix tabs-flying">
                                        <a href="<?php echo site_url() . 'wisata/blog/' . $data['UrlPage'] . '.html'; ?>" class="tab-item active">Blog</a>
                                        <a target="_blank" href="<?php echo site_url() . 'wisata/foto/' . $data['UrlPage'] . '.html'; ?>" class="tab-item">Foto</a>
                                        <a target="_blank" href="<?php echo site_url() . 'wisata/ulasan/' . $data['UrlPage'] . '.html'; ?>" class="tab-item">Ulasan</a>
                                    </ul>

                                    <ul class="tabs-kuliner no-flying clearfix">
                                        <a href="<?php echo site_url() . 'wisata/blog/' . $data['UrlPage'] . '.html'; ?>" class="tab-item active">Blog</a>
                                        <a target="_blank" href="<?php echo site_url() . 'wisata/foto/' . $data['UrlPage'] . '.html'; ?>" class="tab-item">Foto</a>
                                        <a target="_blank" href="<?php echo site_url() . 'wisata/ulasan/' . $data['UrlPage'] . '.html'; ?>" class="tab-item">Ulasan</a>
                                    </ul>
                                </div>
                                    <?php
                                      $info_wisata = json_decode($data['jsonInfoWisata']);
                                    ?>
                                <div id="blog-wisata" class="detail-kuliner-box">
                                    <span sytle="left:-50px;"> Ditulis oleh: </span>
                                    <div class="header">
                                        <img src="<?php echo (!empty($info_wisata[0]->linkfotopenulis)) ? $info_wisata[0]->linkfotopenulis : 'http://via.placeholder.com/100x100'; ?>" class="img-responsive"> &nbsp;&nbsp; <?php echo (!empty($info_wisata[0]->namapenulisblog)) ? $info_wisata[0]->namapenulisblog : "<em>Tanpa nama</em>"; ?>
                                    </div>
                                    <div class="content">
                                    <?php echo (!empty($info_wisata[0]->blog)) ? $info_wisata[0]->blog : "<p class='text-center'><em>Tidak ada ulasan</em></p>"; ?>
                                    </div>
                                    <div class="footer clearfix">
                                        <span class="img-responsive pull-right"> Tanpa Judul</span>
                                        <img src="<?php echo base_url('assets/icon/logo.png'); ?>" class="img-responsive pull-right">
                                        
                                    </div>
                                    
                                </div>

                                    <?php
                                    $foto_user = json_decode($data['jsonFotoUser']);
                                    $data_foto_user = json_decode($foto_user[0]->data);
                                    ?>
                                <div id="foto-detail-kuliner" class="detail-kuliner-box hidden-xs">
                                    <h3 class="detail-title">Foto</h3>
                                    <?php
                                    if (count($data_foto_user) != 0) {
                                        $i = 0;
                                        while ($i < count($data_foto_user) && $i < 4) {
                                            if ($i == 3) {
                                                echo '<div class="col-md-3 foto-360 banyak-lagi">
                  <img src="http://via.placeholder.com/800x450" class="img-responsive">';
                                                if (count($data_foto_user) > 4) {
                                                    echo '<label><a href="' . site_url() . 'kuliner/foto/' . $data['UrlPage'] . '.html">+' . (count($data_foto_user) - 4) . ' lagi</a></label>';
                                                }
                                                echo '</div>';
                                            } else {
                                                echo '<div class="col-md-3 foto-360">
                  <img src="http://via.placeholder.com/800x450" class="img-responsive">
                  </div>';
                                            }
                                            $i++;
                                        }
                                    }
                                    ?>
                                    <div class="clearfix"></div>
                                    <?php
                                    if (count($data_foto_user) != 0) {
                                        ?>
                                        <div class="pull-right upload-gambar-kuliner">
                                            Berbagi dengan kami <button class="btn btn-danger"><img class="img-item-wisata" src="<?php echo base_url(); ?>assets/img/wisata/data-foto-icon.svg"> Tambah Foto</button>
                                        </div>
                                        <div class="clearfix"></div>

<?php } else { ?>
                                        <div class="text-center upload-gambar-kuliner">
                                            Jadilah pengunggah foto yang pertama <button class="btn btn-danger"><img class="img-item-wisata" src="<?php echo base_url(); ?>assets/img/wisata/data-foto-icon.svg"> Tambah Foto</button>
                                        </div>
                                        <?php } ?>
                                </div>

                                <!-- #3:Foto galeri yang akan ditampilkan jika mobile -->
                                <div id="foto-detail-kuliner" class="detail-kuliner-box visible-xs-block">
                                    <h3 class="detail-title">Foto</h3>
                                    <div class="mobile-slider">
                                        <?php
                                        if (count($data_foto_user) != 0) {
                                            $i = 0;
                                            while ($i < count($data_foto_user) && $i < 4) {
                                                if ($i == 3) {
                                                    echo '<div class="col-md-3 foto-360 banyak-lagi">
                    <img src="http://via.placeholder.com/800x450" class="img-responsive">
                    <label><a href="' . site_url() . 'kuliner/foto/' . $data['UrlPage'] . '.html">+' . (count($data_foto_user) - 4) . ' lagi</a></label>
                    </div>';
                                                } else {
                                                    echo '<div class="col-md-3 foto-360">
                    <img src="http://via.placeholder.com/800x450" class="img-responsive">
                    </div>';
                                                }
                                                $i++;
                                            }
                                        }
                                        ?>
                                    </div>
                                    <div class="clearfix"></div>
                                    <?php if (count($data_foto_user) != 0) { ?>
                                        <div class="pull-right upload-gambar-kuliner">
                                            Berbagi dengan kami <button class="btn btn-danger"><img class="img-item-wisata" src="<?php echo base_url(); ?>assets/img/wisata/data-foto-icon.svg"> Tambah Foto</button>
                                        </div>
<?php } else { ?>
                                        <div class="text-center upload-gambar-kuliner">
                                            Jadilah yang pertama <button class="btn btn-danger"><img class="img-item-wisata" src="<?php echo base_url(); ?>assets/img/wisata/data-foto-icon.svg"> Tambah Foto</button>
                                        </div>
<?php } ?>
                                    <div class="clearfix"></div>
                                </div>
                                <!-- End #3 -->

                                <div class="detail-kuliner-box">
                                    <h3 class="detail-title">Tulis ulasan</h3>
                                </div>
                                 <?php $datahastag = "";
$this->load->view('__hastag', $datahastag);
?>
                            </div>
                            <div class="col-md-3 style-content">
<?php get_iklankanan(!empty($iklan) ? $iklan : false) ?>
                                <div class="clearfix"></div>

                            </div>
                           
                        </div>
    

                        </div>
                        </div>
                        </div>
                        <div class="clearfix"></div>

                        <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>
                        <script type="text/javascript" src="<?php echo base_url() . 'assets/js/imam/module-2.js'; ?>"></script>
                        

    

