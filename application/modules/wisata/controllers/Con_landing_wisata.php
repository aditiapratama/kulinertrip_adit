<?php defined('BASEPATH') OR exit('No direct script access allowed');

class con_landing_wisata extends MY_Core
{
    function __construct(){
        parent::__construct();
        $this->urllanding = curl_url() . "Ctr_landing_wisata/getLandingWisata";
        $this->urllistbawah = curl_url() . "ctrlandingpage/getJsonListBawah";
        $this->urliklankanan = curl_url() . "Ctr_iklan/getIklanLandingKuliner";
        $this->idhari = day_php(date("l"));
    }

    function index(){
        $xlat = $this->session->userdata('latitude');
       //echo "latitude--->".$xlat;
        if(empty($xlat)){
            $json_param_post = array(
                'latitude' => "-7.74766584",
                'longitude' => "110.42292105",
                'kabupaten' => "Kabupaten Sleman",
                'propinsi' => "Daerah Istimewa Yogyakarta",
                'idhari' => "1",
                'jam' => "17:38",
                'language' => "ID",
                'iduser' => "1"
            );
        } else {
            $json_param_post = array(
                'latitude' => $this->session->userdata('latitude'),
                'longitude' => $this->session->userdata('longitude'),
                'kabupaten' => $this->session->userdata('kabupatenpilih'),
                'propinsi' => $this->session->userdata('propinsipilih'),
                'idhari' => "1",
                'jam' => "17:38",
                'language' => "ID",
                'iduser' => "1"
            );
        }

        $jsondata = curl_post_data(
            $this->urllanding,
                array('jsonParamAwal' => json_encode($json_param_post))
        );
        
        $jsonDataIklanKanan = curl_post_data(
            $this->urliklankanan,
            array('jsonParamAwal' => json_encode($json_param_post))
        );
        
        
        $data = json_decode($jsondata, TRUE);
        $data = $data['0'];
        
        
        
        $isdiluarDaerahCakupan = $data['isdiluarDaerahCakupan']; 
        $jsonDataPrimer = $data['jsonDataPrimer']; 
        $jsonDataPrimerArr = json_decode($jsonDataPrimer,true);
         //print_r($jsonDataPrimerArr[0]);        
        $jsonLandingWisata = $jsonDataPrimerArr[0]['jsonLandingWisata']; 
        $jsonLandingWisataArr = json_decode($jsonLandingWisata,true);
        $jsonLandMark = $jsonLandingWisataArr[0]['jsonLandMark'];
        $jsonWisata = $jsonLandingWisataArr[0]['jsonWisata'];
        $jsonLandMarkArr = json_decode($jsonLandMark,true);
        $jsonWisataArr = json_decode($jsonWisata,true);
        //print_r($jsonWisataArr);        
        $judul =$jsonWisataArr[0]['judul'];
        $jsonDataWisata = $jsonWisataArr[0]['data'];
        $jsonDataWisataArr = json_decode($jsonDataWisata,true);;
        
        $jsonDataIklanKananArr = json_decode($jsonDataIklanKanan,TRUE);
        $IklanKanan = $jsonDataIklanKananArr[0]['IklanKanan'];
        $IklanKananArr = json_decode($IklanKanan, TRUE);

        $JsonIklanBanner = $IklanKananArr[0]['jsonIklanBar'];
        $jsonIklan1 = $IklanKananArr[0]['jsonIklanData1'];
        $jsonIklanPropinsi = $IklanKananArr[0]['jsonIklanPropinsi'];
        $jsonIklan2 = $IklanKananArr[0]['jsonIklanData2'];
        $jsonIklanKabupaten = $IklanKananArr[0]['jsonIklanKabupaten'];
        $jsonTopRank = $IklanKananArr[0]['jsonTopRank'];
        $jsonWisataPopuler = $IklanKananArr[0]['jsonWisataPopuler'];

        $JsonIklanBanner = json_decode($JsonIklanBanner, TRUE);;
        $jsonIklan1 = $IklanKananArr[0]['jsonIklanData1'];
        $jsonIklanPropinsi = $IklanKananArr[0]['jsonIklanPropinsi'];
        $jsonIklan2 = $IklanKananArr[0]['jsonIklanData2'];
        $jsonIklanKabupaten = $IklanKananArr[0]['jsonIklanKabupaten'];
        $jsonTopRank = $IklanKananArr[0]['jsonTopRank'];
        $jsonWisataPopuler = $IklanKananArr[0]['jsonWisataPopuler'];

        $jsonIklan1Arr = json_decode($jsonIklan1,TRUE);
        $jsonIklan2Arr = json_decode($jsonIklan2,TRUE);
        

        $dataparam = array(
            'iklan' => array(
                'iklanpropinsi' => array(
                    'judul' => $jsonIklan1Arr[0]['JudulGroup'],
                    'data' => $jsonIklan1Arr[0]['data'],
                ),

                'jenismasakan' => array(
                    'judul' => $jsonIklan2Arr[0]['JudulGroup'],
                    'data' => $jsonIklan2Arr[0]['data'],
                ),

                'toprange' => array(
                    'judul' => 'Top Rank',
                    'data' => json_encode(
                        array('data' => array(
                            array(
                                'nama' => 'Susana Ambarwati',
                                'ulasan' => 1123,
                                'following' => 981,
                                'linkthumbnail' => 'assets/icon/avatar1.png',
                                'icon' => 'iconikuti.svg'
                            ),
                            array(
                                'nama' => 'Riana Indah Kurnia',
                                'ulasan' => 928,
                                'following' => 820,
                                'linkthumbnail' => 'assets/icon/avatar2.png',
                                'icon' => 'iconmengikuti.svg'
                            ),
                            array(
                                'nama' => 'Susana Ambarwati',
                                'ulasan' => 928,
                                'following' => 820,
                                'linkthumbnail' => 'assets/icon/avatar1.png',
                                'icon' => 'icondiikuti.svg'

                            ),
                            array(
                                'nama' => 'Susana Ambarwati',
                                'ulasan' => 928,
                                'following' => 820,
                                'linkthumbnail' => 'assets/icon/avatar1.png',
                                'icon' => 'iconsalingmengikuti.svg'

                            ),
                            array(
                                'nama' => 'Riana Indah Kurnia',
                                'ulasan' => 928,
                                'following' => 820,
                                'linkthumbnail' => 'assets/icon/avatar2.png',
                                'icon' => 'iconikuti.svg'

                            ),
                            array(
                                'nama' => 'Susana Ambarwati',
                                'ulasan' => 928,
                                'following' => 820,
                                'linkthumbnail' => 'assets/icon/avatar1.png',
                                'icon' => 'iconikuti.svg'

                            ),
                        ))
                    )
                )),
            'footkategori' => '',

            // View
            'landmark' => $jsonLandMarkArr,
            'title' => $judul,
            'call_page' => 'wisata/view',
            'datawisata' => $jsonDataWisataArr,
        );
        

        $this->parser->parse(MASTER_PAGE, $dataparam);
    }    

}