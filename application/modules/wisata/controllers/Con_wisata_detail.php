<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Con_wisata_detail extends MY_Core
{
    function __construct(){
        parent::__construct();

        $this->DetailWisata = curl_url() . "Ctr_detail_kuliner/getDetailwisata";
        $this->urliklankanan = curl_url() . "Ctr_iklan/getJsonIklanDetailKuliner";
    }

    function index($page_tab='',$idx = ''){        
        $provinsi = $this->session->userdata('propinsipilih');
        $kabupaten = $this->session->userdata('kabupatenpilih');
        $latitude = $this->session->userdata('latitude');
        $longitude = $this->session->userdata('longitude');
            switch ($page_tab) {
                case 'foto'     : $page_view = "detail-foto";      break;
                case 'ulasan'   : $page_view = "detail-ulasan";    break;
                default         : $page_view = "detail-blog";      break;
            }
            
    
        $limit = $this->input->post('limtimage');
        if ($limit === null) {
            $limit = 12;
        }
        $paramimage = array('idproduk' => $idx,
            'start' => 0,
            'limit' => 20
        );

        $url = $this->DetailWisata;
        $data = curl_post_data($url, array(
            'jsonParamDetail' => json_encode(
                array(
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'idx' => $idx,
                    'language' => 'ID'
                )
            ))
        );
        
     //echo $data;           
    if ($data && count($data) > 0) {
            
        $data = json_decode($data, true);
        //echo $data[0]['idx'];
        
        $jsonDataIklanKanan = curl_post_data(
            $this->urliklankanan,
            array(
            'jsonIklanDetailKuliner' => json_encode(
                array(
                    'latitude' => -7.74766584,
                    'longitude' => 110.42292105,
                    'kabupaten'=>'Kabupaten Sleman',
                    'propinsi'=>'Daerah Istimewa Yogyakarta',                    
                    'language' => 'ID',
                    'iduser'=>'1',
                    'idproduk'=>$data[0]['idx']
                )
            
        
        )));
        
       // print_r($data); 
        $dataimage = $this->getDataByIdx($paramimage);

        
//            $jsonPenilaian = json_decode($data[0]['jsonPenilaian'], 1);
//            $jsonScore = json_decode($jsonPenilaian[0]['jsonSkor'], 1);
            $jsonImageDetail = json_decode($data[0]['jsonImageDetail'], 1);
            $jsonFasilitas = json_decode($data[0]['jsonFasilitas'], 1);

            
            
        $IklanKananArr = json_decode($jsonDataIklanKanan,TRUE);
        //print_r($IklanKananArr);
//        $IklanKanan = $jsonDataIklanKananArr[0]['IklanKanan'];
//        $IklanKananArr = json_decode($IklanKanan, TRUE);

//        $JsonIklanBanner = $IklanKananArr[0]['jsonIklanBar'];
        $jsonIklan1 = $IklanKananArr[0]['jsonIklanData1'];
        $jsonIklanPropinsi = $IklanKananArr[0]['jsonIklanPropinsi'];
        $jsonIklan2 = $IklanKananArr[0]['jsonIklanData2'];
        $jsonIklanKabupaten = $IklanKananArr[0]['jsonIklanKabupaten'];
        $jsonTopRank = $IklanKananArr[0]['jsonTopRank'];
        $jsonWisataPopuler = $IklanKananArr[0]['jsonWisataPopuler'];

//        $JsonIklanBanner = json_decode($JsonIklanBanner, TRUE);;
        $jsonIklan1 = $IklanKananArr[0]['jsonIklanData1'];
        $jsonIklanPropinsi = $IklanKananArr[0]['jsonIklanPropinsi'];
        $jsonIklan2 = $IklanKananArr[0]['jsonIklanData2'];
        $jsonIklanKabupaten = $IklanKananArr[0]['jsonIklanKabupaten'];
        $jsonTopRank = $IklanKananArr[0]['jsonTopRank'];
        $jsonWisataPopuler = $IklanKananArr[0]['jsonWisataPopuler'];

        $jsonIklan1Arr = json_decode($jsonIklan1,TRUE);
        $jsonIklan2Arr = json_decode($jsonIklan2,TRUE);
        $jsonWisataPopulerArr = json_decode($jsonWisataPopuler,TRUE);

            $dataparam = array(
                'data' => $data[0],
                'jsonFasilitas1' => json_decode($jsonFasilitas[0]['fasilitas'], 1),
                'jsonImageDetail' => json_decode($data[0]['jsonImageDetail'], 1),                
                'jsonImageMenu' => array(
                    'data' => $jsonImageDetail,
                    'images' => $jsonImageDetail[0]['jmlFoto'] > 0 ? json_decode($jsonImageDetail[0]['data'], 1) : array()
                ),
                'dataimage' => $dataimage,
                'idx' => $idx,
                'title' => $data[0]['JudulProduk'],


                'iklan' => array(
                'iklanpropinsi' => array(
                    'judul' => $jsonIklan1Arr[0]['JudulGroup'],
                    'data' => $jsonIklan1Arr[0]['data'],
                ),

                'iklankabupaten' => array(
                    'judul' => $jsonIklan2Arr[0]['JudulGroup'],
                    'data' => $jsonIklan2Arr[0]['data'],
                ),

                'wisatapopuler' => array(
                    'judul' => $jsonWisataPopulerArr[0]['judul'],
                    'data' => $jsonWisataPopulerArr[0]['data'],
                ),
            ),
                'call_page' => 'wisata/'.$page_view,
            );

            $this->parser->parse(MASTER_PAGE, $dataparam);
        } else {
            show_404();
        }    
    
        
            //$this->parser->parse(MASTER_PAGE, $dataparam);
        // } else {
        //     show_404();
        // }
    }

    function getDataByIdx($data){
        $param = array('jsonParamGaleriFoto'=>json_encode($data));

        $jsondata = curl_post_data(curl_url()."ctrlandingpage/getJsonGaleriFoto", $param);
        $xjsondata = json_decode($jsondata, 1);
        $xdata =  json_decode($xjsondata[0]['jsonGaleriFoto'], 1);
        $jumlahfoto = $xdata[0]['jmlGaleriFoto'];
        $dataimage = json_decode($xdata[0]['data'], 1);
        return $dataimage;
    }
    
    function getIklanKulinerSekitar($idx){
        $sessionlat = $this->session->userdata('lat');
        $sessionlong = $this->session->userdata('long');
        $xnamapropinsipilih = $this->session->userdata('namapropinsipilih');
        $xnamakabupatenpilih = $this->session->userdata('namakabupatenpilih');

        $dataiklan = curl_post_data($this->IklanKuliner, array(
            'jsonIklanDetailKuliner' => json_encode(
                array(
                    'latitude' => $sessionlat,
                    'longitude' => $sessionlong,
                    'kabupaten' => $xnamakabupatenpilih,
                    'propinsi' => $xnamapropinsipilih,
                    'idproduk' => $idx,
                    'iduser' => '1',
                    'language' => 'ID'
                )
            ))
        );

        if ($dataiklan && count($dataiklan) > 0) {
            $dataiklanArr = json_decode($dataiklan, true);

            $jsonKulinerSekitarArray = json_decode($dataiklanArr[0]['jsonKulinerSekitar'], 1);
            $jsonKulinerSekitarJudulGroup = $jsonKulinerSekitarArray[0]['JudulGroup'];
            $jsonKulinerSekitar = json_decode($jsonKulinerSekitarArray[0]['data']);
            $KulinerSekitar = array();
            foreach($jsonKulinerSekitar as $aaa => $bbb){
                foreach ($bbb as $ccc => $ddd){
                    $KulinerSekitar[$aaa] = $ddd;
                }
            }

            $jsonKulinerKategoriArray = json_decode($dataiklanArr[0]['jsonKulinerKategori'], 1);
            $jsonKulinerKategoriJudul = $jsonKulinerKategoriArray[0]['JudulGroup'];
            $jsonKulinerKategori = json_decode($jsonKulinerKategoriArray[0]['data']);
            $KulinerKategori = array();
            foreach($jsonKulinerKategori as $aaa => $bbb){
                foreach ($bbb as $ccc => $ddd){
                    $KulinerKategori[$aaa] = $ddd;
                }
            }

            $jsonWisataPopulerArray = json_decode($dataiklanArr[0]['jsonWisataPopuler'], 1);
            $jsonWisataPopulerJudul = $jsonWisataPopulerArray[0]['judul'];
            $jsonWisataPopuler = json_decode($jsonWisataPopulerArray[0]['data']);

            $WisataPopuler = array();
            foreach($jsonWisataPopuler as $aaa => $bbb){
                foreach ($bbb as $ccc => $ddd){
                    $WisataPopuler[$aaa] = $ddd;
                }
            }
            //print_r(json_decode(json_encode($KulinerSekitar), True));
            //exit();


            return array(
                'iklankulinersekitar' => array(
                    'judul' => $jsonKulinerSekitarJudulGroup,
                    'data' => json_decode(json_encode($KulinerSekitar), TRUE)
                ),
                'iklankategori' => array(
                    'judul' => $jsonKulinerKategoriJudul,
                    'data' => json_decode(json_encode($KulinerKategori), TRUE)
                ),
                'wisatapopuler' => array(
                    'judul' => $jsonWisataPopulerJudul,
                    'data' => json_decode(json_encode($WisataPopuler), TRUE)
                ),
            );
        }

        return false;
    }
}