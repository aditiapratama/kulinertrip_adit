<?php defined('BASEPATH') OR exit('No direct script access allowed');

class con_kuliner_detail extends MY_Core
{
    function __construct(){
        parent::__construct();

        $this->DetailKuliner = curl_url() . "Ctr_detail_kuliner/getDetailKuliner";
        $this->urliklankanan = curl_url() . "Ctr_iklan/getJsonIklanDetailKuliner";
        $this->laporanuser = curl_url() . "ctrlaporanuser/POSTLaporan";
    }

    function index($page_tab='',$idx = ''){        
        $limit = $this->input->post('limtimage');
        if ($limit === null) {
            $limit = 12;
        }
        
//        $provinsi = $this->session->userdata('propinsipilih');               
//        $kabupaten = $this->session->userdata('kabupatenpilih'); 
//        $latitude = $this->session->userdata('latitude');               
//        $longitude = $this->session->userdata('longitude');
        $latitude = (!empty($latitude)? $latitude : '-7.74766584');
            $longitude = (!empty($longitude) ? $longitude : '110.42292105');
            $kabupaten = (!empty($kabupaten) ? $kabupaten : 'Kabupaten Sleman');
            $provinsi = (!empty($propinsi) ? $propinsi : 'Daerah Istimewa Yogyakarta');
                                

        $url = $this->DetailKuliner;
        $data = curl_post_data($url, array(
            'jsonParamDetail' => json_encode(
                array(
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'idx' => $idx,
                    'language' => 'ID',
                    'iduser' => "1",
                    'idsession'=>"1",
                    'idakses' =>"3" 
                )
            ))
        );
       //echo $data;

        $paramimage = array('idproduk' => $idx,
            'start' => 0,
            'limit' => 20
        );
        
        if ($data && count($data) > 0) {
            
        $data = json_decode($data, true);
        //print_r($data);
        $xmap =$data[0]['mapaddress'];
        $xarrmap = explode(",", $xmap);
        
        $jsonDataIklanKanan = curl_post_data(
            $this->urliklankanan,
            array(
            'jsonIklanDetailKuliner' => json_encode(
                array(
                    'latitude' => $xarrmap[0],
                    'longitude' => $xarrmap[1],
                    'kabupaten'=>$kabupaten,
                    'propinsi'=>$provinsi,                    
                    'language' => 'ID',
                    'iduser'=>'1',
                    'idproduk'=>$data[0]['idx']
                )
            
        
        )));
        $dataimage = $this->getDataByIdx($paramimage);
            $jsonPenilaian = json_decode($data[0]['jsonPenilaian'], 1);
            $jsonScore = json_decode($jsonPenilaian[0]['jsonSkor'], 1);
            $jsonImageDetail = json_decode($data[0]['jsonImageMenu'], 1);
            $jsonFasilitas = json_decode($data[0]['jsonFasilitas'], 1);

            switch ($page_tab) {
                case 'foto'     : $page_view = "detail-foto";      break;
                case 'ulasan'   : $page_view = "detail-ulasan";    break;
                default         : $page_view = "detail-informasi"; break;
            }
            
        $IklanKananArr = json_decode($jsonDataIklanKanan,TRUE);
        $jsonIklan1 = $IklanKananArr[0]['jsonIklanData1'];
        $jsonIklanPropinsi = $IklanKananArr[0]['jsonIklanPropinsi'];
        $jsonIklan2 = $IklanKananArr[0]['jsonIklanData2'];
        $jsonIklanKabupaten = $IklanKananArr[0]['jsonIklanKabupaten'];
        $jsonTopRank = $IklanKananArr[0]['jsonTopRank'];
        $jsonWisataPopuler = $IklanKananArr[0]['jsonWisataPopuler'];

        $jsonIklan1 = $IklanKananArr[0]['jsonIklanData1'];
        $jsonIklanPropinsi = $IklanKananArr[0]['jsonIklanPropinsi'];
        $jsonIklan2 = $IklanKananArr[0]['jsonIklanData2'];
        $jsonIklanKabupaten = $IklanKananArr[0]['jsonIklanKabupaten'];
        $jsonTopRank = $IklanKananArr[0]['jsonTopRank'];
        $jsonWisataPopuler = $IklanKananArr[0]['jsonWisataPopuler'];
        $jsonJadwalBuka = json_decode($data[0]['jsonJadwalBuka']);

        $jsonIklan1Arr = json_decode($jsonIklan1,TRUE);
        $jsonIklan2Arr = json_decode($jsonIklan2,TRUE);
        $jsonWisataPopulerArr = json_decode($jsonWisataPopuler,TRUE);
            $imgslide = json_decode($data[0]['jsonImageDetail'], TRUE);
            $dataparam = array(
                'data' => $data[0],
                'informasijadwalbuka'  => $jsonJadwalBuka,
                'jsonFasilitas1' => json_decode($jsonFasilitas[0]['fasilitas'], 1),
                'jsonImageDetail' => json_decode($data[0]['jsonImageDetail'], 1),
                'penilaian' => array(
                    'data' => $jsonPenilaian[0],
                    'score' => $jsonScore
                ),
                'jsonImageMenu' => array(
                    'data' => $jsonImageDetail,
                    'images' => $jsonImageDetail[0]['jmlFoto'] > 0 ? json_decode($jsonImageDetail[0]['data'], 1) : array()
                ),
                'dataimage' => $dataimage,
                'idx' => $idx,
                'title' => $data[0]['JudulProduk'],
                'slide' => array(
                    'jumlah' => (!empty($imgslide[0]['jmlFoto']) ? $imgslide[0]['jmlFoto'] : 0),
                    'data' => (!empty($imgslide[0]['data']) ? json_decode($imgslide[0]['data'], TRUE) : array())
                ),


                'iklan' => array(
                    'iklanpropinsi' => array(
                        'judul' => $jsonIklan1Arr[0]['JudulGroup'],
                        'data' => $jsonIklan1Arr[0]['data'],
                        'tootipjarak'=>"Berdasarkan Titik Pusat Keramaian Kota"
                    ),

                    'iklankabupaten' => array(
                        'judul' => $jsonIklan2Arr[0]['JudulGroup'],
                        'data' => $jsonIklan2Arr[0]['data'],
                        'tootipjarak'=>"Berdasarkan Titik ".$data[0]['kategoritext']." ".$data[0]['JudulProduk']
                    ),
                    

                    'toprange' => array(
                        'judul' => 'Top Rank',
                        'data' => json_encode(
                            array('data' => array(
                                array(
                                    'nama' => 'Susana Ambarwati',
                                    'ulasan' => 1123,
                                    'following' => 981,
                                    'linkthumbnail' => 'assets/icon/avatar1.png',
                                    'icon' => 'iconikuti.svg'
                                ),
                                array(
                                    'nama' => 'Riana Indah Kurnia',
                                    'ulasan' => 928,
                                    'following' => 820,
                                    'linkthumbnail' => 'assets/icon/avatar2.png',
                                    'icon' => 'iconmengikuti.svg'
                                ),
                                array(
                                    'nama' => 'Susana Ambarwati',
                                    'ulasan' => 928,
                                    'following' => 820,
                                    'linkthumbnail' => 'assets/icon/avatar1.png',
                                    'icon' => 'icondiikuti.svg'
                                ),
                                array(
                                    'nama' => 'Susana Ambarwati',
                                    'ulasan' => 928,
                                    'following' => 820,
                                    'linkthumbnail' => 'assets/icon/avatar1.png',
                                    'icon' => 'iconsalingmengikuti.svg'
                                ),
                                array(
                                    'nama' => 'Riana Indah Kurnia',
                                    'ulasan' => 928,
                                    'following' => 820,
                                    'linkthumbnail' => 'assets/icon/avatar2.png',
                                    'icon' => 'iconikuti.svg'
                                ),

                            ))
                        )
                    ),

                    'wisatapopuler' => array(
                        'judul' => $jsonWisataPopulerArr[0]['judul'],
                        'data' => $jsonWisataPopulerArr[0]['data'],
                        'tootipjarak'=>"Berdasarkan Titik ".$data[0]['kategoritext']." ".$data[0]['JudulProduk']
                    ),
                ),
                'call_page' => 'kuliner/'.$page_view,
                'imageEdit' => "assets/icon/pencilicon.svg",
                'imagetelepon' => "assets/icon/icon_Telphon.svg",
                'imagewarning' => "assets/icon/logo.png",
                'content'   =>[
                            "title"             => "Laporan",
                            "subtitleinformasi" => "Perbahuri Informasi",
                            "titlesuccess"      => "Tanpa Judul",
                            "titlewarning"      => "Laporan",
                            'titlewarningparagraf' => "Anda belum memberikan Pembaharuan Informasi apapun, pilih tetap untuk melanjutkan pembeharuan informasi",
                            'titlesuccessparagraf' => "Terimakasih atas pembaharuan informasi yang anda berikan. semoga hari anda menyenangkan"
                        ],
            );

            $this->parser->parse(MASTER_PAGE, $dataparam);
        } else {
            show_404();
        }
    }


    function temp_detail($tab="informasi"){
        switch ($tab) {
            case 'foto'     : $page_view = "temp-detail-foto";      break;
            case 'ulasan'   : $page_view = "temp-detail-ulasan";    break;
            default         : $page_view = "temp-detail-informasi"; break;
        }
        $dataparam = array(
            'image_menu' => 4,
            'image_foto_360' => 8,
            'image_foto' => 8,
            'call_page' => 'kuliner/'.$page_view
        );

        $this->parser->parse(MASTER_PAGE, $dataparam);
    }

    function getDataByIdx($data){
        $param = array('jsonParamGaleriFoto'=>json_encode($data));

        $jsondata = curl_post_data(curl_url()."ctrlandingpage/getJsonGaleriFoto", $param);
        $xjsondata = json_decode($jsondata, 1);
        $xdata =  json_decode($xjsondata[0]['jsonGaleriFoto'], 1);
        $jumlahfoto = $xdata[0]['jmlGaleriFoto'];
        $dataimage = json_decode($xdata[0]['data'], 1);
        return $dataimage;
    }
    
    function getIklanKulinerSekitar($idx){
        $sessionlat = $this->session->userdata('lat');
        $sessionlong = $this->session->userdata('long');
        $xnamapropinsipilih = $this->session->userdata('namapropinsipilih');
        $xnamakabupatenpilih = $this->session->userdata('namakabupatenpilih');

        $dataiklan = curl_post_data($this->IklanKuliner, array(
            'jsonIklanDetailKuliner' => json_encode(
                array(
                    'latitude' => $sessionlat,
                    'longitude' => $sessionlong,
                    'kabupaten' => $xnamakabupatenpilih,
                    'propinsi' => $xnamapropinsipilih,
                    'idproduk' => $idx,
                    'iduser' => '1',
                    'language' => 'ID'
                )
            ))
        );

        if ($dataiklan && count($dataiklan) > 0) {
            $dataiklanArr = json_decode($dataiklan, true);

            $jsonKulinerSekitarArray = json_decode($dataiklanArr[0]['jsonKulinerSekitar'], 1);
            $jsonKulinerSekitarJudulGroup = $jsonKulinerSekitarArray[0]['JudulGroup'];
            $jsonKulinerSekitar = json_decode($jsonKulinerSekitarArray[0]['data']);
            $KulinerSekitar = array();
            foreach($jsonKulinerSekitar as $aaa => $bbb){
                foreach ($bbb as $ccc => $ddd){
                    $KulinerSekitar[$aaa] = $ddd;
                }
            }

            $jsonKulinerKategoriArray = json_decode($dataiklanArr[0]['jsonKulinerKategori'], 1);
            $jsonKulinerKategoriJudul = $jsonKulinerKategoriArray[0]['JudulGroup'];
            $jsonKulinerKategori = json_decode($jsonKulinerKategoriArray[0]['data']);
            $KulinerKategori = array();
            foreach($jsonKulinerKategori as $aaa => $bbb){
                foreach ($bbb as $ccc => $ddd){
                    $KulinerKategori[$aaa] = $ddd;
                }
            }

            $jsonWisataPopulerArray = json_decode($dataiklanArr[0]['jsonWisataPopuler'], 1);
            $jsonWisataPopulerJudul = $jsonWisataPopulerArray[0]['judul'];
            $jsonWisataPopuler = json_decode($jsonWisataPopulerArray[0]['data']);

            $WisataPopuler = array();
            foreach($jsonWisataPopuler as $aaa => $bbb){
                foreach ($bbb as $ccc => $ddd){
                    $WisataPopuler[$aaa] = $ddd;
                }
            }

            return array(
                'iklankulinersekitar' => array(
                    'judul' => $jsonKulinerSekitarJudulGroup,
                    'data' => json_decode(json_encode($KulinerSekitar), TRUE)
                ),
                'iklankategori' => array(
                    'judul' => $jsonKulinerKategoriJudul,
                    'data' => json_decode(json_encode($KulinerKategori), TRUE)
                ),
                'wisatapopuler' => array(
                    'judul' => $jsonWisataPopulerJudul,
                    'data' => json_decode(json_encode($WisataPopuler), TRUE)
                ),
            );
        }

        return false;
    }
    
    function dolaporan(){
        
        $key = $_POST['key'];
         $produk = $_POST['produk'];
         $idx = $_POST['idx'];
         $informasi = $_POST['informasi'];
        
        
        $iduser = "a001";
        $datalaporan = curl_post_data($this->laporanuser, array(
            'jsonLaporanUmum' => json_encode(
                array(
                    'Token' => "0",
                    'idx' => "0",
                    'idproduk' => $idx,
                    'iduser' => $iduser,
                    'idjenisaktifitas' => '3',
                    'idnamafiled' => $key,
                    'Laporan' => $informasi,                    
                )
            ))
        );
        
        echo null;
        
            
    }
    
    public function insertUserImage()
    {   

        $this->load->helper('string');
        $this->load->library('image_lib');

        $rancode           = $this->input->post('rancode');
        $name           = $this->input->post('name');
        $title          = $this->input->post('title');
        $description    = $this->input->post('description');

        $base       = 'public/image/upload';
        $random_name= random_string('unique', 10);

        if (!is_dir($base)) {
            mkdir('./'.$base, 0777, TRUE);
        }

        $image_user = array();

        for($i=0; $i<count($title); $i++)
        {
            $image_name = $random_name.'_'.$i;
            $result     = $this->upload_image( $_FILES['image'.$i], $base.'/', $image_name);
            if($result != FALSE)
            {
                $data_image = array(
                     'linkimage' => $result,
                     'rancodeproduk' => 'TN84_1509698734067',
                     'subjectketerangan' => $title[$i],
                     'keteranganimage' => $description[$i]
                    );

                array_push($image_user, $data_image);
            }
        }

        $image_user_json = json_encode($image_user);
        echo $image_user_json;
    }


    public function insertUlasan()
    {   

        $this->load->helper('string');
        $this->load->library('image_lib');

        $id_produk              = $this->input->post('id_produk');
        $id_user                = $this->input->post('id_user');
        $subject                = $this->input->post('subject');
        $isi_ulasan             = $this->input->post('isi_ulasan');
        $is_draft               = $this->input->post('is_draft');
        $total_biaya            = $this->input->post('total_biaya');
        $jml_orang              = $this->input->post('jumlah_orang_saat_datang');
        $via_app                = $this->input->post('via_app');
        $status_kunjungan       = $this->input->post('status_kunjungan');
        $rancode                = $this->input->post('rancode');
        $score                  = $this->input->post('score');
        $ulasan_tambahan        = $this->input->post('ulasan_tambahan');
        $image['name']          = $this->input->post('name');
        $image['title']         = $this->input->post('title');
        $image['description']   = $this->input->post('description');

        $ulasan = array(
          'idProduk' => $id_produk,
          'iduser' => $id_user,
          'subject' => $subject,
          'ulasan' => $isi_ulasan,
          'isdraft' => $is_draft,
          'TotalBiaya' => $total_biaya,
          'jmlorangsaatdatang' => $jml_orang,
          'viaApp' => $via_app,
          'statuskunjungan' => $status_kunjungan,
          'randcodeulasan' => $rancode,
          // 'jsonParamUlasanTambahan' => array(),
          'jsonParamUlasanTambahan' => $ulasanTambahan,
          'jsonParamhastag' => array(),
          'jsonParamusertag' => array(),
          // 'jsonParamScore' => array(),
          'jsonParamScore' => $score,
          'jsonParamImage' => array()
        );


        for($i=0; $i<count($image['title']); $i++)
        {
            $image_name = $random_name.'_'.$i;
            $result     = $this->upload_image( $_FILES['image'.$i], $base.'/', $image_name);
            if($result != FALSE)
            {
                $data_image = array(
                     'linkimage' => $result,
                     'rancodeproduk' => 'TN84_1509698734067',
                     'subjectketerangan' => $image['title'][$i],
                     'keteranganimage' => $image['description'][$i]
                    );

                array_push($ulasan['jsonParamImage'], $data_image);
            }
        }

        $base       = 'public/image/upload';
        $random_name= random_string('unique', 10);

        if (!is_dir($base)) {
            mkdir('./'.$base, 0777, TRUE);
        }

        $ulasan_json = json_encode($ulasan);
        echo $ulasan_json;


        // {"idProduk":"TN11YO17SLM00011", "iduser":"a001", "subject":"Test Ulasan 1", "ulasan":"Test Ulasan 1Test Ulasan 1Test Ulasan 1Test Ulasan 1Test Ulasan 1Test Ulasan 1Test Ulasan 1Test Ulasan 1Test Ulasan 1Test Ulasan 1Test Ulasan 1Test Ulasan 1Test Ulasan 1Test Ulasan 1Test Ulasan 1Test Ulasan 1Test Ulasan 1Test Ulasan 1Test Ulasan 1Test Ulasan 1", "isdraft":"N", "TotalBiaya":"300000", "jmlorangsaatdatang":"12", "viaApp":"3", "statuskunjungan":"1", "randcodeulasan":"uls6789", "jsonParamUlasanTambahan":[{"NamaMenu":"Bakso","HargaPerporsi":"20000","jmlstar":"2","ismenurekumendasi":"N"},{"NamaMenu":"Rawon","HargaPerporsi":"10000","jmlstar":"4","ismenurekumendasi":"Y"}], "jsonParamhastag":[{"idrancodeproduk":"TN84_1509698734067","hastag":"bakso","idjenisproduk":"1","provinsi":"Dareah Istimewa Yogyakarta","kabupaten":"Sleman"},{"idrancodeproduk":"TN84_1509698734067","hastag":"baksoenak","idjenisproduk":"1","provinsi":"Dareah Istimewa Yogyakarta","kabupaten":"Sleman"}],"jsonParamusertag":[{"idusertag":"a002","idjenisaktifitas":"6"},{"idusertag":"a002","idjenisaktifitas":"6"}],"jsonParamScore":[{"idjenisscore":"1","score":"7"},{"idjenisscore":"2","score":"8"},{"idjenisscore":"3","score":"8"},{"idjenisscore":"4","score":"6"}],"jsonParamImage":[{"linkimage":"a002","rancodeproduk":"TN84_1509698734067","subjectketerangan":"Uji 1","keteranganimage":"Uji 1 Uji 1 Uji 1 Uji 1 Uji 1 Uji 1 Uji 1 Uji 1Uji 1Uji 1Uji 1Uji 1Uji 1vUji 1Uji 1Uji 1Uji 1Uji 1"},{"linkimage":"a002","rancodeproduk":"TN84_1509698734067","subjectketerangan":"Uji 1","keteranganimage":"Uji 1 Uji 1 Uji 1 Uji 1 Uji 1 Uji 1 Uji 1 Uji 1Uji 1Uji 1Uji 1Uji 1Uji 1vUji 1Uji 1Uji 1Uji 1Uji 1"}]}{"status":"FAIL", "Keterangan":"Tidak Boleh Double simpan" }

//         array (
//   'idProduk' => 'TN11YO17SLM00011',
//   'iduser' => 'a001',
//   'subject' => 'Test Ulasan 1',
//   'ulasan' => 'Test Ulasan 1Test Ulasan 1Test Ulasan 1Test Ulasan 1Test Ulasan 1Test Ulasan 1Test Ulasan 1Test Ulasan 1Test Ulasan 1Test Ulasan 1Test Ulasan 1Test Ulasan 1Test Ulasan 1Test Ulasan 1Test Ulasan 1Test Ulasan 1Test Ulasan 1Test Ulasan 1Test Ulasan 1Test Ulasan 1',
//   'isdraft' => 'N',
//   'TotalBiaya' => '300000',
//   'jmlorangsaatdatang' => '12',
//   'viaApp' => '3',
//   'statuskunjungan' => '1',
//   'randcodeulasan' => 'uls6789',
//   'jsonParamUlasanTambahan' => 
//   array (
//     0 => 
//     array (
//       'NamaMenu' => 'Bakso',
//       'HargaPerporsi' => '20000',
//       'jmlstar' => '2',
//       'ismenurekumendasi' => 'N',
//     ),
//     1 => 
//     array (
//       'NamaMenu' => 'Rawon',
//       'HargaPerporsi' => '10000',
//       'jmlstar' => '4',
//       'ismenurekumendasi' => 'Y',
//     ),
//   ),
//   'jsonParamhastag' => 
//   array (
//     0 => 
//     array (
//       'idrancodeproduk' => 'TN84_1509698734067',
//       'hastag' => 'bakso',
//       'idjenisproduk' => '1',
//       'provinsi' => 'Dareah Istimewa Yogyakarta',
//       'kabupaten' => 'Sleman',
//     ),
//     1 => 
//     array (
//       'idrancodeproduk' => 'TN84_1509698734067',
//       'hastag' => 'baksoenak',
//       'idjenisproduk' => '1',
//       'provinsi' => 'Dareah Istimewa Yogyakarta',
//       'kabupaten' => 'Sleman',
//     ),
//   ),
//   'jsonParamusertag' => 
//   array (
//     0 => 
//     array (
//       'idusertag' => 'a002',
//       'idjenisaktifitas' => '6',
//     ),
//     1 => 
//     array (
//       'idusertag' => 'a002',
//       'idjenisaktifitas' => '6',
//     ),
//   ),
//   'jsonParamScore' => 
//   array (
//     0 => 
//     array (
//       'idjenisscore' => '1',
//       'score' => '7',
//     ),
//     1 => 
//     array (
//       'idjenisscore' => '2',
//       'score' => '8',
//     ),
//     2 => 
//     array (
//       'idjenisscore' => '3',
//       'score' => '8',
//     ),
//     3 => 
//     array (
//       'idjenisscore' => '4',
//       'score' => '6',
//     ),
//   ),
//   'jsonParamImage' => 
//   array (
//     0 => 
//     array (
//       'linkimage' => 'a002',
//       'rancodeproduk' => 'TN84_1509698734067',
//       'subjectketerangan' => 'Uji 1',
//       'keteranganimage' => 'Uji 1 Uji 1 Uji 1 Uji 1 Uji 1 Uji 1 Uji 1 Uji 1Uji 1Uji 1Uji 1Uji 1Uji 1vUji 1Uji 1Uji 1Uji 1Uji 1',
//     ),
//     1 => 
//     array (
//       'linkimage' => 'a002',
//       'rancodeproduk' => 'TN84_1509698734067',
//       'subjectketerangan' => 'Uji 1',
//       'keteranganimage' => 'Uji 1 Uji 1 Uji 1 Uji 1 Uji 1 Uji 1 Uji 1 Uji 1Uji 1Uji 1Uji 1Uji 1Uji 1vUji 1Uji 1Uji 1Uji 1Uji 1',
//     ),
//   ),
// )
    }


    private function upload_image($file, $dir, $title)
    {
        $msg='Success';
        $errorCode = $file['error'];

        if ($errorCode === UPLOAD_ERR_OK) {
            $type = exif_imagetype($file['tmp_name']);

            if ($type) {
                $extension = image_type_to_extension($type);
                $src = $dir . $title . $extension;

                if ($type == IMAGETYPE_GIF || $type == IMAGETYPE_JPEG || $type == IMAGETYPE_PNG) {

                    if (file_exists($src)) {
                        unlink($src);
                    }

                    $result = move_uploaded_file($file['tmp_name'], $src);

                    if ($result) {
                        $msg = $title.$extension;
                    }else{
                        $msg = FALSE;
                    }

                } else {
                    $msg = FALSE;
                }
            } else {
                $msg = FALSE;
            }
        } else {
            $msg = FALSE;
        }

        return $msg;
    }
}
