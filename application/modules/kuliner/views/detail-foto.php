<!--
    Usahakan ngoding yang rapi
-->

<?php
$images_detail = json_decode($data['jsonImageDetail']);
$data_detail = json_decode($images_detail[0]->data);
$penilaian = json_decode($data['jsonPenilaian']);
$skor = json_decode($penilaian[0]->jsonSkor);
?>

<div class="container is-mobile">
    <div class="col-md-12 col-xs-12">
        <div id="header-detail-kuliner" class="is-foto-kuliner">
            <div class="col-sm-4 col-md-4">
                <div id="header-detail-kuliner-mobile" class="visible-xs-block">
                    <ul class="pull-left breadcrumb-location">
                        <li><?php echo $data['state']; ?></li>
                        <li><?php echo $data['kabupaten']; ?></li>
                        <li><?php echo $data['city']; ?></li>
                    </ul>
                    <div class="clearfix">
                        <h3 class="judul-item-wisata"><?php echo $data['JudulProduk'] . '-' . $data['area']; ?> <br>
                            <small><?php echo $data['kategoritext'] . " - " . $data['jenisMasakan'] ?> <span><a
                                            href=""><?php echo (!empty($data['jmlCabang']) && ($data['jmlCabang'] != '0 cabang')) ? "/" . $data['jmlCabang'] : ""; ?> </a></span>
                            </small>
                        </h3>
                    </div>
                </div>
                <figure class="gambar-profil-kuliner">
                    <img src="<?php echo $data['linkimage']; ?>" class="img-responsive">
                    <label class="atribut-image-kuliner top"><?php //echo $data_detail[0]->kategorifoto;
                        echo $data['JudulProduk'] . ' - ' . $data['area']; ?></label>
                    <ul class="atribut-image-kuliner bottom list-inline">
                        <li><i class="fa fa-thumbs-o-up"></i> <?php echo $data['jmlLikeFotoProfil']; ?></li>
                        <li><i class="fa fa-comment-o"></i> <?php echo $data['jmlKomentarFotoProfil']; ?></li>
                        <li><i class="fa fa-share-alt"></i></li>
                    </ul>
                </figure>
            </div>
            <div class="col-sm-8 col-md-8">
                <ul class="pull-left breadcrumb-location hidden-xs">
                    <li>
                        <a href="<?php echo base_url('kawasan/' . str_replace(" ", "-", $data['kawasanArea'])) . '.html'; ?>"
                           target="_blank"><?php echo $data['state']; ?></a></li>
                    <li>
                        <a href="<?php echo base_url('kawasan/' . str_replace(" ", "-", $data['kawasanArea'])) . '.html'; ?>"
                           target="_blank"><?php echo $data['kabupaten']; ?></a></li>
                    <li>
                        <a href="<?php echo base_url('kawasan/' . str_replace(" ", "-", $data['kawasanArea'])) . '.html'; ?>"
                           target="_blank"><?php echo $data['kawasanArea']; ?></a></li>
                </ul>
                <ul class="pull-right list-inline">
                    <li>
                        <button class="btn btn-primary btn-xs active-button"><img class="img-item-wisata"
                                                                                  style="width: 14px;"
                                                                                  src="<?php echo base_url('assets/icon/iconsimpanputih.svg'); ?>">&nbsp;Simpan
                        </button>
                    </li>

                    <!--                  <li><button class="btn btn-primary btn-xs deactive-button"><img class="img-item-wisata" src="<?php //echo base_url(); ?>assets/img/wisata/data-simpan-icon.svg">Share</button></li>-->
                </ul>
                <div class="clearfix"></div>
                <h3 class="judul-item-wisata hidden-xs"><?php echo $data['JudulProduk'] . ' - ' . $data['area']; ?> <br><span
                            class="fontgedekatdetail"><?php echo $data['kategoritext'] . " - " . $data['jenisMasakan']; ?>
                        <span class=""><?php echo (!empty($data['jmlCabang']) && ($data['jmlCabang'] != '0 cabang')) ? " / " : ""; ?>
                            <a href=""><?php echo (!empty($data['jmlCabang']) && ($data['jmlCabang'] != '0 cabang')) ? $data['jmlCabang'] : ""; ?></a> </span></span>
                </h3>

                <ul class="list-unstyled atribut-item-wisata">
                    <li class="alamat-kuliner"><img class="img-item-wisata"
                                                    src="<?php echo base_url('assets/icon/iconalamat.png'); ?>"> <span
                                class="kekanandetailatas"><?php echo $data['AlamatSurat'] . ', ' . $data['kabupaten'] . ' ' . $data['state']; ?></span>
                    </li>
                    <li class="status-jadwal-kuliner">


                        <?php
                        if (trim($data['KeteranganBuka']) === 'Buka'){
                        ?>
                        <img class="img-item-wisata" src="<?php echo base_url('assets/icon/iconbuka.png'); ?>"> <span
                                class="kekanandetailatas">
                                                   <span class="kekanandetailatasbuka">  <?php echo $data['KeteranganBuka']; ?></span> <?php echo $data['KeteranganJam']; ?>
                            <?php } else {
                                ?>
                                <img data-u="image" src="<?php echo base_url('assets/icon/icontutup.png') ?>"
                                     width="14px" data-toggle="tooltip" title="Tersimpan" data-placement="top"/> <span
                                        class="kekanandetailatas"> <?php echo $data['KeteranganBuka']; ?> </span>
                            <?php } ?>
                            <!--                                                <i class="fa fa-fw fa-clock-o <?php //echo ((!empty($buka[0]->isbuka)) ? (($buka[0]->isbuka == 'Y') ? 'text-green' : 'text-grey') : 'text-grey') ?> " onclick="produk"></i> <span ><?php //echo $datakuliner[$i]['jadwalBukaSekarang']; ?></span>-->
                                                  
                                           
                      </span> <span
                                class="tombol-jadwal-kuliner visible-xs-inline kekanandetailatas">Lihat jadwal</span>
                        <div class="jadwal-kuliner">
                            <?php
                            $jadwal_buka = json_decode($data['jsonJadwalBuka']);
                            foreach ($jadwal_buka as $jadwal) {
                                echo "<div><span>" . $jadwal->NamaHari . "</span><span>" . $jadwal->jambuka . "-" . $jadwal->jamtutup . " WIB</span>";
                                if (!empty($jadwal->keterangan)) "<span>" . $jadwal->keterangan . "</span>";
                                echo "</div>";
                            }
                            ?>
                    </li>
                    <li><img class="img-item-wisata" src="<?php echo base_url('assets/icon/iconharga.png'); ?>"> <span
                                class="kekanandetailatas"><?php echo 'Rp ' . $data['hargamin'] . '-Rp ' . $data['hargamaks']; ?></span>
                    </li>
                    <li><img class="img-item-wisata" src="<?php echo base_url('assets/icon/icontelepon.png'); ?>">
                        <span class="kekanandetailatas">
                        <?php
                        if (!empty($data['phonekontak'])) echo $data['phonekontak'];
                        elseif (!empty($data['phonekontak2'])) echo $data['phonekontak2'];
                        else echo '-';
                        ?>
                      </span>
                    </li>
                    <li>
                         
                      <span class="location" style="float:left;" data-toggle="tooltip"
                            title="Berdasarkan Titik Pusat Keramaian Kota" data-placement="bottom">
                      <img class="img-item-wisata" src="<?php echo base_url(); ?>assets/img/wisata/jarak-icon.svg"/> 
                      
                      <span class="kekanandetailatas">   <?php echo $data['jarak']; ?> km` </span>
                     
                      </span>
                        <span>
                      <ul class="list-unstyled list-inline text-right atribut-item-wisata">
                        <li><img class="img-item-wisata2"
                                 src="<?php echo base_url(); ?>assets/img/wisata/data-simpan-icon.svg"><?php echo 'Simpan ' . $data['jmlFavorit']; ?></li>
                        <li><img class="img-item-wisata2"
                                 src="<?php echo base_url(); ?>assets/img/wisata/data-ulasan-icon.svg"><?php echo 'Ulasan ' . $data['jmlUlasan']; ?></li>
                        <li><img class="img-item-wisata2"
                                 src="<?php echo base_url(); ?>assets/img/wisata/data-foto-icon.svg"><?php echo ' Foto ' . $data['jmlFoto']; ?></li>
                      </ul>
                    </span>
                    </li>
                </ul>


                <!-- sampai sini --->
            </div>
            <div class="clearfix"></div>
        </div>
        <?php
        $galeri_foto = json_decode($data['jsonGaleriFoto']);
        $data_galeri = json_decode($galeri_foto[0]->data);
        ?>
        <div class="row">
            <div class="col-md-9">

                <div id="detail-kuliner-informasi" class="tab-page">
                    <ul class="tabs-kuliner clearfix tabs-flying">
                        <a href="<?php echo site_url() . 'kuliner/informasi/' . $data['UrlPage'] . '.html'; ?>"
                           class="tab-item">Informasi</a>
                        <a target="_blank"
                           href="<?php echo site_url() . 'kuliner/foto/' . $data['UrlPage'] . '.html'; ?>"
                           class="tab-item active">Foto</a>
                        <a target="_blank"
                           href="<?php echo site_url() . 'kuliner/ulasan/' . $data['UrlPage'] . '.html'; ?>"
                           class="tab-item">Ulasan</a>
                    </ul>

                    <ul class="tabs-kuliner no-flying clearfix">
                        <a href="<?php echo site_url() . 'kuliner/informasi/' . $data['UrlPage'] . '.html'; ?>"
                           class="tab-item">Informasi</a>
                        <a target="_blank"
                           href="<?php echo site_url() . 'kuliner/foto/' . $data['UrlPage'] . '.html'; ?>"
                           class="tab-item active">Foto</a>
                        <a target="_blank"
                           href="<?php echo site_url() . 'kuliner/ulasan/' . $data['UrlPage'] . '.html'; ?>"
                           class="tab-item">Ulasan</a>
                    </ul>
                </div>

                <div id="foto-detail-kuliner">
                    <div class="pull-right upload-gambar-kuliner">
                        <?php if ($galeri_foto[0]->jmlGaleriFoto != 0) { ?>
                            Berbagi dengan kami
                            <button class="btn btn-danger"><img class="img-item-wisata"
                                                                src="<?php echo base_url(); ?>assets/img/wisata/data-foto-icon.svg">
                                Tambah Foto
                            </button>
                        <?php } else { ?>
                            <div class="text-center upload-gambar-kuliner">
                                Jadilah pengunggah foto yang pertama
                                <button class="btn btn-danger"><img class="img-item-wisata"
                                                                    src="<?php echo base_url(); ?>assets/img/wisata/data-foto-icon.svg">
                                    Tambah Foto
                                </button>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div id="detail-foto-360" class="detail-kuliner-box is-mobile">
                    <h3 class="detail-title">Foto 360&deg;</h3>


                    <a class="col-md-3 foto-360">
                        <img src="<?php echo base_url('assets/icon/foto360/foto360.png'); ?>" class="img-responsive">
                    </a>

                    <a class="col-md-3 foto-360">
                        <img src="<?php echo base_url('assets/icon/foto360/foto360a.png'); ?>" class="img-responsive">
                    </a>
                    <a class="col-md-3 foto-360">
                        <img src="<?php echo base_url('assets/icon/foto360/foto360b.png'); ?>" class="img-responsive">
                    </a>
                    <a class="col-md-3 foto-360">
                        <img src="<?php echo base_url('assets/icon/foto360/foto360c.png'); ?>" class="img-responsive">
                    </a>

                    <a class="col-md-3 foto-360">
                        <img src="<?php echo base_url('assets/icon/foto360/foto360.png'); ?>" class="img-responsive">
                    </a>

                    <a class="col-md-3 foto-360">
                        <img src="<?php echo base_url('assets/icon/foto360/foto360a.png'); ?>" class="img-responsive">
                    </a>
                    <a class="col-md-3 foto-360">
                        <img src="<?php echo base_url('assets/icon/foto360/foto360b.png'); ?>" class="img-responsive">
                    </a>
                    <a class="col-md-3 foto-360">
                        <img src="<?php echo base_url('assets/icon/foto360/foto360c.png'); ?>" class="img-responsive">
                    </a>

                    <div class="clearfix"></div>
                </div>

                <div id="foto-detail-kuliner" class="detail-kuliner-box is-mobile">
                    <h3 class="detail-title">Foto</h3>
                    <?php
                    $galeri_show = (count($data_galeri) > 20) ? 20 : count($data_galeri);
                    $sisa = count($data_galeri) - 20;
                    for ($i = 0; $i < $galeri_show; $i++) {
                        ?>
                        <a class="col-md-3 col-xs-6 foto-360">
                            <img src="<?php echo $data_galeri[$i]->linkimage; ?>" class="img-responsive">
                        </a>
                    <?php } ?>
                    <div class="clearfix"></div>
                    <?php
                    if ($sisa > 0) {
                        echo "<a href='' class='next-image'>Lihat Selanjutnya (+" . $sisa . ")</a>";
                    }
                    ?>
                </div>
                <?php $datahastag = "";
                $this->load->view('__hastag', $datahastag);
                ?>


            </div>
            <div class="col-md-3 style-content">
                <div style="margin-top: -20px">
                    <?php get_iklankanan(!empty($iklan) ? $iklan : false) ?>
                </div>
                <div class="clearfix"></div>
            </div>


        </div>
    </div>


</div>
</div>

</div>
<div class="clearfix"></div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() . 'assets/js/imam/module-2.js'; ?>"></script>