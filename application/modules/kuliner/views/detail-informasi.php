<!--
    Usahakan ngoding yang rapi
-->

<?php
$images_detail = json_decode($data['jsonImageDetail']);
$data_detail = json_decode($images_detail[0]->data);
$penilaian = json_decode($data['jsonPenilaian']);
$skor = json_decode($penilaian[0]->jsonSkor);
?>

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div id="header-detail-kuliner">
                <div class="col-sm-5 col-md-5">
                    <div id="header-detail-kuliner-mobile" class="visible-xs-block">
                        <ul class="pull-left breadcrumb-location">
                            <li><?php echo $data['state']; ?></li>
                            <li><?php echo $data['kabupaten']; ?></li>
                            <li>
                                <a href="<?php echo base_url('kawasan/' . str_replace(" ", "-", $data['kawasanArea'])) . '.html'; ?>"
                                   target="_blank"> &nbsp; <?php echo $data['kawasanArea']; ?></a>
                            </li>
                        </ul>
                        <div class="clearfix">
                            <h3 class="judul-item-wisata pull-left"><?php echo $data['JudulProduk'] . ' - ' . $data['area']; ?>
                                <br>
                                <small><?php echo $data['kategoritext'] . " - " . $data['jenisMasakan'] ?>
                                    <span><a href=""><?php echo (!empty($data['jmlCabang']) && ($data['jmlCabang'] != '0 cabang')) ? "/" . $data['jmlCabang'] : ""; ?> </a></span>
                                </small>
                            </h3>
                            <div class="col-point sum pull-right">
                                <div class="sum-score">
                                <span class="text-score"
                                      data-total-rating="restaurant"><?php echo $penilaian[0]->rating; ?></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <figure id="figur1" class="gambar-profil-kuliner" style="display: none;">
                        <img src="<?php echo $data['linkimage']; ?>" class="img-responsive">
                        <label class="atribut-image-kuliner top"><?php echo $data_detail[0]->kategorifoto ?></label>
                        <ul class="atribut-image-kuliner bottom list-inline">
                            <li><i class="fa fa-thumbs-o-up"></i><?php echo $data['jmlLikeFotoProfil']; ?></li>
                            <li><i class="fa fa-comment-o"></i><?php echo $data['jmlKomentarFotoProfil']; ?></li>
                            <li><i class="fa fa-share-alt"></i></li>
                        </ul>
                    </figure>

                    <figure id="figur2" class="gambar-profil-kuliner2">
                        <img src="<?php echo $data['linkimage']; ?>" class="img-responsive">
                        <label class="atribut-image-kuliner top"><?php echo $data['JudulProduk'] ?></label>
                        <ul class="atribut-image-kuliner bottom list-inline">
                            <li><i class="fa fa-thumbs-o-up"></i><?php echo $data['jmlLikeFotoProfil']; ?></li>
                            <li><i class="fa fa-comment-o"></i><?php echo $data['jmlKomentarFotoProfil']; ?></li>
                            <li><i class="fa fa-share-alt"></i></li>
                            <li style="display: block;">
                                <span class="pull-right"><?php if ($data['IsNonHalal'] !== "N") echo "Non Halal"; ?> </span>
                            </li>
                        </ul>
                    </figure>

                    <div class="image-thumbs hidden-xs">
                        <?php $i = 0;
                        while ($i < count($data_detail) && $i < 5) {
                            if ($i == 4) {
                                echo '<div id="get-zoom" data-ind="img_'. $i .'" class="image-thumb" data-full-image="' . $data_detail[$i]->linkimage . '"  data-title="' . $data_detail[$i]->kategorifoto . '"><img src="' . $data_detail[$i]->linkthumbnail . '" class="img-responsive">';
                                if (count($data_detail) > 5) {
                                    echo '<label><a href="' . site_url() . 'kuliner/foto/' . $data['UrlPage'] . '.html">+' . (count($data_detail) - 5) . ' lagi</a></label>';
                                }
                                echo '</div>';
                            } else {
                                echo '<div id="get-zoom" data-ind="'. $i .'" class="image-thumb" data-full-image="' . $data_detail[$i]->linkimage . '" data-title="' . $data_detail[$i]->kategorifoto . '"><img src="' . $data_detail[$i]->linkthumbnail . '" class="img-responsive"></div>';
                            }
                            $i++;
                        }
                        ?>
                    </div>
                </div>
                <div class="col-sm-7 col-md-7">
                    <ul class="pull-left breadcrumb-location hidden-xs">
                        <li>
                            <a href="<?php echo base_url('kawasan/' . str_replace(" ", "-", $data['kawasanArea'])) . '.html'; ?>"
                               target="_blank"><?php echo $data['state']; ?></a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('kawasan/' . str_replace(" ", "-", $data['kawasanArea'])) . '.html'; ?>"
                               target="_blank"><?php echo $data['kabupaten']; ?></a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('kawasan/' . str_replace(" ", "-", $data['kawasanArea'])) . '.html'; ?>"
                               target="_blank"><?php echo $data['kawasanArea']; ?></a>
                        </li>
                    </ul>
                    <ul class="pull-right list-inline">
                        <li>
                            <button class="btn btn-primary btn-xs active-button">
                                <img class="img-item-wisata" style="width: 14px;"
                                     src="<?php echo base_url('assets/icon/iconsimpanputih.svg'); ?>">&nbsp;Simpan
                            </button>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                    <h3 class="judul-item-wisata hidden-xs"><?php echo $data['JudulProduk'] . ' - ' . $data['area']; ?> <br><span
                                class="fontgedekatdetail"><?php echo $data['kategoritext'] . " - " . $data['jenisMasakan']; ?>
                            <span class=""><?php echo (!empty($data['jmlCabang']) && ($data['jmlCabang'] != '0 cabang')) ? " / " : ""; ?>
                                <a href=""><?php echo (!empty($data['jmlCabang']) && ($data['jmlCabang'] != '0 cabang')) ? $data['jmlCabang'] : ""; ?></a> </span></span>
                    </h3>

                    <ul class="list-unstyled atribut-item-wisata">
                        <li class="alamat-kuliner"><img class="img-item-wisata"
                                                        src="<?php echo base_url('assets/icon/iconalamat.png'); ?>"> <span
                                    class="kekanandetailatas"><?php echo $data['AlamatSurat'] . ', ' . $data['kabupaten'] . ' ' . $data['state']; ?></span>
                        </li>
                        <li class="status-jadwal-kuliner">


                            <?php
                            if (trim($data['KeteranganBuka']) === 'Buka'){
                            ?>
                            <img class="img-item-wisata" src="<?php echo base_url('assets/icon/iconbuka.png'); ?>"> <span
                                    class="kekanandetailatas">
                                                   <span class="kekanandetailatasbuka">  <?php echo $data['KeteranganBuka']; ?></span> <?php echo $data['KeteranganJam']; ?>
                                <?php } else {
                                    ?>
                                    <img data-u="image" src="<?php echo base_url('assets/icon/icontutup.png') ?>"
                                         width="14px" data-toggle="tooltip" title="Tersimpan" data-placement="top"/> <span
                                            class="kekanandetailatas"> <?php echo $data['KeteranganBuka']; ?> </span>
                                <?php } ?>
                                <!--                                                <i class="fa fa-fw fa-clock-o <?php //echo ((!empty($buka[0]->isbuka)) ? (($buka[0]->isbuka == 'Y') ? 'text-green' : 'text-grey') : 'text-grey') ?> " onclick="produk"></i> <span ><?php //echo $datakuliner[$i]['jadwalBukaSekarang']; ?></span>-->


                      </span> <span
                                    class="tombol-jadwal-kuliner visible-xs-inline kekanandetailatas">Lihat jadwal</span>
                            <div class="jadwal-kuliner">
                                <?php
                                $jadwal_buka = json_decode($data['jsonJadwalBuka']);
                                foreach ($jadwal_buka as $jadwal) {
                                    echo "<div><span>" . $jadwal->NamaHari . "</span><span>" . $jadwal->jambuka . "-" . $jadwal->jamtutup . " WIB</span>";
                                    if (!empty($jadwal->keterangan)) "<span>" . $jadwal->keterangan . "</span>";
                                    echo "</div>";
                                }
                                ?>
                        </li>
                        <li><img class="img-item-wisata" src="<?php echo base_url('assets/icon/iconharga.png'); ?>"> <span
                                    class="kekanandetailatas"><?php echo 'Rp ' . $data['hargamin'] . '-Rp ' . $data['hargamaks']; ?></span>
                        </li>
                        <li><img class="img-item-wisata" src="<?php echo base_url('assets/icon/icontelepon.png'); ?>">
                            <span class="kekanandetailatas">
                        <?php
                        if (!empty($data['phonekontak'])) echo $data['phonekontak'];
                        elseif (!empty($data['phonekontak2'])) echo $data['phonekontak2'];
                        else echo '-';
                        ?>
                      </span>
                        </li>
                        <li>
                      <span class="location" data-toggle="tooltip" title="Berdasarkan Titik Pusat Keramaian Kota"
                            data-placement="bottom">
                      <img class="img-item-wisata" src="<?php echo base_url(); ?>assets/img/wisata/jarak-icon.svg"/>

                      <span class="kekanandetailatas">   <?php echo $data['jarak']; ?>km </span> </span>

                        </li>
                    </ul>
                    <div id="score" class="sum  hidden-xs">
                        <div class="col-point sum">
                            <div class="sum-score"><span class="text-score"
                                                         data-total-rating="restaurant"><?php echo $penilaian[0]->rating; ?></span>
                            </div>
                        </div>
                        <div class="score text-center" id="score-rasa"><span class="rating-value rating-item-10--color"
                                                                             data-toggle-score="rasa"
                                                                             style="color: rgb(232, 15, 10);"> <?php echo $skor[0]->skorRasaMakanan; ?> </span>
                            <div class="rating-box" data-rating-group="restaurant" data-score-name="rasa"
                                 data-hoverable="false" data-score="0">
                                <span class="rating-item rating-item-1" value="1"></span>
                                <span class="rating-item rating-item-2" value="2"></span>
                                <span class="rating-item rating-item-3" value="3"></span>
                                <span class="rating-item rating-item-4" value="4"></span>
                                <span class="rating-item rating-item-5" value="5"></span>
                                <span class="rating-item rating-item-6" value="6"></span>
                                <span class="rating-item rating-item-7" value="7"></span>
                                <span class="rating-item rating-item-8" value="8"></span>
                                <span class="rating-item rating-item-9" value="9"></span>
                                <span class="rating-item rating-item-10" value="10"></span>
                            </div>
                            <hr>
                            <span>Rasa Makanan</span>
                        </div>
                        <div class="score text-center"><span class="rating-value rating-item-10--color"
                                                             data-toggle-score="lokasi"
                                                             style="color: rgb(232, 15, 10);"> <?php echo $skor[1]->skorPelayanan; ?> </span>
                            <div class="rating-box" data-rating-group="restaurant" data-score-name="lokasi"
                                 data-hoverable="false" data-score="0">
                                <span class="rating-item rating-item-1" value="1"></span>
                                <span class="rating-item rating-item-2" value="2"></span>
                                <span class="rating-item rating-item-3" value="3"></span>
                                <span class="rating-item rating-item-4" value="4"></span>
                                <span class="rating-item rating-item-5" value="5"></span>
                                <span class="rating-item rating-item-6" value="6"></span>
                                <span class="rating-item rating-item-7" value="7"></span>
                                <span class="rating-item rating-item-8" value="8"></span>
                                <span class="rating-item rating-item-9" value="9"></span>
                                <span class="rating-item rating-item-10" value="10"></span>
                            </div>
                            <hr>
                            <span>Suasana</span></div>
                        <div class="score"><span class="rating-value rating-item-4--color" data-toggle-score="kebersihan"
                                                 style="color: rgb(232, 15, 10);"> <?php echo $skor[2]->skorKebersihan; ?> </span>
                            <div class="rating-box" data-rating-group="restaurant" data-score-name="kebersihan"
                                 data-hoverable="false" data-score="0">
                                <span class="rating-item rating-item-1" value="1"></span>
                                <span class="rating-item rating-item-2" value="2"></span>
                                <span class="rating-item rating-item-3" value="3"></span>
                                <span class="rating-item rating-item-4" value="4"></span>
                                <span class="rating-item rating-item-5" value="5"></span>
                                <span class="rating-item rating-item-6" value="6"></span>
                                <span class="rating-item rating-item-7" value="7"></span>
                                <span class="rating-item rating-item-8" value="8"></span>
                                <span class="rating-item rating-item-9" value="9"></span>
                                <span class="rating-item rating-item-10" value="10"></span>
                            </div>
                            <hr>
                            <span>Kebersihan</span></div>
                        <div class="score"><span class="rating-value rating-item-10--color" data-toggle-score="pelayanan"
                                                 style="color: rgb(232, 15, 10);"> <?php echo $skor[3]->skorLokasi; ?> </span>
                            <div class="rating-box" data-rating-group="restaurant" data-score-name="pelayanan"
                                 data-hoverable="false" data-score="0">
                                <span class="rating-item rating-item-1" value="1"></span>
                                <span class="rating-item rating-item-2" value="2"></span>
                                <span class="rating-item rating-item-3" value="3"></span>
                                <span class="rating-item rating-item-4" value="4"></span>
                                <span class="rating-item rating-item-5" value="5"></span>
                                <span class="rating-item rating-item-6" value="6"></span>
                                <span class="rating-item rating-item-7" value="7"></span>
                                <span class="rating-item rating-item-8" value="8"></span>
                                <span class="rating-item rating-item-9" value="9"></span>
                                <span class="rating-item rating-item-10" value="10"></span>
                            </div>
                            <hr>
                            <span>Pelayanan</span></div>
                    </div>
                    <ul class="list-unstyled list-inline text-right atribut-item-wisata">
                        <li><img class="img-item-wisata2"
                                 src="<?php echo base_url(); ?>assets/img/wisata/data-simpan-icon.svg"><?php echo 'Simpan ' . $data['jmlFavorit']; ?>
                        </li>
                        <li><img class="img-item-wisata2"
                                 src="<?php echo base_url(); ?>assets/img/wisata/data-ulasan-icon.svg"><?php echo 'Ulasan ' . $data['jmlUlasan']; ?>
                        </li>
                        <li><img class="img-item-wisata2"
                                 src="<?php echo base_url(); ?>assets/img/wisata/data-foto-icon.svg"><?php echo ' Foto ' . $data['jmlFoto']; ?>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row">
                <div class="col-md-9">
                    <div id="detail-daftar-menu" class="detail-kuliner-box hidden-xs"
                         style="padding-left: 20px;padding-right:0px;">
                        <h3 class="detail-title">Share Ke Teman</h3>
                        <div class="col-md-7ths " style="float:left; margin-right: 5px; "><img
                                    style="height: 31px; width:122.25px"
                                    src="<?php echo base_url(); ?>assets/icon/share/facebook.png" class="img-responsive">
                        </div>
                        <div class="col-md-7ths " style="float:left;margin-right: 5px;"><img
                                    style="height: 31px; width:122.25px"
                                    src="<?php echo base_url(); ?>assets/icon/share/line.png" class="img-responsive"></div>
                        <div class="col-md-7ths " style="float:left;margin-right: 5px;"><img
                                    style="height: 31px; width:122.25px"
                                    src="<?php echo base_url(); ?>assets/icon/share/twitter.png" class="img-responsive">
                        </div>
                        <div class="col-md-7ths " style="float:left;margin-right: 5px;"><img
                                    style="height: 31px; width:122.25px"
                                    src="<?php echo base_url(); ?>assets/icon/share/whatsapp.png" class="img-responsive">
                        </div>
                        <div class="col-md-7ths " style="float:left;margin-right: 5px;"><img
                                    style="height: 31px; width:122.25px"
                                    src="<?php echo base_url(); ?>assets/icon/share/email.png" class="img-responsive"></div>
                        <div class="col-md-7ths " style="float:left;margin-right: 5px;"><img
                                    style="height: 31px; width:122.25px"
                                    src="<?php echo base_url(); ?>assets/icon/share/kulinertrip.png" class="img-responsive">
                        </div>
                        <div class="col-md-7ths " style="float:left;margin-right: 5px;"><img
                                    style="height: 31px; width:122.25px"
                                    src="<?php echo base_url(); ?>assets/icon/share/share.png" class="img-responsive"></div>
                        <div class="clearfix"></div>
                    </div>

                    <?php
                    $images_menu = json_decode($data['jsonImageMenu']);
                    $data_menu = json_decode($images_menu[0]->data);
                    //echo '<pre>'.print_r($data_menu, true).'</pre>';
                    ?>
                    <?php if (count($data_menu) != 0) { ?>
                        <div id="detail-daftar-menu" class="detail-kuliner-box hidden-xs">
                            <h3 class="detail-title">Daftar Menu</h3>
                            <div id="detaildaftarmenu" class="text-center">
                                <?php if (count($data_menu) == 1) { ?>
                                    <a class="col-md-5ths daftar-menu" onclick="showgallery(<?php echo '0' ?>)">
                                        <img src="<?php echo $data_menu[0]->linkthumbnail; ?>" class="img-responsive">
                                    </a>

                                    <?php
                                } elseif (count($data_menu) == 2) {
                                    $a = 0;
                                    $j = 0;
                                    for ($i = 0; $i < 4; $i++) { ?>
                                        <a class="col-md-5ths daftar-menu" onclick="showgallery(<?php echo $j ?>)">
                                            <?php if ($i % 2) { ?>
                                                <img src="<?php echo $data_menu[$a]->linkthumbnail; ?>"
                                                     class="img-responsive">

                                                <?php $a++;
                                                $j++;
                                            } ?>
                                        </a>
                                    <?php }
                                } elseif (count($data_menu) == 3) {
                                    $a = 0;
                                    $j = 0;
                                    for ($i = 0; $i < 4; $i++) { ?>
                                        <a class="col-md-5ths daftar-menu" onclick="showgallery(<?php echo $j ?>)">
                                            ` <?php if ($i != 0) {
                                                $j++; ?>
                                                <img src="<?php echo $data_menu[$a]->linkthumbnail; ?>"
                                                     class="img-responsive">
                                                <?php $a++;
                                            } ?>
                                        </a>
                                    <?php }

                                } elseif (
                                    count($data_menu) == 4 || count($data_menu) == 5
                                ) { ?>
                                    <?php $j = 0;
                                    foreach ($data_menu as $image) { ?>
                                        <a class="col-md-5ths daftar-menu" onclick="showgallery(<?php echo $j ?>)">
                                            <img src="<?php echo $image->linkthumbnail; ?>" class="img-responsive">
                                        </a>
                                        <?php $j++;
                                    } ?>

                                <?php } else { ?>

                                    <?php
                                    $j = 0;
                                    for ($i = 0; $i < 4; $i++) { ?>
                                        <div class="col-md-5ths daftar-menu" onclick="showgallery(<?php echo $j ?>)">
                                            <?php ?>
                                            <img src="<?php echo $data_menu[$i]->linkthumbnail; ?>" class="img-responsive">
                                            <?php $j++; ?>
                                        </div>
                                    <?php } ?>
                                    <div class="col-md-5ths daftar-menu banyak-lagi"
                                         onclick="showgallery(<?php echo $j ?>)">
                                        <img src="<?php echo $data_menu[5]->linkthumbnail; ?>" class="img-responsive">
                                        <label><a href="<?php echo base_url('kuliner/foto/' . $data['UrlPage'] . '.html') ?>">+<?php echo count($data_menu) - 5; ?>
                                                lagi</a></label>

                                    </div>

                                <?php } ?>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <!-- #1:Gambar menu yang akan ditampilkan jika mobile -->
                        <div id="detail-daftar-menu" class="detail-kuliner-box visible-xs-block">
                            <h3 class="detail-title">Daftar Menu</h3>
                            <div class="mobile-slider">
                                <?php
                                $image_total = (count($data_menu) > 4) ? 4 : count($data_menu);
                                for ($i = 0; $i < $image_total; $i++) { ?>
                                    <a class="col-md-5ths daftar-menu">
                                        <img src="http://via.placeholder.com/450x800" class="img-responsive">
                                    </a>
                                <?php } ?>
                                <?php if (count($data_menu) >= 5) { ?>
                                    <a class="col-md-5ths daftar-menu banyak-lagi" href="">
                                        <img src="http://via.placeholder.com/450x800" class="img-responsive">
                                        <?php
                                        if (count($data_menu) > 5) {
                                            echo '<label><a href="' . site_url() . 'kuliner/foto/' . $data['UrlPage'] . '.html">+' . (count($data_menu) - 5) . ' lagi</a></label>';
                                        } ?>
                                    </a>
                                <?php } ?>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <!-- End #1 -->
                    <?php } ?>

                    <div id="more-detail-info">
                        ingin tahu lebih lanjut hal-hal yang menarik dari Taman Sari <a href="#detail-foto-360">klik
                            disini</a>
                    </div>

                    <div id="detail-foto-360" class="detail-kuliner-box hidden-xs">
                        <h3 class="detail-title">Foto 360&deg;</h3>

                        <?php //for($i=0; $i<3; $i++) { ?>

                        <?php //} ?>
                        <div id="lightgallery">
                            <a class="col-md-3 foto-360" href="<?php echo base_url('assets/icon/foto360/foto360b.png'); ?>">
                                <img src="<?php echo base_url('assets/icon/foto360/foto360.png'); ?>"
                                     class="img-responsive">
                            </a>
                            <a class="col-md-3 foto-360" href="<?php echo base_url('assets/icon/foto360/foto360b.png'); ?>">
                                <img src="<?php echo base_url('assets/icon/foto360/foto360a.png'); ?>"
                                     class="img-responsive">
                            </a>
                            <a class="col-md-3 foto-360" href="<?php echo base_url('assets/icon/foto360/foto360b.png'); ?>">
                                <img src="<?php echo base_url('assets/icon/foto360/foto360b.png'); ?>"
                                     class="img-responsive">
                            </a>
                        </div>

                        <div class="col-md-3 foto-360 banyak-lagi">
                            <img src="<?php echo base_url('assets/icon/foto360/foto360c.png'); ?>" class="img-responsive" <a
                                    class="col-md-3 foto-360"
                                    href="<?php echo base_url('assets/icon/foto360/foto360b.png'); ?>">
                                <label><?php echo '<a href="' . site_url() . 'kuliner/foto/' . $data['UrlPage'] . '.html">+30 lagi</a>'; ?></label>
                        </div>

                        <div class="clearfix"></div>
                    </div>

                    <!-- #2:Foto 360 yang akan ditampilkan jika mobile -->
                    <div id="detail-foto-360" class="detail-kuliner-box visible-xs-block">
                        <h3 class="detail-title">Foto 360&deg;</h3>
                        <div class="mobile-slider">
                            <?php for ($i = 0; $i < 3; $i++) { ?>
                                <a class="col-md-3 foto-360 banyak-lagi">
                                    <img src="<?php echo base_url(); ?>assets/img/nature.jpg" class="img-responsive">
                                </a>
                            <?php } ?>
                            <a class="col-md-3 foto-360 banyak-lagi" href="">
                                <img src="<?php echo base_url(); ?>assets/img/nature.jpg" class="img-responsive">
                                <label>+30 lagi</label>
                            </a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <!-- End #2 -->

                    <?php
                    $fasilitas = json_decode($data['jsonFasilitas']);
                    $item_fasilitas = json_decode($fasilitas[0]->fasilitas);
                    ?>
                    <div id="fasilitas-detail-kuliner" class="detail-kuliner-box">
                        Lihat Fasilitas
                        <ul class="list-inline">
                            <?php //echo '<pre>'.print_r($fasilitas, true).'</pre>';
                            ?>
                            <?php

                            $jumlah_fasilitas = ($fasilitas[0]->jmlFasilitas <= 6) ? $fasilitas[0]->jmlFasilitas : 6;

                            for ($i = 0; $i < $jumlah_fasilitas; $i++) { ?>
                                <li>

                                    <img src="<?php echo $item_fasilitas[$i]->imgfasilitas ?>" class="img-responsive"
                                         style="width: 24px;height: 24px;"/>
                                </li>
                            <?php } ?>
                        </ul>
                        <a href="#" class="visible-xs-block">Lihat lebih</a>
                        <div class="lebih-banyak">
                            <h4>Fasilitas</h4>
                            <ul>
                                <?php foreach ($item_fasilitas as $item) {
                                    echo "<li><img class='img-responsive' src='" . $item->imgfasilitas . "'><span>" . $item->jenisfasilitas . "</span></li>";
                                    // echo "<li><object class=\"iconfasilitas\" type=\"image/svg+xml\" data='$item->imgfasilitas'></object><span>".$item->jenisfasilitas."</span></li>";
                                } ?>
                            </ul>
                        </div>
                    </div>
                    <div id="detail-kuliner-informasi">
                        <ul class="tabs-kuliner clearfix tabs-flying">
                            <a href="<?php echo site_url() . 'kuliner/informasi/' . $data['UrlPage'] . '.html'; ?>"
                               class="tab-item active">Informasi</a>
                            <a target="_blank"
                               href="<?php echo site_url() . 'kuliner/foto/' . $data['UrlPage'] . '.html'; ?>"
                               class="tab-item">Foto</a>
                            <a target="_blank"
                               href="<?php echo site_url() . 'kuliner/ulasan/' . $data['UrlPage'] . '.html'; ?>"
                               class="tab-item">Ulasan</a>
                        </ul>

                        <ul class="tabs-kuliner no-flying clearfix">
                            <a href="<?php echo site_url() . 'kuliner/informasi/' . $data['UrlPage'] . '.html'; ?>"
                               class="tab-item active">Informasi</a>
                            <a target="_blank"
                               href="<?php echo site_url() . 'kuliner/foto/' . $data['UrlPage'] . '.html'; ?>"
                               class="tab-item">Foto</a>
                            <a target="_blank"
                               href="<?php echo site_url() . 'kuliner/ulasan/' . $data['UrlPage'] . '.html'; ?>"
                               class="tab-item">Ulasan</a>
                        </ul>
                        <div class="clearfix detail-kuliner-box top">
                            <div class="col-md-4 col-xs-6">
                                <h4>Jenis Masakan<br>
                                    <small class="text-truncate"><?php echo (!empty($data['jenisMasakan'])) ? '<span class="tooltips-bottom">' . $data['jenisMasakan'] . '</span><span>' . str_replace(',', '<br>', $data['jenisMasakan']) . '</span>' : '<em>belum ada data</em>'; ?></small>
                                </h4>
                            </div>
                            <div class="col-md-4 col-xs-6">
                                <h4>Halal atau Non Halal<br>
                                    <small class="text-truncate"><?php echo (!empty($data['HalalNonHalal'])) ? '<span  class="tooltips-bottom">' . $data['HalalNonHalal'] . '</span><span>' . str_replace(',', '<br>', $data['HalalNonHalal']) . '</span>' : '<em>belum ada data</em>'; ?></small>
                                </h4>
                            </div>
                            <div class="col-md-4 col-xs-6">
                                <h4>Makanan Khas<br>
                                    <small class="text-truncate"><?php echo (!empty($data['IsKhasSini'])) ? '<span  class="tooltips-bottom">' . $data['IsKhasSini'] . '</span><span>' . str_replace(',', '<br>', $data['IsKhasSini']) . '</span>' : '<em>belum ada data</em>'; ?></small>
                                </h4>
                            </div>
                            <div class="col-md-4 col-xs-6">
                                <h4>Cabang<br>
                                    <small class="text-truncate"><?php echo (!empty($data['cabang'])) ? '<span class="tooltips-bottom">' . $data['cabang'] . '</span><span>' . str_replace(',', '<br>', $data['cabang']) . '</span>' : '<em>belum ada data</em>'; ?></small>
                                </h4>
                            </div>
                            <div class="col-md-4 col-xs-6">
                                <h4>Jenis Tujuan<br>
                                    <small class="text-truncate"><?php echo (!empty($data['JenisTujuanResto'])) ? '<span  class="tooltips-bottom">' . $data['JenisTujuanResto'] . '</span><span>' . str_replace(',', '<br>', $data['JenisTujuanResto']) . '</span>' : '<em>belum ada data</em>'; ?></small>
                                </h4>
                            </div>
                            <div class="col-md-4 col-xs-6">
                                <h4>Jenis Waktu<br>
                                    <small class="text-truncate"><?php echo (!empty($data['JenisWaktu'])) ? '<span  class="tooltips-bottom">' . $data['JenisWaktu'] . '</span><span>' . str_replace(',', '<br>', $data['JenisWaktu']) . '</span>' : '<em>belum ada data</em>'; ?></small>
                                </h4>
                            </div>
                            <div class="col-md-4 col-xs-6">
                                <h4>Cara Masak<br>
                                    <small class="text-truncate"><?php echo (!empty($data['caramasak'])) ? '<span class="tooltips-bottom">' . $data['caramasak'] . '</span><span>' . str_replace(',', '<br>', $data['caramasak']) . '</span>' : '<em>belum ada data</em>'; ?></small>
                                </h4>
                            </div>
                            <div class="col-md-4 col-xs-6">
                                <h4>Kapasitas<br>
                                    <small class="text-truncate"><?php echo (!empty($data['kapasitas'])) ? '<span class="tooltips-bottom">' . $data['kapasitas'] . '</span><span>' . str_replace(',', '<br>', $data['kapasitas']) . '</span>' : '<em>belum ada data</em>'; ?></small>
                                </h4>
                            </div>
                            <div class="col-md-4 col-xs-6">
                                <h4>Last Order<br>
                                    <small class="text-truncate"><?php echo (!empty($data['jamterakhirpesan'])) ? '<span  class="tooltips-bottom">' . $data['jamterakhirpesan'] . '</span><span>' . str_replace(',', '<br>', $data['jamterakhirpesan']) . '</span>' : '<em>belum ada data</em>'; ?></small>
                                </h4>
                            </div>
                            <div class="col-md-4 col-xs-6">
                                <h4>Gambaran Umum<br>
                                    <small class="text-truncate"><?php echo (!empty($data['gambaranUmum'])) ? '<span  class="tooltips-bottom">' . $data['gambaranUmum'] . '</span><span>' . str_replace(',', '<br>', $data['gambaranUmum']) . '</span>' : '<em>belum ada data</em>'; ?></small>
                                </h4>
                            </div>
                            <div class="col-md-4 col-xs-6">
                                <h4>Durasi Tunggu<br>
                                    <small class="text-truncate"><?php echo (!empty($data['MasaTungguHidang'])) ? '<span  class="tooltips-bottom">' . $data['MasaTungguHidang'] . '</span><span>' . str_replace(',', '<br>', $data['MasaTungguHidang']) . '</span>' : '<em>belum ada data</em>'; ?></small>
                                </h4>
                            </div>
                            <div class="col-md-4 col-xs-6 visible-xs-block">
                                <h4>Yang Perlu Dicoba<br>
                                    <small class="text-truncate"><?php echo (!empty($data['jenisMasakan'])) ? '<span  class="tooltips-bottom">' . $data['jenisMasakan'] . '</span><span>' . str_replace(',', '<br>', $data['jenisMasakan']) . '</span>' : '<em>belum ada data</em>'; ?></small>
                                </h4>
                            </div>
                        </div>
                        <div class="clearfix detail-kuliner-box bottom">
                            <div class="col-md-6 col-xs-4">
                                <h4>Map</h4>
                                <a href="http://maps.google.com/maps?q=<?php echo $data['mapaddress']; ?>&z=10"
                                   target="_blank"><img
                                            src="https://maps.googleapis.com/maps/api/staticmap?center=<?php echo $data['mapaddress']; ?>&zoom=13&size=400x225&maptype=roadmap&markers=anchor:32,10%7Cicon:icon:<?php echo base_url() . "assets/icon/iconmarker.svg" ?>color:red%7Clabel:S%7C<?php echo $data['mapaddress']; ?>&key=AIzaSyAU41R8r4EPPCefTKIEoDV6aPWVShRjyNQ&format=png"
                                            class="img-responsive"></a>
                            </div>
                            <div class="col-md-3 col-xs-4">
                                <h4 class="hidden-xs">Yang Perlu Dicoba<br>
                                    <small class="text-truncate"><?php echo (!empty($data['jenisMasakan'])) ? '<span class="tooltips-bottom">' . $data['jenisMasakan'] . '</span><span>' . str_replace(',', '<br>', $data['jenisMasakan']) . '</span>' : '<em>belum ada data</em>'; ?></small>
                                </h4>
                                <div>
                                    <h6>Google Street View</h6>
                                    <a href="http://maps.google.com/maps?q=&layer=c&cbll=<?php echo $data['mapaddress']; ?>"
                                       target="_blank">
                                        <img src="https://maps.googleapis.com/maps/api/streetview?size=400x225&location=<?php echo $data['mapaddress']; ?>&heading=151.78&key=AIzaSyAU41R8r4EPPCefTKIEoDV6aPWVShRjyNQ&pitch=-0.76"
                                             class="img-responsive">
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-4">
                                <a href="" class="text-center text-muted">
                                    <i class="fa fa-exclamation-triangle fa-2x" aria-hidden="true"></i><span>Perbaharui Informasi</span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <?php
                    $foto_user = json_decode($data['jsonFotoUser']);
                    $data_foto_user = json_decode($foto_user[0]->data);
                    ?>
                    <div id="foto-detail-kuliner" class="detail-kuliner-box hidden-xs">
                        <h3 class="detail-title">Foto</h3>
                        <?php
                        if (count($data_foto_user) != 0) {
                            $i = 0;
                            while ($i < 4 && $i < count($data_foto_user)) {
                                if ($i == 3) {
                                    echo '<a class="col-md-3 foto-360 banyak-lagi">
                  <img src="http://via.placeholder.com/800x450" class="img-responsive">';
                                    if (count($data_foto_user) > 4) {
                                        echo '<label><a href="' . site_url() . 'kuliner/foto/' . $data['UrlPage'] . '.html">+' . (count($data_foto_user) - 4) . ' lagi</a></label>';
                                    }
                                    echo '</a>';
                                } else {
                                    echo '<a class="col-md-3 foto-360">
                  <img src="http://via.placeholder.com/800x450" class="img-responsive">
                  </a>';
                                }
                                $i++;
                            }
                        } ?>
                        <div class="clearfix"></div>
                        <?php if (count($data_foto_user) != 0) { ?>
                            <div class="pull-right upload-gambar-kuliner">
                                Berbagi dengan kami
                                <label class="btn btn-danger btn-upload" for="inputImageUser" title="Upload image file">
                                    <input class="sr-only inputImage" id="inputImageUser" name="file" accept="image/*" type="file" multiple="" data-source="user-image">
                                    <img class="img-item-wisata" src="<?php echo base_url(); ?>assets/img/wisata/data-foto-icon.svg"> Tambah Foto
                                </label>
                            </div>
                            <div class="clearfix"></div>

                        <?php } else { ?>
                            <div class="text-right upload-gambar-kuliner">
                                Jadilah pengunggah foto yang pertama
                                <label class="btn btn-danger btn-upload" for="inputImageUser" title="Upload image file">
                                    <input class="sr-only inputImage" id="inputImageUser" name="file" accept="image/*" type="file" multiple="" data-source="user-image">
                                    <img class="img-item-wisata" src="<?php echo base_url(); ?>assets/img/wisata/data-foto-icon.svg"> Tambah Foto
                                </label>
                            </div>
                        <?php } ?>
                    </div>

                    <!-- #3:Foto galeri yang akan ditampilkan jika mobile -->
                    <div id="foto-detail-kuliner" class="detail-kuliner-box visible-xs-block">
                        <h3 class="detail-title">Foto</h3>
                        <div class="mobile-slider">
                            <?php
                            if (count($data_foto_user) != 0) {
                                $i = 0;
                                while ($i < 4 && $i < count($data_foto_user)) {
                                    if ($i == 3) {
                                        echo '<div class="col-md-3 foto-360 banyak-lagi">
                    <img src="http://via.placeholder.com/800x450" class="img-responsive">
                    <label>+50 lagi</label>
                    </div>';
                                    } else {
                                        echo '<div class="col-md-3 foto-360">
                    <img src="http://via.placeholder.com/800x450" class="img-responsive">
                    </div>';
                  }
                  $i++;
                } 
              }?>
            </div>
            <div class="clearfix"></div>
            <?php if(count($data_foto_user) != 0){ ?>
            <div class="pull-right upload-gambar-kuliner">
              Berbagi dengan kami <label class="btn btn-danger btn-upload" for="inputImageUser" title="Upload image file">
                                    <input class="sr-only inputImage" id="inputImageUser" name="file" accept="image/*" type="file" multiple="" data-source="user-image">
                                    <img class="img-item-wisata" src="<?php echo base_url(); ?>assets/img/wisata/data-foto-icon.svg"> Tambah Foto
                                </label>
            </div>
            <?php }else{ ?>
            <div class="text-center upload-gambar-kuliner">
              Jadilah yang pertama <label class="btn btn-danger btn-upload" for="inputImageUser" title="Upload image file">
                                    <input class="sr-only inputImage" id="inputImageUser" name="file" accept="image/*" type="file" multiple="" data-source="user-image">
                                    <img class="img-item-wisata" src="<?php echo base_url(); ?>assets/img/wisata/data-foto-icon.svg"> Tambah Foto
                                </label>
                
            </div>
            <?php }?>
          <div class="clearfix"></div>
        </div>
        <!-- End #3 -->



        <!-- ID : tulis-ulasan -->
        <div id="tulis-ulasan-box" class="detail-kuliner-box">
          <h3 class="detail-title">Tulis ulasan</h3>
          
          <div class="kirim-ulasan-kuliner">
              <textarea></textarea>
              <button class="btn btn-danger"><img class="img-item-wisata" src="<?php echo base_url(); ?>assets/img/wisata/data-foto-icon.svg"> Tambah Ulasan</button> Bagikan dan ceritakan pengalaman anda
          </div>
        </div>
        <!-- END : tulis-ulasan -->


        <!-- ID : add-kuliner-review -->
        <div id="add-kuliner-review" class="detail-kuliner-box clearfix">
          <div class="col-md-2">
            <img class="img-review-profile" src="http://via.placeholder.com/100x100">
          </div>
          <div class="col-md-10 col-sm-10 col-xs-12">
            <!-- section-rating -->
            <div class="section-rating">
              <h4 class="title">Rating</h4>
              <span id="subtitle"> <?php echo $data['JudulProduk'].' - '.$data['area']; ?>, menanti review anda untuk meningkatkan layanannya </span>
              <div id="score" class="sum">
                <div class="sum-score">
                  <span class="text-score Roboto-300" data-total-rating="rating-tulis-ulasan">0</span>
                </div>
                <div class="score text-center" id="score-rasa">
                  <span class="rating-value rating-item-10--color Roboto" data-toggle-score="tulis-ulasan-rasa" style="color: rgb(232, 15, 10);"> 0 </span>
                  <div class="rating-box" data-rating-group="rating-tulis-ulasan" data-score-name="tulis-ulasan-rasa" data-hoverable="true" data-score="0">
                    <span class="rating-item rating-item-1" value="1"></span>
                    <span class="rating-item rating-item-2" value="2"></span>
                    <span class="rating-item rating-item-3" value="3"></span>
                    <span class="rating-item rating-item-4" value="4"></span>
                    <span class="rating-item rating-item-5" value="5"></span>
                    <span class="rating-item rating-item-6" value="6"></span>
                    <span class="rating-item rating-item-7" value="7"></span>
                    <span class="rating-item rating-item-8" value="8"></span>
                    <span class="rating-item rating-item-9" value="9"></span>
                    <span class="rating-item rating-item-10" value="10"></span>
                  </div>
                  <hr>
                  <span>Rasa Makanan</span>
                </div>
                <div class="score text-center">
                  <span class="rating-value rating-item-10--color Roboto" data-toggle-score="tulis-ulasan-lokasi" style="color: rgb(232, 15, 10);">0</span>
                  <div class="rating-box" data-rating-group="rating-tulis-ulasan" data-score-name="tulis-ulasan-lokasi" data-hoverable="true" data-score="0">
                    <span class="rating-item rating-item-1" value="1"></span>
                    <span class="rating-item rating-item-2" value="2"></span>
                    <span class="rating-item rating-item-3" value="3"></span>
                    <span class="rating-item rating-item-4" value="4"></span>
                    <span class="rating-item rating-item-5" value="5"></span>
                    <span class="rating-item rating-item-6" value="6"></span>
                    <span class="rating-item rating-item-7" value="7"></span>
                    <span class="rating-item rating-item-8" value="8"></span>
                    <span class="rating-item rating-item-9" value="9"></span>
                    <span class="rating-item rating-item-10" value="10"></span>
                  </div>
                  <hr>
                  <span>Susana</span>
                </div>
                <div class="score">
                  <span class="rating-value rating-item-4--color Roboto" data-toggle-score="tulis-ulasan-kebersihan" style="color: rgb(232, 15, 10);">0</span>
                  <div class="rating-box" data-rating-group="rating-tulis-ulasan" data-score-name="tulis-ulasan-kebersihan" data-hoverable="true" data-score="0">
                    <span class="rating-item rating-item-1" value="1"></span>
                    <span class="rating-item rating-item-2" value="2"></span>
                    <span class="rating-item rating-item-3" value="3"></span>
                    <span class="rating-item rating-item-4" value="4"></span>
                    <span class="rating-item rating-item-5" value="5"></span>
                    <span class="rating-item rating-item-6" value="6"></span>
                    <span class="rating-item rating-item-7" value="7"></span>
                    <span class="rating-item rating-item-8" value="8"></span>
                    <span class="rating-item rating-item-9" value="9"></span>
                    <span class="rating-item rating-item-10" value="10"></span>
                  </div>
                  <hr>
                  <span>Kebersihan</span>
                </div>
                <div class="score">
                  <span class="rating-value rating-item-10--color Roboto" data-toggle-score="tulis-ulasan-pelayanan" style="color: rgb(232, 15, 10);">0</span>
                  <div class="rating-box" data-rating-group="rating-tulis-ulasan" data-score-name="tulis-ulasan-pelayanan" data-hoverable="true" data-score="0">
                    <span class="rating-item rating-item-1" value="1"></span>
                    <span class="rating-item rating-item-2" value="2"></span>
                    <span class="rating-item rating-item-3" value="3"></span>
                    <span class="rating-item rating-item-4" value="4"></span>
                    <span class="rating-item rating-item-5" value="5"></span>
                    <span class="rating-item rating-item-6" value="6"></span>
                    <span class="rating-item rating-item-7" value="7"></span>
                    <span class="rating-item rating-item-8" value="8"></span>
                    <span class="rating-item rating-item-9" value="9"></span>
                    <span class="rating-item rating-item-10" value="10"></span>
                  </div>
                  <hr><span>Pelayanan</span>
                </div>
              </div>
            </div>
            <!-- end section-rating -->
            <!-- section-form-ulasan -->
            <div id="section-form-ulasan--form-basic">
              <div class="form-group">
                <label class="subtitle">Subject</label>
                <input id="subject-ulasan" class="form-control input-round" placeholder="Tujuan Kunjungan" type="text" required>
              </div>
              <div class="form-group input-isi-ulasan">
                <label class="subtitle">Ulasan</label>
                <textarea  id="content-ulasan" class="form-control input-square" rows="3" data-min-word="30" placeholder="Bagikan dan ceritakan pengalaman anda saat mengunjungi tempat ini" required></textarea>
                <span class="text-count"></span>
              </div>
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <input name="" id="tag-teman-ulasan" class="form-control input-round" placeholder="@Tag Teman" type="text">
                    <label>Ajak teman anda untuk berbagi</label>
                  </div>
                </div>
                <div class="col-md-3 col-md-offset-5 clearfix">
                  <div class="form-group">
                    <label class="btn btn-default input-round btn-upload pull-right" for="inputImageUlasan" title="Upload image file">
                      <input class="sr-only inputImage" id="inputImageUlasan" name="file" accept="image/*" type="file" multiple=""  data-source="ulasan-image">
                      <img class="img-item-wisata" style="width: 24px;" src="<?php echo base_url('assets/img/wisata/data-foto-icon.svg'); ?>"> Tambahkan Foto
                    </label>
                    <label>Berbagi dengan Kami</label>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="col-xs-1">#</label>
                    <div class="col-xs-11">
                      <input name="tag_friend" id="hastag-ulasan" class="form-control input-round" placeholder="@Tag Teman" type="text">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- end section-form-ulasan -->
            <!-- section-form-ulasan informasi-tambahan -->
            <div id="section-form-ulasan--informasi-tambahan">
              <h5>Informasi Tambahan</h5>
              <div class="informasi-tambahan-box">
                <p> Berikan informasi tambahan untuk melengkapi ulasan anda ( Dapatkan poin tambahan ) </p>
                <div class="informasi-tambahan--item"> 
                  <span>Berapa orang? </span>
                  <span> <input id="jumlah-orang" class="form-control input-sm input-round required-number" name="jumlah_orang" value="1" type="text" style="width: 45px; text-align: center;"> </span> <span class="point"> + 0.5 Poin </span>  
                </div>
                <div class="informasi-tambahan--item" style="flex-direction: row; flex-wrap: wrap;"> 
                  <h5>Menu yang dipesan</h5>
                  
                </div>
                <div class="informasi-tambahan--item input-menu-order"> 
                    <span> <input name="menu_order_name" class="form-control input-sm input-round" type="text"> </span><span class="label-name">Harga/Porsi (Rp) </span>
                    <span> <input name="menu_order_price" class="menu-order-price form-control input-sm input-round price" value="0" type="text" style="width: 130px;"> </span> <span class="point"> + 1 Poin </span><br>
                    Cita rasa : 
                    <div class="rating">
                        <input type="radio" id="star5" name="menu_order_rating" value="5" /><label class = "full" for="star5" title="Lezat"></label>
                        <input type="radio" id="star4" name="menu_order_rating" value="4" /><label class = "full" for="star4" title="Enak"></label>
                        <input type="radio" id="star3" name="menu_order_rating" value="3" /><label class = "full" for="star3" title="Lumayan"></label>
                        <input type="radio" id="star2" name="menu_order_rating" value="2" /><label class = "full" for="star2" title="Biasa saja"></label>
                        <input type="radio" id="star1" name="menu_order_rating" value="1"  checked/><label class = "full" for="star1" title="Mengecewakan"></label>
                    </div>
                    <span class="label-name">Mengecewakan </span> 
                    <span class="point"> + 1 Poin </span> <a href="#" class="delete-item">Delete</a>
                </div>
                <a href="#" class="btn-add-recommend btn-menu-order">
                    Tambah daftar menu lain yang anda pesan
                </a>
              </div>
              <div class="informasi-tambahan-box">
                <div class="informasi-tambahan--item"> 
                  Total biaya yang dikeluarkan dalam kunjungan <span class="label-name">Total (Rp)</span> 
                  <span> <input class="form-control input-sm input-round price total-biaya-ulasan" name="total_biaya" value="0" style="width: 100px;" type="text"> </span> 
                  <span class="point"> + 0.5 Poin </span>
                </div>
                <div class="informasi-tambahan--item"> 
                  <span style="flex: 0 1 100%;"> Menu yang direkomendasi </span>
                </div>
                <div class="input-menu-recommend informasi-tambahan--item"> 
                  <span class=""> <input class="form-control input-sm input-round menu-rekomendasi-ulasan" name="menu_rekomendasi_ulasan" type="text"> </span>
                  <span class="point"> + 1 Poin </span> <a href="#" class="delete-item">Delete</a>
                </div>
                <a href="#" id="btn-recommend-menu" class="btn-add-recommend btn-menu-recommend">
                  Tambah kolom list menu yang direkomendasikan 
                </a>
              </div>
              <div class="informasi-tambahan-box">
                <div class="informasi-tambahan--item Roboto"> 
                  Apakah anda akan mengunjungi kembali tempat ini dilain waktu
                </div>
                <div class="informasi-tambahan--item"> 
                  <ul class="list-inline">
                    <li class="radio">
                      <label>
                        <input name="berkunjung_lagi_ulasan" checked="" type="radio"> pasti
                      </label>
                    </li>
                    <li class="radio">
                      <label>
                        <input name="berkunjung_lagi_ulasan" checked="" type="radio"> Mungkin
                      </label>
                    </li>
                    <li class="radio">
                      <label>
                        <input name="berkunjung_lagi_ulasan" checked="" type="radio"> Tidak lagi
                      </label>
                    </li>
                    <li>
                      <span class="point"> + 0.5 Poin </span> 
                    </li>
                  </ul>
                </div> 
              </div>
            </div>
            <!-- end section-form-ulasan informasi-tambahan -->
            <!-- section-view-image -->
            <div id="preview-image-ulasan" class="detail-kuliner-box clearfix">
            </div>
            <p>- Dengan <a href="#">Agung Wijaya, Khairani</a></p>
            <div id="aksi-kirim-ulasan" class="clearfix">
                <a href="#" class="btn btn-default btn-sm btn-action-ulasan pull-right" data-action="send">kirim</a> <a href="#" class="btn btn-danger btn-sm btn-action-ulasan pull-right" data-action="draft">Simpan Draft</a> 
            </div>
          </div>
        </div>
        <!-- END : add-kuliner-review -->


        <!-- ID : hasil-ulasan -->
        <div class="col-md-12 detail-kuliner-box" id="ulasan">
          <h3 id="menu">Ulasan </h3>
          <div id="respon">
            <div class="isi-ulasan">
              <div id="cool-atas">
                <!-- profil-ulasan -->
                <!-- <div id="profil-ulasan">
                  <img src="http://172.104.166.160/kulinertripweb/assets/img/avatar_2x.png" id="user">
                </div>
                <div id="profil-ulasan" class="nama-report">
                  <h4 id="nama-user">Novi Kusuma Jaya</h4>
                  <h4 id="nama-user" class="report">50 Ulasan. 30 Foto 200 Pengikut</h4>
                  <div>
                    <span><img src="http://172.104.166.160/kulinertripweb/assets/img/Local guide Emas Icon.svg" id="level">
                    </span><span id="text-level">emas </span>
                  </div>
                </div>
                <div id="profil-ulasan" class="follow">
                  <div id="folow">
                    <img src="http://172.104.166.160/kulinertripweb/assets/img/Ikon follow tidak terseleksi.svg" id="follow">
                  </div>
                  <span class="mengikuti">Mengikuti </span>
                </div>
                <div id="report-ulasan">
                  <p class="report">22 Oktober 2017 Via Android 10 lihat </p>
                  <p class="point">9.0 </p>
                </div> -->
                <!-- end profil-ulasan -->
                <!-- tempat-ulasan -->
               <!--  <div id="tempat-ulasan">
                  <h4 class="judul-ulasan">Nyobain Makanan yang pas</h4>
                  <p class="isi-ulasan">Dalam 1 <strong>paragraf</strong> terdapat beberapa bentuk kalimat, kalimat-kalimat itu ialah kalimat pengenal, kalimat utama (kalimat topik), kalimat penjelas, dan kalimat penutup. Kalimat-kalimat ini terangkai menjadi satu kesatuan yang dapat membentuk suatu gagasan. </p>
                  <p class="report-ulasan">Berkunjung 3 orang dengan menu nasii goreng (Rp. 18.000)</p>
                  <p class="report-ulasan">Nasi Gandul Pati (26.000) dengan total pengeluaran Rp. 68.000/kunjungan </p>
                  <p class="report-ulasan">Menu Rekomendasi yang Harus Di coba</p>
                  <p class="report-ulasan">Akan Berkunjung kembali : Pasti</p>
                  <p id="uls" class="report-ulasan">Ulasan ini dan segala isinya merupakan pendapat pribadi dan tidak terkait dengan kulinerTrip</p>
                </div> -->
                <!-- end tempat-ulasan -->
                <!-- foto-ulasan -->
            <!--     <div id="foto-ulasan">
                  <div id="img-ulasan">
                    <div class="item-img"></div>
                  </div>
                  <div id="img-ulasan">
                    <div class="item-img"></div>
                  </div>
                  <div id="img-ulasan">
                    <div class="item-img"></div>
                  </div>
                  <div id="img-ulasan">
                    <div class="item-img"></div>
                  </div>
                  <p id="tag">- Dengan <span class="name-tag">Agung Wijaya, </span><span class="name-tag">Agung Wijaya </span></p>
                </div> -->
                <!-- end foto-ulasan -->
                <!-- action-ulasan -->
            <!--     <div id="action-komentar">
                  <span class="action">
                    <i class="fa fa-thumbs-up"></i>
                    <span id="jumlah">25 </span>
                  </span>
                  <span class="action">
                    <i class="fa fa-comment-o"></i>
                    <span id="jumlah">25 </span>
                  </span>
                  <span class="action">
                    <i class="fa fa-share-alt"></i>
                    <span id="jumlah">25 </span>
                  </span>
                  <span class="action">
                    <i class="fa fa-exclamation-triangle"></i>
                  </span>
                </div> -->
                <!-- end action-ulasan -->
              </div>
              <div class="komentar-ulasan">
                <ul class="media-list">
                    <li class="media message">
                        <div class="media-left">
                            <a href="#">
                                <img class="media-object img-review-profile" src="http://via.placeholder.com/60x60" alt="Putri perdana">
                            </a>
                        </div>
                        <div class="media-body">
                            <div class="message-content">
                                <a href="#">Putri Wulandari</a> Kemarin saya juga makan disini, sumpah makanannya enak banget !!!
                            </div>
                            <div class="message-attribute clearfix">
                                <a href=""><i class="fa fa-thumbs-up"></i> Suka 5</a> <a href="#" class="btn-message-reply">Balas</a> 20 Oktober 2017
                                <i class="message-control fa fa-ellipsis-h pull-right" data-toggle="tooltip" data-placement="top" data-original-title="Edit dan Hapus"></i>
                                <span class="context">
                                        <a href="#" class="btn-message-edit"><i class="fa fa-pencil"> Edit</i></a>
                                        <a href="#"  class="btn-message-remove"><i class="fa fa-trash"> Hapus</i></a>
                                </span>
                            </div>
                            <ul class="media-list">
                                <li class="media message reply">
                                    <div class="media-left">
                                        <a href="#">
                                            
                                            <img class="media-object img-review-profile" src="http://via.placeholder.com/60x60" alt="Putri perdana">
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <div class="message-content">
                                            <a href="#">Putri Wulandari</a> Kemarin saya juga makan disini, sumpah makanannya enak banget !!!
                                        </div>
                                        <div class="message-attribute clearfix">
                                            <a href=""><i class="fa fa-thumbs-up"></i> Suka 5</a> <a href="" class="btn-message-reply">Balas</a> 20 Oktober 2017
                                            <i class="message-control fa fa-ellipsis-h pull-right" data-toggle="tooltip" data-placement="top" data-original-title="Edit dan Hapus"></i>
                                             <span class="context">
                                                <a href="#" class="btn-message-edit"><i class="fa fa-pencil"> Edit</i></a>
                                                <a href="#"  class="btn-message-remove"><i class="fa fa-trash"> Hapus</i></a>
                                            </span>
                                        </div>
                                    </div>
                                </li>
                                <li class="media message reply">
                                    <div class="media-left">
                                        <a href="#">
                                            <img class="media-object img-review-profile" src="http://via.placeholder.com/60x60" alt="Putri perdana">
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <form>
                                            <textarea class="form-control"></textarea>
                                        </form>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="media">
                        <div class="media-left">
                            <a href="#">
                                <img class="media-object img-review-profile" src="http://via.placeholder.com/60x60" alt="Putri perdana">
                            </a>
                        </div>
                        <div class="media-body">
                            <form>
                                <textarea class="form-control"></textarea>
                            </form>
                        </div>
                    </li>
                </ul>
              </div>

            </div>
          </div>
        </div>
        <!-- END : hasil-ulasan -->


        <!-- ID : Hastag -->
        <?php 
          $datahastag="";
          $this->load->view('__hastag',$datahastag); 
        ?> 
        <!-- END : Hastag -->

      </div>
      <div class="col-md-3 style-content">
          <div style="margin-top: -20px">
           <?php get_iklankanan(!empty($iklan) ? $iklan : false) ?>
          <div class="clearfix"></div>
          </div>

      </div>
    </div>
</div>
<div class="clearfix"></div>

<div class="view-modal" id="for-zoom" style="display: none;">
    <div class="view-close" id="for-zoom-close"><i class="fa fa-fw fa-times"></i></div>
    <div class="view-frame">
        <div class="frame-kiri">
            <div class="panel-up">
                <a id="view-full">
                    <i class="fa fa-fw fa-expand text-grey"></i>
                </a>
                <div class="my-clear"></div>
            </div>

            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel"><!--  data-interval="false" -->
                <div class="carousel-inner" role="listbox">
                    <?php if (!empty($slide['data'])) {
                        $n = 0;
                        for ($sl = 0; $sl < count($slide['data']); $sl++) { ?>
                            <div class="item <?php echo($n == 0 ? 'active' : ''); ?>" data-ind="<?php echo $n; ?>">
                                <img src="<?php echo $slide['data'][$sl]['linkimage']; ?>">
                            </div>
                            <?php $n++;
                        }
                    } ?>
                </div>
                <div class="carousel-ul">
                    <ul class="carousel-indicators">
                        <?php if (!empty($slide['data'])) {
                            $n = 0;
                            for ($sl = 0; $sl < count($slide['data']); $sl++) { ?>
                                <li data-target="#carousel-example-generic" data-slide-to="<?php echo $n; ?>" class="<?php echo(($n == 0) ? 'active' : ''); ?>">
                                    <img src="<?php echo $slide['data'][$sl]['linkimage']; ?>">
                                </li>
                                <?php $n++;
                            }
                        } ?>
                    </ul>
                </div>
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <div class="frame-kanan" id="frame-kanan">
            <div class="infouser">
                <div class="infouser-foto">
                    <img src="<?php echo base_url('assets/icon/avatar.png') ?>" alt="">
                </div>
                <div class="infouser-text">
                    <div class="infouser-nama">
                        Mulya Hari Vano
                    </div>
                    <div class="infouser-panel">
                        <i class="fa fa-fw fa-thumbs-up"></i> 0 &nbsp;
                        <i class="fa fa-fw fa-thumbs-down"></i> 0 &nbsp;
                        <i class="fa fa-fw fa-bookmark"></i> 0 &nbsp;
                        <i class="fa fa-fw fa-share"></i> 0 &nbsp;
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="view-full" id="for-full" style="display: none;">
    <div class="view-close" id="for-full-close"><i class="fa fa-fw fa-times"></i></div>
    <div class="view-frame">
        <div class="frame-full">
            <div id="carousel-full" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner" role="listbox">
                    <?php if (!empty($slide['data'])) {
                        $n = 0;
                        for ($sl = 0; $sl < count($slide['data']); $sl++) { ?>
                            <div class="item <?php echo($n == 0 ? 'active' : ''); ?>">
                                <img src="<?php echo $slide['data'][$sl]['linkimage']; ?>">
                            </div>
                            <?php $n++;
                        }
                    } ?>
                </div>
                <div class="carousel-ul">
                    <ul class="carousel-indicators">
                        <?php if (!empty($slide['data'])) {
                            $n = 0;
                            for ($sl = 0; $sl < count($slide['data']); $sl++) { ?>
                                <li data-target="#carousel-full" data-slide-to="<?php echo $n; ?>" class="<?php echo(($n == 0) ? 'active' : ''); ?>">
                                    <img src="<?php echo $slide['data'][$sl]['linkimage']; ?>">
                                </li>
                                <?php $n++;
                            }
                        } ?>
                    </ul>
                </div>
                <a class="left carousel-control" href="#carousel-full" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-full" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
    <div class="modal fade" id="modal-upload-ulasan" role="dialog" aria-labelledby="modalLabel" tabindex="-1">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form id="form-modal-ulasan" role="form" method="post">
                    <div class="modal-header">
                        <h4>Bagikan kenangan anda dengan foto</h4>
                        <p><span class="count-image"></span> Foto terpilih</p>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer clearfix">
                        <button type="submit" class="btn btn-success">Ok</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<!-- End : Modal -->

<script type="text/javascript" src="<?php echo base_url().'assets/js/imam/module-2.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/js/imam/watermark.min.js'; ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/autocomplete.js/0.30.0/autocomplete.jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/caret/1.0.0/jquery.caret.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tag-editor/1.0.20/jquery.tag-editor.min.js"></script>
<script type="text/javascript">
function showgallery(i){                         
  $(document).ready(function() {
    var jsonData = <?php echo $data['jsonImageMenuGallery'] ?>;
    //alert(i+"--->"+jsonData);
    $(this).lightGallery({
      download:false, 
      dynamic: true,
      index: i,
      html:true,
      dynamicEl:jsonData
    });  
  });
}

//alert($("#detaildaftarmenu").html());
$(document).on('onCloseAfter.lg', function(event) {
  $(document).data('lightGallery').destroy(true);
});


$('#section-form-ulasan--form-basic .input-isi-ulasan textarea').on('input',function () {
  var $this = $(this);
  var min = $this.data('min-word');
  var regex = /\s+/gi;
  var textCount = $(this).siblings( ".text-count" );
  var word = 0;
  if($this.val().length != 0){
    word = jQuery.trim($this.val()).replace(regex, ' ').split(' ').length;
  }

  textCount.text(word + '/' + min + ' words');

  if (word < min) {
    textCount.css('color','red');
  } else {  
    textCount.css('color','green');
  }
})
.on('focusout', function(){
  var $this = $(this);
  var min = $this.data('min-word');
  var regex = /\s+/gi;
  var textCount = $(this).siblings( ".text-count" );
  var word = 0;
  if($this.val().length != 0){
    word = jQuery.trim($this.val()).replace(regex, ' ').split(' ').length;
  }

  if (0 < word && word < min) {
    var conf = confirm('Anda belum memenuhi syarat jumlah kata ulasan, apakah anda tidak jadi memberi ulasan? meninggalkan ulasan akan menghapus isi');
    if(conf)
    {
      $this.val('');
      var word = jQuery.trim($this.val()).replace(regex, ' ').split(' ').length;
      textCount.text(word + '/' + min + ' words');
    }else{
      setTimeout(function () { $this.focus(); }, 5);
    }
  }
});
    

$("#tulis-ulasan-box textarea").on('focus', function(){
  $('#tulis-ulasan-box, #add-kuliner-review').toggle();
});

// Import image
var base_url = '<?php echo base_url(); ?>';
var modalBody = $('#modal-upload-ulasan .modal-body');

var subject, isi_ulasan, is_draft, total_biaya, jumlah_orang;

var score = new Array();
var uploadedImageURL;
var sourceImage;
var totalImage = 0;

var preUploadImage  = {
        image : [],
        name : [],
        title : [],
        description : []
    };

var preUlasanImage  = {
        image : [],
        name : [],
        title : [],
        description : []
    };

var preUserImage  = {
        image : [],
        name : [],
        title : [],
        description : []
    };

var base = "";
var ID = () => {
  let array = new Uint32Array(8)
  window.crypto.getRandomValues(array)
  let str = ''
  for (let i = 0; i < array.length; i++) {
    str += (i < 2 || i > 5 ? '' : '-') + array[i].toString(16).slice(-4)
  }
  return str
}

// console.log(ID());


function clearUpload()
{
    preUploadImage.image      = [];
    preUploadImage.name         = [];
    preUploadImage.title        = [];
    preUploadImage.description  = [];
    modalBody.html('');
}

if (URL) {
    $('.inputImage')
    .on('click', function(){
    // inputImage.onclick = function () {
        // if(preUploadImage.image.length > 0) {
            // if(confirm("Sudah ada foto yg diupload, apakah anda ingin mengganti?")) {
            //     clearUpload(); 
            // } else {
            //     return false; 
            // }
        // }
        // console.log($(this).data('source'));
    })

    .on('change', function () {
        var maxImage = 8;
        var files = this.files;
        sourceImage = $(this).data('source');

        if (sourceImage=='ulasan-image') {
            
            if(typeof preUlasanImage !== 'undefined'){
                console.log('panjang data ulasan :'+preUlasanImage.image.length);
                maxImage -= preUlasanImage.image.length;
            }
        }
        
        if(files.length > maxImage) {
            alert("Gambar yang diupload maksimal " + maxImage + " buah.");
            clearUpload();
            return false;
        }

        if (files && files.length) {
            totalImage = files.length;

            $('#modal-upload-ulasan .modal-header .count-image').text(totalImage);
            for(i = 0; i< files.length; i++)
            {

                if (/^image\/\w+/.test(files[i].type)) {

                    if (files[i].type != 'image/jpg' && files[i].type != 'image/jpeg' && files[i].type != 'image/png') {
                        alert("Semua gambar upload harus berjenis JPEG, JPG, atau PNG.");
                        return false;
                    }

                    if (files[i].size > 7000000) { //Batas maksimal file 7 MB
                        alert("Silahkan upload atau compres gambar dibawah ukuran 7 MB.<br>Perhatikan : Semakin besar gambar, semakin lama proses upload !");
                        return false;
                    }

                    if (uploadedImageURL) {
                        URL.revokeObjectURL(uploadedImageURL);
                    }

                    placeMark(i, files[i]);

                } else {
                    alert("Pilih file yang berjenis gambar.");
                }
            }
            $(this).val(null);
            $('#modal-upload-ulasan').modal();
        }
    });
} else {
  inputImage.disabled = true;
  inputImage.parentNode.className += ' disabled';            
}


function placeMark(i, file) {
    watermark([file, base_url+'assets/icon/logo.png'])
        .blob(watermark.image.center(0.1))
        .then(function(img) {
            var imageRow = '<div class="form-group row img-preview-ulasan">'
            +'<div class="col-xs-4">'
                +'<div class="img-wrapper">'
                    +'<div class="img-container">'       
                        +'<img class="img-responsive" src="'+ URL.createObjectURL(img) +'">'
                    +'</div>'
                    +'<a class="remove-image" href="#" data-id=' + i + '>X</a>'
                +'</div>'
            +'</div>'
            +'<div class="col-xs-8">'
                +'<input class="form-control input-sm" name="title" data-id="'+ i +'" placeholder="Tulis judul foto kenangan anda" type="text">'
                +'<label>Deskripsi Foto</label>'
                +'<textarea class="form-control input-sm" name="description" data-id="'+ i +'" placeholder="Tulis Kenangan mengagumkan anda tentang foto ini"></textarea>'
            +'</div>'
        +'</div>';
        modalBody.append(imageRow);
        preUploadImage.image[i] = img;
        preUploadImage.name[i]  = file.name;
    });
}


$('#modal-upload-ulasan .modal-body').on('click', '.remove-image', function(e){
    e.preventDefault();
    if(totalImage == 1)
    {
        if(confirm('anda akan menghapus gambar terakhir, upload akan dibatalkan ?'))
        {
            clearUpload();
            $('#modal-upload-ulasan').modal('hide');
        }else{
            return false;
        }
    }else{
        var id = $(this).data('id');
        preUploadImage.image[id] = null;
        preUploadImage.name[id] = null;
        totalImage -= 1;
        $(this).closest('.img-preview-ulasan').remove();
        $('#modal-upload-ulasan .modal-header .count-image').text(totalImage);
    }
});


$('#preview-image-ulasan').on('click', '.remove-image', function(e){
    e.preventDefault();
    $(this).closest('.img-preview-ulasan').remove();
    var id = $(this).data('id');
    preUlasanImage.image[id] = null;
    preUlasanImage.name[id] = null;
    preUlasanImage.title[id] = null;
    preUlasanImage.description[id] = null;
    $(this).closest('.foto-360').remove();
});


$(document).click(function(event) { 
    if(!$(event.target).closest('#modal-upload-ulasan .modal-dialog').length && !$(event.target).closest('.remove-image').length) {
        clearUpload();
    }        
});


$("#form-modal-ulasan").on('submit', function(e){
    e.preventDefault();
    var id, viewImage;
    if(this.title.length){
        for(var i=0; i<this.title.length; i++)
        {
            id = this.title[i].getAttribute('data-id');
            preUploadImage.title[id] = this.title[i].value;
            preUploadImage.description[id] = this.description[i].value;
        }
    }else{
        id = this.title.getAttribute('data-id');
        preUploadImage.title[id] = this.title.value;
        preUploadImage.description[id] = this.description.value;
    }

    if(sourceImage == 'user-image'){
        var a = 0;
        for(var i=0; i<preUploadImage.image.length; i++){
            if(preUploadImage.image[i] !== null){
                viewImage = '<div class="col-md-3 foto-360">'
                            +'<div class="img-wrapper">'
                            +'<div class="img-container">'
                            +'<img src="' + URL.createObjectURL(preUploadImage.image[i]) + '" class="img-responsive">'
                            +'</div>'
                            +'</div>'
                            +'</div>';
                $(viewImage).insertAfter('#foto-detail-kuliner .detail-title'); 

                console.log('gambar blob--');
                console.log(preUploadImage.image[i]);
                preUserImage.image.push(preUploadImage.image[i]);
                preUserImage.name.push(preUploadImage.name[i]);
                preUserImage.title.push(preUploadImage.title[i]);
                preUserImage.description.push(preUploadImage.description[i]);
            }
        }

        sendUserImage();

    }else{

        for(var i=0; i<preUploadImage.image.length; i++){
            if(preUploadImage.image[i] !== null){
                viewImage = '<div class="col-md-3 foto-360"><div class="img-wrapper"><div class="img-container">'
                              +'<img src="' + URL.createObjectURL(preUploadImage.image[i]) + '" class="img-responsive"></div>'
                              +'<a class="remove-image" href="#" data-id="'+ i +'">X</a></div>'
                              +'</div>';
                $('#preview-image-ulasan').append(viewImage);

                preUlasanImage.image.push(preUploadImage.image[i]);
                preUlasanImage.name.push(preUploadImage.name[i]);
                preUlasanImage.title.push(preUploadImage.title[i]);
                preUlasanImage.description.push(preUploadImage.description[i]);
            }
        }
    }

    $('#modal-upload-ulasan').modal('hide');
});


function sendUserImage() {
    var formData = new FormData();
    console.log('data dikirim');
    console.log('-------preUserImage-------');

    formData.append('rancode', '12314');

    for(i=0; i< preUserImage.name.length; i++)
    {
        formData.append('name[]', preUserImage.name[i]);
        formData.append('title[]', preUserImage.title[i]);
        formData.append('description[]', preUserImage.description[i]);
        formData.append('image'+i, preUserImage.image[i]);
    }

    $.ajax(base_url+'index.php/kuliner/con_kuliner_detail/insertUserImage', {
        method: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function (data) {
            console.log(data);
            alert('gambar berhasil terupload');
            preUserImage.image        = [];
            preUserImage.name         = [];
            preUserImage.title        = [];
            preUserImage.description  = [];
        },
        error: function () {
            alert('terjadi kesalahan');                   
        }
    });
};


function sendUlasan() {
    var formData = new FormData();
    console.log('data dikirim');
    console.log('-------preUlasanImage-------');
    formData.append('id_produk', '123');
    formData.append('id_user', '12314');
    formData.append('subject', subject);
    formData.append('isi_ulasan', isi_ulasan);
    formData.append('is_draft', is_draft);
    formData.append('total_biaya', total_biaya);
    formData.append('jumlah_orang_saat_datang', jumlah_orang);
    formData.append('via_app', '12314');
    formData.append('status_kunjungan', '12314');
    formData.append('rancode', '12314');
    formData.append('ulasan_tambahan', new Array());
    formData.append('score', new Array());

//     array (
//       'NamaMenu' => 'Bakso',
//       'HargaPerporsi' => '20000',
//       'jmlstar' => '2',
//       'ismenurekumendasi' => 'N',
//     ),

    for(i=0; i< preUserImage.name.length; i++)
    {
        formData.append('name[]', preUserImage.name[i]);
        formData.append('title[]', preUserImage.title[i]);
        formData.append('description[]', preUserImage.description[i]);
        formData.append('image'+i, preUserImage.image[i]);
    }

    $.ajax(base_url+'index.php/kuliner/con_kuliner_detail/insertUlasan', {
        method: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function (data) {
            console.log(data);
            alert('gambar berhasil terupload');
            preUserImage.image        = [];
            preUserImage.name         = [];
            preUserImage.title        = [];
            preUserImage.description  = [];
        },
        error: function () {
            alert('terjadi kesalahan');                   
        }
    });
};


$('#section-form-ulasan--informasi-tambahan .btn-menu-recommend, #section-form-ulasan--informasi-tambahan .btn-menu-order').on('click', function(e){
    e.preventDefault();
    var length = $(this).siblings('.input-menu-order, .input-menu-recommend').length;
    if(length < 4)
    {
        var cln = $(this).prev().clone(true);
        cln.insertBefore($(this));
        cln.find('.delete-item').show();
    }else{
        alert('sudah mencapai jumlah maksimal yang diperbolehkan');
    }
});

/* 
| ====================================================== 
| Function rating
| 
| ======================================================
*/
(function ( $ ) {
    
    // gunakan .rating-box
    $.fn.rating = function(value) {
        this.each(function(){
            that = $(this);
            var data = that.data();
            var options = {
                score: 0,
                autochange: true,
                hoverable: false
            }

            options = $.extend(options, data)

            if(value && typeof parseInt(value) != 'number')
            {
                options = $.extend(options, value)
            }else
            {
                options.score = value;
            }
            // extend with element attribute

            var color = [
                '#e80f0a',
                '#ef1818',
                '#f4740b',
                '#f98630',
                '#f4c914',
                '#f8e26f',
                '#a4ddc5',
                '#6fc9a6',
                '#18c814',
                '#037b00'
            ]
            var type = that.hasClass('.rating-box'),
                data = that.data(),
                item = that.find('.rating-item'),
                index = options.score > 0? options.score -1 : 0,
                me  = that

            $.each(options, function(a,b){
                me.data(a, b);
            })
            item.removeClass('set latest active')
            if(options.score > 0)
            {
                $(item[index]).addClass('active set latest')
                $(item[index]).prevAll().addClass('active');
                $(item[index]).nextAll().removeClass('active');
            }
            val = $(item[index]).attr('value');
            if(options.autochange && options.score > 0)
            {
                $('[data-toggle-score="'+data.scoreName+'"]').text(val).css('color', color[index])
            }else
            {
                $('[data-toggle-score="'+data.scoreName+'"]').text(options.score).css('color', color[0])
                that.data({score: options.score})
            }

            // rerata
            if(options.ratingGroup)
            {
                var group = $('[data-rating-group="'+options.ratingGroup+'"]'),
                    i = 0,
                    len = group.length,
                    sum = 0,
                    rerata = 0;

                    $('[data-rating-group="'+options.ratingGroup+'"]')
                    .each(function(){
                        var score = $(this).data().score;
                        sum += parseInt(score);
                        i++;
                        if(i == len)
                        {
                            rerata = sum / len;
                            rerata = rerata.toFixed(1)
                            $('[data-total-rating]').text(rerata)
                        }
                    })

            }
            me.trigger('KT.rating.change')
        })
        
        return this;
    };
 
}( jQuery ));

$('.rating-item')
.on('mouseenter', function(res){
    var parent = $(this).closest('.rating-box');
    // console.log(parent)
    if(parent.data().hoverable)
    {
        $(this).addClass('active')
        $(this).siblings().removeClass('set')
        $(this).prevAll().addClass('active');
        $(this).nextAll().removeClass('active');
    }
    $(this).trigger('KT.rating.enter')
})
.on('click', function(res){
    var parent = $(this).closest('.rating-box');
    if(parent.data().hoverable)
    {
        $(this).siblings().removeClass('set latest')
        $(this).addClass('active set latest')
        $(this).prevAll().addClass('active');
        $(this).nextAll().removeClass('active');
        val = $(this).attr('value');
        color = $(this).css('background-color');
        parent.rating(val)
    }
    $(this).trigger('KT.rating.click')

})
.on('mouseleave', function(res){
    var parent = $(this);
    if(parent.data().hoverable)
    {
        if($(this).find('.set').length < 1)
        {
            $(this).children().removeClass('active');
        }else
        {
            $(this).find('.set').nextAll().removeClass('active');
            $(this).find('.set').prevAll().addClass('active');
            $(this).find('.set').addClass('active')
        }

        if($(this).find('.length').length < 1)
        {
            $(this).find('.latest').prevAll().addClass('active');
            $(this).find('.latest').nextAll().removeClass('active');
            $(this).find('.latest').addClass('active')
        }
    }
    $(this).trigger('KT.rating.leave')
});

$('#section-form-ulasan--informasi-tambahan .delete-item').on('click', function(e){
    e.preventDefault();
    $(this).parent().closest('.informasi-tambahan--item').remove();
});

$('.price').on('keyup change', function(e){
  $(this).val(function(index, value) {
    return (value=="")? 0 : value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  });
});

$('.required-number').on('keyup change', function(e){
  $(this).val(function(index, value) {
    return (value=="")? 0 : value.replace(/\D/g, "");
  });
});

$("[name='rating']").on('change', function(e){
    var label = $(this).next().attr('title');
  $(this).closest('.rating').next('.label-name').text(label);
});


$("#hastag-ulasan").tagEditor({ 
    // autocomplete: { 'source': 'http://172.104.166.160/kulinertripbo/index.php/Ctr_data_user_api/getHastag', minLength: 1 } 
    autocomplete: {
        delay: 0, // show suggestions immediately
        position: { collision: 'flip' }, // automatic menu position up/down
        source: ['ActionScript', 'AppleScript', 'Asp', 'Python', 'Ruby']
    },
});

$("#tag-teman-ulasan").tagEditor({ 
    // autocomplete: { 'source': 'http://172.104.166.160/kulinertripbo/index.php/Ctr_data_user_api/getUserData', minLength: 1 } 
    autocomplete: {
        delay: 0, // show suggestions immediately
        position: { collision: 'flip' }, // automatic menu position up/down
        source: ['ActionScript', 'AppleScript', 'Asp', 'Python', 'Ruby']
    }
});
// $("#hastag-ulasan").on('change', function(e){
//     var value = $(this).val();
//     console.log( value );
//     var jsonData = [];
//     $.ajax({
//         type: 'POST', 
//         url: 'http://172.104.166.160/kulinertripbo/index.php/Ctr_data_user_api/getHastag',
//         dataType: 'json',
//         data: { param: value }, 
//         success: function(data){
//             console.log(data)
//         }
//     });

//     var fruits = 'Apple,Orange,Banana,Strawberry'.split(',');
//     for(var i=0;i<fruits.length;i++) jsonData.push({id:i,name:fruits[i]});
//         var ms1 = $(this).tagSuggest({
//             // data: jsonData,
//             data: tes,
//             sortOrder: 'name',
//             maxDropHeight: 400,
//             name: 'ms1'
//         });
// });


// $("#tag-teman-ulasan").on('change', function(e){
//     var value = $(this).val();
//     console.log( value );
//     var jsonData = [];
//     $.ajax({
//         type: 'POST', 
//         url: 'http://172.104.166.160/kulinertripbo/index.php/Ctr_data_user_api/getUserData',
//         dataType: 'json',
//         data: { param: value }, 
//         success: function(data){
//             console.log(data)
//         }
//     });

//     var fruits = 'Apple,Orange,Banana,Strawberry'.split(',');
//     for(var i=0;i<fruits.length;i++) jsonData.push({id:i,name:fruits[i]});
//         var ms1 = $(this).tagSuggest({
//             data: jsonData,
//             sortOrder: 'name',
//             maxDropHeight: 400,
//             name: 'ms1'
//         });
// });

$('.btn-action-ulasan').on('click', function(e){
    e.preventDefault();

    var action = $(this).data('action');
    console.log(action);
    // var ulasan_tambahan = new Array(
    //     ''
    //     );

    subject = "" ;
    isi_ulasan = ""; 
    is_draft = "" ; 
    total_biaya = "0";
    jumlah_orang = "0";


    // class="rating-box" data-rating-group="rating-tulis-ulasan" data-score-name="tulis-ulasan-rasa" data-hoverable="true" data-score="0"
    // score = $('.rating-box').data('score');
    // console.log(score);
    /*https://stackoverflow.com/questions/22455466/how-to-use-if-this-has-data-attribute*/
     //'jsonParamScore' => 
//   array (
//     0 => 
//     array (
//       'idjenisscore' => '1',
//       'score' => '7',
//     ),
//     1 => 
//     array (
//       'idjenisscore' => '2',
//       'score' => '8',
//     ),
//     2 => 
//     array (
//       'idjenisscore' => '3',
//       'score' => '8',
//     ),
//     3 => 
//     array (
//       'idjenisscore' => '4',
//       'score' => '6',
//     ),
// score.push('')
    // sendUlasan();
    console.log($(this).serialize());
})

$('.message-control').on('click', function(e){
    e.preventDefault();
    $('.context').hide();
    $(this).siblings(".context").show();
});

$('.context .btn-message-reply').on('click', function(e){
    e.preventDefault();
    $('.context').hide();
    $(this).siblings(".context").show();
});

$('.context .btn-message-edit').on('click', function(e){
    e.preventDefault();
    $('.context').hide();
    $(this).siblings(".context").show();
});

$('.context .btn-message-remove').on('click', function(e){
    e.preventDefault();
    $(this).closest(".message").remove();
});

$(document).on('click', function(event) { 
        if(!$(event.target).closest('.message-control').length && !$(event.target).closest('.context').length) {
            $ (".context").hide();
        }        
    });
</script>
