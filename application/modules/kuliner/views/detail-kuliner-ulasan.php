<style type="text/css">
	/*module*/
	.point
	{
		color: green;
	}

	/*element*/
	#section-form-ulasan--rating
	{
		margin-top: 10px;
		border-radius: 10px;
		padding: 15px;
		min-height: 100px;
		border: 1px solid #ddd;
	}
	#section-form-ulasan--rating .score
	{
		padding: 0px 3px;
	}
	#section-form-ulasan--rating .score .rating-item
	{
		width: 9px;
		height: 16px;
	}
	#section-form-ulasan--rating div.col-point.sum
	{
		width: unset;
		/*flex-grow: 1;*/
		margin-top: -10px;
		padding-right: 7px;

	}
	#section-form-ulasan--rating .sum-score
	{
		background-color: unset;
	}
	#section-form-ulasan--rating .sum-score .text-score
	{
		color: #2D8E41;
	    font-size: 75px;
	    /*letter-spacing: -8px;*/

	}

	#section-form-ulasan--informasi-tambahan
	{
		background-color: #ddd;
		min-height: 300px;
		padding: 10px;
	}
		.informasi-tambahan--box
		{
			margin-left: 10px;
		}
			.informasi-tambahan--box>.informasi-tambahan--item
			{
				font-size: 13px;
				display: flex;
				align-items: center;
				margin: 5px 0px; 
			}
			.rating-star
			{
				background-color: #999;
				border-radius: 50%;
				display: flex;
				padding: 5px;
				align-items: center;
				justify-content: center;
			}
			.rating-star.active
			{
				background-color: #F9690E;
			}
			.rating-star.active .glyphicon
			{
				color: white;
			}
			.rating-star .glyphicon
			{
				color: #666;
			}

	#section-form-ulasan--form-images
	{

	}
		.container-image
		{
			display: flex;
			justify-content: flex-start;
			flex-wrap: wrap;
			margin-top: 15px;
		}
		.ulasan-new-photo
		{
			margin-right: 10px;
			margin-bottom: 10px;
			width: 200px;
			height: 120px;
			background-size: 100%;
			background-position: top;
			background-repeat: no-repeat;
		}
			.ulasan-new-photo .close
			{
				margin-right: 5px;
				margin-top: 5px;
				width: 25px;
				height: 25px;
				display: flex;
				justify-content: center;
				align-items: center;
				border-radius: 50%;
				background-color: black;
			    color: white;
			    font-weight: 300;
			    opacity: .6;
		        border: 1px solid transparent;
			    box-shadow: 0px 0px 11px 1px white;
			}	
</style>
<div class="col-md-12">
	<div class="col-md-2"></div>
	<div class="col-md-10 col-sm-10 col-xs-12">
		<div id="section-form-ulasan--rating">
            <h4 id="judul-produk"><?php echo @$data['JudulProduk'] ?></h4><span id="categori"> <?php echo @$data['kategoritext'] ?>, menanti ulasan anda untuk meningkatkan layanannya. </span>
			<div id="score" class="sum">
                    <div class="col-point sum">
                        <div class="sum-score"><span class="text-score Roboto-300" data-total-rating="rating-tulis-ulasan"> <?php echo @$penilaian['data']['rating'] ?> </span></div>
                    </div>
                    <div class="score text-center" id="score-rasa"><span class="rating-value rating-item-10--color Roboto" data-toggle-score="tulis-ulasan-rasa"> 0 </span>
                        <div class="rating-box" data-rating-group="rating-tulis-ulasan" data-score-name="tulis-ulasan-rasa" data-hoverable="true" data-score="0">
                            <span class="rating-item rating-item-1" value="1"></span>
                            <span class="rating-item rating-item-2" value="2"></span>
                            <span class="rating-item rating-item-3" value="3"></span>
                            <span class="rating-item rating-item-4" value="4"></span>
                            <span class="rating-item rating-item-5" value="5"></span>
                            <span class="rating-item rating-item-6" value="6"></span>
                            <span class="rating-item rating-item-7" value="7"></span>
                            <span class="rating-item rating-item-8" value="8"></span>
                            <span class="rating-item rating-item-9" value="9"></span>
                            <span class="rating-item rating-item-10" value="10"></span>
                        </div>
                        <hr><span>Rasa Makanan</span>
                    </div>
                    <div class="score text-center"><span class="rating-value rating-item-10--color Roboto" data-toggle-score="tulis-ulasan-lokasi">0</span>
                        <div class="rating-box" data-rating-group="rating-tulis-ulasan" data-score-name="tulis-ulasan-lokasi" data-hoverable="true" data-score="0">
                            <span class="rating-item rating-item-1" value="1"></span>
                            <span class="rating-item rating-item-2" value="2"></span>
                            <span class="rating-item rating-item-3" value="3"></span>
                            <span class="rating-item rating-item-4" value="4"></span>
                            <span class="rating-item rating-item-5" value="5"></span>
                            <span class="rating-item rating-item-6" value="6"></span>
                            <span class="rating-item rating-item-7" value="7"></span>
                            <span class="rating-item rating-item-8" value="8"></span>
                            <span class="rating-item rating-item-9" value="9"></span>
                            <span class="rating-item rating-item-10" value="10"></span>
                        </div>
                        <hr><span>Lokasi</span></div>
                    <div class="score"><span class="rating-value rating-item-4--color Roboto" data-toggle-score="tulis-ulasan-kebersihan">0</span>
                        <div class="rating-box" data-rating-group="rating-tulis-ulasan" data-score-name="tulis-ulasan-kebersihan" data-hoverable="true" data-score="0">
                            <span class="rating-item rating-item-1" value="1"></span>
                            <span class="rating-item rating-item-2" value="2"></span>
                            <span class="rating-item rating-item-3" value="3"></span>
                            <span class="rating-item rating-item-4" value="4"></span>
                            <span class="rating-item rating-item-5" value="5"></span>
                            <span class="rating-item rating-item-6" value="6"></span>
                            <span class="rating-item rating-item-7" value="7"></span>
                            <span class="rating-item rating-item-8" value="8"></span>
                            <span class="rating-item rating-item-9" value="9"></span>
                            <span class="rating-item rating-item-10" value="10"></span>
                        </div>
                        <hr><span>Kebersihan</span></div>
                    <div class="score"><span class="rating-value rating-item-10--color Roboto" data-toggle-score="tulis-ulasan-pelayanan">0</span>
                        <div class="rating-box" data-rating-group="rating-tulis-ulasan" data-score-name="tulis-ulasan-pelayanan" data-hoverable="true" data-score="0">
                            <span class="rating-item rating-item-1" value="1"></span>
                            <span class="rating-item rating-item-2" value="2"></span>
                            <span class="rating-item rating-item-3" value="3"></span>
                            <span class="rating-item rating-item-4" value="4"></span>
                            <span class="rating-item rating-item-5" value="5"></span>
                            <span class="rating-item rating-item-6" value="6"></span>
                            <span class="rating-item rating-item-7" value="7"></span>
                            <span class="rating-item rating-item-8" value="8"></span>
                            <span class="rating-item rating-item-9" value="9"></span>
                            <span class="rating-item rating-item-10" value="10"></span>
                        </div>
                        <hr><span>Pelayanan</span></div>
                </div>
		</div>
		<div id="section-form-ulasan--form-basic">
			<div class="form-group">
				<label>Subject</label>
				<input type="text" name="" class="form-control radius-10" placeholder="Tujuan Kunjungan">
			</div>
			<div class="form-group">
				<label>Ulasan</label>
				<textarea class="form-control" id="input_tambah_ulasan" data-limit-text="100" placeholder="Bagikan dan ceritakan pengalaman anda saat mengunjungi tempat ini"></textarea>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<input type="text" name="" class="form-control radius-10" placeholder="@Tag Teman">
					</div>
				</div>
				<div class="col-md-6">

					<div class="form-group">
						<button class="btn btn-default bs-default pull-right" onclick="window.upload_photo_ulasan.browse()"> Tambahkan Foto </button>
						<input type="file" name="upload_photo_ulasan" class="sr-only" multiple="">
					</div>
				</div>
			</div>
		</div>
		<div id="section-form-ulasan--informasi-tambahan" class="radius-10">
			<h5>Informasi Tambahan</h5>
			<div class="informasi-tambahan--box">
				<div class="informasi-tambahan--item Roboto"> Berikan informasi tambahan untuk melengkapi ulasan anda ( Dapatkan poin tambahan ) </div>
				<div class="informasi-tambahan--item Roboto"> Berapa orang? <span class="col-md-2"> <input type="number" class="form-control input-sm" name="" value="3"> </span> <span class="point"> + 0.5 Poin </span>  </div>
				<div class="informasi-tambahan--item Roboto" style="flex-direction: row; flex-wrap: wrap;"> 
					<span style="flex: 0 1 100%;"> Menu yang dipesan</span>
					<span class=""> <input type="number" class="form-control input-sm" name="" value=""> </span> &nbsp; Harga/Porsi (Rp) &nbsp; <span class=""> <input type="number" class="form-control input-sm" name="" value=""> </span> &nbsp; <span class="point"> + 1 Poin </span>  
				</div>
				<div class="informasi-tambahan--item Roboto"> Cita rasa : 
					&nbsp; <span class="rating-star active"><i class="glyphicon glyphicon-star"></i></span> &nbsp; <span class="rating-star active"><i class="glyphicon glyphicon-star"></i></span> &nbsp; <span class="rating-star active"><i class="glyphicon glyphicon-star"></i></span> &nbsp; <span class="rating-star active"><i class="glyphicon glyphicon-star"></i></span> &nbsp; <span class="rating-star"><i class="glyphicon glyphicon-star"></i></span>  
					&nbsp; Enak &nbsp; <span class="point"> + 1 Poin </span>
				</div>
				<div class="informasi-tambahan--item Roboto" style="color: #663399;"> Tambah kolom list menu yang dipesan </div>
				<div class="informasi-tambahan--item Roboto"> Total biaya yang dikeluarkan dalam kunjungan &nbsp; Total (Rp) &nbsp; <span> <input type="number" class="form-control input-sm" name="" value="3" style="width: 100px;"> </span> <span class="point"> + 0.5 Poin </span></div>
				<div class="informasi-tambahan--item Roboto"> 
					<span style="flex: 0 1 100%;"> Menu yang direkomendasi </span>					 
				</div>
				<div class="informasi-tambahan--item Roboto"> 
					<span class=""> <input type="number" class="form-control input-sm" name="" value=""> </span>
				</div>
				<div class="informasi-tambahan--item Roboto" style="color: #663399;"> Tambah kolom list menu yang direkomendasikan </div>
				<div class="informasi-tambahan--item Roboto"> 
					Apakah anda akan mengunjungi kembali tempat ini dilain waktu
				</div>
				<div class="informasi-tambahan--item Roboto" style="justify-content: space-around;"> 
					
					<div class="form-group">
						<div class="radio">
							<label>
								<input type="radio" name="ulasan_form_ingin_berkunjung_lagi" checked> pasti
							</label>
						</div>
					</div>
					
					<div class="form-group">
						<div class="radio">
							<label>
								<input type="radio" name="ulasan_form_ingin_berkunjung_lagi"> Mungkin
							</label>
						</div>
					</div>

					<div class="form-group">
						<div class="radio">
							<label>
								<input type="radio" name="ulasan_form_ingin_berkunjung_lagi"> Tidak lagi
							</label>
						</div>
					</div>
					
					<div class="form-group">
						<span class="point"> + 0.5 Poin </span> 
					</div>

				</div>
			</div>
		</div>
		<div id="section-form-ulasan--form-images">
			<div class="container-image">
				
			</div>
		</div>
	</div>
</div>
