        <?php //echo '<pre>'.print_r($data, true).'</pre>'; ?>
        <?php 
        $images_detail  = json_decode($data['jsonImageDetail']);
        $data_detail    = json_decode($images_detail[0]->data);
        $penilaian      = json_decode($data['jsonPenilaian']);
        $skor           = json_decode($penilaian[0]->jsonSkor);
        ?>

        <div class="container is-mobile">
          <div class="col-md-12 col-xs-12">
            <div id="header-detail-kuliner">
              <div class="col-sm-5 col-md-5">
                <div id="header-detail-kuliner-mobile" class="visible-xs-block">
                  <ul class="pull-left breadcrumb-location">
                    <li><?php echo $data['state']; ?></li>
                    <li><?php echo $data['kabupaten']; ?></li>
                    <li><?php echo $data['city']; ?></li>
                  </ul>
                  <div class="clearfix">
                    <h3 class="judul-item-wisata pull-left"><?php echo $data['JudulProduk'].'-'.$data['area']; ?> <br><small><?php echo $data['kategoritext']." - ". $data['jenisMasakan'] ?>  <span><a href=""><?php  echo  ( !empty($data['jmlCabang'])&&($data['jmlCabang']!='0 cabang'))? "/".$data['jmlCabang'] : ""; ?> </a></span></small></h3>
                    <div class="col-point sum pull-right">
                      <div class="sum-score"><span class="text-score" data-total-rating="restaurant"><?php echo $penilaian[0]->rating; ?></span></div>
                    </div>
                  </div>
                </div>
                <figure class="gambar-profil-kuliner">
                  <img src="<?php echo $data['linkimage']; ?>" class="img-responsive">
                  <label class="atribut-image-kuliner top"><?php //echo $data_detail[0]->kategorifoto;
                  echo $data['JudulProduk'].' - '.$data['area'];?></label>
                  <ul class="atribut-image-kuliner bottom list-inline">
                    <li><i class="fa fa-thumbs-o-up"></i> <?php echo $data['jmlLikeFotoProfil']; ?></li>
                    <li><i class="fa fa-comment-o"></i> <?php echo $data['jmlKomentarFotoProfil']; ?></li>
                    <li><i class="fa fa-share-alt"></i></li>
                  </ul>
                </figure>
                <div class="image-thumbs hidden-xs">
                  <?php $i=0; while ($i < 5) {
                    if($i == 4 ){
                        
                      echo '<div class="image-thumb" data-full-image="'.@$data_detail[$i]->linkimage.'"  data-title="'.@$data_detail[$i]->kategorifoto.'">
                      <img src="'.@$data_detail[$i]->linkthumbnail.'" class="img-responsive">';
                      if($images_detail[0]->jmlFoto>5){
                      echo '<label><a href="'.site_url().'kuliner/foto/'.$data['UrlPage'].'.html">+'.($images_detail[0]->jmlFoto-5).' lagi</a></label>';
                      }
                      echo '</div>';
                    }else{
                      echo '<div class="image-thumb" data-full-image="'.@$data_detail[$i]->linkimage.'" data-title="'.@$data_detail[$i]->kategorifoto.'">
                      <img src="'.@$data_detail[$i]->linkthumbnail.'" class="img-responsive">
                      </div>';
                    }
                    $i++;
                  }
                  ?>
                </div>
              </div>
              <div class="col-sm-7 col-md-7">
                <ul class="pull-left breadcrumb-location hidden-xs">
                  <li><a href="<?php echo base_url('kawasan/'. str_replace(" ", "-", $data['kawasanArea'])).'.html';  ?>" target="_blank"><?php echo $data['state']; ?></a></li>
                  <li><a href="<?php echo base_url('kawasan/'. str_replace(" ", "-", $data['kawasanArea'])).'.html';  ?>" target="_blank"><?php echo $data['kabupaten']; ?></a></li>
                  <li><a href="<?php echo base_url('kawasan/'. str_replace(" ", "-", $data['kawasanArea'])).'.html';  ?>" target="_blank"><?php echo  $data['kawasanArea']; ?></a></li>
                </ul>
                <ul class="pull-right list-inline">
                  <li><button class="btn btn-primary btn-xs active-button"><img class="img-item-wisata" style="width: 14px;" src="<?php echo base_url('assets/icon/iconsimpanputih.svg'); ?>">&nbsp;Simpan</button></li>
                  
<!--                  <li><button class="btn btn-primary btn-xs deactive-button"><img class="img-item-wisata" src="<?php //echo base_url(); ?>assets/img/wisata/data-simpan-icon.svg">Share</button></li>-->
                </ul>
                <div class="clearfix"></div>
                <h3 class="judul-item-wisata hidden-xs"><?php echo $data['JudulProduk'].' - '.$data['area']; ?> <br><span class="fontgedekatdetail"><?php echo $data['kategoritext']." - ". $data['jenisMasakan']; ?> <span class=""><?php echo  (!empty($data['jmlCabang']) && ($data['jmlCabang']!='0 cabang'))? " / ": ""; ?> <a href=""><?php echo  (!empty($data['jmlCabang']) && ($data['jmlCabang']!='0 cabang'))? $data['jmlCabang'] : ""; ?></a> </span></span></h3>

                <ul class="list-unstyled atribut-item-wisata">
                  <li class="alamat-kuliner"><img class="img-item-wisata" src="<?php echo base_url('assets/icon/iconalamat.png'); ?>"> <span class="kekanandetailatas"><?php echo $data['AlamatSurat'].', '.$data['kabupaten'].' '.$data['state']; ?></span></li>
                  <li class="status-jadwal-kuliner">
                      
                      
                                                <?php 
                                                if(trim($data['KeteranganBuka'])==='Buka'){                                                    
                                                ?>  
                                                   <img class="img-item-wisata" src="<?php echo base_url('assets/icon/iconbuka.png');?>"> <span class="kekanandetailatas"> 
                                                   <span class="kekanandetailatasbuka">  <?php echo $data['KeteranganBuka']; ?></span>  <?php echo $data['KeteranganJam']; ?>
                                                <?php }else
                                                {?>
                                                   <img data-u="image" src="<?php echo base_url('assets/icon/icontutup.png') ?>" width="14px" data-toggle="tooltip" title="Tersimpan" data-placement="top"/> <span class="kekanandetailatas"> <?php echo $data['KeteranganBuka']; ?> </span>
                                                <?php }?>
<!--                                                <i class="fa fa-fw fa-clock-o <?php //echo ((!empty($buka[0]->isbuka)) ? (($buka[0]->isbuka == 'Y') ? 'text-green' : 'text-grey') : 'text-grey') ?> " onclick="produk"></i> <span ><?php //echo $datakuliner[$i]['jadwalBukaSekarang']; ?></span>-->
                                                  
                                           
                      </span> <span class="tombol-jadwal-kuliner visible-xs-inline kekanandetailatas">Lihat jadwal</span>                      
                    <div class="jadwal-kuliner">
                      <?php 
                      $jadwal_buka = json_decode($data['jsonJadwalBuka']); 
                      foreach ($jadwal_buka as $jadwal) {
                        echo "<div><span>".$jadwal->NamaHari."</span><span>".$jadwal->jambuka."-".$jadwal->jamtutup." WIB</span>";
                        if(!empty($jadwal->keterangan)) "<span>".$jadwal->keterangan."</span>";
                        echo "</div>";
                      }
                      ?>
                    </li>
                    <li><img class="img-item-wisata" src="<?php echo base_url('assets/icon/iconharga.png');?>"> <span class="kekanandetailatas"><?php echo 'Rp '.$data['hargamin'].'-Rp '.$data['hargamaks'];?></span></li>
                    <li><img class="img-item-wisata" src="<?php echo base_url('assets/icon/icontelepon.png');?>"> 
                      <span class="kekanandetailatas">  
                        <?php 
                    if(!empty($data['phonekontak'])) echo $data['phonekontak']; 
                    elseif(!empty($data['phonekontak2'])) echo $data['phonekontak2']; 
                    else echo '-'; 
                    ?>
                      </span>      
                  </li>
                  <li>
                      <span class="location" data-toggle="tooltip" title="Berdasarkan Titik Pusat Keramaian Kota" data-placement="bottom">
                      <img class="img-item-wisata" src="<?php echo base_url(); ?>assets/img/wisata/jarak-icon.svg"/> 
                      
                      <span class="kekanandetailatas">   <?php echo $data['jarak'];?>km </span> </span>
                      
                  </li>
                </ul>
                <div id="score" class="sum  hidden-xs">
                  <div class="col-point sum">
                    <div class="sum-score"><span class="text-score" data-total-rating="restaurant"><?php echo $penilaian[0]->rating; ?></span></div>
                  </div>
                  <div class="score text-center" id="score-rasa"><span class="rating-value rating-item-10--color" data-toggle-score="rasa" style="color: rgb(232, 15, 10);"> <?php echo $skor[0]->skorRasaMakanan; ?> </span>
                    <div class="rating-box" data-rating-group="restaurant" data-score-name="rasa" data-hoverable="false" data-score="0">
                      <span class="rating-item rating-item-1" value="1"></span>
                      <span class="rating-item rating-item-2" value="2"></span>
                      <span class="rating-item rating-item-3" value="3"></span>
                      <span class="rating-item rating-item-4" value="4"></span>
                      <span class="rating-item rating-item-5" value="5"></span>
                      <span class="rating-item rating-item-6" value="6"></span>
                      <span class="rating-item rating-item-7" value="7"></span>
                      <span class="rating-item rating-item-8" value="8"></span>
                      <span class="rating-item rating-item-9" value="9"></span>
                      <span class="rating-item rating-item-10" value="10"></span>
                    </div>
                    <hr><span>Rasa Makanan</span>
                  </div>
                  <div class="score text-center"><span class="rating-value rating-item-10--color" data-toggle-score="lokasi" style="color: rgb(232, 15, 10);"> <?php echo $skor[1]->skorPelayanan; ?> </span>
                    <div class="rating-box" data-rating-group="restaurant" data-score-name="lokasi" data-hoverable="false" data-score="0">
                      <span class="rating-item rating-item-1" value="1"></span>
                      <span class="rating-item rating-item-2" value="2"></span>
                      <span class="rating-item rating-item-3" value="3"></span>
                      <span class="rating-item rating-item-4" value="4"></span>
                      <span class="rating-item rating-item-5" value="5"></span>
                      <span class="rating-item rating-item-6" value="6"></span>
                      <span class="rating-item rating-item-7" value="7"></span>
                      <span class="rating-item rating-item-8" value="8"></span>
                      <span class="rating-item rating-item-9" value="9"></span>
                      <span class="rating-item rating-item-10" value="10"></span>
                    </div>
                    <hr><span>Lokasi</span></div>
                    <div class="score"><span class="rating-value rating-item-4--color" data-toggle-score="kebersihan" style="color: rgb(232, 15, 10);"> <?php echo $skor[2]->skorKebersihan; ?> </span>
                      <div class="rating-box" data-rating-group="restaurant" data-score-name="kebersihan" data-hoverable="false" data-score="0">
                        <span class="rating-item rating-item-1" value="1"></span>
                        <span class="rating-item rating-item-2" value="2"></span>
                        <span class="rating-item rating-item-3" value="3"></span>
                        <span class="rating-item rating-item-4" value="4"></span>
                        <span class="rating-item rating-item-5" value="5"></span>
                        <span class="rating-item rating-item-6" value="6"></span>
                        <span class="rating-item rating-item-7" value="7"></span>
                        <span class="rating-item rating-item-8" value="8"></span>
                        <span class="rating-item rating-item-9" value="9"></span>
                        <span class="rating-item rating-item-10" value="10"></span>
                      </div>
                      <hr><span>Kebersihan</span></div>
                      <div class="score"><span class="rating-value rating-item-10--color" data-toggle-score="pelayanan" style="color: rgb(232, 15, 10);"> <?php echo $skor[3]->skorLokasi; ?> </span>
                        <div class="rating-box" data-rating-group="restaurant" data-score-name="pelayanan" data-hoverable="false" data-score="0">
                          <span class="rating-item rating-item-1" value="1"></span>
                          <span class="rating-item rating-item-2" value="2"></span>
                          <span class="rating-item rating-item-3" value="3"></span>
                          <span class="rating-item rating-item-4" value="4"></span>
                          <span class="rating-item rating-item-5" value="5"></span>
                          <span class="rating-item rating-item-6" value="6"></span>
                          <span class="rating-item rating-item-7" value="7"></span>
                          <span class="rating-item rating-item-8" value="8"></span>
                          <span class="rating-item rating-item-9" value="9"></span>
                          <span class="rating-item rating-item-10" value="10"></span>
                        </div>
                        <hr><span>Pelayanan</span></div>
                      </div>
                      <ul class="list-unstyled list-inline text-right atribut-item-wisata">
                        <li><img class="img-item-wisata" src="<?php echo base_url(); ?>assets/img/wisata/data-simpan-icon.svg"><?php echo 'Simpan '.$data['jmlFavorit']; ?></li>
                        <li><img class="img-item-wisata" src="<?php echo base_url(); ?>assets/img/wisata/data-ulasan-icon.svg"><?php echo 'Ulasan '.$data['jmlUlasan']; ?></li>
                        <li><img class="img-item-wisata" src="<?php echo base_url(); ?>assets/img/wisata/data-foto-icon.svg"><?php echo 'Foto '.$data['jmlFoto']; ?></li>
                      </ul>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="row">
        <div class="col-md-9">
          <div id="detail-kuliner-informasi" class="tab-page">
            <ul class="tabs-kuliner clearfix tabs-flying">
              <a href="<?php echo site_url().'index.php/kuliner/informasi/'.$data['UrlPage'].'.html';?>" class="tab-item">Informasi</a>
              <a target="_blank" href="<?php echo site_url().'index.php/kuliner/foto/'.$data['UrlPage'].'.html';?>" class="tab-item">Foto</a>
              <a target="_blank" href="<?php echo site_url().'index.php/kuliner/ulasan/'.$data['UrlPage'].'.html';?>" class="tab-item active">Ulasan</a>
            </ul>

            <ul class="tabs-kuliner no-flying clearfix">
              <a href="<?php echo site_url().'index.php/kuliner/informasi/'.$data['UrlPage'].'.html';?>" class="tab-item">Informasi</a>
              <a target="_blank" href="<?php echo site_url().'index.php/kuliner/foto/'.$data['UrlPage'].'.html';?>" class="tab-item">Foto</a>
              <a target="_blank" href="<?php echo site_url().'index.php/kuliner/ulasan/'.$data['UrlPage'].'.html';?>" class="tab-item active">Ulasan</a>
            </ul>
          </div>
        
             <?php $datahastag="";
               $this->load->view('__hastag',$datahastag); ?>
          <div class="detail-kuliner-box">
            <h3 class="detail-title">Tulis ulasan</h3>
          </div>
        </div>
        <div class="col-md-3 style-content">
         <div style="margin-top: -20px">   
           <?php get_iklankanan(!empty($iklan) ? $iklan : false) ?>
         </div>   
          <div class="clearfix"></div>

      </div>
      </div>
    
    </div>
        


  </div>
</div>

</div>
<div class="clearfix"></div>

<script type="text/javascript" src="<?php echo base_url().'assets/js/imam/module-2.js'; ?>"></script>