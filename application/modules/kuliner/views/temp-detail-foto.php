<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/module-2.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/module-2-responsive.css'); ?>">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css" />
</head>
<body>
	<div class="container is-mobile">
		<div class="col-md-12 col-xs-12">
			<div id="header-detail-kuliner">
				<div class="col-sm-5 col-md-5">
					<div id="header-detail-kuliner-mobile" class="visible-xs-block">
						<ul class="pull-left breadcrumb-location">
						<li>Daerah Istimewa Yogyakarta</li>
						<li>Kabupaten Bantul</li>
						<li>Delingo</li>
					</ul>
					<div class="clearfix">
						<h3 class="judul-item-wisata pull-left">Sesanti Restorant-Wedomartani <br><small>Restoran - Masakan Indonesia - Jawa <span>/</span> <span>5 Cabang</span></small></h3>
						<div class="col-point sum pull-right">
							<div class="sum-score"><span class="text-score" data-total-rating="restaurant">0.0</span></div>
                        </div>
					</div>
					</div>
					<figure class="gambar-profil-kuliner">
						<img src="https://placeimg.com/800/450/nature" class="img-responsive">
						<label class="atribut-image-kuliner top">Foto menu sajian</label>
						<ul class="atribut-image-kuliner bottom list-inline">
							<li><i class="fa fa-thumbs-o-up"></i>3</li>
							<li><i class="fa fa-comment-o"></i>3</li>
							<li><i class="fa fa-share-alt"></i></li>
						</ul>
					</figure>
					<div class="image-thumbs hidden-xs">
						<?php for($i=0; $i<4; $i++){ ?>
						<div class="image-thumb">
							<img src="http://via.placeholder.com/800x450" class="img-responsive">
						</div>
						<?php } ?>
						<a class="image-thumb" href="">
							<img src="https://placeimg.com/800/450/nature" class="img-responsive">
							<label>+50 lagi</label>
						</a>
					</div>
				</div>
				<div class="col-sm-7 col-md-7">
					<ul class="pull-left breadcrumb-location hidden-xs">
						<li>Daerah Istimewa Yogyakarta</li>
						<li>Kabupaten Bantul</li>
						<li>Delingo</li>
					</ul>
					<ul class="pull-right list-inline">
						<li><button class="btn btn-primary btn-xs active-button"><img class="img-item-wisata" src="<?php echo base_url(); ?>assets/img/wisata/data-simpan-icon.svg">Simpan</button></li>
						<li><button class="btn btn-primary btn-xs deactive-button"><img class="img-item-wisata" src="<?php echo base_url(); ?>assets/img/wisata/data-simpan-icon.svg">Share</button></li>
					</ul>
					<div class="clearfix"></div>
					<h3 class="judul-item-wisata hidden-xs">Sesanti Restorant-Wedomartani <br><small>Restoran - Masakan Indonesia - Jawa / <span>5 Cabang</span></small></h3>
					
					<ul class="list-unstyled atribut-item-wisata">
						<li class="alamat-kuliner"><img class="img-item-wisata" src="<?php echo base_url(); ?>assets/img/wisata/buka-on-icon.svg"> <span>Jl. Pelanggaran Tentara Pelajar No. 52 A, Sariharjo, ngaglik, Kabupaten Sleman Yogyakarta</span></li>
						<li class="status-jadwal-kuliner">
							<img class="img-item-wisata" src="<?php echo base_url(); ?>assets/img/wisata/buka-on-icon.svg">  07.00-17.00 WIB <span class="tombol-jadwal-kuliner visible-xs-inline">Lihat jadwal</span>
							<div class="jadwal-kuliner">
								<div class="hari-aktif">
									<span>Senin</span><span>07.00-15.00 WIB</span>
									<span>Spesial masakan jawa</span>
								</div>
								<div><span>Selasa</span><span>07.00-15.00 WIB</span></div>
								<div><span>Rabu</span><span>07.00-15.00 WIB</span></div>
								<div><span>Kamis</span><span>07.00-15.00 WIB</span></div>
								<div>
									<span>Jumat</span><span>07.00-15.00 WIB</span>
									<span>Buka setelah sholat jum'at</span>
								</div>
								<div><span>Sabtu</span><span>07.00-15.00 WIB</span></div>
								<div><span>Minggu</span><span>Tutup</span></div>
							</div>
						</li>
						<li><img class="img-item-wisata" src="<?php echo base_url(); ?>assets/img/wisata/buka-on-icon.svg"> Rp 8.000-Rp 12.000</li>
						<li><img class="img-item-wisata" src="<?php echo base_url(); ?>assets/img/wisata/buka-on-icon.svg"> 08132456793</li>
						<li><img class="img-item-wisata" src="<?php echo base_url(); ?>assets/img/wisata/jarak-icon.svg"> 2km</li>
					</ul>
                    <div id="score" class="sum  hidden-xs">
                            <div class="col-point sum">
                                <div class="sum-score"><span class="text-score" data-total-rating="restaurant">0.0</span></div>
                            </div>
                            <div class="score text-center" id="score-rasa"><span class="rating-value rating-item-10--color" data-toggle-score="rasa" style="color: rgb(232, 15, 10);"> 0 </span>
                                <div class="rating-box" data-rating-group="restaurant" data-score-name="rasa" data-hoverable="false" data-score="0">
                                    <span class="rating-item rating-item-1" value="1"></span>
                                    <span class="rating-item rating-item-2" value="2"></span>
                                    <span class="rating-item rating-item-3" value="3"></span>
                                    <span class="rating-item rating-item-4" value="4"></span>
                                    <span class="rating-item rating-item-5" value="5"></span>
                                    <span class="rating-item rating-item-6" value="6"></span>
                                    <span class="rating-item rating-item-7" value="7"></span>
                                    <span class="rating-item rating-item-8" value="8"></span>
                                    <span class="rating-item rating-item-9" value="9"></span>
                                    <span class="rating-item rating-item-10" value="10"></span>
                                </div>
                                <hr><span>Rasa Makanan</span>
                            </div>
                            <div class="score text-center"><span class="rating-value rating-item-10--color" data-toggle-score="lokasi" style="color: rgb(232, 15, 10);">0</span>
                                <div class="rating-box" data-rating-group="restaurant" data-score-name="lokasi" data-hoverable="false" data-score="0">
                                    <span class="rating-item rating-item-1" value="1"></span>
                                    <span class="rating-item rating-item-2" value="2"></span>
                                    <span class="rating-item rating-item-3" value="3"></span>
                                    <span class="rating-item rating-item-4" value="4"></span>
                                    <span class="rating-item rating-item-5" value="5"></span>
                                    <span class="rating-item rating-item-6" value="6"></span>
                                    <span class="rating-item rating-item-7" value="7"></span>
                                    <span class="rating-item rating-item-8" value="8"></span>
                                    <span class="rating-item rating-item-9" value="9"></span>
                                    <span class="rating-item rating-item-10" value="10"></span>
                                </div>
                                <hr><span>Lokasi</span></div>
                            <div class="score"><span class="rating-value rating-item-4--color" data-toggle-score="kebersihan" style="color: rgb(232, 15, 10);">0</span>
                                <div class="rating-box" data-rating-group="restaurant" data-score-name="kebersihan" data-hoverable="false" data-score="0">
                                    <span class="rating-item rating-item-1" value="1"></span>
                                    <span class="rating-item rating-item-2" value="2"></span>
                                    <span class="rating-item rating-item-3" value="3"></span>
                                    <span class="rating-item rating-item-4" value="4"></span>
                                    <span class="rating-item rating-item-5" value="5"></span>
                                    <span class="rating-item rating-item-6" value="6"></span>
                                    <span class="rating-item rating-item-7" value="7"></span>
                                    <span class="rating-item rating-item-8" value="8"></span>
                                    <span class="rating-item rating-item-9" value="9"></span>
                                    <span class="rating-item rating-item-10" value="10"></span>
                                </div>
                                <hr><span>Kebersihan</span></div>
                            <div class="score"><span class="rating-value rating-item-10--color" data-toggle-score="pelayanan" style="color: rgb(232, 15, 10);">0</span>
                                <div class="rating-box" data-rating-group="restaurant" data-score-name="pelayanan" data-hoverable="false" data-score="0">
                                    <span class="rating-item rating-item-1" value="1"></span>
                                    <span class="rating-item rating-item-2" value="2"></span>
                                    <span class="rating-item rating-item-3" value="3"></span>
                                    <span class="rating-item rating-item-4" value="4"></span>
                                    <span class="rating-item rating-item-5" value="5"></span>
                                    <span class="rating-item rating-item-6" value="6"></span>
                                    <span class="rating-item rating-item-7" value="7"></span>
                                    <span class="rating-item rating-item-8" value="8"></span>
                                    <span class="rating-item rating-item-9" value="9"></span>
                                    <span class="rating-item rating-item-10" value="10"></span>
                                </div>
                                <hr><span>Pelayanan</span></div>
                        </div>
					<ul class="list-unstyled list-inline text-right atribut-item-wisata">
						<li><img class="img-item-wisata" src="<?php echo base_url(); ?>assets/img/wisata/data-simpan-icon.svg">Simpan 42</li>
						<li><img class="img-item-wisata" src="<?php echo base_url(); ?>assets/img/wisata/data-ulasan-icon.svg">Ulasan 2</li>
						<li><img class="img-item-wisata" src="<?php echo base_url(); ?>assets/img/wisata/data-foto-icon.svg">Foto 35</li>
					</ul>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="row">
				<div class="col-md-8">

					<div id="detail-kuliner-informasi"  class="tab-page">
						<ul class="tabs-kuliner clearfix">
						  <a target="_blank" href="<?php echo site_url().'index.php/kuliner/con_kuliner_detail/temp_detail/informasi';?>" class="tab-item">Informasi</a>
						  <a href="<?php echo site_url().'index.php/kuliner/con_kuliner_detail/temp_detail/foto';?>" class="tab-item active">Foto</a>
						  <a target="_blank" href="<?php echo site_url().'index.php/kuliner/con_kuliner_detail/temp_detail/ulasan';?>" class="tab-item">Ulasan</a>
						</ul>
					</div>

					<div id="foto-detail-kuliner">
						<div class="pull-right upload-gambar-kuliner">
							Berbagi dengan kami <button class="btn btn-danger"><img class="img-item-wisata" src="<?php echo base_url(); ?>assets/img/wisata/data-foto-icon.svg"> Tambah Foto</button>
						</div>
						<div class="clearfix"></div>
					</div>
					
					<div id="detail-foto-360" class="detail-kuliner-box is-mobile">
						<h3 class="detail-title">Foto 360&deg;</h3>
						<?php for($i=0; $i<8; $i++) { ?>
							<div class="col-md-3 col-xs-6 foto-360">
								<img src="http://via.placeholder.com/800x450" class="img-responsive">
							</div>
						<?php } ?>
						<div class="clearfix"></div>
					</div>
					
					<div id="foto-detail-kuliner" class="detail-kuliner-box is-mobile">
						<h3 class="detail-title">Foto</h3>
						<?php for($i=0; $i<32; $i++) { ?>
							<div class="col-md-3 col-xs-6 foto-360">
								<img src="http://via.placeholder.com/800x450" class="img-responsive">
							</div>
						<?php } ?>
						<div class="clearfix"></div>
						<a href="" class="next-image">Lihat Selanjutnya (+20)</a>
					</div>

				</div>
				<div class="col-md-4">
					
				</div>
			</div>
			<div class="row">
				<div class="col-md-8">
					<div id="hashtag" class="detail-kuliner-box">
							<form class="form-inline">
								<label>#</label>
								<input type="text" class="form-control" placeholder="Cari berdasarkan hashtag">
								<button type="submit" class="btn btn-default"><img class="img-item-wisata" src="<?php echo base_url(); ?>assets/img/wisata/search-icon.svg" alt="cari"></button>
							</form>
							<label class="hashtag-title">Popular Hashtag di Sleman, D.I Yogyakarta</label>
							<div>
								<?php for($i=0;$i<11;$i++) { ?>
								
								<a class="tag" href="">sateenak</a>
								<a class="tag" href="">baksohits</a>
								<a class="tag" href="">jajanseru</a>
								<a class="tag" href="">semplakenak</a>
								<a class="tag" href="">vegetarian</a>
								<a class="tag" href="">bestspageti</a>
								<?php } ?>
							</div>
					</div>
				</div>
			</div>

		</div>
	</div>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url().'assets/js/module-2.js'; ?>"></script>
</body>
</html>