<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="utf-8" />
    <title><?php echo (!empty($title) ? $title : 'Hallo') ?></title>
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script>var domain = "<?php echo base_url();?>";</script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">

    <!--<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />-->

    <link rel="stylesheet" href="<?php echo base_url('assets/js/jquery-ui/jquery-ui.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap-submenu.min.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/fonts/awesome/4.7.0/css/font-awesome.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/fonts/material-icons.css');?>">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css" />

    <link rel="stylesheet" href="<?php echo base_url('assets/css/kuliner.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/kuliner.media.css') ?>">

    <link rel="stylesheet" href="<?php echo base_url('assets/css/salim/slider.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/salim/general.css') ?>">

    <link rel="stylesheet" href="<?php echo base_url('assets/css/menu-wisata.css') ?>">

    <link rel="stylesheet" href="<?php echo base_url('assets/css/imam/module-2.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/imam/module-2-responsive.css') ?>">

    <!--[if !IE]> --><script src="<?php echo base_url('assets/js/jquery-2.1.4.min.js'); ?>"></script><!-- <![endif]-->
    <!--[if IE]>--><script src="<?php echo base_url('assets/js/jquery-1.11.3.min.js'); ?>"></script><!--[endif]-->
    <!--[if lt IE 9]>-->
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <!--[endif]-->
    <script src="<?php echo base_url('assets/js/jquery-ui/jquery-ui.js'); ?>"></script>

    <script src="<?php echo base_url('assets/js/jssor.slider-27.1.0.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js') ?>" type="text/javascript"></script>
</head>

<body>
  <header>
      <div class="header-top">
          <div class="container section-navbar" style="border-bottom:1px solid white;">
              <div class="col-sm-2 section-navbar-box topleftmenu">
                  <div class="row">
                      <div class="my-top-box">
                          <h2 class="title">
                              <a style ="color:white" href="<?php echo base_url(); ?>">Tanpa Judul </a>
                          </h2>
                      </div>
                  </div>
              </div>
              <div class="col-sm-7 section-navbar-box topcentermenu">
                  <div class="row">
                      <div class="my-top-box">
                          <div class="col-md-4" style="padding:0;">
                            <div class="input-group">
                              <button type="button" class="form-button active" style="border-top-right-radius:0;border-bottom-right-radius:0;">Lokasi</botton>
                                <button class="form-button" type="botton" style="border-top-left-radius:0;border-bottom-left-radius:0;">
                                    Nama Restoran
                                </button>
                            </div>
                          </div>
                          <div class="col-md-8" style="padding:0;">
                              <form action="search" method="GET">
                                  <input type="text" class="search-map" placeholder="Apa yang anda cari ?" autocomplete="off" id="search" name="search">
                              </form>
                          </div>

                      </div>
                  </div>
              </div>
              <div class="col-sm-3 section-navbar-box toprightmenu">
                  <div class="row">
                      <div class="my-top-box">
                          <div class="navbar-topicon-search">
                              <span class=""  style="cursor:pointer;border-bottom: 1px solid transparent;float: left;margin-right: 10px;border-radius: 4px;width:66px;height:36px;border-radius:15px;padding-right:5px;padding-left:5px;background-color:orange;">
                                  <img class="img-responsive" style="max-width:36px;margin-left:10px;" src="<?php echo base_url('assets/icon/search.png'); ?>">
                              </span>
                          </div>
                          <div class="navbar-topicon-start" id="topicon-start">
                            <div class="top-wilayahhover">
                                <div class="top-wilayahhover-content">
                                    <a style="color:white;display:inline-block;">
                                        <div class="">Kabupaten Sleman</div>
                                        <div class="">D.I Yogyakarta</div>

                                    </a>
                                    <span class=""  style="cursor:pointer;display:inline-block;">
                                        <img class="img-responsive" style="max-width:36px;margin-left:10px;" src="<?php echo base_url('assets/icon/map_white.png'); ?>">
                                    </span>
                                </div>
                            </div>
                          </div>


                          <div class="my-clear"></div>
                      </div>
                  </div>
              </div>
          </div>
          <div id="map-selected">
            <div class="col-md-9"></div>
            <div class="col-md-3"></div>
            &nbsp;
          </div>

      </div>


      <!-- <div class="header-mobile" style="display: none;">
          <div class="container">
              <div class="row">
                  <div class="mobile-top">
                      <div class="top-1st">
                          <div class="mobile-button-menu">
                              <a href="#menu" id="resmenu_open">
                                  <i class="fa fa-fw fa-bars"></i>
                              </a>
                          </div>
                          <a href="#"><img src="<?php echo base_url('assets/icon/wisata_white.png'); ?>"></a>
                          <h2>Kuliner Trip</h2>
                      </div>
                      <div class="top-2nd">
                          <div class="mobile-button-menu">&nbsp;</div>
                          <a id="search_open" href="#search"><img src="<?php echo base_url('assets/icon/search.png'); ?>"></a>
                          <h2 class="kab">Kabupaten Sleman</h2>
                          <h2 class="prov">D.I Yogyakarta</h2>
                      </div>
                  </div>
                  <div class="mbl-search" id="show_search" style="display: none;">
                      <div class="mbl-heightsearch">
                          <div class="mbl-search-form">
                              <a class="search-close" id="search_close">
                                  <i class="fa fa-fw fa-times"></i>
                              </a>
                              <h2><i class="fa fa-fw fa-map-marker"></i> Kuliner Trip</h2>
                              <input type="search" placeholder="Apa yang anda cari ?" autocomplete="off" id="mbl-search" name="search" class="my-input">
                              <button><i class="fa fa-fw fa-search"></i></button>
                          </div>

                          <div class="mbl-search-content">
                              <a class="" href="<?php echo base_url('kulinerkhas'); ?>">
                                  <i class="fa fa-fw fa-map-marker"></i>
                                  <span>Makanan Khas Sini</span>
                              </a>
                              <a class="">
                                  <i class="fa fa-fw fa-history"></i>
                                  <span>Terakhir Di Lihat</span>
                              </a>
                          </div>
                      </div>
                      <div class="mbl-rell">
                          <div class="mbl-out">
                              <h5 class="mbl-heighth5 text-center">Hasil Pencarian Anda</h5>
                              <div class="mbl-result" id="mbl-result"></div>
                          </div>
                      </div>
                  </div>
                  <div class="mbl-dropdown" id="show_menu_resp" style="display: none;">
                      <div class="menuhover-frame">
                          <div class="menuhover-user">
                              <a class="res_close" id="resmnu_close">
                                  <i class="fa fa-fw fa-times"></i>
                              </a>
                              <div class="menuhover-userfoto">
                                  <img src="<?php echo base_url('assets/icon/avatar.png'); ?>" alt="">
                              </div>
                              <div class="menuhover-userinfo">
                                  <h4>Mulya Hari Vano</h4>
                                  <span>
                                      <a href="#">Sign In</a>
                                  </span>
                                  <span>
                                      <a href="#">Sign Up</a>
                                  </span>
                              </div>
                          </div>
                          <ul>
                              <li><a id="showsecond" data-target="menu_1">Test AAA <i class="fa fa-fw fa-angle-right"></i></a></li>
                              <li><a id="" data-target="menu_2">Test BBB <i class="fa fa-fw fa-angle-right"></i></a></li>
                              <li><a id="" data-target="menu_3">Test CCC <i class="fa fa-fw fa-angle-right"></i></a></li>
                              <li><a id="" data-target="menu_4">Test DDD <i class="fa fa-fw fa-angle-right"></i></a></li>
                          </ul>
                      </div>

                      <div class="dropdown-2nd" id="show_menu_resp_2nd" style="display: none;">
                          <div class="dropdown-back"><a><i class="fa fa-fw fa-angle-left"></i> Back</a></div>
                          <ul id="menu_1">
                              <li><a href="#">2and Menu AAA <i class="fa fa-fw fa-angle-right"></i></a></li>
                              <li><a href="#">2and Menu AAA <i class="fa fa-fw fa-angle-right"></i></a></li>
                              <li><a href="#">2and Menu AAA <i class="fa fa-fw fa-angle-right"></i></a></li>
                              <li><a href="#">2and Menu AAA <i class="fa fa-fw fa-angle-right"></i></a></li>
                          </ul>
                          <ul id="menu_2">
                              <li><a href="#">2and Menu BBB <i class="fa fa-fw fa-angle-right"></i></a></li>
                              <li><a href="#">2and Menu BBB <i class="fa fa-fw fa-angle-right"></i></a></li>
                              <li><a href="#">2and Menu BBB <i class="fa fa-fw fa-angle-right"></i></a></li>
                              <li><a href="#">2and Menu BBB <i class="fa fa-fw fa-angle-right"></i></a></li>
                          </ul>
                          <ul id="menu_3">
                              <li><a href="#">2and Menu CCC <i class="fa fa-fw fa-angle-right"></i></a></li>
                              <li><a href="#">2and Menu CCC <i class="fa fa-fw fa-angle-right"></i></a></li>
                              <li><a href="#">2and Menu CCC <i class="fa fa-fw fa-angle-right"></i></a></li>
                              <li><a href="#">2and Menu CCC <i class="fa fa-fw fa-angle-right"></i></a></li>
                          </ul>
                          <ul id="menu_4">
                              <li><a href="#">2and Menu DDD <i class="fa fa-fw fa-angle-right"></i></a></li>
                              <li><a href="#">2and Menu DDD <i class="fa fa-fw fa-angle-right"></i></a></li>
                              <li><a href="#">2and Menu DDD <i class="fa fa-fw fa-angle-right"></i></a></li>
                              <li><a href="#">2and Menu DDD <i class="fa fa-fw fa-angle-right"></i></a></li>
                          </ul>
                      </div>
                  </div>
              </div>
          </div>
      </div> -->

      <div class="header-2nd" style="background-color:#5E3313 !important;color:white;">
        <div style="display:inline-block;">
          <div target="kategori" class="filtermap" style="padding:10px;display:inline-block;margin-left:10px;">Kategori | Pilih</div>
          <div id="kategori" class="hide map5" style="padding:15px;position:absolute;background-color:white;z-index:100;color:black;margin-left:10px;"></div>
        </div>
        <div style="display:inline-block;">
          <div target="masakan" class="filtermap" style="padding:10px;margin-left:10px;">Masakan | Pilih</div>
          <div id="masakan" class="hide map5" style="padding:15px;position:absolute;background-color:white;z-index:100;color:black;margin-left:10px;"></div>
        </div>
        <div style="display:inline-block;">
          <div target="fasilitas" class="filtermap" style="padding:10px;margin-left:10px;">Fasilitas | Pilih</div>
          <div id="fasilitas" class="hide map5" style="padding:15px;position:absolute;background-color:white;z-index:100;color:black;margin-left:10px;"></div>
        </div>
        <div style="display:inline-block;">
          <div target="tujuan" class="filtermap" style="padding:10px;margin-left:10px;">Tujuan | Pilih</div>
          <div id="tujuan" class="hide map5" style="padding:15px;position:absolute;background-color:white;z-index:100;color:black;margin-left:10px;"></div>
        </div>
      </div>

  </header>
  <?php $this->load->view('__global/form/modal_filter2'); ?>
  <?php $this->load->view('__global/form/modal_filter_masakan'); ?>

  <!-- <iframe
  width="100%"
  height="100%"
  frameborder="0" style="border:0;height:100vh;"
  src="https://www.google.com/maps/embed/v1/search?key=AIzaSyBgfrBb-2PZW8_gLuuWROORFJLQtoY4Vrw
    &q=Space+Needle,Seattle+WA" allowfullscreen>
</iframe> -->
<div id="map" style="border:0;height:100vh;width:100%;"></div>
    <script>
      function initMap() {
        var uluru = {lat: -25.363, lng: 131.044};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 4,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBgfrBb-2PZW8_gLuuWROORFJLQtoY4Vrw&callback=initMap">
    </script>

<script src="<?php echo base_url('assets/js/jquery.redirect.js') ?>" type="text/javascript"></script>
<!-- <script src="<?php echo base_url('assets/js/KulinerTrip.SDK.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.matchHeight.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/menu-wisata.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/wisata.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/wisata_landing.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/kuliner.js') ?>" type="text/javascript"></script> -->
<script src="<?php echo base_url('assets/js/salim/price.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/salim/map.js') ?>" type="text/javascript"></script>

</body>
</html>
