<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Core extends CI_controller {

    public $pageload;
    public $user_data = array();

    function __construct(){
        parent :: __construct ();

        $this->load->helper(array(
            GLOBAL_LIB. '/core',
            GLOBAL_LIB. '/kulinertrip',
            GLOBAL_LIB. '/pagecontent',
            GLOBAL_LIB. '/pageiklan',
            GLOBAL_LIB. '/kanan',
        ));
    }

}