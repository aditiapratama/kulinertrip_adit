<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$autoload['packages'] = array();


$autoload['libraries'] = array(
    'database',
    'pagination',
    'parser',
    'session',
    'form_validation',
    'table',
    'xmlrpc',
    'encrypt'
);


$autoload['drivers'] = array();
$autoload['helper'] = array('url', 'file', 'form', 'captcha', 'cookie');
$autoload['config'] = array();
$autoload['language'] = array('sn');
$autoload['model'] = array();