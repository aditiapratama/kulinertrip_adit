<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| New Routing
| -------------------------------------------------------------------------
*/

$route['default_controller'] = 'kuliner/con_kuliner';
$route['kuliner']               = 'kuliner/con_kuliner';
$route['kuliner/(:any)/(:any).html']   = 'kuliner/con_kuliner_detail/index/$1/$2';
//$route['kuliner/foto/(:any).html']        = 'kuliner/con_kuliner_detail/foto/$1';

//$route['kuliner/(:any).html']   = 'kuliner/con_kuliner_detail/temp_detail/$1';


$route['wisata']                = 'wisata/con_landing_wisata';
$route['wisata/detail']         = 'wisata/con_wisata_detail';
//$route['wisata/(:any)/(:any).html']   = 'kuliner/con_wisata_detail/index';
$route['wisata/(:any)/(:any).html']   = 'wisata/con_wisata_detail/index/$1/$2';

$route['pilihan/(:any)/(:any).html']                = 'pilihan/con_pilihan/index/$1/$2';
$route['kawasan/(:any).html']                = 'kawasan/con_kawasan/index/$1';
$route['filter/(:any).html']                = 'hasilfilter/con_hasilfilter/index/$1';

$route['p_pengaturan']='pengaturan/Con_pengaturan/setpengaturan';
/* ------------------------------------------------------------------------- */
/* Get Single */
$route['getdaerah']         = GLOBAL_LIB . '/con_global/get_daerah';
$route['setdaerah']         = GLOBAL_LIB . '/con_global/set_daerah';
$route['readjsondaerah']    = GLOBAL_LIB . '/con_global/read_json_daerah';
$route['getfilter']         = GLOBAL_LIB . '/con_global/json_filter';
//$route['terapkanfilter']    = GLOBAL_LIB . '/con_global/terapkan_filter';
$route['terapkanfilter']    = 'hasilfilter/con_hasilfilter/index/';
$route['jumlahfilter']      = GLOBAL_LIB . '/con_global/jumlah_filter';
$route['getpilihdaerah']    = GLOBAL_LIB . '/con_global/get_pilihdaerah';
$route['getauto']           = GLOBAL_LIB . '/con_search';

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
//$route['default_controller'] = 'welcome';
//$route['default_controller'] = 'start/con_start';
$route['404_override'] = '';
//$route['404_override'] = GLOBAL_LIB . '/con_global/nourl';
$route['translate_uri_dashes'] = FALSE;
