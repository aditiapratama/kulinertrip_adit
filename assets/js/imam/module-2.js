$('#header-detail-kuliner .image-thumb')
.on("mouseenter", function () {
    $('#figur1').show();
    $('#figur2').hide();
		var source = $(this).data('full-image');
		var title = $(this).data('title');
		$('#header-detail-kuliner .gambar-profil-kuliner img').attr('src', source);
		$('#header-detail-kuliner .gambar-profil-kuliner figure .atribut-image-kuliner.top').text(title);
});

$('#header-detail-kuliner .image-thumb')
.on("mouseleave", function () {
    $('#figur1').hide();
    $('#figur2').show();
		
});

$('#detail-daftar-menu .mobile-slider').slick({
  // centerMode: true,
  infinite: false,
  centerPadding: '1.5px',
  slidesToShow: 3
  });

$('#detail-foto-360 .mobile-slider, #foto-detail-kuliner .mobile-slider').slick({
  // variableHeight: true,
  // centerMode: true,
  infinite: false,
  centerPadding: '1.5px',
  slidesToShow: 2,
  
  });

$('#fasilitas-detail-kuliner a').on('click', function(e){
	e.preventDefault();
	$('#fasilitas-detail-kuliner .lebih-banyak').toggle();
});

$('.tombol-jadwal-kuliner').on('click', function(e){
	e.preventDefault();
	$('.jadwal-kuliner').toggle().css('margin-top','0');
});
   
$(function() {
	var parent    = $('#detail-kuliner-informasi'),
		tabsFlying= $('.tabs-kuliner.tabs-flying'),
    tabsTop	  = $('.tabs-kuliner.tabs-flying').css('top'),
    originalY = parent.offset().top - parseInt(tabsTop);
    if(parseInt(tabsTop) != 0)
    {
      originalY-= 57;
    }
    $(window).scroll(function() {
        if ($(window).scrollTop() > originalY ) {
            tabsFlying.css('display', 'block');
            tabsFlying.css('width', parent.width());
        } else {
        	tabsFlying.css('display', 'none');
        }
    });
    
});