$(document).ready(function () {
    var modalDownload = false;
	$('#btn-wisata').on('click', function(e){ 
		e.preventDefault();
        if(modalDownload == false)
        {
            $.ajax({
                url: kulinertrip.site_url('index.php/__tema/con_kategori_wisata'),
                type: 'GET',
                success: function (data) {
                    $('body').append(data);
                    modalDownload = true;
                    $("#open-kategori-wisata")[0].click();
                },
                error: function (data) {
                    alert('Terjadi error pada modal kategori wisata');
                }
            });
        }else{
            $('#open-kategori-wisata')[0].click();
        }
	});
});