var kulinertrip = (function(){
	var o = function(){
		this.cache_records = {}
		this.initialize_object = {}
	}
	o.prototype = {
		request: function(options)
		{
			var that = this;
			options = $.extend({
				caching: false,
				name: 'default'
			}, options)

			var req = $.ajax(options);
			$(document).trigger('request.'+options.name)
			if(options.caching && options.name)
			{
				that.cache_records[options.name] = {result: {}, options: options, process: req};
			}
			req.done(function(res){
				$(document).trigger('request.'+options.name+'.done', [res])
				$(document).trigger('KT.request.'+options.name+'.done', [res])
//				that.cache_records[options.name].result = res;
			})
			.fail(function(res){
				$(document).trigger('KT.request.'+options.name+'.fail', [res])
			})
			.always(function(res){
				$(document).trigger('KT.request.'+options.name, [res])
			})
			.then(function(res){
				$(document).trigger('KT.request.'+options.name+'.then', [res])
			})

			return req;
		},
		get_cache_records: function(name)
		{
			return this.cache_records[name];
		},
		cache: function(name)
		{
			return this.cache_records[name];
		},
		site_url: function(url)
		{
			url = url?url:'';
			return this.initialize_object.site_url+url;
		},
		base_url: function(url)
		{
			url = url?url:'';
                        //alert(this.initialize_object.base_url+url);
			return this.initialize_object.base_url+url;
		},
		/*
		|------------------------------------------
		| Function to initialize this class for the first-time.
		|------------------------------------------
		| @params @object
		| 	- site_url : CI site_url() @string
		| 	- base_url : CI base_url() @string
		| 	
		*/
		initialize: function(options)
		{
			options.site_url = options.site_url.substring(options.site_url.length -1 ) == '/' ? options.site_url : options.site_url+'/';
			options.base_url = options.base_url.substring(options.base_url.length -1 ) == '/' ? options.base_url : options.base_url+'/';
			this.initialize_object = options;
			$(document).trigger('KT.onInitialize');

		}
	}

	return new o();
})();


kulinertrip.components = (function(){
	var o = function (){}
	o.prototype = 
	{
		rating: function(name, value)
		{

		}
	}
	return new o();
})()
