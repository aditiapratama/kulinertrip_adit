
$(document).on('ready', function () {

    $('#provinsiclear').on('click', function () {
        $('#searchprovinsi').val('');
        $('#searchprovinsi').attr('dta-id','');
        $(this).addClass('hide');
        $('#searchkabupaten').val('');
        $('#searchkabupaten').attr('dta-id','');
        $('.input-provinsi').addClass('hide');
        $('.input-kabupaten').addClass('hide');
        $('.box-provinsi').removeClass('active');
        $('.box-kabupaten').removeClass('active');

    });

    $('.modal-close').on('click', function () {
      var target = $(this).attr('target');
      $('#'+target).modal('hide');
    });

//set daerah
      var prov = 'Semua wilayah';
      var kab = '';
      //profinsi dan kab pilihan
      if(localStorage.getItem('sdaerah') !== null){
        var sdaerah = JSON.parse(localStorage.getItem('sdaerah'));
        if(sdaerah.provinsi.nama!==''){
          prov = sdaerah.provinsi.nama;
        }
        if(sdaerah.kabupaten.nama!==''){
          kab = ' / '+sdaerah.kabupaten.nama;
        }
      }

      $('.provpilihan').html(prov);
      $('.kabpilihan').html(kab);
//end daerah

  $('.modal-back').on('click',function(){
    $('#modal_filter_masakan').modal('hide');
    $('#modal_filter2').modal('hide');
  });

    $('#kabupatenclear').on('click', function() {
        $('#searchkabupaten').val('');
        $('#searchkabupaten').attr('dta-id','');
        $('.input-kabupaten').addClass('hide');
        $('.box-kabupaten').removeClass('active');

    });

    $('.terapkan_daerah').on('click', function () {
      var prov = 'Semua wilayah';
      var kab = '';
        var provinsi = $('#searchprovinsi').val();
        var kabupaten = $('#searchkabupaten').val();

        var provpilihan = {nama:provinsi,idx:$('#searchprovinsi').attr('dta-id')}
        var kbpilihan = {nama:kabupaten,idx:$('#searchkabupaten').attr('dta-id')};
        var sdaerah = {provinsi:provpilihan,kabupaten:kbpilihan};
        localStorage.setItem("sdaerah", JSON.stringify(sdaerah));
        if(sdaerah.provinsi.nama!==''){
          prov = sdaerah.provinsi.nama;
        }
        if(sdaerah.kabupaten.nama!==''){
          kab = ' / '+sdaerah.kabupaten.nama;
        }
        // alert(localStorage.getItem('sdaerah'));
        // $('#modal-daerah').modal('hide');
        if (provinsi !== '') {
            post_refresh(domain + 'setdaerah', sdaerah);
        }
    });

    $('.show_daerah').on('click', function () {
        show_daerah();
    });


    get_daerah();
    // if (provinsipilihan == '') {
    //     show_daerah();
    // }

    $('.box-kabupaten').toggle('slide', 'left', 500);
});

function show_daerah() {

    $('#modal-daerah').modal('show');
}

function get_daerah() {
    $.get(domain + "index.php/getdaerah", function (data, status) {
        var data_daerah = JSON.parse(data);
        //alert(data_daerah[0].jsonListProvinsiKuliner);
        // console.log(data_daerah[0].jsonListProvinsiKuliner);
        //alert()
        var daerah = JSON.parse(data_daerah[0].jsonListProvinsiKuliner);
        daerah.map(show_provinsi);
        //  $( ".box-provinsi" ).toggle('slide', 'right', 500);
        $(".box-provinsi").animate({
            // opacity: 0.25,
            right: '200px'
                    // height: "toggle"
        }, 5000, function () {
            // Animation complete.
        });
        hover_provinsi(daerah);
    });
}

function show_provinsi(item, index) {
    //console.log(item);

    //membuat div box-provinsi
    var main = document.createElement("div");
    main.className += 'box-provinsi';
    main.id = item.idx;
    main.setAttribute('data_provinsi', item.PropinsiSingkat);

    var img = document.createElement('img');
    img.src = item.imagepilihan;
    //img.src=domain+'public/image/crop.jpg';
    img.className += 'img-responsive';

    var divtext = document.createElement("div");
    divtext.className += 'box-provinsi-text';

    var textnode = document.createTextNode(item.PropinsiSingkat);
    divtext.appendChild(textnode);

    var div = document.createElement("div");
    div.className += 'img-daerah';

    div.appendChild(img);
    main.appendChild(div);
    main.appendChild(divtext);



    if(localStorage.getItem('sdaerah') !== null){
      var sdaerah = JSON.parse(localStorage.getItem('sdaerah'));
      if(sdaerah.provinsi.idx!=='' && item.idx===sdaerah.provinsi.idx){
        $('#searchprovinsi').val(sdaerah.provinsi.nama);
        $('#searchprovinsi').attr('dta-id',sdaerah.provinsi.idx);
        $('#provinsiclear').removeClass('hide');
        $('.input-provinsi').removeClass('hide');
        main.className+=' active';
        var kabupaten = JSON.parse(item.jsonListKabupaten);
        kabupaten.map(show_kabupaten);
      }else if(sdaerah.provinsi.idx == ''){
        if(index == 0){
            var kabupaten = JSON.parse(item.jsonListKabupaten);
            kabupaten.map(show_kabupaten);

        }
      }
    }else{
      if(index == 0){
          var kabupaten = JSON.parse(item.jsonListKabupaten);
          kabupaten.map(show_kabupaten);

      }
    }

    document.getElementById("main-provinsi").appendChild(main);

}

function show_kabupaten(itemm, index) {
    // console.log(item);

    //membuat div box-provinsi
    // console.log(itemm);
    var main = document.createElement("div");
    main.className += 'box-kabupaten';
    main.id = itemm.idx;
    main.setAttribute('data_kabupaten', itemm.kabupaten);
    main.setAttribute('idp', itemm.idprovinsi);
    main.setAttribute('provinsi', itemm.namaprovinsisingkat);
    //att.value(item.kabupaten);
    //membuat image element
    var img = document.createElement('img');
    //img.src=item.imagepilihan;
    img.src = itemm.imagepilihan;
    img.className += 'img-responsive';

    alert(itemm.imagepilihan);
    var divtext = document.createElement("div");
    divtext.className += 'box-provinsi-text';

    var textnode = document.createTextNode(itemm.kabupaten);
    divtext.appendChild(textnode);

    var div = document.createElement("div");
    div.className += 'img-daerah';

    div.appendChild(img);
    main.appendChild(div);
    main.appendChild(divtext);

    if(localStorage.getItem('sdaerah') !== null){
      var sdaerah = JSON.parse(localStorage.getItem('sdaerah'));
      if(sdaerah.kabupaten.idx!=='' && itemm.idx===sdaerah.kabupaten.idx){
        main.className+=' active';
        $('#searchkabupaten').val(sdaerah.kabupaten.nama);
        $('#searchkabupaten').attr('dta-id',sdaerah.kabupaten.idx);
        $('#kabupatenclear').removeClass('hide');
        $('.input-kabupaten').removeClass('hide');
      }
    }

    document.getElementById("main-kabupaten").appendChild(main);
    $(".box-kabupaten").animate({
        display: 'inline-block',
        float: '',
    }, 500, function () {
        $(".box-kabupaten").css({'display': 'inline-block', 'position': ''});
    });


}

function hover_provinsi(daerahh) {

    $('body').delegate('.box-provinsi', 'click', function () {
        $('.box-provinsi').removeClass('active');
        $(this).addClass('active');
        var provinsi = $(this).attr('data_provinsi');
        $('#searchprovinsi').val(provinsi);
        $('#searchprovinsi').attr('dta-id',$(this).attr('id'));
        $('#provinsiclear').removeClass('hide');
        $('#searchkabupaten').val('');
        $('#searchkabupaten').attr('dta-id','');
        $('#kabupatenclear').addClass('hide');
        $('.input-provinsi').removeClass('hide');
        $('.input-kabupaten').addClass('hide');
        var id = $(this).attr('id');
        // re_provinsi();
        kabupaten(daerahh, id);
        // console.log(daerah);
    });



    $('body').delegate('.box-kabupaten', 'click', function () {
        $('.box-kabupaten').removeClass('active');
        $(this).addClass('active');
        var kabupaten = $(this).attr('data_kabupaten');
        $('#searchkabupaten').val(kabupaten);
        $('#searchkabupaten').attr('dta-id',$(this).attr('id'));
        $('#kabupatenclear').removeClass('hide');
        $('.input-kabupaten').removeClass('hide');
        //provinsi
        var id_prop = $(this).attr('idp');
        var provinsi = $(this).attr('provinsi');
        $('#searchprovinsi').val(provinsi);
        $('#searchprovinsi').attr('dta-id',id_prop);
        $('#provinsiclear').removeClass('hide');
        $('.input-provinsi').removeClass('hide');
        $('.box-provinsi').removeClass('active');
        $('#' + id_prop).addClass('active');


    });

    // ae_profinsi()

}

function kabupaten(json, id) {
    var kab = filterByProperty(json, 'idx', id);
    // var kabupaten = JSON.parse(kab[0]);

    console.log(kab);
    // console.log(kabupaten);
    // $('#main-kabupaten').html('');
    // kabupaten.map(show_kabupaten);
}

function filterByProperty(arrayy, propp, value) {
    var filteredd = [];
    for (var i = 0; i < arrayy.length; i++) {
        var objj = arrayy[i];
        if (objj[propp] == value) {
          // console.log(value);
            filteredd.push(objj);
        }
    }

    return filteredd;

}

function del_border_provinsi() {

}

function post_refresh(url, dta) {
    $.post(url, dta)
            .done(function (data) {
              window.location.reload();
            });
}
// function re_provinsi(){
//   $('.box-provinsi').unbind('mouseenter mouseleave');
// }
//
// function ae_profinsi(){
//   $(".box-provinsi").hover(
//     function() {
//       kabupaten(daerah[this.id].jsonListKabupaten);
//       $( ".box-provinsi" ).animate({'left' : '0px'},'slow');
//     }, function() {
//       //$( this ).removeClass( "hover" );
//     }
//   );
// }
