$(document).on('ready',function(){
  //dapatkan data filter
  var url = domain+'getfilter';
    $.get(url, function(data, status){
      json_filter=JSON.parse(data);
      masakan(json_filter[0].jsonJenisMasakanProvKabParentChild);
      kategori(json_filter[0].jsonKategoriTenantProvKab)
      tujuan(json_filter[0].jsonJenisTujuanProvKab)
      fasilitas(json_filter[0].jsonFasilitasTenantProvKab)
    });


  $('.filtermap').hover(function(){
    var target = $(this).attr('target');
    $('#'+target).removeClass('hide');
    $(this).addClass('white');
  },function(){
    var target = $(this).attr('target');
    $('#'+target).addClass('hide');
    $(this).removeClass('white');
  });
  $('.map5').hover(function(){
    $(this).removeClass('hide');
    $('.header-2nd').find("[target='" + $(this).attr('id') + "']").addClass('white');
  },function(){
    $(this).addClass('hide');
    $('.header-2nd').find("[target='" + $(this).attr('id') + "']").removeClass('white');
  });

  $('.modal-close').on('click', function () {
    var target = $(this).attr('target');
    $('#'+target).modal('hide');
  });
});
//delegate
$("body").delegate(".filter1_max5",'change',function(){
  var name = $(this).attr('name');
  var number = number_cb(name);
  // alert();
  if(number>3){
    $(this).prop("checked", false);
    $('.alertmasakan3').removeClass('hide');
        setTimeout(function(){
           $('.alertmasakan3').addClass('hide');
       }, 2000);
   }else{
     //alert(1)
     var idx = $(this).attr('idx');
     var modal = $(this).attr('modal');
     var kategori = $(this).attr('kategori');
     var val = $(this).val();
     if(modal==2){
       if(this.checked) {
         $('#selected22').append('<div id="'+kategori+'2__'+idx+'" class="selected" style=""><span style="padding-right:20px;">'+val+'</span><span class="glyphicon glyphicon-remove-circle pilihanx rm" type-data="'+kategori+'" type-modal="2" kategori="'+kategori+'" target-id="'+kategori+'2__'+idx+'" style=""></span></div>');
        //  alert('checked')
       }else{
        $('#'+kategori+'2__'+idx).remove();

       }
     }else if(modal==1){
       if(this.checked) {
         $('#filter_checked').append('<div id="'+kategori+'1__'+idx+'" class="selected" style=""><span style="padding-right:20px;">'+val+'</span><span class="glyphicon glyphicon-remove-circle pilihanx rm" type-data="'+kategori+'" type-modal="1" kategori="'+kategori+'" target-id="'+kategori+'1__'+idx+'" style=""></span></div>');
        //  alert('checked')
       }else{
        $('#'+kategori+'1__'+idx).remove();

       }

       if($('#filter_checked').find('.selected').length>0){
         $('.dellall').removeClass('hide');
         var datapost = get_all_checked();
         var url = domain+"jumlahfilter";
         $.post(url, datapost)
                 .done(function (data) {
                     // window.location.reload();
                     $(".terapkan_filter").html('Tampilkan '+data+' hasil pencarian');
                 });
       }else{
         $('.dellall').addClass('hide');
         $(".terapkan_filter").html('Terapkan');
       }
     }
   }
});
//--delegate
function kategori(data){
  show_html(JSON.parse(data),'kategori','Kategori')
}

function tujuan(data){
  show_html(JSON.parse(data),'tujuan','jenistujuanresto')
}

function fasilitas(data){
  show_html(JSON.parse(data),'fasilitas','jenisfasilitas')
}

function show_html(data, div,field){
  // console.log(data);
  data.forEach(function (element, index){
    //untuk modal 1
    if(index<5){
      $('#'+div).append('<div class="checkbox '+div+'"><label><input type="checkbox" modal="1" kategori="'+div+'" id="'+element.idx+'__'+div+'1" idx="'+element.idx+'" class="filter1_max5 red_cb" box_onchange value="'+element[field]+'" name="'+div+'">'+element[field]+'</label></div>')
    }
    //untuk modal selanjutnya
    $('#'+div+'2-konten').append('<div class="col-md-3 checkbox '+div+'"><label><input name="'+div+'2" modal="2" kategori="'+div+'" type="checkbox" idx="'+element.idx+'" id="'+element.idx+'__'+div+'2" class="select2 cb2 box_onchange red_cb filter1_max5 '+div+'2" value="'+element[field]+'" did="'+element.idx+'">'+element[field]+'</label></div>');
  });
  if(data.length>5){
    $('#'+div).append('<a href="javascript:void(0)" class="'+div+' filter-a-next '+div+'-next">selanjutnya <i class="material-icons carret_next">arrow_drop_down</i></a>');
    $('.'+div+'-next').on('click',function(){
      var vall= cbbyname(div);
      $('.'+div+'2').removeClass('hide');
      $('.non-'+div+'2').addClass('hide');
      $('#modal_filter2').modal('show');
    });
  }
}

function masakan(data){
  var aa = JSON.parse(data);
  show_masakan(aa);
}

function show_masakan(data){
  var aa = 1;
  var number_masakan =0;
  data.forEach(function (element, index){
    if(aa===1){
      var collapse='';
      var cls ='in';
    }else {
      cls ="";
      collapse='aria-expanded="false"';
    }

    $('#masakan2-konten').append('<div class="panel panel-default masakan"><div class="panel-heading white" data-toggle="collapse" data-target="#'+element.idx+'__masakan">'+element.jenismakanan+' <div  style="float:right;padding-right:10px;"><i class="material-icons carret_next">expand_more</i></div></div><div class="panel-body collapse '+cls+'" id="'+element.idx+'__masakan" '+collapse+'></div></div>');
    // masakan_child(element.idx+'__masakan',JSON.parse(element.child),'masakan2')
    //child
    child = JSON.parse(element.child);
    id_div = element.idx;
    name1 = 'masakan';
    name2 = 'masakan2';
    child.forEach(function (element, index){
      //lima masakan yg di show pertama
      if(number_masakan<5){
        $('#masakan').append('<div class="checkbox"><label class="custom-checkbox"><input name="'+name1+'" type="checkbox" modal="1" kategori="masakan" idx="'+element.idx+'" id="'+element.idx+'__masakan1" class="select2 red_cb filter1_max5" value="'+element.jenismakanan+'">'+element.jenismakanan+'</label></div>');

        number_masakan++;
      }
      //masakan dimodal 2
      $('#'+id_div+'__masakan').append('<div class="col-md-3 checkbox"><label class="custom-checkbox"><input name="'+name2+'" type="checkbox" id="'+element.idx+'__masakan2" class="select2 red_cb masakan filter1_max5" value="'+element.jenismakanan+'">'+element.jenismakanan+'</label></div>');
    });
    aa++;
  });
  $('#masakan').append('<a href="javascript:void(0)" class="'+id_div+' filter-a-next show_masakan">selanjutnya <i class="material-icons carret_next">arrow_drop_down</i></a>');
//   $('.show_masakan').on('click',function(){
//     $('#modal_filter_masakan').modal('show');
// });
}

function masakan_child(id_div,data,name){
  data.forEach(function (element, index){
    if(number_masakan<5){

      number_masakan++;
    }
    $('#'+id_div).append('<div class="col-md-3 checkbox"><label class="custom-checkbox"><input name="'+name+'" type="checkbox" id="'+element.idx+'__masakan" class="select2 filter1_max5 red_cb masakan" value="'+element.nama+'">'+element.nama+'</label></div>');
  });
}

$("body").delegate(".kategori-next", "click", function(){
  var value_cb = cbbyname('kategori');
  var value_kategori2 = cbbyname('kategori2');
  // console.log(value_cb)
  if(value_cb.length>0){
    $('.dellall-kategori').removeClass('hide');
    $('.non-kategoridell').addClass('hide');
  }else{
    $('.dellall-kategori').addClass('hide');
  }
  hapuske2(value_kategori2,'__kategori2');
  terapkanke2(value_cb,'__kategori2');
  selected_div(value_cb,'selected22',2);
});

$("body").delegate(".tujuan-next", "click", function(){
  var value_tujuan1 = cbbyname('tujuan');
  var value_tujuan2 = cbbyname('tujuan2');
  // console.log(value_tujuan1)
  if(value_tujuan1.length>0){
    $('.dellall_tujuan').removeClass('hide');
    $('.non-tujuandell').addClass('hide');
  }else{
    $('.dellall_tujuan').addClass('hide');
  }
  hapuske2(value_tujuan2,'__tujuan2');
  terapkanke2(value_tujuan1,'__tujuan2');
  selected_div(value_tujuan1,'selected22',2);
});

$("body").delegate(".fasilitas-next", "click", function(){
  var value_fasilitas1 = cbbyname('fasilitas');
  var value_fasilitas2 = cbbyname('fasilitas2');
  // console.log(value_fasilitas1)
  if(value_fasilitas1.length>0){
    $('.dellall-fasilitas').removeClass('hide');
    $('.non-fasilitasdell').addClass('hide');
  }else{
    $('.dellall-fasilitas').addClass('hide');
  }
  hapuske2(value_fasilitas2,'__fasilitas2');
  terapkanke2(value_fasilitas1,'__fasilitas2');
  selected_div(value_fasilitas1,'selected22',2);
});


function cbbyname(name){
  var values =[];
  $("input:checkbox[name="+name+"]:checked").each(function(){
    var id_vals = $(this).attr('id');
    var real = id_vals.split('__');
      values.push({nama:$(this).val(),idx:real[0],type:name});
  });
  return values;
}

function number_cb(name){
  var values =[];
  $("input:checkbox[name="+name+"]:checked").each(function(){
      values.push($(this).val());
  });
  return values.length;
}

function hapuske2(data,akhiranid){
  data.forEach(function (element, index){
    $('#'+element.idx+akhiranid).prop("checked", false);
  });
}

function terapkanke2(data,akhiranid){
  data.forEach(function (element, index){
    $('#'+element.idx+akhiranid).prop("checked", true);
  });
}

function selected_div(data,id_induk,type){
  $('#'+id_induk).html('');
  console.log(data);
  data.forEach(function (element, index){
    $('#'+id_induk).append('<div id="'+element.type+type+'__'+element.idx+'" class="selected" style=""><span style="padding-right:20px;">'+element.nama+'</span><span class="glyphicon glyphicon-remove-circle pilihanx rm" type-data="'+element.type+'" type-modal="'+type+'" kategori="'+element.type+'" target-id="'+element.type+type+'__'+element.idx+'" style=""></span></div>')
  });
}
