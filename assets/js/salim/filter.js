
$(document).on('ready',function(){
  var start = $('.start').val();
  var to = $('.to').val();


  $('.show_filter').on('click',function(){
    $('#modal_filter1').modal('show');
  });
  $(".mat-input").focus(function(){
    $(this).parent().addClass("is-active is-completed");
  });

  $("body").delegate(".show_masakan", "click", function(){
    $('#modal_filter_masakan').modal('show');
    var value_cb = cbbyname('masakan');
    var value_kategori2 = cbbyname('masakan2');
    if(value_cb.length>0){
      $('.dellall-masakan').removeClass('hide');
    }else{
      $('.dellall-masakan').addClass('hide');
    }
    hapuske2(value_kategori2,'__masakan2');
    terapkanke2(value_cb,'__masakan2');
    selected_div(value_cb,'pilihanmasakan2',2);
  });



  $(".mat-input").focusout(function(){
    if($(this).val() === "")
      $(this).parent().removeClass("is-completed");
    $(this).parent().removeClass("is-active");
  })
  show_filter();
});

function show_filter(){
  data = ajax_data(domain+"index.php/getfilter");
}

$("body").delegate("#ex12c", "change", function(){
  var valu = $(this).val();
  var split_val = valu.split(',');
  var harga = '';
  if(parseInt(split_val[0]) > parseInt(split_val[1])){
    $('.from').val(split_val[1]);
    $('.to').val(split_val[0]);
    $('.harga_end').html(split_val[0]);
    price();
    harga = $('.from').val() +' - '+$('.to').val();
    $('.harga_start').html(split_val[1]);
  }else if( parseInt(split_val[0]) < parseInt(split_val[1]) ){
    $('.from').val(split_val[0]);
    $('.to').val(split_val[1]);
    $('.harga_end').html(split_val[1]);
    $('.harga_start').html(split_val[0]);
    price();
    harga = $('.from').val() +' - '+$('.to').val();
  }else if(parseInt(split_val[0]) === parseInt(split_val[1])){
    $('.from').val(split_val[0]);
    $('.to').val(split_val[1]);
    $('.harga_end').html(split_val[1]);
    $('.harga_start').html(split_val[0]);
    price();
    harga = $('.from').val() +' - '+$('.to').val();
  }

  $('#range_harga').remove();
  $('#filter_checked').append('<div id="range_harga" class="selected" style=""><span style="padding-right:20px;">'+harga+'</span><span class="glyphicon glyphicon-remove-circle pilihanx rm" type-data="masakan" type-modal="1" kategori="harga" target-id="range_harga" style=""></span></div>');
});



$("body").delegate("input.search-kategori", "keyup", function(){
  var search = $(this).val();
  search_filter('kategori','Kategori',search);
});


$("body").delegate(".masakan", "change", function(){

  if(this.checked) {
    var num = number_cb('masakan2');
    if(num>3){
      $(this).prop("checked", false);
      $('.alertmasakan3').removeClass('hide');
          setTimeout(function(){
             $('.alertmasakan3').addClass('hide');
         }, 2000);
     }else{
         var val_masakan = $(this).val();
         var id = $(this).attr('id');
         var spil = id.split('__');
         $('#pilihanmasakan2').append('<div id="masakan2'+'__'+spil[0]+'" class="selected" style=""><span style="padding-right:20px;">'+val_masakan+'</span><span class="glyphicon glyphicon-remove-circle pilihanx rm" type-data="masakan" type-modal="2" kategori="masakan" target-id="masakan2'+'__'+spil[0]+'" style=""></span></div>');
     }

 }else{
   var id_div = $(this).attr('id');
   var spill = id_div.split('__');
   $('#masakan2__'+spill[0]).remove();
 }

});

$("body").delegate("input.search-tujuan", "keyup", function(){
  var search = $(this).val();
  search_filter('tujuan','jenistujuanresto',search);
});

$("body").delegate(".rm", "click", function(){
  var target_id = $(this).attr('target-id');
  var type_modal = $(this).attr('type-modal');
  var type = $(this).attr('kategori');
  var id = target_id.split('__');
  var sid = '#'+id[1]+'__'+type+type_modal;
  $(sid).prop("checked", false);
  $('#'+target_id).remove();
});


$("body").delegate("input.search-fasilitas", "keyup", function(){
  var search = $(this).val();
  search_filter('fasilitas','jenisfasilitas',search);
});

$("body").delegate("input.search-citarasa", "keyup", function(){
  var search = $(this).val();
  search_filter('citarasa','NamaCitaRasa',search);
});

$("body").delegate("input.search-caramasak", "keyup", function(){
  var search = $(this).val();
  search_filter('caramasak','caramasak',search);
});

//terapkan kategori 2
$("body").delegate(".terapkan-kategori2", "click", function(){
  var selected = cbbyname('kategori2');
  terapkan2(selected,'kategori');
  update_div_depan();
});

$("body").delegate(".terapkan-tujuan2", "click", function(){
  var selected = cbbyname('tujuan2');
  // box_selected(1);
  terapkan2(selected,'tujuan');
  update_div_depan();
});

$("body").delegate(".terapkan_filter", "click", function(){
  var allchecked =  get_all_checked();
  var jumlah_item = numberallcb(allchecked);
  $('.badges-filter').html(jumlah_item);
  // localStorage.setItem("filter", JSON.stringify(allchecked));
  // alert(localStorage.getItem("filter"));
  // console.log(localStorage.getItem("filter"));
  var url = domain+"terapkanfilter";
           $.redirect(url,
            {json: JSON.stringify(allchecked)}, "POST", "_blank", true)

//  $.post(url, allchecked)
//          .done(function (data) {
//              // window.location.reload();
//              alert('session(yang diencode json):'+data);
//          });


   localStorage.setItem("filter", JSON.stringify(allchecked));
   $('#modal_filter1').modal('hide');

});

function get_all_checked(){
  var kategori = cbbyname('kategori');
  var masakan = cbbyname('masakan');
  var tujuan = cbbyname('tujuan');
  var waktu = cbbyname('waktu');
  var fasilitas = cbbyname('fasilitas');
  var citarasa = cbbyname('citarasa');
  var caramasak = cbbyname('caramasak');
  var halal = cbbyname('halal');
  var harga = [];
  harga.push({harga_start:$('#harga_start').val(),harga_end:$('#harga_end').val()})

  return {kategori:kategori,masakan:masakan,tujuan:tujuan,waktu:waktu,fasilitas:fasilitas,citarasa:citarasa,caramasak:caramasak,halal:halal,harga:harga}
}

function postbyname(name){
  var values =[];
  $("input:checkbox[name="+name+"]:checked").each(function(){
    var id_vals = $(this).attr('id');
    var real = id_vals.split('__');
      values.push({nama:$(this).val(),idx:real[0]});
  });
  return values;
}

function update_div_depan(){
  $('#filter_checked').html('');
  //kategori
  var selected = cbbyname('kategori');
  selected_(selected,1);

  var selected = cbbyname('tujuan');
  selected_(selected,1);

  var selected = cbbyname('fasilitas');
  selected_(selected,1);

  var selected = cbbyname('citarasa');
  selected_(selected,1);

  var selected = cbbyname('caramasak');
  selected_(selected,1);

  var selected = cbbyname('masakan');
  selected_(selected,1);

  var selected = cbbyname('halal');
  selected_(selected,1);

  var selected = cbbyname('waktu');
  selected_(selected,1);

}



function selected_(selected,type){
  selected.forEach(function (element, index){
    $('#filter_checked').append('<div id="'+element.type+type+'__'+element.idx+'" class="selected" style=""><span style="padding-right:20px;">'+element.nama+'</span><span class="glyphicon glyphicon-remove-circle pilihanx rm" type-data="'+element.type+'" type-modal="'+type+'" kategori="'+element.type+'" target-id="'+element.type+type+'__'+element.idx+'" style=""></span></div>')
  });
}

$("body").delegate(".terapkan-fasilitas2", "click", function(){
  var selected = cbbyname('fasilitas2');
  terapkan2(selected,'fasilitas');
});

$("body").delegate(".terapkan-citarasa2", "click", function(){
  var selected = cbbyname('citarasa2');
  terapkan2(selected,'citarasa');
});

$("body").delegate(".terapkan-caramasak2", "click", function(){
  var selected = cbbyname('caramasak2');
  terapkan2(selected,'caramasak');
});

$("body").delegate(".terapkan-masakan", "click", function(){
  var value_cb = cbbyname('masakan2');
  terapkan2(value_cb,'masakan');
  $('#modal_filter_masakan').modal('hide');
  update_div_depan();
});

//delete input search
$('body').delegate('.del-filter','click',function(){
  var target = $(this).attr('target');
  $('.search-'+target).val('');
  search_filter(target,get_nama_field_array(target),'');
  // alert(target);
});

function box_selected(namacheckbox,id_induk,type){

}

function number_cb(name){
  var values =[];
  $("input:checkbox[name="+name+"]:checked").each(function(){
      values.push($(this).val());
  });
  return values.length;
}

function selected_div(data,id_induk,type){
  $('#'+id_induk).html('');
  console.log(data);
  data.forEach(function (element, index){
    $('#'+id_induk).append('<div id="'+element.type+type+'__'+element.idx+'" class="selected" style=""><span style="padding-right:20px;">'+element.nama+'</span><span class="glyphicon glyphicon-remove-circle pilihanx rm" type-data="'+element.type+'" type-modal="'+type+'" kategori="'+element.type+'" target-id="'+element.type+type+'__'+element.idx+'" style=""></span></div>')
  });
}
// function terapkan2(data,idname,name){
//   masakan5(idname,data,name)
// }

function terapkan2(data,id_div){
  //show yg dipilih
  $('#'+id_div).empty();
  // console.log(data);
  var sambung = ','
  data.forEach(function (element, index){
    $('#'+id_div).append('<div class="checkbox"><label class="custom-checkbox"><input name="'+id_div+'" modal="1" kategori="'+id_div+'" type="checkbox" id="'+element.idx+'__'+id_div+'1" idx="'+element.idx+'" class="select2 red_cb filter1_max5" value="'+element.nama+'">'+element.nama+'</label></div>');
    $('#'+element.idx+'__'+id_div+'1').prop("checked", true);
    sambung+=element.nama+',';
  });

  var nama_arr = get_nama_array(id_div);
  var nama_field = get_nama_field_array(id_div);

  //SHOW DILUAR YG DIPILIH
  var index=0;
  var jmltambahan = 5 - data.length;
  var databykategory = JSON.parse(json_filter[0][nama_arr]);

  //jika masakan
  if(id_div=='masakan'){
    //tamabahan yang bukan diselek
    for (i = 0; i < jmltambahan; index++) {

      // if(databykategory[index].child!=='[]'){
      var chld = JSON.parse(databykategory[index].child);
      if(chld.length>0){
      for (iu = 0; iu < chld.length;iu++){
        if(sambung.indexOf(','+chld[iu][nama_field]+',')===-1){
          $('#'+id_div).append('<div class="checkbox"><label class="custom-checkbox"><input name="'+id_div+'" type="checkbox" kategori="masakan" modal="1" idx="'+chld[iu].idx+'" id="'+chld[iu].idx+'__'+id_div+'1" class="select2 filter1_max5 red_cb masakan" value="'+chld[iu][nama_field]+'">'+chld[iu][nama_field]+'</label></div>');
          // iu++;
          i++;
          if(i===jmltambahan-1){
            break;
          }
        }
      }

      }
    }

    //button selanjutnya
    $('#masakan').append('<a href="javascript:void(0)" class="'+id_div+' filter-a-next show_masakan">selanjutnya <i class="material-icons carret_next">arrow_drop_down</i></a>');

  }else{
    //jika bukan masakan
    //tambahan selain yang diselek
    for (i = 0; i < jmltambahan; index++) {
          if(sambung.indexOf(','+databykategory[index][nama_field]+',')===-1){
            $('#'+id_div).append('<div class="checkbox"><label class="custom-checkbox"><input name="'+id_div+'" type="checkbox" modal="1" kategori="'+id_div+'" idx="'+databykategory[index].idx+'"   id="'+databykategory[index].idx+'__'+id_div+'1" class="select2 filter1_max5 red_cb" value="'+databykategory[index][nama_field]+'">'+databykategory[index][nama_field]+'</label></div>');
            i++;
          }

      }

    //button selanjutnya
    $('#'+id_div).append('<a href="javascript:void(0)" class="'+id_div+' filter-a-next '+id_div+'-next">selanjutnya <i class="material-icons carret_next">arrow_drop_down</i></a>');
    $('.'+id_div+'-next').on('click',function(){
      $('.'+id_div+'2').removeClass('hide');
      $('.non-'+id_div+'2').addClass('hide');
      $('#modal_filter2').modal('show');
    });
  }

  $('#modal_filter2').modal('hide');
}

function show_with_filter(data, selected){
  var databykategory = JSON.parse(data);
  $i = 0
  // for()
  $('#'+div).append('<div class="checkbox '+div+'"><label><input type="checkbox" id="'+element.idx+'__'+div+'1" class="filter1_max5 red_cb" value="'+element[field]+'" name="'+div+'">'+element[field]+'</label></div>')

}



function cbbyname(name){
  var values =[];
  $("input:checkbox[name="+name+"]:checked").each(function(){
    var id_vals = $(this).attr('id');
    var real = id_vals.split('__');
      values.push({nama:$(this).val(),idx:real[0],type:name});
  });
  return values;
}

function ajax_data(url){
  $.get(url, function(data, status){
    json_filter=JSON.parse(data);
    // console.log(json_filter);
    masakan(json_filter[0].jsonJenisMasakanProvKabParentChild);
    kategori(json_filter[0].jsonKategoriTenantProvKab)
    tujuan(json_filter[0].jsonJenisTujuanProvKab)
    waktu(json_filter[0].jsonJenisWaktuProvKab)
    fasilitas(json_filter[0].jsonFasilitasTenantProvKab)
    citarasa(json_filter[0].jsonCitaRasaProvKab)
    caramasak(json_filter[0].jsonCaraMasakProvKab)
    halal(json_filter[0].jsonListJenisHalal)

    var jumlah_item = 0;
    if (localStorage.getItem("filter") === null || localStorage.getItem("filter")==='') {

    }else{
      var ses_storage = JSON.parse(localStorage.getItem("filter"));
      //kategori
      if(ses_storage.kategori.length>0){
        jumlah_item=jumlah_item +ses_storage.kategori.length;
        terapkan2(ses_storage.kategori,'kategori');
        update_div_depan();
      }

      //tujuan
      if(ses_storage.tujuan.length>0){

        jumlah_item=jumlah_item +ses_storage.tujuan.length;
        terapkan2(ses_storage.tujuan,'tujuan');
        update_div_depan();
      }

      //masakan
      if(ses_storage.masakan.length>0){

        jumlah_item=jumlah_item+ses_storage.masakan.length;
        terapkan2(ses_storage.masakan,'masakan');
        update_div_depan();
      }

      //fasilitas
      if(ses_storage.fasilitas.length>0){

        jumlah_item=jumlah_item+ses_storage.fasilitas.length;
        terapkan2(ses_storage.fasilitas,'fasilitas');
        update_div_depan();
      }

      //citarasa
      if(ses_storage.citarasa.length>0){
        jumlah_item=jumlah_item+ses_storage.citarasa.length;
        terapkan2(ses_storage.citarasa,'citarasa');
        update_div_depan();
      }

      //caramasak
      if(ses_storage.caramasak.length>0){
        jumlah_item=jumlah_item+ses_storage.caramasak.length;
        terapkan2(ses_storage.caramasak,'caramasak');
        update_div_depan();
      }

      if(ses_storage.halal.length>0){
        jumlah_item=jumlah_item+ses_storage.halal.length;
        terapkanke2(ses_storage.halal,'__halal1');
        update_div_depan();
      }

      if(ses_storage.waktu.length>0){
        jumlah_item=jumlah_item+ses_storage.waktu.length;
        terapkanke2(ses_storage.waktu,'__waktu1');
        update_div_depan();
      }

      if(ses_storage.harga.length>0){
        var harga_end = 300000;
        var harga_start = 0;
        if(ses_storage.harga[0].harga_start!==undefined){
          if(ses_storage.harga[0].harga_start!==''){
            harga_start = ses_storage.harga[0].harga_start;

          }
        }
        if(ses_storage.harga[0].harga_end!==undefined){
          if(ses_storage.harga[0].harga_end!==''){
            harga_end = ses_storage.harga[0].harga_end;
          }
        }
        $('.harga_end').html(harga_end);
        $('.harga_start').html(harga_start);
        $('#harga_start').val(harga_start);
        $('#harga_end').val(harga_end);
        $("#ex12c").bootstrapSlider({ id: "slider12c", min: 0, max: 1500000,step:300000, range: true, value: [parseInt(harga_start), parseInt(harga_end)] });
        // terapkanke2(ses_storage.waktu,'__waktu1');
        // update_div_depan();
        price();
      }
    }
    if(jumlah_item>0){
      $('.dellall').removeClass('hide');

    }
    $('.badges-filter').html(jumlah_item);
    //harga()
  });

}
$('body').delegate('.dellall-kategori','click',function(){
  $( "input[name='kategori2']").prop('checked',false);
})

function kategori(data){
  show_html(JSON.parse(data),'kategori','Kategori')
}

function masakan(data){
  var aa = JSON.parse(data);
  show_masakan(aa);
}

function show_masakan(data){
  var aa = 1;
  var number_masakan =0;
  data.forEach(function (element, index){
    if(aa===1){
      var collapse='';
      var cls ='in';
    }else {
      cls ="";
      collapse='aria-expanded="false"';
    }

    $('#masakan2-konten').append('<div class="panel panel-default masakan"><div class="panel-heading white" data-toggle="collapse" data-target="#'+element.idx+'__masakan">'+element.jenismakanan+' <div  style="float:right;padding-right:10px;"><i class="material-icons carret_next">expand_more</i></div></div><div class="panel-body collapse '+cls+'" id="'+element.idx+'__masakan" '+collapse+'></div></div>');
    // masakan_child(element.idx+'__masakan',JSON.parse(element.child),'masakan2')
    //child
    child = JSON.parse(element.child);
    id_div = element.idx;
    name1 = 'masakan';
    name2 = 'masakan2';
    child.forEach(function (element, index){
      //lima masakan yg di show pertama
      if(number_masakan<5){
        $('#masakan').append('<div class="checkbox"><label class="custom-checkbox"><input name="'+name1+'" type="checkbox" modal="1" kategori="masakan" idx="'+element.idx+'" id="'+element.idx+'__masakan1" class="select2 red_cb filter1_max5" value="'+element.jenismakanan+'">'+element.jenismakanan+'</label></div>');

        number_masakan++;
      }
      //masakan dimodal 2
      $('#'+id_div+'__masakan').append('<div class="col-md-3 checkbox"><label class="custom-checkbox"><input name="'+name2+'" type="checkbox" id="'+element.idx+'__masakan2" class="select2 red_cb masakan filter1_max5" value="'+element.jenismakanan+'">'+element.jenismakanan+'</label></div>');
    });
    aa++;
  });
  $('#masakan').append('<a href="javascript:void(0)" class="'+id_div+' filter-a-next show_masakan">selanjutnya <i class="material-icons carret_next">arrow_drop_down</i></a>');
//   $('.show_masakan').on('click',function(){
//     $('#modal_filter_masakan').modal('show');
// });
}

function masakan_child(id_div,data,name){
  data.forEach(function (element, index){
    if(number_masakan<5){

      number_masakan++;
    }
    $('#'+id_div).append('<div class="col-md-3 checkbox"><label class="custom-checkbox"><input name="'+name+'" type="checkbox" id="'+element.idx+'__masakan" class="select2 filter1_max5 red_cb masakan" value="'+element.nama+'">'+element.nama+'</label></div>');
  });
}

function tujuan(data){
  show_html(JSON.parse(data),'tujuan','jenistujuanresto')
}

function waktu(data){
  show_html(JSON.parse(data),'waktu','jeniswaktu')
}

function fasilitas(data){
  show_html(JSON.parse(data),'fasilitas','jenisfasilitas')
}

function citarasa(data){
  show_html(JSON.parse(data),'citarasa','NamaCitaRasa')
}

function caramasak(data){
  show_html(JSON.parse(data),'caramasak','caramasak')
}

function halal(data){
  show_html(JSON.parse(data),'halal','StatusHalal')
}

function show_html(data, div,field){
  // console.log(data);
  data.forEach(function (element, index){
    //untuk modal 1
    if(index<5){
      $('#'+div).append('<div class="checkbox '+div+'"><label><input type="checkbox" modal="1" kategori="'+div+'" id="'+element.idx+'__'+div+'1" idx="'+element.idx+'" class="filter1_max5 red_cb" box_onchange value="'+element[field]+'" name="'+div+'">'+element[field]+'</label></div>')
    }
    //untuk modal selanjutnya
    $('#'+div+'2-konten').append('<div class="col-md-3 checkbox '+div+'"><label><input name="'+div+'2" modal="2" kategori="'+div+'" type="checkbox" idx="'+element.idx+'" id="'+element.idx+'__'+div+'2" class="select2 cb2 box_onchange red_cb filter1_max5 '+div+'2" value="'+element[field]+'" did="'+element.idx+'">'+element[field]+'</label></div>');
  });
  if(data.length>5){
    $('#'+div).append('<a href="javascript:void(0)" class="'+div+' filter-a-next '+div+'-next">selanjutnya <i class="material-icons carret_next">arrow_drop_down</i></a>');
    $('.'+div+'-next').on('click',function(){
      var vall= cbbyname(div);
      $('.'+div+'2').removeClass('hide');
      $('.non-'+div+'2').addClass('hide');
      $('#modal_filter2').modal('show');
    });
  }
}

$("body").delegate(".kategori-next", "click", function(){
  var value_cb = cbbyname('kategori');
  var value_kategori2 = cbbyname('kategori2');
  // console.log(value_cb)
  if(value_cb.length>0){
    $('.dellall-kategori').removeClass('hide');
    $('.non-kategoridell').addClass('hide');
  }else{
    $('.dellall-kategori').addClass('hide');
  }
  hapuske2(value_kategori2,'__kategori2');
  terapkanke2(value_cb,'__kategori2');
  selected_div(value_cb,'selected22',2);
});

$("body").delegate(".citarasa-next", "click", function(){
  var value_1 = cbbyname('citarasa');
  var value_2 = cbbyname('citarasa2');
  if(value_1.length>0){
    $('.dellall-citarasa').removeClass('hide');
    $('.non-citarasadell').addClass('hide');
  }
  hapuske2(value_2,'__citarasa2');
  terapkanke2(value_1,'__citarasa2');
  selected_div(value_1,'selected22',2);
});

$("body").delegate(".caramasak-next", "click", function(){
  var value_1 = cbbyname('caramasak');
  var value_2 = cbbyname('caramasak2');
  if(value_1.length>0){
    $('.dellall-caramasak').removeClass('hide');
    $('.non-caramasakdell').addClass('hide');
  }
  hapuske2(value_2,'__caramasak2');
  terapkanke2(value_1,'__caramasak2');
  selected_div(value_1,'selected22',2);
});

$("body").delegate(".hapusselected1", "click", function(){
  $('#filter_checked').html('');
  $('.filter1_max5').prop('checked',false);
  $('.dellall').addClass('hide');
  $('.terapkan_filter').html('Terapkan')
});

$('body').delegate('.dellall-masakan','click',function(){
  $( "input[name='masakan2']").prop('checked',false);
  $('#pilihanmasakan2').empty();
  $('.dellall-masakan').addClass('hide');
});

$('body').delegate('.dellall-kategori','click',function(){
  $( "input[name='kategori2']").prop('checked',false);
  $('#selected22').empty();
  $('.dellall-kategori').addClass('hide');
});

$('body').delegate('.dellall_tujuan','click',function(){
  $( "input[name='tujuan2']").prop('checked',false);
  $('#selected22').empty();
  $('.dellall_tujuan').addClass('hide');
});

$('body').delegate('.dellall-fasilitas','click',function(){
  $( "input[name='fasilitas2']").prop('checked',false);
  $('#selected22').empty();
  $('.dellall-fasilitas').addClass('hide');
});

$('body').delegate('.dellall-caramasak','click',function(){
  $( "input[name='caramasak2']").prop('checked',false);
  $('#selected22').empty();
  $('.dellall-caramasak').addClass('hide');
});

$('body').delegate('.dellall-citarasa','click',function(){
  $( "input[name='citarasa2']").prop('checked',false);
  $('#selected22').empty();
  $('.dellall-citarasa').addClass('hide');
});

$('body').delegate("input[name='kategori2']",'change',function(){
  var dt = cbbyname('kategori2');
  if(dt.length>0){
    $('.dellall-kategori').removeClass('hide');
  }else{
    $('.dellall-kategori').addClass('hide');
  }
});

$('body').delegate("input[name='tujuan2']",'change',function(){
  var dt = cbbyname('tujuan2');
  if(dt.length>0){
    $('.dellall-tujuan').removeClass('hide');
  }else{
    $('.dellall-tujuan').addClass('hide');
  }
});

$('body').delegate("input[name='masakan2']",'change',function(){
  var dt = cbbyname('masakan2');
  if(dt.length>0){
    $('.dellall-masakan').removeClass('hide');
  }else{
    $('.dellall-masakan').addClass('hide');
  }
});

$('body').delegate("input[name='fasilitas2']",'change',function(){
  var dt = cbbyname('fasilitas2');
  if(dt.length>0){
    $('.dellall-fasilitas').removeClass('hide');
  }else{
    $('.dellall-fasilitas').addClass('hide');
  }
});

$('body').delegate("input[name='citarasa2']",'change',function(){
  var dt = cbbyname('citarasa2');
  if(dt.length>0){
    $('.dellall-citarasa').removeClass('hide');
  }else{
    $('.dellall-citarasa').addClass('hide');
  }
});

$('body').delegate("input[name='caramasak2']",'change',function(){
  var dt = cbbyname('caramasak2');
  if(dt.length>0){
    $('.dellall-caramasak').removeClass('hide');
  }else{
    $('.dellall-caramasak').addClass('hide');
  }
});

$("body").delegate(".tujuan-next", "click", function(){
  var value_tujuan1 = cbbyname('tujuan');
  var value_tujuan2 = cbbyname('tujuan2');
  // console.log(value_tujuan1)
  if(value_tujuan1.length>0){
    $('.dellall_tujuan').removeClass('hide');
    $('.non-tujuandell').addClass('hide');
  }else{
    $('.dellall_tujuan').addClass('hide');
  }
  hapuske2(value_tujuan2,'__tujuan2');
  terapkanke2(value_tujuan1,'__tujuan2');
  selected_div(value_tujuan1,'selected22',2);
});

$("body").delegate(".fasilitas-next", "click", function(){
  var value_fasilitas1 = cbbyname('fasilitas');
  var value_fasilitas2 = cbbyname('fasilitas2');
  // console.log(value_fasilitas1)
  if(value_fasilitas1.length>0){
    $('.dellall-fasilitas').removeClass('hide');
    $('.non-fasilitasdell').addClass('hide');
  }else{
    $('.dellall-fasilitas').addClass('hide');
  }
  hapuske2(value_fasilitas2,'__fasilitas2');
  terapkanke2(value_fasilitas1,'__fasilitas2');
  selected_div(value_fasilitas1,'selected22',2);
});

$("body").delegate(".filter1_max5",'change',function(){
  var name = $(this).attr('name');
  var number = number_cb(name);
  // alert();
  if(number>3){
    $(this).prop("checked", false);
    $('.alertmasakan3').removeClass('hide');
        setTimeout(function(){
           $('.alertmasakan3').addClass('hide');
       }, 2000);
   }else{
     //alert(1)
     var idx = $(this).attr('idx');
     var modal = $(this).attr('modal');
     var kategori = $(this).attr('kategori');
     var val = $(this).val();
     if(modal==2){
       if(this.checked) {
         $('#selected22').append('<div id="'+kategori+'2__'+idx+'" class="selected" style=""><span style="padding-right:20px;">'+val+'</span><span class="glyphicon glyphicon-remove-circle pilihanx rm" type-data="'+kategori+'" type-modal="2" kategori="'+kategori+'" target-id="'+kategori+'2__'+idx+'" style=""></span></div>');
        //  alert('checked')
       }else{
        $('#'+kategori+'2__'+idx).remove();

       }
     }else if(modal==1){
       if(this.checked) {
         $('#filter_checked').append('<div id="'+kategori+'1__'+idx+'" class="selected" style=""><span style="padding-right:20px;">'+val+'</span><span class="glyphicon glyphicon-remove-circle pilihanx rm" type-data="'+kategori+'" type-modal="1" kategori="'+kategori+'" target-id="'+kategori+'1__'+idx+'" style=""></span></div>');
        //  alert('checked')
       }else{
        $('#'+kategori+'1__'+idx).remove();

       }

       if($('#filter_checked').find('.selected').length>0){
         $('.dellall').removeClass('hide');
         var datapost = get_all_checked();
         var url = domain+"jumlahfilter";
         $.post(url, datapost)
                 .done(function (data) {
                     // window.location.reload();
                     $(".terapkan_filter").html('Tampilkan '+data+' hasil pencarian');
                 });
       }else{
         $('.dellall').addClass('hide');
         $(".terapkan_filter").html('Terapkan');
       }
     }
   }
});

function terapkanke2(data,akhiranid){
  data.forEach(function (element, index){
    $('#'+element.idx+akhiranid).prop("checked", true);
  });
}


function hapuske2(data,akhiranid){
  data.forEach(function (element, index){
    $('#'+element.idx+akhiranid).prop("checked", false);
  });
}

function show_filtered(data, div,field){
  data.forEach(function (element, index){
    $('#'+div+'2-konten').append('<div class="col-md-3 checkbox '+div+'"><label><input type="checkbox" name="'+div+'2" id="'+element.idx+'__'+div+'2" class="select2 filter1_max5 cb2 red_cb" value="'+element[field]+'">'+element[field]+'</label></div>');
  });
}

function search_filter(div,field,val){
  $('#'+div+'2-konten').html('');
  var nama_array = get_nama_array(div);
  var dat = JSON.parse(json_filter[0][nama_array]);
  if(val!==''){
    var filtered=dat.filter( se => se[field].toLowerCase().search(val.toLowerCase())>-1 );
  }else{
    var filtered = dat;
  }

  show_filtered(filtered,div,field);
}

function filter_json(div,json){

}

function get_nama_array(div){
  var nama_arr ='';
  switch(div) {
      case 'kategori':
          nama_arr = 'jsonKategoriTenantProvKab';
          break;
      case 'tujuan':
          nama_arr = 'jsonJenisTujuanProvKab';
          break;
      case 'waktu':
          nama_arr = 'jsonJenisWaktuProvKab';
          break;
      case 'fasilitas':
          nama_arr = 'jsonFasilitasTenantProvKab';
          break;
      case 'citarasa':
          nama_arr = 'jsonCitaRasaProvKab';
          break;
      case 'caramasak':
          nama_arr = 'jsonCaraMasakProvKab';
          break;
      case 'masakan':
          nama_arr = 'jsonJenisMasakanProvKabParentChild';
          break;
  }
  return nama_arr;

}

function get_nama_field_array(div){
  var nama_arr ='';
  switch(div) {
      case 'kategori':
          nama_arr = 'Kategori';
          break;
      case 'tujuan':
          nama_arr = 'jenistujuanresto';
          break;
      case 'waktu':
          nama_arr = 'jeniswaktu';
          break;
      case 'fasilitas':
          nama_arr = 'jenisfasilitas';
          break;
      case 'citarasa':
          nama_arr = 'NamaCitaRasa';
          break;
      case 'caramasak':
          nama_arr = 'caramasak';
          break;
      case 'masakan':
          nama_arr = 'jenismakanan';
          break;
  }
  return nama_arr;

}

function numberallcb(arr){
  var jumlah_item=0
    jumlah_item=jumlah_item +arr.kategori.length;
    jumlah_item=jumlah_item +arr.tujuan.length;
    jumlah_item=jumlah_item+arr.masakan.length;
    jumlah_item=jumlah_item+arr.fasilitas.length;
    jumlah_item=jumlah_item+arr.citarasa.length;
    jumlah_item=jumlah_item+arr.caramasak.length;
    jumlah_item=jumlah_item+arr.halal.length;
    jumlah_item=jumlah_item+arr.waktu.length;
  return jumlah_item;
}
function price(){
$(".price").priceFormat({
  prefix: 'Rp. ',
  centsLimit: '0',
  thousandsSeparator: '.',
  allowNegative: true
});
}
