// $(function () {

/*
    -- BIASAKAN NGODING YANG RAPI --
*/

! function () {
    "use strict";
    var protocol = window.location.protocol,
        hostname = window.location.hostname,
        port = window.location.port,
        pathname = window.location.pathname;

    var pathArray = pathname.split ( '/' ),
        getHost = (hostname === 'localhost' || hostname === '127.0.0.1') ? hostname + '/' + pathArray[ 1 ] : hostname,
        segmenTwo = (hostname === 'localhost' || hostname === '127.0.0.1') ? '/' + pathArray[ 2 ] : '/' + pathArray[ 1 ],
        pathOri = protocol + '//' + getHost + '/',
        pathSub = protocol + '//' + getHost + segmenTwo + '/';
    var kulval = null,
        windowWidth = $(window).width(),
        windowHeight = $(window).height(),
        headerHeight = $(".header-top").outerHeight(),
        winheight = (windowHeight - headerHeight);

    var gethass = '';

    $('[data-toggle="tooltip"]').tooltip();

    /* -------------------------------------------------------------------------------------------------------------- */
    function searchUp(val, back) {
        $(".section-navbar .wait-result").show();
        if(val.length > 2){
            $.ajax({
                type: "POST",
                // cache: false,
                url: pathOri + 'index.php/getauto',
                data: {"s": val, "session": "1"},
                success: function (data) {
                    $("h5.mbl-heighth5").html('Hasil Pencarian Anda');
                    back(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("h5.mbl-heighth5").html('-- No Result --');
                }
            })
        } else {
            back('');
        }
    }

    function setpage(){
        $ (".kuliner-modal").removeAttr('style').css({'top' : headerHeight}); //, 'height' : (winheight - 20)
    }
    function setmenu(){
        windowHeight = $(window).height();
        headerHeight = $(".header-top").outerHeight();
        winheight = (windowHeight - headerHeight);
        windowWidth = $(window).width();

        $ ("#show_menu").css ({'height' : '100%', 'top' : headerHeight});
    }

    /* -------------------------------------------------------------------------------------------------------------- */
    /* Search Top */
    $ (window).on ("scroll", function (e){
        e.stopPropagation ();
        e.preventDefault ();
        setmenu();

        if ($('.section-navbar').length){
            var hovered = $ ("#topicon-twoand").find ("span.change-wilayah:hover").length;
            setpage();

            if ($ (this).scrollTop () > 150){
                $ (".navbar-topicon-start").hide ();
                $ (".navbar-topicon-twoand").show ();

                $ (".header-top").animate (1000).css ({
                    'top' : 0
                }).addClass ('fix').show ();
                if ($ ("input#search").is (":focus")){
                    $ ("input#search").focus ();
                }
            } else {
                $ (".navbar-topicon-start").show ();
                $ (".navbar-topicon-twoand").hide ();

                $ (".header-top").animate (1000).css ({
                    'top' : -90
                }).removeClass ('fix');
                if ($ ("input#search").is (":focus")){
                    $ ("input#search").focus ();
                }
            }

            if (hovered){
                $ (".kuliner-modal").show (kulval);
            }

            $ (".dropdown_kat").hide ();
            // $ ("a#showmenu").removeClass ('active');
            $ ("#box-search").hide ();
        }
    });
    if ($('.section-navbar').length){
        $ ("ul.menuhover-height li a").hover (
            function (){
                var page = $ (this).data ('target');

                $ (".menuhover-height li").removeClass ("active");
                $ (".menuhover-height li a").removeClass ("active");
                $ (".menuhover-content").find ("div.menuhover-contenttab").hide ();
                $ ("#" + page).fadeIn (500);
                $ (this).addClass ('active');
                $ (this).parent ().addClass ('active');

                $ ("#box-search").hide ();
            }, function (){

            }
        );

        $("a#showmenu").click(function (){
            $ (".menuhover-height li").removeClass("active");
            $ (".menuhover-height li a").removeClass ("active");
            $ (".menuhover-content").find ("div.menuhover-contenttab").hide ();

            $('.menuhover-height li:first').addClass('active');
            $('.menuhover-height li:first a').addClass('active');
            $ (".menuhover-content").find ("div.menuhover-contenttab:first").show ();

            if (winheight < 350){
                $ (".menuhover-left, .menuhover-right").css ({'height' : 250});
                $ (".menuhover-right .menuhover-right-frame").css ({'height' : (250 - ($(".menuhover-contenttitle").outerHeight() + 10))});
            } else {
                $ (".menuhover-left, .menuhover-right").css ({'height' : (winheight - 30)});
                $ (".menuhover-right .menuhover-right-frame").css ({'height' : (winheight - 30) - ($(".menuhover-contenttitle").outerHeight() + 10)});
            }

            $("#show_menu").slideToggle("fast", function (){
                if ($(this).is(':hidden')) {
                    $ ("a#showmenu").css ({'background' : 'transparent'});
                    $ ("a#showmenu i").css ({'color' : '#fff'}).removeClass('fa-times').addClass('fa-bars');
                } else {
                    $ ("a#showmenu").css ({'background' : '#fff'});
                    $ ("a#showmenu i").css ({'color' : '#000'}).removeClass('fa-bars').addClass('fa-times');
                }
            });
        });

        $ ("span.change-wilayah").hover (
            function (){
                $ (".kuliner-modal").stop().slideDown(50);
            }, function (){
                $ (".kuliner-modal").stop().slideUp(50);
                setpage();
            }
        );

        $ ("#show_menu").css ({'height' : '100%', 'top' : headerHeight});
    }

    /* ============================================================================================================== */
    $ (document).on ("focus input", "input#search", function (e){
        e.stopPropagation ();
        e.preventDefault ();
        var keyword = $("input#search").val();
        if(keyword.length === 0){
            $ ("#search-suggestion.input-keyword").hide();
            $ ("#search-suggestion.default").show();
        } else {
            var param = $ (this).val ();
            searchUp (param, function (msg){
                $ ("#search-suggestion.default").hide();
                $ ("#search-suggestion.input-keyword").show().html(msg);;
            });
        }
        $ ("#search-suggestion-box").show();
    });

    $(document).on('click', function(event) { 
        if(!$(event.target).closest('input#search').length && !$(event.target).closest('#search-suggestion-box').length) {
            $ ("#search-suggestion-box").hide();
        }        
    });
    
    $(document).on("click", '.button-more', function(e) { 
        e.preventDefault();
        var hiddenItem = $(this).parent().siblings(".pin");
        var parent     = $(this).parent();
        var totalItem  = parent.data('total-item');
        hiddenItem.toggleClass("hidden");
        parent.toggleClass('item-3 item-'+totalItem);
        $(this).find("i").toggleClass('fa-angle-double-down fa-angle-double-up');
    });

    // $ (document).on ("keyup", "input#search", function (e){
    //     e.stopPropagation ();
    //     e.preventDefault ();
    //     $ ("#search-suggestion").addClass("input-keyword");
    //     console.log("tambah class input keyword");


    //     // var param = $ (this).val (),
    //     //     topheight = $(".mbl-heightsearch").outerHeight();

    //     // $(".mbl-search .mbl-out").css({'top':(topheight)});

    //     // searchUp (param, function (msg){
    //     //     $(".mbl-search #mbl-result").fadeIn().html(msg);
    //     // });
    // });
    $ (document).on ("click", "a#resmenu_open", function (e){
        $ ("#show_menu_resp").toggle ("slide", {direction: "left"}, 300);
        window.location.hash = 'menu';
    });
    $ (document).on ("click", "a#resmnu_close", function (e){
        history.pushState({}, document.title, location.href.replace('#menu', ""));
        $ ("#show_menu_resp").toggle ("slide", {direction: "right"}, 300);
    });
    $ (document).on ("click", "a#search_open", function (e){
        $ ("#show_search").toggle("slide", {direction: "right"}, 300);
        window.location.hash = 'search';
    });
    $ (document).on ("click", "a#search_close", function (e){
        $ ("#show_search").toggle("slide", {direction: "left"}, 300);
        $("input#mbl-search").val('');
        $(".mbl-search #mbl-result").fadeIn().html('');
        history.pushState({}, document.title, location.href.replace('#search', ""));
    });

    if ($('.thumbnail-custom').length){
        $ ('.thumbnail-custom').matchHeight ({
            byRow : true,
            property : 'height',
            target : null,
            remove : false
        });
    }
    if ($('.thumbnail').length){
        $ ('.thumbnail').matchHeight ({
            byRow : true,
            property : 'height',
            target : null,
            remove : false
        });
    }
    if ($('.thumbnail-kanan').length){
        $ ('.thumbnail-kanan').matchHeight ({
            byRow : true,
            property : 'height',
            target : null,
            remove : false
        });
    }
    if ($('#get-zoom').length){
        $('#get-zoom:not(:last-child)')
            .click(function () {
                var dt_img = $(this).data('full-image');
                var dt_title = $(this).data('title');
                var dt_ind = $(this).data('ind');
                $("#for-zoom").fadeIn('fast', function (){
                    $('#carousel-example-generic').carousel(dt_ind);
                });
            });
        $("#for-zoom, #for-full")
            .click(function (event) {
                if (event.target.id == 'for-zoom') {
                    $("#for-zoom").fadeOut('fast');
                }
                if (event.target.id == 'for-full') {
                    $("#for-full").fadeOut('fast');
                }
            });
        $('#for-zoom-close')
            .click(function () {
                $("#for-zoom").fadeOut('fast');
            });
        $('#view-full')
            .click(function () {
                $("#for-full").fadeIn('fast', function (){
                    $('#for-full').carousel();
                });
                $("#for-zoom").fadeOut('fast');
            });
        $('#for-full-close')
            .click(function () {
                $("#for-full").fadeOut('fast');
            });
    }
    if ($('#kategori').length){
        $('a#landing-kat')
            .click(function () {
                var d = $(this).data('d');
                $("li#list-" + d).toggle("fast");
            });
    }
    if ($("#slidehome").length){
        $("#slidehome").slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 2.1,
            autoplay: false,
            adaptiveHeight: true,
            prevArrow:"<button class='slid-prev'><i class='fa fa-arrow-left'></i></button>",
            nextArrow:"<button class='slid-next'><i class='fa fa-arrow-right'></i></button>"
        });
    }

    /* ============================================================================================================== */
    if (window.history && window.history.pushState) {
        $(window).on('popstate', function() {});
    }

    $ (document).on ("click", "li a#showsecond", function (){
        var tar = $ (this).data ('target');
        alert (tar);
    });

    $(window).on('resize', function(){
        setmenu();
        setpage();
    });

    setmenu();
    setpage();
}();
