$(function() {
	$("#menu-dua").on('click', function (e) {
		e.preventDefault();
		$("#menu-dua").addClass("active");
		$(".focus-foto").show();
		$("#col-foto").hide();
		$("#ulasan").hide();

		$(".list-ulasan").css('display', 'none');
		$("#col-ulasan").hide();
		$("#menu-satu").removeClass("item-satu");
		$("#menu-tiga").removeClass("active");
	});
	$("#menu-satu").on('click', function (e) {
		e.preventDefault();
		$("#menu-satu").addClass("item-satu");
		$("#menu-dua").removeClass("active");
		$("#menu-tiga").removeClass("active");
		$(".focus-foto").hide();
		$("#col-foto").show();
		$("#ulasan").show();
		$(".list-ulasan").css('display', 'block');
	});
	$("#menu-tiga").on('click', function (e) {
		e.preventDefault();
		$(".focus-foto").hide();
		$("#col-foto").hide();
		$("#ulasan").hide();
		$(".list-ulasan").css('display', 'block');
		$("#col-ulasan").hide();
		$("#menu-tiga").addClass("active");
		$("#menu-satu").removeClass("item-satu");
		$("#menu-dua").removeClass("active");
	});
	$("#other-fasilitas").on('click', function(e) {
		// $("#other-fasilitas").css("color", "blue");
		$(".fasilitas-other").toggleClass("fasilitas-other-show");
	});

})

function is_scrool_on_top(elem) {
	var docViewTop = $(window).scrollTop();
	var docViewBottom = docViewTop + $(window).height();

	var elemTop = $(elem).offset().top;
	var elemBottom = elemTop + $(elem).height();
	var show = ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
	return {fromTop: elemTop - docViewTop, fromBottom: docViewBottom - elemBottom, is_show: show, element: $(elem)}
}

$(window).on('scroll', function(){
    if ($('.sign--menu-informasi').length){
        var position = is_scrool_on_top ('.sign--menu-informasi');
        if (position.fromTop < 5){
            var width = position.element.css ('width');
            console.log (position.fromTop)
            $ ('#menu-informasi').css ('width', width).addClass ('tab-fixed-top')
            console.log ('tidak normal')
        } else {
            var width = position.element.css ('width');
            console.log (width)
            $ ('#menu-informasi').css ('width', '').removeClass ('tab-fixed-top')

            console.log ('normal')
        }
    }
})

function focusFoto() {
	var vheight = $(window).height();
	$('html, body').animate({
		scrollTop: (Math.floor($(window).scrollTop() / vheight)+1) * vheight
	}, 400);  
};

$(document).ready(function() {
	$('.shading-last-image').click(function(event) {
		focusFoto();
		event.preventDefault();
		// alert("hello");
		$("#menu-dua").addClass("active");
		$(".focus-foto").show();
		$("#col-foto").hide();
		$("#ulasan").hide();

		$(".list-ulasan").css('display', 'none');
		$("#col-ulasan").hide();
		$("#menu-satu").removeClass("item-satu");
		$("#menu-tiga").removeClass("active");
	});	
});
