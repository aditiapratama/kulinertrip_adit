	
	/*
	|------------------------------------------------------
	| Function openAdvanceFilter
	|------------------------------------------------------
	| Fungsi untuk membuka dan menutup filter lanjutan.
	| @params 
	| 	- e <object> object element. (this)
	|------------------------------------------------------
	*/
	function openAdvanceFilter(e){
		if(!e) return false;

		// check apakah .filter-advance punya class hide.
		// var isFilterAdvanceShow = $('.filter-advance').hasClass('hide')
		var isFilterAdvanceShow = $(e).prop('checked'),
			toggle = $(e).attr('data-toggle'),
			toggle_element = $(toggle)
		if(isFilterAdvanceShow){ 
			toggle_element.show()
			// hapus class pada button
			$(e).closest('.btn').removeClass('btn-outline-success'); 
			toggle_element.removeClass('hide');
		}else{
			toggle_element.hide()
			// tambahkan class pada button
			$(e).closest('.btn').removeClass('active'); 
			$(e).closest('.btn').addClass('btn-outline-success');
			toggle_element.addClass('hide');
		}
			// toggle class pada element .filter-advance

		Main_filter_components.slider.set_width_range_element(toggle_element.find('.input-multiple-range'))
		
	}

	/*
	|------------------------------------------------------
	| Function setActiveTab
	|------------------------------------------------------
	| Fungsi untuk toggling switch pada tab Pilih filter (Filter umum | Filter kesukaan).
	| @params 
	| 	- e <object> object element. (this)
	|------------------------------------------------------
	*/
	function setActiveTab(e, force)
	{
		var parents = $(e).closest('.filter-session--item'),
			input 	= parents.find('input'),
			isActive = input.prop('checked'),
			data 	= input.data()

			if(data.forceSwitch)
			{
				input.prop('checked',!isActive)
				parents.toggleClass('on');
			}

			$(document).trigger('read-data-dropdown');
			Main_filter_components.slider.set_width_range_element()
			
	}

	/*
	|------------------------------------------------------
	| Function toRupiahFormat
	|------------------------------------------------------
	| Fungsi untuk menjadikan input menjadi rupiah format.
	| @params 
	| 	- input <string | integer> 
	|------------------------------------------------------
	*/
	function toRupiahFormat(input)
	{
		/*src = http://jagowebdev.com/format-rupiah-dengan-javascript/;*/

		input = input.toString(); 
		var sisa = input.length % 3, // mod 3 
			rupiah = input.substr(0, sisa), // ambil angka n angka dari depan. n = hasil mod. 
			ribuan = input.substr(sisa).match(/\d{3}/g), // pisahkan hasil mod per 3 item.
			separator = '';

		// jika ribuan
		if(ribuan)
		{
			// tambah separator sisa
			separator = sisa?'.':'';
			// join data ribuan. tambahkan data ribuan tersebut pada rupiah
			rupiah += separator+ribuan.join('.');
		}

		return rupiah;
	}

	/*
	|------------------------------------------------------
	| Function Submit
	|------------------------------------------------------
	| Fungsi untuk mengirim data.
	| 
	|------------------------------------------------------
	*/
       (function ($) {
    $.fn.serializeFormJSON = function () {

        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
    })(jQuery);
    

function getJson(data){
    var o = {};
        var a = data;
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
}
	function process_filter(event)
	{
		event.preventDefault();
		var data_session = $('[name="session[]"]').serializeArray(),
			value_session = data_session.map(function(res) {
				return res.value
			}),
			form_required = $('[data-session="general"]').serializeArray(),
			data = $.merge(form_required, data_session),
			data_notification = []
			url = $(event.target).attr('action')		
		
		value_session.forEach(function(b, a){
			var session = $('[data-session="'+b+'"]').serializeArray();
			data = $.merge(data, session);
			data_notification = $.merge(data_notification, session);
		});

		data_notification = Main_filter.notification_data(data_notification, data);

		data = Main_filter.filter_normalization_data(data.slice(0));
		
		Main_filter.set_notification(data_notification.length);

              /*
		$.ajax({
			url : url,
			type: 'POST',
			data: data
		})
		.done(function(data){
                //console.log(data);
			//$('#modal-filter').modal('hide')
			// window.location = url;
//                        $('.modal').modal('hide')
//                $.redirect(url,data);
                
		})
             */
            $.redirect(url,getJson(data));
	}

	function changeInput(that)
	{
		var me 				= $(that),
			parents 		= $(that).closest('.label-modify'),
			is_checked 		= $(that).prop('checked')

			if(is_checked)
			{
				parents.addClass('active')
			}else
			{
				parents.removeClass('active')
			}
	}


	/*
	|------------------------------------------------------
	| Function Clear Filter
	|------------------------------------------------------
	| Fungsi untuk clear data.
	| 
	|------------------------------------------------------
	*/
	function clear_filter(form, identifier)
	{	
		$(identifier).find('input[form="'+form+'"][data-if-null]').prop('checked',true).trigger('change');
		$(identifier).find('input[form="'+form+'"]:not([data-if-null])').prop('checked',false).trigger('change');
		$(identifier).find('input[form="'+form+'"]:not([data-if-null])').closest('.label-modify').removeClass('active')

		// reset semua input yang menggunakan id :form
		$('#'+form)[0].reset();

		Main_filter_components.slider.data.name.forEach(function(b,a){
			var range_data = Main_filter_components.slider.data.get(b),
				as_form = $(range_data.slider).attr('form')
			if(as_form == form)
			{
				range_data.reset()
			}
		});

		var count = Main_filter_components.badge.count_notification('umum');
		Main_filter_components.badge.set('umum', count.length);
		var count = Main_filter_components.badge.count_notification('kesukaanku');
		Main_filter_components.badge.set('kesukaanku', count.length);

		// update data range. set to default.
		// update_onchange_range_input()

		// set dropdown value to default. (default = first-child)
		$(identifier).find('.filter-advance-options .dropdown-menu').each(function(res){
			var me = $(this).find('li.option-item--parent:first-child')
			me.siblings().find('input').prop('checked', false)
			me.find('input').prop('checked',true)
		});

		$(identifier).find('.filter-advance-options').trigger('read-data-dropdown');

	}
	
	$(document).ready(function(){
            //alert('filter modal ---Error on fetching data inline 87');
		$('.modal-filter').on('shown.bs.modal', function () {
			Main_filter_components.slider.set_width_range_element()
		})
		//$(document).on('request.getLandingPagev2.done', function(){
                    
//			kulinertrip.get_cache_records('getLandingPagev2')
//			.process
                    $.ajax({
                                    type: "POST",
                                    data: {'jsonParamFilter':JSON.stringify({
                                            kabupaten: 'Kabupaten Sleman',
                                            propinsi: 'Daerah Istimewa Yogyakarta',
                                            latitude: -7.74766584,
                                            longitude: 110.42292105,
                                            language: 'ID',
                                            iduser :"1",
                                            jenisfilter:""

                                        })
                                    },
                                     url: 'http://localhost/kulinertripbo/index.php/ctrlandingpage/getJsonFilterAll',
                                   //  url: kulinertrip.base_url('ctrlandingpage/getJsonFilterAll'),
                                   //  url: kulinertrip.base_url('ctrlandingpage/getJsonFilterAll'),
                                     //url: kulinertrip.base_url('Ctrindex/getDaerah'),
                                    //url: kulinertrip.base_url('dummy/getdata'),
                                    caching: true,
                                    dataType: 'json',
                                    name: 'getLandingPagev2'
                            })
			.done(function(res){
				// kategori
				Main_filter_components.write.kategori('umum', res[0].jsonKategoriTenantProvKab)
				Main_filter_components.write.kategori('kesukaanku', res[0].jsonKategoriTenantProvKab)
				
				// tujuan
				Main_filter_components.write.tujuan('umum', res[0].jsonJenisTujuanProvKab)
				Main_filter_components.write.tujuan('kesukaanku', res[0].jsonJenisTujuanProvKab)
				
				// tujuan
				Main_filter_components.write.masakan('umum', res[0].jsonJenisMasakanProvKabParentChild)
				Main_filter_components.write.masakan('kesukaanku', res[0].jsonJenisMasakanProvKabParentChild)
				// waktu
				Main_filter_components.write.waktu('umum', res[0].jsonJenisWaktuProvKab)
				Main_filter_components.write.waktu('kesukaanku', res[0].jsonJenisWaktuProvKab)
				
				// rasa
				Main_filter_components.write.cita_rasa('umum', res[0].jsonCitaRasaProvKab)
				Main_filter_components.write.cita_rasa('kesukaanku', res[0].jsonCitaRasaProvKab)
				// cara masak
				Main_filter_components.write.cara_masak('umum', res[0].jsonCaraMasakProvKab)
				Main_filter_components.write.cara_masak('kesukaanku', res[0].jsonCaraMasakProvKab)
				// halal
				Main_filter_components.write.halal('umum', res[0].jsonListJenisHalal)
				Main_filter_components.write.halal('kesukaanku', res[0].jsonListJenisHalal)

				/*---------------------------------------- B A G I A N  I N D I V I D U A L -----------------------------------------*/
				Main_filter_components.write.kategori('individual', res[0].jsonKategoriTenantProvKab, {
					selfAppend: function(opt)
					{
						// create <li> element and html inside with opt.template
						var template = $('<li>',{class: 'option-item--parent'}).html(opt.template); 
						$('#filter-individual--kategori .dropdown-menu .option-item--parent:last-child').after(template)
					}
				});

				Main_filter_components.write.tujuan('individual', res[0].jsonJenisTujuanProvKab, {
					selfAppend: function(opt)
					{
						var template = $('<li>',{class: 'option-item--parent'}).html(opt.template); // create <li> element and html inside with opt.template
						$('#filter-individual--tujuan .dropdown-menu .option-item--parent:last-child').after(template)
					}
				});
				Main_filter_components.write.waktu('individual', res[0].jsonJenisWaktuProvKab, {
					selfAppend: function(opt)
					{
						// create <li> element and html inside with opt.template
						var template = $('<li>',{class: 'option-item--parent'}).html(opt.template); 
						$('#filter-individual--waktu .dropdown-menu .option-item--parent:last-child').after(template)
					}
				})
				Main_filter_components.write.masakan('individual', res[0].jsonJenisMasakanProvKabParentChild,{
					child_template: function(opt){
						
						var template = opt.template.addClass('option-item--parent');
						return  template;
					}
				});

				Main_filter_components.write.cita_rasa('individual', res[0].jsonCitaRasaProvKab);

				Main_filter_components.write.cara_masak('individual', res[0].jsonCaraMasakProvKab);

				Main_filter_components.write.halal('individual', res[0].jsonListJenisHalal);
                                
                                

			}).fail(function(res){
		                  console.log(res);
                               // alert('Error on fetching data inline 294');
                        })

		//})


		Main_filter_components.slider.set_related_with({
			parent: $('.col-harga--range-input'),
			slider: '#harga-range-input--umum',
			slider_ghost: '#harga-range-input--umum.ghost',
			on: 'input',
			range: document.getElementById('harga-range-input--umum'),
			input:
			{
				high: '#input-multiple-range--detail-high--umum',
				low: '#input-multiple-range--detail-low--umum'
			},
			onchange: function(a)
			{
				/*
				|------------------------------------------------------
				| Event Reader
				|------------------------------------------------------
				| fungsi untuk membaca ketika slider-range pada harga berubah.
				|------------------------------------------------------
				*/
				$(a.input.low).val('Rp. '+toRupiahFormat(a.value.low)); // tulis nilai terendah pada input bawah slider-range
				$(a.input.high).val('Rp. '+toRupiahFormat(a.value.high)); // tulis nilai tertinggi pada input bawah slider-range
			},
			onKeyup: function(a)
			{
				/*
				|------------------------------------------------------
				| Event Reader
				|------------------------------------------------------
				| fungsi untuk membaca ketika input harga dibawah slider-range berubah ketika diganti.
				|------------------------------------------------------
				*/
				var getLow = $(a.input.low).val().match(/\d/g), // ambil nilai dari input low-range
					getHigh = $(a.input.high).val().match(/\d/g) // ambil nilai dari input high-range

					

				if(getLow.length > 0 || getHigh.length > 0)
				{
					getLow = parseInt(getLow.join(''))
					getHigh = parseInt(getHigh.join(''))

					if(getLow > getHigh)
					{
						var defGetLow = getLow;
						getLow =  getHigh;
						getHigh = defGetLow;
						
					}

					$(a.input.low).val('Rp. '+toRupiahFormat(getLow))
					$(a.input.high).val('Rp. '+toRupiahFormat(getHigh))
					$(a.options.slider).val(getLow+','+getHigh)
				}

				
			}
		});

		Main_filter_components.slider.set_related_with({
			parent: $('.col-harga--range-input'),
			slider: '#harga-range-input--kesukaanku',
			slider_ghost: '#harga-range-input--kesukaanku.ghost',
			on: 'input',
			range: document.getElementById('harga-range-input--kesukaanku'),
			input: {
				high: '#input-multiple-range--detail-high--kesukaanku',
				low: '#input-multiple-range--detail-low--kesukaanku'
			},
			onchange: function(a)
			{
				/*
				|------------------------------------------------------
				| Event Reader
				|------------------------------------------------------
				| fungsi untuk membaca ketika slider-range pada harga berubah.
				|------------------------------------------------------
				*/
				$(a.input.low).val('Rp. '+toRupiahFormat(a.value.low)); // tulis nilai terendah pada input bawah slider-range
				$(a.input.high).val('Rp. '+toRupiahFormat(a.value.high)); // tulis nilai tertinggi pada input bawah slider-range
			},
			onKeyup: function(a)
			{
				/*
				|------------------------------------------------------
				| Event Reader
				|------------------------------------------------------
				| fungsi untuk membaca ketika input harga dibawah slider-range berubah ketika diganti.
				|------------------------------------------------------
				*/
				var getLow = $(a.input.low).val().match(/\d/g), // ambil nilai dari input low-range
					getHigh = $(a.input.high).val().match(/\d/g) // ambil nilai dari input high-range

				if(getLow.length > 0 || getHigh.length > 0)
				{
					getLow = parseInt(getLow.join(''))
					getHigh = parseInt(getHigh.join(''))

					if(getLow > getHigh)
					{
						var defGetLow = getLow;
						getLow =  getHigh;
						getHigh = defGetLow;
						
					}

					$(a.input.low).val('Rp. '+toRupiahFormat(getLow))
					$(a.input.high).val('Rp. '+toRupiahFormat(getHigh))
					$(a.options.slider).val(getLow+','+getHigh)
				}
			}
		});

		Main_filter_components.slider.set_related_with({
			parent: $('.col-harga--range-input'),
			slider: '#harga-range-input--individual',
			slider_ghost: '#harga-range-input--individual.ghost',
			on: 'input',
			range: document.getElementById('harga-range-input--individual'),
			input: {
				high: '#input-multiple-range--detail-high--individual',
				low: '#input-multiple-range--detail-low--individual'
			},
			onchange: function(a)
			{
				/*
				|------------------------------------------------------
				| Event Reader
				|------------------------------------------------------
				| fungsi untuk membaca ketika slider-range pada harga berubah.
				|------------------------------------------------------
				*/
				$(a.input.low).val('Rp. '+toRupiahFormat(a.value.low)); // tulis nilai terendah pada input bawah slider-range
				$(a.input.high).val('Rp. '+toRupiahFormat(a.value.high)); // tulis nilai tertinggi pada input bawah slider-range
			},
			onKeyup: function(a)
			{
				/*
				|------------------------------------------------------
				| Event Reader
				|------------------------------------------------------
				| fungsi untuk membaca ketika input harga dibawah slider-range berubah ketika diganti.
				|------------------------------------------------------
				*/
				var getLow = $(a.input.low).val().match(/\d/g), // ambil nilai dari input low-range
					getHigh = $(a.input.high).val().match(/\d/g) // ambil nilai dari input high-range

				if(getLow.length > 0 || getHigh.length > 0)
				{
					getLow = parseInt(getLow.join(''))
					getHigh = parseInt(getHigh.join(''))

					if(getLow > getHigh)
					{
						var defGetLow = getLow;
						getLow =  getHigh;
						getHigh = defGetLow;
						
					}

					$(a.input.low).val('Rp. '+toRupiahFormat(getLow))
					$(a.input.high).val('Rp. '+toRupiahFormat(getHigh))
					$(a.options.slider).val(getLow+','+getHigh)
				}
			}
		});


		/*
		|------------------------------------------------------
		| initialize immedietly
		|------------------------------------------------------
		| fungsi untuk menyusun struktur scale slider-range.
		| 
		| @ Notes
		| -
		|------------------------------------------------------
		*/
		var i = 0
		$('.range-labels li').each(function(){
			total = $(this).siblings().length + 1;
			var inc = i
			var value = $(this).text().match(/\d/g).join('');
			var add = value<600000?1-(i*.7):(value>900000)?i*-.3:-.3
			var percentage = (value/1600000)*100+add
			$(this).css('left', (percentage+(value<900000?1:-i*.2))+'%')

			i++;
			if(i == total)
			{
				i = 0;
			}
		});


		
		/*
		|------------------------------------------------------
		| Event Reader
		|------------------------------------------------------
		| fungsi untuk membaca event reader jika toggle di switch.
		| @params 
		| 	- input <string | integer> 
		| @ Notes
		| kenapa di-trigger disini? karena filter item adalah sebuah bootstrap tab, fungsi onclick tidak terbaca.
		|------------------------------------------------------
		*/
		$(document).delegate('[name="is_filter_lanjutan"]','change', function(e){
			openAdvanceFilter(e.target);
		})

		$('.switch').on('click', function(e){
			setActiveTab(e.target);
		})

		

		/*
		|------------------------------------------------------
		| Event Reader
		|------------------------------------------------------
		| fungsi untuk membaca event ketika input pada tiap pilihan pada filter berubah.
		|------------------------------------------------------
		*/
		
		$('content.tab-pane .label-modify').delegate('input','change', function(event){
			var me = $(this);
			changeInput(me)
		});

		
		/*
		|------------------------------------------------------
		| Event Reader
		|------------------------------------------------------
		| fungsi untuk membaca event ketika input[checkbox|radio] yang ada didalam list pilihan pada dropdown advance filter berubah
		|------------------------------------------------------
		*/
		/*$('.filter-advance-options .dropdown-menu').delegate('input','change', function(event){
			// check apakah yang diklik adalah null?
				var input = $(this);
				var is_checked = input.prop('checked')
				if(is_checked)
				{
					$(this).closest('.option-item--parent').addClass('active');
				}else
				{
					$(this).closest('.option-item--parent').removeClass('active');
				}

				$(this).closest('.dropdown-menu').find('.option-item').removeAttr('checked');
				$(this).attr('checked',true)

			return false;
		});*/
            
	   


	});

function doshowtabfilter(xfilter){
    
   $.redirect(kulinertrip.base_url()+'Ctrfilter/setfilterke',{filtertabke:xfilter,jsonparam:$("#jsonparam").html()});                             
   //$.redirect(kulinertrip.base_url()+'Ctrfilter/submitfilter/'+$("#jsonparam").html()+'/'+xfilter);                             
}