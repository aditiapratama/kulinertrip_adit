
/* 
| ====================================================== 
| Function rating
| 
| ======================================================
*/
(function ( $ ) {
 	
 	// gunakan .rating-box
    $.fn.rating = function(value) {
    	this.each(function(){
	    	that = $(this);
	    	var data = that.data();
	    	var options = {
	    		score: 0,
	    		autochange: true,
	    		hoverable: false
	    	}

			options = $.extend(options, data)

	    	if(value && typeof parseInt(value) != 'number')
	    	{
	    		options = $.extend(options, value)
	    	}else
	    	{
	    		options.score = value;
	    	}
	    	// extend with element attribute

	    	var color = [
	    		'#e80f0a',
	    		'#ef1818',
	    		'#f4740b',
	    		'#f98630',
	    		'#f4c914',
	    		'#f8e26f',
	    		'#a4ddc5',
	    		'#6fc9a6',
	    		'#18c814',
	    		'#037b00'
	    	]
	    	var type = that.hasClass('.rating-box'),
	    		data = that.data(),
	    		item = that.find('.rating-item'),
	    		index = options.score > 0? options.score -1 : 0,
	    		me  = that

	    	$.each(options, function(a,b){
	    		me.data(a, b);
	    	})
	    	item.removeClass('set latest active')
	    	if(options.score > 0)
	    	{
				$(item[index]).addClass('active set latest')
				$(item[index]).prevAll().addClass('active');
				$(item[index]).nextAll().removeClass('active');
	    	}
			val = $(item[index]).attr('value');
			if(options.autochange && options.score > 0)
			{
				$('[data-toggle-score="'+data.scoreName+'"]').text(val).css('color', color[index])
			}else
			{
				$('[data-toggle-score="'+data.scoreName+'"]').text(options.score).css('color', color[0])
				that.data({score: options.score})
			}

			// rerata
			if(options.ratingGroup)
			{
				var group = $('[data-rating-group="'+options.ratingGroup+'"]'),
					i = 0,
					len = group.length,
					sum = 0,
					rerata = 0;

					$('[data-rating-group="'+options.ratingGroup+'"]')
					.each(function(){
						var score = $(this).data().score;
						sum += parseInt(score);
						i++;
						if(i == len)
						{
							rerata = sum / len;
							rerata = rerata.toFixed(1)
							$('[data-total-rating]').text(rerata)
						}

					})

			}
			me.trigger('KT.rating.change')
    	})
    	
        return this;
    };
 
}( jQuery ));
$(document).ready(function(){
	$('.rating-item').on('mouseenter', function(res){
		var parent = $(this).closest('.rating-box');
		console.log(parent)
		if(parent.data().hoverable)
		{
			$(this).addClass('active')
			$(this).siblings().removeClass('set')
			$(this).prevAll().addClass('active');
			$(this).nextAll().removeClass('active');
		}
		$(this).trigger('KT.rating.enter')
	})
	$('.rating-item').on('click', function(res){
		var parent = $(this).closest('.rating-box');
		if(parent.data().hoverable)
		{
			$(this).siblings().removeClass('set latest')
			$(this).addClass('active set latest')
			$(this).prevAll().addClass('active');
			$(this).nextAll().removeClass('active');
			val = $(this).attr('value');
			color = $(this).css('background-color');
			parent.rating(val)
		}
		$(this).trigger('KT.rating.click')

	})
	$('.rating-box').on('mouseleave', function(res){
		var parent = $(this);
		if(parent.data().hoverable)
		{
			if($(this).find('.set').length < 1)
			{
				$(this).children().removeClass('active');
			}else
			{
				$(this).find('.set').nextAll().removeClass('active');
				$(this).find('.set').prevAll().addClass('active');
				$(this).find('.set').addClass('active')
			}

			if($(this).find('.length').length < 1)
			{
				$(this).find('.latest').prevAll().addClass('active');
				$(this).find('.latest').nextAll().removeClass('active');
				$(this).find('.latest').addClass('active')
			}
		}
		$(this).trigger('KT.rating.leave')
	})
})