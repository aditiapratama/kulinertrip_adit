	var Main_filter_components = (function(){
		var a = function(){
		}
		a.prototype = {
			/*
			|------------------------------------------------------
			| Function toRupiahFormat
			|------------------------------------------------------
			| Fungsi untuk menjadikan input menjadi rupiah format.
			| @params 
			| 	- input <string | integer> 
			|------------------------------------------------------
			*/
			toRupiahFormat : function(input)
			{
				/*src = http://jagowebdev.com/format-rupiah-dengan-javascript/;*/

				input = input.toString(); 
				var sisa = input.length % 3, // mod 3 
					rupiah = input.substr(0, sisa), // ambil angka n angka dari depan. n = hasil mod. 
					ribuan = input.substr(sisa).match(/\d{3}/g), // pisahkan hasil mod per 3 item.
					separator = '';

				// jika ribuan
				if(ribuan)
				{
					// tambah separator sisa
					separator = sisa?'.':'';
					// join data ribuan. tambahkan data ribuan tersebut pada rupiah
					rupiah += separator+ribuan.join('.');
				}

				return rupiah;
			},
			slider:{
				/*
				options
					- parent
					- slider
					- slider_ghost
					- on
					- range // menggunakan  document.getElementById {valueHigh|valueLow}
					- onchange
					- input
						- high
						- low 
				*/
				set_related_with: function(options)
				{

					/*
					|------------------------------------------------------
					| Event Reader
					|------------------------------------------------------
					| fungsi untuk membaca ketika slider-range pada harga berubah.
					|------------------------------------------------------
					*/
					// $('.col-harga--range-input').delegate('.input-multiple-range, .input-multiple-range.ghost','input', function(e){
					$(options.parent).delegate(options.slider+', '+options.slider_ghost, options.on, function(e){
						var valLow = options.range.valueLow, // ambil nilai terendah dari slider-range
							valHigh = options.range.valueHigh // ambil nilai tertinggi pada slider range

						/*$(options.input.low).val('Rp. '+toRupiahFormat(valLow)); // tulis nilai terendah pada input bawah slider-range
						$(options.input.high).val('Rp. '+toRupiahFormat(valHigh)); // tulis nilai tertinggi pada input bawah slider-range*/
					
						options.onchange({
							value:{high:valHigh, low:valLow}
						});

					
					});

					/*
					|------------------------------------------------------
					| Event Reader
					|------------------------------------------------------
					| fungsi untuk membaca ketika input harga dibawah slider-range berubah ketika diganti.
					|------------------------------------------------------
					*/
					$('#input-multiple-range--detail-low, #input-multiple-range--detail-high').on('keyup', function(){
						update_onchange_range_input(this)

					})
				},
				set_width_range_element: function(el)
				{
					el = el?el:$('.input-multiple-range');
					// initialize panjang slide-range harga pada advance filter.
					el.each(function(e){
						var width = $(this).parent().width();
						$(this).css('width',width)
					});
				}
			}
		}

		return new a();
	})()

	var Main_filter = (function(){
		var a = function(){
			this.session = '';
		}
		a.prototype = {
			get_active_tab: function()
			{
				return {target: $('.filter-session--item.active'), session: this.session};
			},
			set_active_tab: function(alias)
			{
				var el = $('[tab-alias="'+alias+'"]'),
					href = el.attr('href')

				this.session = alias;
				$('[href="'+href+'"]').tab('show');
				$('<a href="#filter-lanjutan--'+alias+'"> </a>').tab('show');
				Main_filter_components.slider.set_width_range_element();
			},
			set_notification: function(num, el)
			{
				el = el? $(el) : $('.filter-modal-toggling')
				num = num||0;
				var badge = el.find('.badge');
				if(num > 0)
				{
					badge.removeClass('hide').text(num)
				}else
				{
					badge.addClass('hide')
				}
			},

			notification_data: function(data)
			{
				var is_filter_lanjutan = data.map(function(res){return res.name}).indexOf('is_filter_lanjutan') > -1;

				data = data.filter(function(res){
					if(res.name['is_filter_lanjutan'])
					{
						is_filter_lanjutan = true
					}

					if(!is_filter_lanjutan)
					{
						if(res.name.indexOf('filter_lanjutan') == 0)
						{
							return false
						}	
					}
					if(res.name != 'is_filter_lanjutan' && res.name != 'session[]' && res.value != 'null' && res.value[0] != 'null'  )
					{
						return res;
					}
				});
				return data;
			}
		}

		return new a();

	})();

	/*
	|------------------------------------------------------
	| Function openAdvanceFilter
	|------------------------------------------------------
	| Fungsi untuk membuka dan menutup filter lanjutan.
	| @params 
	| 	- e <object> object element. (this)
	|------------------------------------------------------
	*/
	function openAdvanceFilter(e){
		if(!e) return false;

		// check apakah .filter-advance punya class hide.
		// var isFilterAdvanceShow = $('.filter-advance').hasClass('hide')
		var isFilterAdvanceShow = $(e).prop('checked');
		if(isFilterAdvanceShow){ 
			$('.filter-advance').show()
			// tambahkan class pada button
			$(e).closest('.btn').removeClass('btn-outline-success'); 
			$('.filter-advance').removeClass('hide');
		}else{
			// hapus class pada button
			$('.filter-advance').hide()
			$(e).closest('.btn').addClass('btn-outline-success');
		}
			// toggle class pada element .filter-advance

		Main_filter_components.slider.set_width_range_element()
		
	}

	/*
	|------------------------------------------------------
	| Function setActiveTab
	|------------------------------------------------------
	| Fungsi untuk toggling switch pada tab Pilih filter (Filter umum | Filter kesukaan).
	| @params 
	| 	- e <object> object element. (this)
	|------------------------------------------------------
	*/
	function setActiveTab(e)
	{
		var parents = $(e).closest('.filter-session--item'),
			input 	= parents.find('input'),
			isActive = input.prop('checked')


			input.prop('checked',!isActive)
			parents.toggleClass('on');

			$(document).trigger('read-data-dropdown');
			Main_filter_components.slider.set_width_range_element()
			
	}

	/*
	|------------------------------------------------------
	| Function toRupiahFormat
	|------------------------------------------------------
	| Fungsi untuk menjadikan input menjadi rupiah format.
	| @params 
	| 	- input <string | integer> 
	|------------------------------------------------------
	*/
	function toRupiahFormat(input)
	{
		/*src = http://jagowebdev.com/format-rupiah-dengan-javascript/;*/

		input = input.toString(); 
		var sisa = input.length % 3, // mod 3 
			rupiah = input.substr(0, sisa), // ambil angka n angka dari depan. n = hasil mod. 
			ribuan = input.substr(sisa).match(/\d{3}/g), // pisahkan hasil mod per 3 item.
			separator = '';

		// jika ribuan
		if(ribuan)
		{
			// tambah separator sisa
			separator = sisa?'.':'';
			// join data ribuan. tambahkan data ribuan tersebut pada rupiah
			rupiah += separator+ribuan.join('.');
		}

		return rupiah;
	}

	/*
	|------------------------------------------------------
	| Function Submit
	|------------------------------------------------------
	| Fungsi untuk mengirim data.
	| 
	|------------------------------------------------------
	*/
	function process_filter(event)
	{
		event.preventDefault();
		var data = $('#form-filter').serializeArray(),
			url = $(event.target).attr('action')
		console.log(data['is_filter_lanjutan'])
		var notification_data = Main_filter.notification_data(data.slice(0));
		/*if(!data['is_filter_lanjutan'])
		{
		}*/
		console.log(notification_data)
		Main_filter.set_notification(notification_data.length);

		$.ajax({
			url: url,
			type: 'POST',
			data: data
		})
		.done(function(res){
			console.log(res)
			$('#modal-filter').modal('hide')
		})
	}

	function changeInput(that)
	{
		var me 				= $(that),
			parents 		= $(that).closest('.label-modify'),
			content 		= $(that).closest('.list-group--content'),
			id_filter 		= $(that).closest('content').attr('id'),
			e_tab_filter 	= $('[role="tab"][href="#'+id_filter+'"]'),
			inputTotal 		= $(that).closest('content').find('input:checked').serializeArray(),
			value 			= $(that).val(),
			is_checked 		= $(that).prop('checked'),
			e_switch 		= e_tab_filter.find('.switch'),
			is_switch_active= e_switch.find('input[type="checkbox"]').prop('checked')

			if(is_checked)
			{
				parents.addClass('active')
			}else
			{
				parents.removeClass('active')
			}

			
			inputTotal = content
						.find('input:checked') // ambil input yang di checked
						.serializeArray() // serializeArray hasil input yang terpilih
						.map(function(res){return res.value}) // keluarkan hasil "value"
						.filter(function(res){return res != 'null' || !res}); // filter hasil. jika ada nilai null, jangan keluarkan. 

			// ubah nilai pada badge
			e_tab_filter.find('.badge').text(inputTotal.length);

			if(inputTotal.length < 1)
			{
				e_tab_filter.find('.badge').addClass('null-data')
				content.find('.label-modify:first-child input[type="radio"]').prop('checked', true)
			}else
			{
				if(!is_switch_active)
				{
					e_switch.trigger('click');
				}
				e_tab_filter.find('.badge').removeClass('null-data')
			}
	}

	/*
	|------------------------------------------------------
	| Function Clear Filter
	|------------------------------------------------------
	| Fungsi untuk clear data.
	| 
	|------------------------------------------------------
	*/
	function clear_filter(event)
	{	
		// cari element filter yang aktif dan bukan null.
		$('.label-modify input[type="checkbox"][form="form-filter"]:not([value="null"]):checked, .label-modify input[type="radio"][form="form-filter"]:not([value="null"]):checked').each(function(){
			var me = $(this);
			me.closest('.label-modify').trigger('click');				
		});

		// reset semua form-filter
		$('#form-filter')[0].reset();

		// update data range. set to default.
		update_onchange_range_input()

		// set dropdown value to default. (default = first-child)
		$('.filter-advance-options .dropdown-menu').each(function(res){
			var me = $(this).find('li.option-item--parent:first-child')
			me.siblings().find('input').prop('checked', false)
			me.find('input').prop('checked',true)
		});
		$(document).trigger('read-data-dropdown');

	}
	/*
	Update range input
	*/
	function update_onchange_range_input(that)
	{
		var value = $(that).val(),
				getLow = $('#input-multiple-range--detail-low--umum').val().match(/\d/g), // ambil nilai dari input low-range
				getHigh = $('#input-multiple-range--detail-high--umum').val().match(/\d/g), // ambil nilai dari input high-range
				getLowFav = $('#input-multiple-range--detail-low--favorite').val().match(/\d/g), // ambil nilai dari input low-range
				getHighFav = $('#input-multiple-range--detail-high--favorite').val().match(/\d/g) // ambil nilai dari input high-range

			if(getLow.length > 0 || getHigh.length > 0)
			{
				getLow = getLow.join('')
				getHigh = getHigh.join('')
				$('#input-multiple-range--detail-low--umum').val('Rp. '+toRupiahFormat(getLow))
				$('#input-multiple-range--detail-high--umum').val('Rp. '+toRupiahFormat(getHigh))
				$('#harga-range-input--umum').val(getLow+','+getHigh)
			}

			if(getLowFav.length > 0 || getHighFav.length > 0)
			{
				getLowFav = getLowFav.join('')
				getHighFav = getHighFav.join('')
				$('#input-multiple-range--detail-low--favorite').val('Rp. '+toRupiahFormat(getLowFav))
				$('#input-multiple-range--detail-high--favorite').val('Rp. '+toRupiahFormat(getHighFav))
				$('#harga-range-input--favorite').val(getLowFav+','+getHighFav)
			}
	}
	$(document).ready(function(){
		/*
		|------------------------------------------------------
		| Event Reader
		|------------------------------------------------------
		| fungsi untuk membaca ketika slider-range pada harga berubah.
		|------------------------------------------------------
		*/
		Main_filter_components.slider.set_related_with({
			parent: $('.col-harga--range-input'),
			slider: '#harga-range-input--umum',
			slider_ghost: '#harga-range-input--umum.ghost',
			on: 'input',
			range: document.getElementById('harga-range-input--umum'),
			onchange: function(a)
			{
				$('#input-multiple-range--detail-low--umum').val('Rp. '+toRupiahFormat(a.value.low)); // tulis nilai terendah pada input bawah slider-range
				$('#input-multiple-range--detail-high--umum').val('Rp. '+toRupiahFormat(a.value.high)); // tulis nilai tertinggi pada input bawah slider-range
			}
		});

		Main_filter_components.slider.set_related_with({
			parent: $('.col-harga--range-input'),
			slider: '#harga-range-input--favorite',
			slider_ghost: '#harga-range-input--favorite.ghost',
			on: 'input',
			range: document.getElementById('harga-range-input--favorite'),
			onchange: function(a)
			{
				$('#input-multiple-range--detail-low--favorite').val('Rp. '+toRupiahFormat(a.value.low)); // tulis nilai terendah pada input bawah slider-range
				$('#input-multiple-range--detail-high--favorite').val('Rp. '+toRupiahFormat(a.value.high)); // tulis nilai tertinggi pada input bawah slider-range
			}
		});

		// /*
		// |------------------------------------------------------
		// | Event Reader
		// |------------------------------------------------------
		// | fungsi untuk membaca ketika input harga dibawah slider-range berubah ketika diganti.
		// |------------------------------------------------------
		// */
		$('#input-multiple-range--detail-low--favorite, #input-multiple-range--detail-high--favorite, #input-multiple-range--detail-low--umum, #input-multiple-range--detail-high--umum').on('keyup', function(){
			update_onchange_range_input(this)

		})

		/*
		|------------------------------------------------------
		| initialize immedietly
		|------------------------------------------------------
		| fungsi untuk menyusun struktur scale slider-range.
		| 
		| @ Notes
		| -
		|------------------------------------------------------
		*/
		var i = 0
		$('.range-labels li').each(function(){
			var inc = i
			var value = $(this).text().match(/\d/g).join('');
			var add = value<600000?1-(i*.7):(value>900000)?i*-.3:-.3
			var percentage = (value/1600000)*100+add
			$(this).css('left', (percentage+(value<900000?1:-i*.2))+'%')
			i++;
		});


		
		/*
		|------------------------------------------------------
		| Event Reader
		|------------------------------------------------------
		| fungsi untuk membaca event reader jika toggle di switch.
		| @params 
		| 	- input <string | integer> 
		| @ Notes
		| kenapa di-trigger disini? karena filter item adalah sebuah bootstrap tab, fungsi onclick tidak terbaca.
		|------------------------------------------------------
		*/
		$(document).delegate('[name="is_filter_lanjutan"]','change', function(e){
			openAdvanceFilter(e.target);
		})

		$('.switch').on('click', function(e){
			setActiveTab(e.target);
		})

		

		/*
		|------------------------------------------------------
		| Event Reader
		|------------------------------------------------------
		| fungsi untuk membaca event ketika input pada tiap pilihan pada filter berubah.
		|------------------------------------------------------
		*/
		
		$('content.tab-pane .label-modify input').on('change', function(event){
			var me = $(this);
			changeInput(me)
		});

		/*
		|------------------------------------------------------
		| Event Reader
		|------------------------------------------------------
		| fungsi untuk membaca event ketika parent / list pada tiap pilihan pada filter di klik.
		| Apa beda nya dengan fungsi atas? - sebenarnya ini masih dalam satu lingkup fungsi yang sama, hanya saja jika fungsi diatas
		| untuk membaca ketika input berubah, fungsi yang ini untuk membaca event ketika list pilihan di-klik!
		|------------------------------------------------------
		*/
		$('content.tab-pane').delegate('.label-modify','click', function(event){
			var this_input 	 	= $(this).find('input[type="checkbox"]'),
				me 				= $(this),
				content 		= $(this).closest('.list-group--content'),
				id_filter 		= $(this).closest('content').attr('id'),
				e_tab_filter 	= $('[role="tab"][href="#'+id_filter+'"]'),
				inputTotal 		= $(this).closest('content').find('input:checked').serializeArray(),
				value 			= this_input.val(),
				is_checked 		= $(this).prop('checked'),
				e_switch 		= e_tab_filter.find('.switch'),
				is_switch_active= e_switch.find('input[type="checkbox"]').prop('checked')


				if(value == 'null' || !value)
				{
					me.siblings().find('input').prop('checked', false)
					me.siblings().removeClass('active')
				}else
				{
					me.siblings().find('input[type="radio"]').prop('checked', false);
					$(me.siblings()[0]).removeClass('active');
				}

				// if there are group
			var group = me.find('input').attr('group');
			if(group)
			{
				if(value == 'null' || !value)
				{
					$('input[group="'+group+'"]:not([value="null"])').prop('checked', false)
					console.log(group, value )
					me.find('input').prop('checked',true)
				}else
				{
					if(!me.hasClass('parents'))
					{
						$('input[group="'+group+'"][value="null"]').prop('checked', false)
					}else
					{
						this_input.prop('checked', false)
					}
				}
			}

		});


		/*
		|------------------------------------------------------
		| Event Trigger
		|------------------------------------------------------
		| fungsi untuk membaca event dengan nama "Read-data-dropdown" ter-trigger.
		| Fungsi ini digunakan ketika data dropdown pada filter-advance berubah. 
		| Fungsi ini akan otomatis membaca perubahan dan menuliskannya pada element button.dropdown-advance-filter
		|------------------------------------------------------
		*/
		$(document).on('read-data-dropdown', function(){
			
			$('.dropdown-advance-filter').each(function(element){
				var data_for 	= $(this).attr('for'),
					val 		= $('[name="'+data_for+'"]:checked'),
					value_preview = [],
					value_preview_text = ''

				$('[name="'+data_for+'"]:checked').each(function(){
					value_preview.push($(this).siblings('.option-item--value').text())
				})

				if(value_preview.length < 1)
				{
					$('[name="'+data_for+'"]:first-child').prop('checked',true)
				}
				switch(value_preview.length)
				{
					case 1:
					case 2:
						value_preview_text = value_preview.join(',')
						break;
					default:
						var clone_value_preview = value_preview.slice(0);
						value_preview_text = clone_value_preview[0];
						clone_value_preview.shift();
						value_preview_text += ' dan '+clone_value_preview.length+' lainnya...';
						break;
				}

				$(this).attr('data-original-title', value_preview.join(', ')).tooltip({placement:'left'})
				$(this).find('span[for="'+data_for+'"]').text(value_preview_text)
			})
		});
		// trigger event "read-data-dropdown" immedietly after initialized
		$(document).trigger('read-data-dropdown');


		/*
		|------------------------------------------------------
		| Event Reader
		|------------------------------------------------------
		| fungsi untuk membaca event ketika list pilihan pada dropdown advance filter di klik
		|------------------------------------------------------
		*/
		/*$('.filter-advance-options .dropdown-menu .option-item--parent').on('click', function(event){
			// set all child that not this parent to value off
			if(!me.hasClass('option-item--child-of') )
			{
				$(this).siblings('.option-item--child-of').find('label:not([parent="'+labelFor+'"]) input:checked').closest('label').trigger('click');
				input.prop('checked',true)
			}

			if(dataLabel.is_child==1)
			{
				$(this).closest('.dropdown-menu').find('[for="'+dataLabel.parent+'"] input').prop('checked',true)
			}


			$(document).trigger('read-data-dropdown');
		});
*/
		$('.filter-advance-options .dropdown-menu .option-item--parent').on('click', function(event){
			// check apakah yang diklik adalah null?
				var input 		= $(this).find('input'),
					id 			= input.attr('id'),
					is_checked 	= $(this).find('input').prop('checked'),
					me 			= $(this),
					labelFor 	= me.find('label').attr('for'),
					dataLabel 	= me.find('label').data()
				

				is_checked = input.prop('checked')	

				
				if(input.val() == 'null')
				{
					input.prop('checked',true)
					$(this).siblings().find('input').prop('checked',false);
				}else
				{
					
					if(is_checked)
					{
						// $(this).closest('.option-item--parent').removeClass('active');
						$('#'+id).prop('checked',false)
					}else
					{
						// $(this).closest('.option-item--parent').addClass('active');
						$('#'+id).prop('checked',true)
					}

					var lenChecked = me.closest('.dropdown-menu').find('input').serializeArray();

					if(lenChecked.length < 1)
					{
						$(this).closest('.dropdown-menu').find('.option-item--parent:first-child input').prop('checked',true)
					}else
					{
						$(this).closest('.dropdown-menu').find('.option-item--parent:first-child input').prop('checked',false)
					}

					if(!me.hasClass('option-item--child-of') )
					{
						$(this).siblings('.option-item--child-of').find('label:not([parent="'+labelFor+'"]) input:checked').closest('label').trigger('click');
						// input.prop('checked',true)
					}
					
				}


				if(dataLabel.is_child==1)
				{
					$(this).closest('.dropdown-menu').find('[for="'+dataLabel.parent+'"] input').prop('checked',true)
				}

				$(document).trigger('read-data-dropdown');

			return false;
		});
		/*
		|------------------------------------------------------
		| Event Reader
		|------------------------------------------------------
		| fungsi untuk membaca event ketika input[checkbox|radio] yang ada didalam list pilihan pada dropdown advance filter berubah
		|------------------------------------------------------
		*/
		/*$('.filter-advance-options .dropdown-menu').delegate('input','change', function(event){
			// check apakah yang diklik adalah null?
				var input = $(this);
				var is_checked = input.prop('checked')
				if(is_checked)
				{
					$(this).closest('.option-item--parent').addClass('active');
				}else
				{
					$(this).closest('.option-item--parent').removeClass('active');
				}

				$(this).closest('.dropdown-menu').find('.option-item').removeAttr('checked');
				$(this).attr('checked',true)

			return false;
		});*/
		

	});

