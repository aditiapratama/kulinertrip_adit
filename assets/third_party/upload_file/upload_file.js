'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/*
------------------------------------------
upload_file.js
------------------------------------------
Plugin javascript ini digunakan untuk kepentingan upload file.
pengguna tidak perlu membuat controller untuk upload / addEventListener untuk onchange file. semua sudah diampu oleh upload file.
-------------------------------------------
#HOW TO USE

- initialize
let file = new upload_file('.element', {options})

- trigger browser file
file.browse();

- watching input file changed
file.watch_change(function)

- get uploaded file
file.get_data()

- get file_reader
file.reader()
.done(function)
.fail(function)

- return file as Formdata // in case you want to send using other ajax
file.get_file_formdata()
also you can pass other object to add to formdata
file.get_file_formdata(object)

- send file 
file.send(ajaxoptions)
.done(function)
.fail(function)

- get error
file.get_error();
---------------------------------------

Dependency : 
	- Jquery


*/
var Upload_file = function () {
	function Upload_file(element, options) {
		var _this = this;

		_classCallCheck(this, Upload_file);

		var def_opts = {
			minimum_size: 100000000,
			allowed_files: [], // typea, typeb, typec, | ['typea', 'typeb', ... ] / ['image', ]
			not_allowed_files: null,
			name: 'file',
			url: undefined,
			resetOnChange: true
		};
		this.element = element || '<input type="file" name="file" class="sr-only">';
		this.e = $(element);
		this.options = $.extend(def_opts, options);
		this.files = [];
		this.errors = [];

		$(document).delegate(this.element, 'change', function (e) {
			_this.reset_error();
			var files = _this.e[0].files;
			$.each(files, function (i, v) {
				var ext = _this._get_extention(v.name);
				var is_allow = _this._is_allowed_extention(ext);
				var is_exceeds = _this._is_minimum_size(v.size);
				if (is_allow && !is_exceeds) {
					_this.set_data(v);
				} else {
					if (is_exceeds) {
						_this.set_error('500.2');
					}
					if (!is_allow) {
						_this.set_error('500.1');
					}
				}
			});
		});
	}

	_createClass(Upload_file, [{
		key: '_is_minimum_size',
		value: function _is_minimum_size(file) {
			return file.size > this.options.minimum_size;
		}
	}, {
		key: '_is_allowed_extention',
		value: function _is_allowed_extention(ext) {
			var type = this.options.allowed_files;
			if (typeof this.options.allowed_files == 'string') {
				type = this.options.allowed_files.replace(/\s/g, '').split(',');
			}

			if (type.length < 1) {
				return true;
			}

			return type.indexOf(ext) > -1 ? true : false;
		}
	}, {
		key: '_get_extention',
		value: function _get_extention(filename) {
			return filename.split('.').pop();
		}
	}, {
		key: 'browse',
		value: function browse() {
			$(this.e).trigger('click');
			if (this.options.onTrigger) {
				if (typeof this.options.onTrigger == 'function') {
					this.options.onTrigger.call(this.e);
				}
			}
			$(document).trigger('browse', [event, this.e]);
		}
	}, {
		key: 'set_data',
		value: function set_data(data) {
			this.files.push(data);
		}
	}, {
		key: 'get_data',
		value: function get_data() {
			return this.files;
		}
	}, {
		key: 'reset_data',
		value: function reset_data() {
			this.files = [];
		}
	}, {
		key: 'set_error',
		value: function set_error(code, text) {
			var error = this._error_list(code);
			error.text = text || error.text;
			this.errors.push(error);
		}
	}, {
		key: 'get_error',
		value: function get_error() {
			return this.errors;
		}
	}, {
		key: 'reset_error',
		value: function reset_error() {
			this.errors = [];
		}
	}, {
		key: 'reset_input',
		value: function reset_input() {
			this.e.val('');
		}
	}, {
		key: 'watch_change',
		value: function watch_change(onchange) {
			var self = this;
			$(document).delegate(this.element, 'change', function (e) {

				e.errors = self.get_error();
				e.files = self.get_data();
				e.latest_file = e.target.files;

				onchange(e, self.options);
			});
		}
	}, {
		key: 'reset',
		value: function reset() {
			this.errors = [];
		}
	}, {
		key: 'reader',
		value: function reader(data, timeout) {
			timeout = timeout ? timeout : 1000;
			var def = $.Deferred();
			if (data) {
				var reader = new FileReader();

				reader.onload = function (e) {
					if (typeof success == 'function') {
						success(e);
					}
					setTimeout(function () {
						def.resolve(e);
					}, timeout);
				};

				def.pipe(function (res) {
					return res.reader = reader;
				});
				reader.readAsDataURL(data);
			}
			return $.when(def.promise());
		}
	}, {
		key: 'get_file_formdata',
		value: function get_file_formdata(other_data) {
			var fData = new FormData(),
			    data = this.get_data();

			$.each(data, function (a, b) {
				fData.append(a, b);
			});

			if (other_data) {
				$.each(other_data, function (a, b) {
					b = (typeof b === 'undefined' ? 'undefined' : _typeof(b)) == 'object' ? JSON.stringify(b) : b;
					fData.append(a, b);
				});
			}
			return fData;
		}
	}, {
		key: 'send',
		value: function send(options) {
			var self = this;
			options = $.extend({
				url: null,
				upload_progress: function upload_progress() {},
				download_progress: function download_progress() {}
			}, options);

			if (!options.url) return;

			var deff = $.Deferred(),
			    fData = new FormData();
			var data = self.get_data();
			if (options.url && Object.keys(data).length > 0) {
				$.each(data, function (a, b) {

					fData.append(a, b);
					$.each(b, function (c, d) {});
				});
			} else {
				console.error('no url defined! or no file uploaded. sent data without file!');
			}

			if (options.data) {
				$.each(options.data, function (a, b) {
					b = (typeof b === 'undefined' ? 'undefined' : _typeof(b)) == 'object' ? JSON.stringify(b) : b;
					fData.append(a, b);
				});
			}

			$.ajax({
				url: options.url,
				data: fData,
				cache: false,
				contentType: false,
				processData: false,
				type: 'POST',
				success: function success(data) {
					deff.resolve(data);
					self.reset();
				},
				error: function error(data) {
					deff.reject(data);
				},
				xhr: function xhr() {
					var xhr = new window.XMLHttpRequest();
					xhr.upload.addEventListener("progress", function (evt) {
						if (evt.lengthComputable) {
							var percentComplete = evt.loaded / evt.total * 100;
							options.upload_progress(event, percentComplete);
							//Do something with upload progress here
						}
					}, false);

					xhr.addEventListener("progress", function (evt) {
						if (evt.lengthComputable) {
							var percentComplete = evt.loaded / evt.total * 100;
							options.download_progress(event, percentComplete);
							//Do something with download progress
						}
					}, false);

					return xhr;
				}

			});

			return deff.promise();
		}
	}, {
		key: '_error_list',
		value: function _error_list(code) {
			var error = [{
				code: '500.1',
				text: 'Maaf file tidak dapat diproses karena termasuk dalam file yang tidak diperbolehkan. silahkan pilih file yang lain!'
			}, {
				code: '500.2',
				text: 'Maaf ukuran file yang anda pilih melebihi batas minimum. silahkan pilih file yang lain!'
			}];

			if (code) {
				var index = error.map(function (res) {
					return res.code;
				}).indexOf(code);
				return error[index];
			}
			return error;
		}
	}]);

	return Upload_file;
}();